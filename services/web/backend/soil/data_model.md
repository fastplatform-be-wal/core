```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "soil.SoilDerivedObject <Soil>" as soil.SoilDerivedObject #f4d6dc {
    estimated soil properties
    ..
    A synthetic collection of soil observations computed from other soil sites
    and attached to a specific plot
    --
    - id (AutoField) - 
    + is_derived_from (JSONField) - 
    + soil_sample_origin (CharField) - 
    + soil_sites_count (IntegerField) - Number of soil samples used to estimate the
soil properties
    ~ plot (ForeignKey) - 
    --
}


class "soil.DerivedObservation <Soil>" as soil.DerivedObservation #f4d6dc {
    estimated soil properties observation
    ..
    DerivedObservation(id, soil_derived_object, observed_property, result)
    --
    - id (AutoField) - 
    ~ soil_derived_object (ForeignKey) - 
    ~ observed_property (ForeignKey) - 
    + result (JSONField) - 
    --
}
soil.DerivedObservation *-- soil.SoilDerivedObject
soil.DerivedObservation *-- soil.ObservableProperty


class "soil.SoilSite <Soil>" as soil.SoilSite #f4d6dc {
    soil sample
    ..
    A private soil site of the holding
    (public soil sites are stored in the external app/database)
    Origin: INSPIRE Annex III - Soil
    --
    - id (AutoField) - 
    + authority_id (CharField) - 
    ~ holding (ForeignKey) - 
    + geometry (GeometryField) - 
    ~ soil_investigation_purpose (ForeignKey) - 
    + valid_from (DateTimeField) - 
    + valid_to (DateTimeField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + source (JSONField) - 
    + label (CharField) - 
    --
}
soil.SoilSite *-- soil.SoilInvestigationPurpose


class "soil.Observation <Soil>" as soil.Observation #f4d6dc {
    soil sample observation
    ..
    A soil observation of a private soil site
    Origin: INSPIRE Annex III - Soil
    --
    - id (AutoField) - 
    ~ soil_site (ForeignKey) - 
    ~ observed_property (ForeignKey) - 
    + result (JSONField) - 
    + phenomenon_time (DateTimeField) - 
    + result_time (DateTimeField) - 
    + valid_time (DateTimeRangeField) - 
    + source (JSONField) - 
    --
}
soil.Observation *-- soil.SoilSite
soil.Observation *-- soil.ObservableProperty


class "soil.ObservableProperty <Soil>" as soil.ObservableProperty #f4d6dc {
    observable property
    ..
    A property that can be observed in private soil sites and in derived soil
objects
    Origin: INSPIRE Annex III - Soil
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    ~ base_phenomenon (ForeignKey) - 
    ~ uom (ForeignKey) - 
    + validation_rules (JSONField) - 
    --
}
soil.ObservableProperty *-- soil.PhenomenonType


class "soil.PhenomenonType <Soil>" as soil.PhenomenonType #f4d6dc {
    soil phenomenon type
    ..
    http://inspire.ec.europa.eu/codelist/SoilSiteParameterNameValue
    Origin: INSPIRE Annex III - Soil
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
soil.PhenomenonType *-- soil.PhenomenonType


class "soil.SoilInvestigationPurpose <Soil>" as soil.SoilInvestigationPurpose #f4d6dc {
    soil investigation purpose
    ..
    https://inspire.ec.europa.eu/codelist/SoilInvestigationPurposeValue
    Origin: INSPIRE Annex III - Soil
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
soil.SoilInvestigationPurpose *-- soil.SoilInvestigationPurpose


@enduml
```