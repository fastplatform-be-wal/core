import os
from datetime import timedelta

# As per https://django-axes.readthedocs.io/en/latest/4_configuration.html

# The integer number of login attempts allowed before a record is created for the failed logins
AXES_FAILURE_LIMIT = int(os.environ.get("AXES_FAILURE_LIMIT", 3))

# After the number of allowed login attempts are exceeded, should we lock out this IP (and optional user agent)?
AXES_LOCK_OUT_AT_FAILURE = (
    os.environ.get("AXES_LOCK_OUT_AT_FAILURE", "TRUE").upper() == "TRUE"
)

# If set, defines a period of inactivity after which old failed login attempts will be cleared.
# Can be set to a Python timedelta object, an integer, a callable, or a string path to a callable which takes no arguments.
# If an integer, will be interpreted as a number of hours.
AXES_COOLOFF_TIME = timedelta(minutes=int(os.environ.get("AXES_COOLOFF_TIME", 30)))

# If True, a successful login will reset the number of failed logins.
AXES_RESET_ON_SUCCESS = (
    os.environ.get("AXES_RESET_ON_SUCCESS", "TRUE").upper() == "TRUE"
)

# If True, lock is only enabled for admin site. Admin site is determined by checking request path against
# the path of "admin:index" view. If admin urls are not registered in current urlconf, all requests will
# not be locked.
AXES_ONLY_ADMIN_SITE = os.environ.get("AXES_ONLY_ADMIN_SITE", "FALSE").upper() == "TRUE"

# If True, only lock based on username, and never lock based on IP if attempts exceed the limit.
# Otherwise utilize the existing IP and user locking logic.
AXES_ONLY_USER_FAILURES = (
    os.environ.get("AXES_ONLY_USER_FAILURES", "FALSE").upper() == "TRUE"
)

# If True, admin views for access attempts and logins are shown in Django admin interface.
AXES_ENABLE_ADMIN = os.environ.get("AXES_ENABLE_ADMIN", "TRUE").upper() == "TRUE"

# If True, prevent login from IP under a particular username if the attempt limit has been exceeded
# otherwise lock out based on IP.
AXES_LOCK_OUT_BY_COMBINATION_USER_AND_IP = (
    os.environ.get("AXES_LOCK_OUT_BY_COMBINATION_USER_AND_IP", "TRUE").upper() == "TRUE"
)

# If True, prevent login from if the attempt limit has been exceeded for IP or username.
AXES_LOCK_OUT_BY_USER_OR_IP = (
    os.environ.get("AXES_LOCK_OUT_BY_USER_OR_IP", "FALSE").upper() == "TRUE"
)

# If True, lock out and log based on the IP address and the user agent.
# This means requests from different user agents but from the same IP are treated differently.
# This settings has no effect if the AXES_ONLY_USER_FAILURES setting is active.
AXES_USE_USER_AGENT = os.environ.get("AXES_USE_USER_AGENT", "FALSE").upper() == "TRUE"

# If True, you’ll see slightly more logging for Axes
AXES_VERBOSE = os.environ.get("AXES_VERBOSE", "TRUE").upper() == "TRUE"

# If set, specifies a template to render when a user is locked out.
# Template receives cooloff_timedelta, cooloff_time, username and failure_limit as context variables.
AXES_LOCKOUT_TEMPLATE = os.environ.get(
    "AXES_LOCKOUT_TEMPLATE", "authentication/lockout.html"
)

# If set, specifies a URL to redirect to on lockout.
AXES_LOCKOUT_URL = os.environ.get("AXES_LOCKOUT_URL", "/lockout/")

# The name of the form field that contains your users usernames.
AXES_USERNAME_FORM_FIELD = os.environ.get("AXES_USERNAME_FORM_FIELD", "username")
