from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class MessagingConfig(AppConfig):
    name = 'messaging'
    verbose_name = _('Messaging')

    help_text = _('Two-way messaging capabilities between farmers and the Paying Agency')
