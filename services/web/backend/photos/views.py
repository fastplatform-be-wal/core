import json
import base64

from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.core.files.base import ContentFile
from django.http import JsonResponse

from django.utils import timezone

from farm.models import HoldingCampaign, Plot
from photos.models import GeoTaggedPhoto
from authentication.models import User


class UploadGeoTaggedPhotoView(View):

    http_method_names = ["post"]

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UploadGeoTaggedPhotoView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        body = json.loads(request.body)
        inputs = body["input"]
        session_variables = body["session_variables"]

        user_id = session_variables.get("x-hasura-user-id", None)

        if not user_id:
            return JsonResponse({"success": False, "error": "UNAUTHORIZED"}, status=200)

        user = User.objects.filter(id=user_id).first()
        if user is None:
            return JsonResponse({"success": False, "error": "UNAUTHORIZED"}, status=200)

        holding_campaign_id = inputs["holding_campaign_id"]
        holding_campaign = HoldingCampaign.objects.filter(
            pk=holding_campaign_id, holding__user_related_parties__user=user
        ).first()
        if holding_campaign is None:
            return JsonResponse(
                {
                    "success": False,
                    "error": "HOLDING_CAMPAIGN_DOES_NOT_EXIST",
                },
                status=200,
            )

        # Get the plot if specified
        plot_id = inputs.get("plot_id", None)
        if plot_id:
            plot = Plot.objects.filter(
                pk=plot_id, site__holding_campaign=holding_campaign
            ).first()
            if plot is None:
                return JsonResponse(
                    {
                        "success": False,
                        "error": "PLOT_DOES_NOT_EXIST_IN_HOLDING_CAMPAIGN",
                    },
                    status=200,
                )
        else:
            plot = None

        # Create a file from the photo data base64 encoded
        photo_data = inputs["photo_data_base64"]
        photo_name = inputs["photo_name"]
        # check the format of the photo data
        if photo_data.startswith("data:image/png;base64,"):
            photo_format, photo_raw_data = photo_data.split(";base64,")
            photo_ext = photo_format.split("/")[-1]
        else:
            photo_raw_data = photo_data
            photo_ext = None

        # Check if the photo name has a supported extension
        def file_name_has_supported_extension(file_name: str):
            for ext in settings.PHOTO_ALLOWED_EXTENSIONS:
                if file_name.endswith(ext):
                    return True
            return False

        if not file_name_has_supported_extension(photo_name):
            # check if the extension could be deduced from the photo data
            if photo_ext and photo_ext in settings.PHOTO_ALLOWED_EXTENSIONS:
                photo_name = photo_name + "." + photo_ext
            else:
                # No valid extension for this photo
                return JsonResponse(
                    {"success": False, "error": "INVALID_PHOTO_FORMAT"}, status=200
                )

        photo_file = ContentFile(base64.b64decode(photo_raw_data), name=photo_name)

        # Compute heading modulo 360°
        heading = inputs.get("heading", None)
        if heading is not None:
            heading = heading % 360

        GeoTaggedPhoto.objects.create(
            location=inputs["location"],
            shot_at=inputs["shot_at"],
            uploaded_by=user,
            uploaded_at=timezone.now(),
            holding_campaign=holding_campaign,
            plot=plot,
            heading=heading,
            elevation=inputs.get("elevation", None),
            horizontal_dilution_of_precision=inputs.get(
                "horizontal_dilution_of_precision", None
            ),
            vertical_dilution_of_precision=inputs.get(
                "vertical_dilution_of_precision", None
            ),
            photo=photo_file,
            photo_width=inputs["photo_width"],
            photo_height=inputs["photo_height"],
            description=inputs.get("description", None),
        )

        return JsonResponse({"success": True, "error": None})
