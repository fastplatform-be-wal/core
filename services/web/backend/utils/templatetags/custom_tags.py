from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag
def get_setting(name):
    return getattr(settings, name, "")

@register.filter
def as_absolute_url(path, request):
    return request.build_absolute_uri(path)

@register.simple_tag
def as_i18n(obj, attr, language):
    if language is None:
        language = settings.LANGUAGE_CODE
    if hasattr(obj, "i18n") and obj.i18n is not None and f"{attr}_{language}" in obj.i18n:
        return mark_safe(obj.i18n[f"{attr}_{language}"])
    return mark_safe(getattr(obj, attr))
