# `core/web/media`: Media authentication proxy 

The [core/web/media](./) service is an [NGINX](https://www.nginx.com/) proxy that performs authentication and authorization of users who attempt to access FaST media files stored in private S3 buckets.

## Architecture

The authentication and authorization are performed using the `auth_request` module of NGINX. See [Authentication Based on Subrequest Result](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-subrequest-authentication/) from the NGINX documentation for more details on how this module works.

The initial request from the client is forwarded to an authentication endpoint (served by the [core/web/backend](../backend/)), which determines if the request is to be authorized or not. To make this decision, the authentication endpoint uses the user's token (from the headers) and the requested URL (which determines the object being requested). If the request is authorized, the authentication endpoint returns the signed headers necessary to request the S3 storage. If not, the request is rejected.

```plantuml
actor "User" as user
component "Farmer mobile application" as app
component "Media authentication proxy\n""core/web/media""" as media_proxy << NGINX >>
component "Web backend\n(Administration Portal)\n""core/web/backend""" as portal << Django >>
storage "Media storage" as s3  << S3 >>

user --- app
app --> media_proxy : **[1]** Request media file
media_proxy -> portal : **[2]** Forward request
media_proxy <- portal : **[3]** Forward signed S3 info
media_proxy --> s3: **[4]** Request S3 with\nsigned info
s3 --> media_proxy: **[5]** Return media file
```

For example, the below query requests the thumbnail of photo `ef779179-d399-4a82-86fc-dc1f3ac83fcf.png` which was taken in campaign `2022` of farm `DEMO-admin`. The request is being authenticated with the user's token `ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890` and accepted only if the token is valid and corresponds to a user that is allowed to access media files for the farm `DEMO-admin`. If the user is not authorized or the token is invalid, the request is rejected.

```http
Authorization: Bearer ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890

https://media.beta.be-wal.fastplatform.eu/fastplatform-be-wal-private/media/beta/photos/holding/DEMO-admin/campaign/2022/thumbnail/ef779179-d399-4a82-86fc-dc1f3ac83fcf.png
```

This request is forwarded by the NGINX proxy to the [core/web/backend](../../web/backend/) media authentication endpoint, which returns the signed headers to access the object on the S3 storage:

```http
HTTP 200 OK

Authorization: AWS4-HMAC-SHA256 Credential=XXXXXXX, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=ZZZZZZZZ
Host: https://oss.eu-west-0.prod-cloud-ocb.orange-business.com
x-amz-content-sha256: <hash of "" for GET requests>
x-amz-date: 20220201T081359Z  # Example
```

The initial request is then proxied to the S3 storage, as the proxy configuration:

```nginx
location / {

    proxy_http_version      1.1;

    proxy_pass              $(fastplatform.core.services.web.media.s3-url);
    proxy_read_timeout      90;

    auth_request            /auth;
    auth_request_set        $s3host $upstream_http_host;
    auth_request_set        $authorization $upstream_http_authorization;
    auth_request_set        $xamzdate $upstream_http_x_amz_date;
    auth_request_set        $xamzcontentsha256 $upstream_http_x_amz_content_sha256;

    proxy_set_header        Authorization $authorization;
    proxy_set_header        Host $s3host;
    proxy_set_header        x-amz-content-sha256 $xamzcontentsha256;
    proxy_set_header        x-amz-date $xamzdate;

    if ($request_method = 'OPTIONS') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Max-Age' 1728000;
        add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
        add_header 'Content-Type' 'text/plain; charset=utf-8';
        add_header 'Content-Length' 0;
        return 204;
    }
}
```



See the [NGINX configuration](./nginx.conf) and the [core/web/backend media authentication implementation](../../web/backend/authentication/views/media.py) for more details.

## Development

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- The `make` utility

You can mimic a distant S3 server by running a local [MinIO](https://min.io/) server. If you want to do so, you first need to initialize the MinIO server:
```bash
# Initialize the certificates
make init-minio-config
# If needed add existing media files to the MinIO server
make init-region-media REGION=<region>
```

Start the service and the MinIO server at once:
```bash
make start
```
