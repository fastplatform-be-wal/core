# Authentication

The process of _authenticating_ users is, very basically, the task of answering the following question: 
> _"Who is the human being currently looking at the screen?"_.

There are several ways to perform this task. The most common ones are:

- through a combination of a `username and a password`
- by `delegating` this task to another system

FaST provides:

- an internal identity provider, based on a username / password combination
- a way to delegate user authentication to external identity providers

FaST relies on the fact that, whatever provider is used to authenticate the user, **the username returned by this provider will be the username in FaST**. Hence, a user can log in through different Identity Providers, but will always get the same account if the username returned by the Identity Provider is the same. 

> The fact that the same user can login using either the internal or external Identity Provider is not always desirable. This situation can be remedied by removing the access through the internal Identity Provider, and only keeping the external one. This can be achieve by selecting the user in the [list](/admin/authentication/user/), and clicking on the `Remove this user's password` button at the top right of the screen. Now the user can only login using the external Identity Provider.

Note that each Identity Provider does not necessarily cover the full spectrum of FaST users for a given region. For example, some Identity Providers provide only authentication for farmers (like the providers from Paying Agencies, usually), while some other providers cover all the individuals within a country (like national systems such as [SPID](https://www.spid.gov.it/) or [TARA](https://www.ria.ee/en/state-information-system/eid/partners.html#tara)).

```plantuml
skinparam defaultFontSize 12

actor "User" as user

rectangle idp_select as "
FaST IdP selection
--
- FaST Internal Identity Provider
- Federated Identity Provider #1 (e.g. CSAM)
- Federated Identity Provider #2 (e.g. APIA)
..."

rectangle fast_idp as "
FaST login screen
--
Username: ______________
Password: ______________
"

cloud fed_idp_1 #cyan as "
IdP #1 login screen
--[outside FaST]--
Custom mechanism that
depends on the IdP
"

cloud fed_idp_2 #cyan as "
IdP #2 login screen
--[outside FaST]--
Custom mechanism that
depends on the IdP
"

usecase return as "
IdPs return the
**username** to FaST
upon successful
authentication"

usecase logged_in as "User is logged in to FaST"

user --> idp_select
idp_select --> fast_idp : Selects\nFaST IdP
idp_select --> fed_idp_1 : Selects\nFederated IdP #1
idp_select --> fed_idp_2 : Selects\nFederated IdP #2

fast_idp --> return
fed_idp_1 --> return
fed_idp_2 --> return

return --> logged_in
```

[](#fast-internal-identity-provider)
### The FaST Internal Identity Provider

The FaST identity provider should be used only for admin users, test users, or when no other identity provider is available for the region. This provider is based on a username + password combination. The accounts and the corresponding passwords have to be manually created in the **Administration Portal**.

> The credentials also have to be _manually_ provided to each user by the Paying Agency. FaST does not provide an automated way to communicate the password, it has to be sent by other means such as email.

The passwords are not stored in clear text in the FaST database, and hence cannot be retrieved once created. However, FaST provides users a workflow to reset their FaST password.

[](#federated-identity-providers)
### The Federated Identity Providers

When a user chooses to login using a federated identity provider, the whole authentication procedures happens _outside of FaST_, and FaST eventually receives the username of the user _only once the whole authentication process is complete and successful_.

The following identity providers have already been connected to FaST:

| Provider name                                                                                                                                                                                       | Country/Region  | Username                                |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------- | --------------------------------------- |
| [Organismo Pagador de Castilla y Leon](https://agriculturaganaderia.jcyl.es/web/es/organismo-pagador-castilla-leon.html)                                                                            | Castilla y Leon | Documento Nacional de Identidad (_DNI_) |
| [Documento de Acompañamiento al Transporte (DAT)](https://www.juntadeandalucia.es/organismos/agriculturaganaderiapescaydesarrollosostenible/areas/agricultura/produccion-agricola/paginas/dat.html) | Andalucia       | Documento Nacional de Identidad (_DNI_) |
| [TARA](https://www.ria.ee/en/state-information-system/eid/partners.html#tara)                                                                                                                       | Estonia         | Personal Code (_isikukood_)             |
| [SPID](https://www.spid.gov.it/)                                                                                                                                                                    | Italy           | Fiscal Code (_codice fiscale_)          |
| [CSAM](https://csam.be/fr/index.html)                                                                                                                                                               | Belgium         | Numéro de Registre National (_NRN_)     |
| [ДФЗ](https://www.dfz.bg/)                                                                                                                                                                          | Bulgaria        | User ID in the DFZ system
| [Ο.Π.Ε.Κ.Ε.Π.Ε](https://www.opekepe.gr/)                                                                                                                                                            | Greece          | TIN (Tax Identification Numbers)        |
| [APIA](http://www.apia.org.ro/)                                                                                                                                                                     | Romania         | User ID in the APIA system                                       | 
| [APA](https://www.apa.sk)                                                                                                                                                                           | Slovakia        | User ID in the APA system
