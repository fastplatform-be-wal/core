from django.test import TestCase, override_settings
from django.conf import settings

from utils.tests.test_admin import AdminTestCaseMixin

from soil.models import (
    SoilSite,
    Observation,
    PhenomenonType,
    SoilInvestigationPurpose,
    ObservableProperty,
)

from configuration.tests.recipes import configuration_recipe

from soil.tests.recipes import (
    soil_site_recipe,
    observation_recipe,
    phenomenon_type_recipe,
    soil_investigation_purpose_recipe,
    observable_property_recipe,
)


@override_settings(AXES_ENABLED=False)
class SoilSiteAdminTestCase(AdminTestCaseMixin, TestCase):
    model = SoilSite
    recipe = soil_site_recipe
    enable = settings.ENABLE_SOIL_SITE

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class ObservationAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Observation
    recipe = observation_recipe
    enable = settings.ENABLE_SOIL_SITE


@override_settings(AXES_ENABLED=False)
class PhenomenonTypeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = PhenomenonType
    recipe = phenomenon_type_recipe
    enable = settings.ENABLE_SOIL_SITE


@override_settings(AXES_ENABLED=False)
class SoilInvestigationPurposeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = SoilInvestigationPurpose
    recipe = soil_investigation_purpose_recipe
    enable = settings.ENABLE_SOIL_SITE


@override_settings(AXES_ENABLED=False)
class ObservablePropertyAdminTestCase(AdminTestCaseMixin, TestCase):
    model = ObservableProperty
    recipe = observable_property_recipe
    enable = settings.ENABLE_SOIL_SITE
