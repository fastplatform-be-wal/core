# Generated by Django 3.0.5 on 2020-06-29 17:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('add_ons', '0002_auto_20200629_1716'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='addonsubscription',
            unique_together=set(),
        ),
        migrations.AddConstraint(
            model_name='addonsubscription',
            constraint=models.UniqueConstraint(fields=('add_on_id', 'user_id'), name='add_on_subscription_add_on_id_user_id_unique'),
        ),
    ]
