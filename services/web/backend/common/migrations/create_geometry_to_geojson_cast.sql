CREATE OR REPLACE FUNCTION geometry_json_cast (geometry) RETURNS json 
AS $$ SELECT ST_AsGeoJSON($1)::json $$ LANGUAGE SQL;

DROP CAST IF EXISTS (geometry AS json);
CREATE CAST (geometry AS json) WITH FUNCTION geometry_json_cast(geometry);