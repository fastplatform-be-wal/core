import logging
from functools import partial

from opentelemetry import trace
from gql import gql
from app.settings import config
from app.db.graphql_clients import fastplatform

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def delete_all_plot_constraints_and_soil_derived_object(
    plot_id: int,
):
    """For a single plot, execute 2 delete operations:
        - delete_constraint for plot
        - delete_soil_derived_object for plot

    Args:
        plot_id (int): the id of the plot whose constraints and soil_derived_object
                        we are deleting

    Returns:
        For both delete operations, return affected_rows
    """
    with tracer().start_as_current_span(
        "delete_all_plot_constraints_and_soil_derived_object"
    ):
        logger.info(
            "Deleting all plot constraints and soil_derived_object for plot_id: %s",
            plot_id,
        )
        mutation = (
            config.API_DIR
            / "controllers"
            / "graphql"
            / "mutation_delete_all_plot_constraints_and_soil_derived_object.graphql"
        ).read_text()
        variables = {"plot_id": plot_id}

        return await fastplatform.execute(gql(mutation), variables)
