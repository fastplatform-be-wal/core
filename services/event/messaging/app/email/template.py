import os

from app.i18n import _, translation, locale
from jinja2 import Environment, PackageLoader, select_autoescape

ADMIN_PORTAL_URL = os.environ.get("ADMIN_PORTAL_URL")
WEB_APP_URL = os.environ.get("WEB_APP_URL")
FAST_SUBJECT_PREFIX = "[FaST]"

ADMIN_PORTAL_ROOT_URL = f"{ADMIN_PORTAL_URL}/{locale}/admin"

jinja_env = Environment(
    loader=PackageLoader("app.email", "templates"),
    autoescape=select_autoescape(["html"]),
    extensions=["jinja2.ext.i18n"],
    trim_blocks=True,
    lstrip_blocks=True,
)

# pylint: disable=no-member
jinja_env.install_gettext_translations(translation, newstyle=True)


def get_subject(id, title, re=False):
    title_trunc = (title[:50] + "...") if len(title) > 50 else title
    subject = f"{FAST_SUBJECT_PREFIX} {title_trunc} (#{id})"
    if re:
        subject = f"Re: {subject}"
    return subject


def get_body_html(template_name, data):
    template = jinja_env.get_template(template_name)
    return template.render(data)


def get_link_html(href, text):
    data = {"href": href, "text": text}
    template = jinja_env.get_template("link.html")
    return template.render(data)


def get_ticket_admin_link_html(ticket_id, ticket_title):
    ticket_href = f"{ADMIN_PORTAL_ROOT_URL}/messaging/ticket/{ticket_id}"
    ticket_text = f"{ticket_title} (#{ticket_id})"
    return get_link_html(ticket_href, ticket_text)


def get_ticket_webapp_link_html(ticket_id, ticket_title):
    ticket_href = f"{WEB_APP_URL}/messaging/ticket/{ticket_id}"
    ticket_text = f"{ticket_title} (#{ticket_id})"
    return get_link_html(ticket_href, ticket_text)


def get_user_admin_link_html(user_id, user_name):
    user_href = f"{ADMIN_PORTAL_ROOT_URL}/authentication/user/{user_id}"
    return get_link_html(user_href, user_name)


def get_holding_admin_link_html(holding_id, holding_name):
    holding_href = f"{ADMIN_PORTAL_ROOT_URL}/farm/holding/{holding_id}"
    return get_link_html(holding_href, holding_name)
