import secrets
import uuid

from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from utils.models import ModelDiffMixin

from utils.models import GetAdminURLMixin, TranslatableModel, validate_latin1


class Provider(GetAdminURLMixin, TranslatableModel, models.Model):
    """A provider of add-ons"""

    id = models.CharField(
        primary_key=True,
        max_length=50,
        default=uuid.uuid4,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    name = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        verbose_name=_("name"),
        help_text=_(
            "The name of the provider, as displayed in the application and in the admin portal"
        ),
    )

    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("description"),
        help_text=_("A description of the provider, that will be displayed in the app"),
    )

    logo = models.ImageField(
        null=True, blank=True, verbose_name=_("logo"), upload_to="provider/logo/"
    )

    website = models.URLField(
        max_length=256,
        verbose_name=_("website"),
        null=True,
        blank=True,
        help_text=_("URL of the marketing/vanity website of the provider"),
    )

    is_active = models.BooleanField(
        default=False,
        null=False,
        blank=False,
        verbose_name=_("is active"),
        help_text=_(
            "Whether this provider and all its add-ons should be active "
            "(visible) in the app. Disabling a provider also disables all "
            "the add-ons of this provider."
        ),
    )

    members = models.ManyToManyField(
        to="authentication.User",
        blank=True,
        through="add_ons.ProviderMember",
        related_name=_("providers"),
        verbose_name=_("members"),
        help_text=_("The users that are allowed to manage this provider"),
    )

    def __str__(self):
        return f"{self.name} ({self.id})"

    class Meta:
        db_table = "provider"
        verbose_name = _("provider")
        verbose_name_plural = _("providers")
        help_text = _(
            "The add-on providers are groups of users responsible over the "
            "implementation and maintenance of one or more add-ons."
        )


class ProviderMember(GetAdminURLMixin, models.Model):
    provider = models.ForeignKey(
        to="add_ons.Provider",
        related_name="provider_members",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("provider"),
    )

    user = models.ForeignKey(
        to="authentication.User",
        related_name="provider_members",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("user"),
    )

    def __str__(self):
        return str(self.user.username)

    class Meta:
        db_table = "provider_member"
        verbose_name = _("provider member")
        verbose_name_plural = _("provider members")


def generate_key():
    return secrets.token_urlsafe(42)


class AddOn(GetAdminURLMixin, ModelDiffMixin, TranslatableModel, models.Model):
    __user_modified_id = None

    id = models.CharField(
        max_length=50,
        default=uuid.uuid4,
        primary_key=True,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    # The `is_api_access` field determines whether this is really an 'add-on' or if it is an APIAccess
    # - is_api_access=False --> Add-on
    # - is_api_access=True --> APIAccess
    # This field does not get exposed in the admin backend.
    is_api_access = models.BooleanField(
        default=False,
        null=False,
        blank=False,
        verbose_name=_("is an APIAccess"),
    )

    provider = models.ForeignKey(
        to="add_ons.Provider",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_("provider"),
        help_text=_("The provider that manages this add-on"),
    )

    name = models.CharField(
        max_length=256,
        null=False,
        verbose_name=_("name"),
        help_text=_(
            "The name of the add-on, as displayed in the app. Try to keep it short."
        ),
    )

    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("description"),
        help_text=_("A description of the add-on, that will be displayed in the app"),
    )

    is_active = models.BooleanField(
        default=False,
        null=False,
        blank=False,
        verbose_name=_("is active"),
        help_text=_(
            "Whether this add-on should be active. Inactive add-ons cannot query the "
            "FaST API and are not visible in the app."
        ),
    )

    groups = models.ManyToManyField(
        to="authentication.Group",
        verbose_name=_("groups"),
        db_table="add_on_group",
        blank=True,
        help_text=_(
            "The groups this add-on belongs to. An add-on will get all permissions "
            "granted to each of their groups."
        ),
        related_name="add_ons",
    )

    permissions = models.ManyToManyField(
        to="auth.Permission",
        db_table="add_on_permission",
        verbose_name=_("permissions"),
        blank=True,
        help_text=_("Specific permissions for this add-on"),
        related_name="add_ons",
    )

    is_visible = models.BooleanField(
        default=False,
        null=False,
        blank=False,
        verbose_name=_("is visible"),
        help_text=_("Whether this add-on should be visible"),
    )

    # Add-on fields that do not apply to pure APIAccess (see below proxy model)

    logo = models.ImageField(
        null=True, blank=True, verbose_name=_("logo"), upload_to=("add_on/logo/")
    )

    website = models.URLField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_("website"),
        help_text=_("URL of the marketing/vanity website of the add-on"),
    )

    callback = models.URLField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_("callback URL"),
        help_text=_(
            "The callback URL that FaST will call using GET when the user accesses the "
            "service from the app."
        ),
    )

    callback_secret_key = models.CharField(
        max_length=64,
        null=True,
        blank=True,
        unique=True,
        validators=[
            RegexValidator(
                r"^[A-Za-z0-9\-\_]{36,}$",
                _(
                    "Key must use only '-', A-Z, a-z and 0-9. "
                    "Key must be at least 36 characters long."
                ),
            )
        ],
        verbose_name=_("callback secret key"),
        help_text=_(
            "The callback secret key is used to create the jwt token. If a callback URL has been set and this field is left empty, a secret key will be randomly generated on save."
        ),
    )

    callback_with_user_id = models.BooleanField(
        verbose_name=_("callback with user ID"),
        default=False,
        null=False,
        blank=False,
        help_text=_(
            "Whether the username should be passed when FaST calls the callback URL"
        ),
    )

    callback_with_holding_id = models.BooleanField(
        verbose_name=_("callback with holding ID"),
        default=False,
        null=False,
        blank=False,
        help_text=_(
            "Whether the holding identifier should be passed when FaST calls the "
            "callback URL"
        ),
    )

    callback_with_campaign_id = models.BooleanField(
        verbose_name=_("callback with campaign ID"),
        default=False,
        null=False,
        blank=False,
        help_text=_(
            "Whether the campaign identifier should be passed when FaST calls the "
            "callback URL"
        ),
    )

    # Subscriptions

    holdings = models.ManyToManyField(
        to="farm.Holding",
        blank=True,
        verbose_name=_("subscribed add-ons"),
        through="add_ons.AddOnSubscription",
        related_name="add_ons",
    )

    auto_subscribe = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name=_("auto subscribe"),
        help_text=_(
            "Whether this add-on can access the data of all holdings (without users "
            "first needing to subscribe)"
        ),
    )

    def clean(self):
        # Prevent ugly crash
        if self.is_visible and not self.provider:
            raise ValidationError(
                _("The provider cannot be empty if 'Is visible' is checked.")
            )

        # Problem:
        #   When an add_on is created,
        #       - if user manually defines a custom id
        #       - and sets an AddOnAPIKey right away
        #   Django BaseInlineFormSet add_fields() replaces the custom id with a new one (default).
        # Solution:
        #   Store manually created id in local field, and restore it on save.

        id_field_diff = self.get_field_diff("id")
        if (
            id_field_diff is not None
            and id_field_diff[1] is not None
            and id_field_diff[0] != id_field_diff[1]
        ):
            # Store user modified id for later use in save() override
            self.__user_modified_id = id_field_diff[1]

        return super().clean()

    def get_admin_url_environment_variable(self):
        return self.id.replace("-", "_").upper() + "_ADMIN_URL"

    def __str__(self):
        return f"{self.name} ({self.id})"

    def save(self, *args, **kwargs):
        # Restore manually created id in case
        # BaseInlineFormSet add_fields() has replaced it (see clean() override).
        if self.__user_modified_id is not None and self.id is None:
            self.id = self.__user_modified_id

        self.__user_modified_id = None

        # see clean() override for friendlier validation message to user
        if self.is_visible and self.provider is None:
            raise ValidationError(_("The provider cannot be empty."))

        if self.callback and not self.callback_secret_key:
            self.callback_secret_key = generate_key()

        return super().save(*args, **kwargs)

    class Meta:
        db_table = "add_on"
        verbose_name = _("add-on")
        verbose_name_plural = _("add-ons")
        help_text = _(
            "The add-ons are additional services that are proposed to the farmer and "
            "appear in the mobile app"
        )


class APIAccess(AddOn):
    def save(self, *args, **kwargs):
        # We first specify the `is_api_access` field for easy filtering in admin
        # This field is not exposed in the backend admin
        self.is_api_access = True

        # Setting `is_visible` to False because APIAccess are never
        # displayed in app to the user
        self.is_visible = False

        # We finally fill in all the fields that are needed only for
        # AddOn but not for APIAccess
        self.logo = None
        self.provider = None
        self.website = None
        self.callback = None
        self.callback_secret_key = None
        self.callback_with_holding_id = False
        self.callback_with_user_id = False
        self.callback_with_campaign_id = False

        # APIAccesses do not appear in the app so farmers cannot "subscribe"
        # hence they are auto_subscribed, which grants access to all the farms
        self.auto_subscribe = True

        return super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.name} ({self.id})"

    class Meta:
        proxy = True
        verbose_name = _("API access")
        verbose_name_plural = _("API accesses")
        help_text = _(
            "Permission-based API accesses to all the holding data in the FaST database"
            " (for example for researchers)."
        )


class AddOnAPIKey(GetAdminURLMixin, models.Model):
    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    add_on = models.ForeignKey(
        to="add_ons.AddOn",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        verbose_name=_("add-on"),
        related_name="api_keys",
        help_text=_("The add-on this key belongs to"),
    )

    key = models.CharField(
        max_length=64,
        null=False,
        blank=False,
        default=generate_key,
        validators=[
            RegexValidator(
                r"^[A-Za-z0-9\-\_]{36,}$",
                _(
                    "Key must use only '-', A-Z, a-z and 0-9. "
                    "Key must be at least 36 characters long."
                ),
            )
        ],
        unique=True,
        verbose_name=_("key"),
        help_text=_("The randomly generated key"),
    )

    generated_by = models.ForeignKey(
        to="authentication.User",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        verbose_name=_("generated by"),
        help_text=_("The user that generated this key"),
    )

    generated_at = models.DateTimeField(
        auto_now_add=True,
        null=False,
        blank=False,
        verbose_name=_("generated at"),
        help_text=_("The date/time this key was generated"),
    )

    def __str__(self):
        return f"Key #{self.id} ({self.add_on.name})"

    class Meta:
        db_table = "add_on_api_key"
        verbose_name = _("add-on API key")
        verbose_name_plural = _("add-on API keys")

        constraints = (
            models.CheckConstraint(
                name="add_on_api_key_key_must_be_letters_digits_36_long",
                check=models.Q(key__regex=r"^[A-Za-z0-9\-\_]{36,}$"),
            ),
        )


class AddOnSubscription(GetAdminURLMixin, models.Model):
    add_on = models.ForeignKey(
        to="add_ons.AddOn",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("add-on"),
        related_name="add_on_subscriptions",
    )

    holding = models.ForeignKey(
        to="farm.Holding",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("farm"),
        related_name="add_on_subscriptions",
    )

    subscribed_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("subscription date"),
    )

    subscribed_by = models.ForeignKey(
        to="authentication.User",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("subscribed by"),
        related_name="add_on_subscriptions",
    )

    def __str__(self):
        return f"{self.holding} subscription to {self.add_on}"

    class Meta:
        db_table = "add_on_subscription"
        verbose_name = _("add-on subscription")
        verbose_name_plural = _("add-on subscriptions")
        constraints = [
            models.UniqueConstraint(
                fields=["add_on_id", "holding_id"],
                name="add_on_subscription_add_on_id_holding_id_unique",
            )
        ]
