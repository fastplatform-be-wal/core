��          �               ,  #   -  I   Q  3   �  @   �  ,     4   =  A   r  5   �  &   �  A     4   S     �  	   �     �     �     �  �  �  &   >  Q   e  2   �  ?   �  -   *  7   X  D   �  ?   �  '     F   =  :   �     �  	   �     �  	   �     �   %(user_display_name)s sent a photo. %(user_link)s created ticket %(ticket_link)s for holding %(holding_link)s %(user_link)s replied to the ticket %(ticket_link)s %(user_link)s replied to the ticket %(ticket_link)s with a photo %(user_name)s created ticket %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Request %(ticket_link)s status changed to %(status)s. The request status has been changed to This email has been generated automatically, please do not reply. Ticket %(ticket_link)s status changed to %(status)s. closed duplicate open reopened resolved Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-25 19:03+0100
PO-Revision-Date: 2020-12-04 14:35+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 %(user_display_name)s envió una foto. %(user_link)s creó el ticket %(ticket_link)s para la propiedad  %(holding_link)s %(user_link)s respondió al ticket %(ticket_link)s %(user_link)s respondió al ticket %(ticket_link)s con una foto %(user_name)s creó el ticket %(ticket_link)s %(user_name)s respondió a la solicitud %(ticket_link)s %(user_name)s respondió a la solicitud %(ticket_link)s con una foto El estado de la solicitud %(ticket_link)s cambió a %(status)s. El estado de solicitud se ha cambiado a Este correo electrónico se ha generado automáticamente. No conteste. El estado del ticket %(ticket_link)s cambió a %(status)s. cerrado duplicado abierto reabierto resuelto 