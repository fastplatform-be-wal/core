from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("farm", "0043_generate_fk_cascade_farm"),
    ]

    operations = [
        migrations.RunSQL(
            sql="UPDATE plot_plant_variety SET percentage = 100.0 WHERE percentage IS NULL;",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE plot_plant_variety ALTER COLUMN percentage SET DEFAULT 100.0;"
        ),
        migrations.RunSQL(
            sql="ALTER TABLE plot_plant_variety ALTER COLUMN percentage SET NOT NULL;"
        ),
    ]
