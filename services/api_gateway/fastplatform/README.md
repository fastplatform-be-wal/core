# `core/api_gateway/fastplatform`: GraphQL API gateway

- [`core/api_gateway/fastplatform`: GraphQL API gateway](#coreapi_gatewayfastplatform-graphql-api-gateway)
  - [Deployment specific for each region](#deployment-specific-for-each-region)
  - [Hasura metadata: a "no-code" approach](#hasura-metadata-a-no-code-approach)
  - [Authentication](#authentication)
  - [Hasura Metadata Management](#hasura-metadata-management)
  - [`fastplatform` and `external` databases](#fastplatform-and-external-databases)
  - [Remote schemas](#remote-schemas)
    - [Development involving interaction between services](#development-involving-interaction-between-services)
    - [Environment variables](#environment-variables)
  - [Development setup](#development-setup)
    - [Prerequisites](#prerequisites)
    - [Start the API Gateway](#start-the-api-gateway)

This service exposes the data and the services of the FaST platform through a unified GraphQL API endpoint provided by a [Hasura](https://hasura.io/) server.

It supports GraphQL 
- [automatic database introspection](https://hasura.io/docs/latest/graphql/core/databases/postgres/schema/using-existing-database/)
- [remote schemas](https://hasura.io/docs/latest/graphql/core/remote-schemas/index/), 
- [database events](https://hasura.io/docs/latest/graphql/core/event-triggers/index/).

`Hasura` provides **Role Base Access Control** (RBAC), allowing authenticated users to access data according to the role assigned to them in the authentication web hook. Each role has a set of permissions defined at table-level, column-level and object-level. More info [here](https://hasura.io/docs/latest/graphql/core/auth/authorization/basics/).

```plantuml
package "GraphQL" as schema {
    component """core/api_gateway/fastplatform""\n\n GraphQL" as api_gateway << hasura >>  #yellow
    component """core/web/backend""\n\n Authentication" as backend << backend >> 

    package "Remote schemas" {
        package "Core" as core {
            [event / farm]
        }
    }

    package "Remote schemas"  {
        package "Custom" as custom {
            [regulatory / nitrate]
        }
    }

    package "Remote schemas"  {
        package "Addon" as addon {
            [weather]
        }
    }

    package "Remote schemas"  {
        package "etc…" as etc {
            […]
        }
    }

    database "Postgres" as fastplatform_db {
      frame "private data" << fastplatform >> {
      }
    }

    database "Postgres" as external_db {
      frame "public GIS data" << external >>{
      }
    }
}

backend <-right--> api_gateway
api_gateway <-down--> core
api_gateway <-down--> custom
api_gateway <-down--> addon
api_gateway <-down--> etc
api_gateway <-up-> fastplatform_db
api_gateway <-up-> external_db
```

## Deployment specific for each region

Many of the files described hereafter are empty in the following repository. It is because their content is in fact **specific for each region**, so we only keep here an empty file as a placeholder. 

Those empty files need to be manually changed whenever you need to launch any of the services locally (see [here](#development-involving-interaction-between-services)). It is recommended not to commit those changes and keep the files empty in the repository.

> The full list of Hasura remote schemas mounted in your production environment can be found here: [REGION]/manifests/resources/modules/core/services/api_gateway/fastplatform/metadata/remote_schemas.yaml.

## Hasura metadata: a "no-code" approach

`core/api_gateway/fastplatform` is built on top of the [Hasura](https://hasura.io/) GraphQL server. It **does not involve any custom code**, only `metadata` files, in the form of `.yaml` files.

For cleaner readability/maintainability of the metadata, the main [database.yaml](metadata/databases/databases.yaml) file is split into many files, one for each table in the 2 databases:
- [fastplatform](metadata/databases/default/tables/)
- [external](metadata/databases/external/tables/)

Any modification to those files need first to be reflected to the main [database.yaml](metadata/databases/databases.yaml) file. This can be accomplished by running the following `Makefile` command:

```bash
make reconcile-hasura-metadata

# Now apply the changes to your local instance of Hasura:
make apply
```

## Authentication

Always keep in mind that any GraphQL query needs first to be authenticated by the backend before it can be run successfully. This in turn implies that an instance of the backend is up and running. 

The delegation of authentication to the backend is configured here in the `Makefile` command, and it is used when launching your local instance of Hasura:

```makefile
starts: 
	docker run --name hasura-fastplatform \
        …
		-e HASURA_GRAPHQL_AUTH_HOOK=https://localhost.fastplatform.eu:48000/authentication/hasura/ \
		-e HASURA_GRAPHQL_AUTH_HOOK_MODE=GET \
```

An instance of the backend needs to run locally, and be accessible at the address `https://localhost.fastplatform.eu:48000/authentication/hasura/` (in this configuration):

```bash
cd …/core/services/web/backend

make starts
```

## Hasura Metadata Management

Any configuration added in the `Hasura admin interface` running locally can be exported to your local `.yaml` files by running the following `Makefile` command:

```bash 
# Export Hasura metadata from a running instance
make export
```

Any manual configuration altered in the `.yaml` files can be reflected by importing the values to the Hasura instance:

```bash
# Attempt to apply the metadata from the ./metadata directory
make apply
```

> Any modification of the environment variables needed to run the service is done by modifying the `metadata.env` file.
To reflect any `envvar` changes on a running instance of hasura:
- modify `metadata.env` file 
- run in bash `make apply`
- restart the Hasura service


## `fastplatform` and `external` databases 

Hasura allows for several PostgreSQL databases to be accessible through the same GraphQL endpoint. We are using this functionality to expose data from both the `fastplatform` and `external` Postgres databases.

This feature, in combination with [remote schemas](#remote-schemas), provides us with the possibility to expose all FaST services and databases from a single GraphQL endpoint, thus providing a unified ontology throughout the platform.

To prevent any naming conflict between the tables in the two databases, we are using the prefix `external__` for the graphql nodes linking the `external` database tables.

Only the tables expressly linked in Hasura metadata are exposed in the GraphQL endpoint.

## Remote schemas

Many services are connected to the main hasura instance using [remote schemas](https://hasura.io/docs/latest/graphql/core/remote-schemas/index/):
- `meteorology/weather`
- `regulatory/nitrate`
- `core/event/farm`
- …

All of those services expose some combination of `queries` and `mutations` accessible from a single GraphQL endpoint for each service.

Those remotes schemas are declared in the [remote_schemas.yaml](metadata/remote_schemas.yaml) file. 

```yaml
# Example for service Wallonia's regulatory / nitrate`:
- name: be-wal-regulatory-nitrate
  definition:
    url_from_env: REMOTE_SCHEMA_URL_BE_WAL_REGULATORY_NITRATE
    timeout_seconds: 60
    forward_client_headers: false
```

Each service exposes a single GraphQL endpoint at a single URL. This URL is assigned for each service in the [metadata.env](metadata/metadata.env) file to an environment variable. Any modification to this file does not only require to apply the changes to the hasura instance, but also to restart the service (see [below](#development-involving-interaction-between-services)).

### Development involving interaction between services

Sometimes while developing a service, you need to have some other service running in the background, and expose its endpoint through the unified GraphQL endpoint of the main Hasura instance.

Example: `regulatory/nitrate` requires an instance of `core/event/farm` to be running and accessible as a remote schema in Hasura.

To do that:
- Launch first `core/event/farm` in the background (in bash, cd to the `services/core/event/farm` directory, and run `make start`)
- Modify your local [remote_schemas.yaml](metadata/remote_schemas.yaml) file to add the remote schema of `core/event/farm`:
    ```yaml
    - name: core-event-farm
    definition:
        url_from_env: REMOTE_SCHEMA_URL_CORE_EVENT_FARM
        timeout_seconds: 60
    ```
- Apply the schema: 
    ```bash
    make apply
    ```
- Reload the metadata from the Hasura server by accessing the "Hasura Metadata Actions" page in the Hasura UI: check "Reload all remote schemas", then click the "Reload" button.

> Finally, keep in mind that you might also need to add some specific `envvar` variables to the `metadata.env`, because environment variables are used to define the URLs of the remote schemas. 

Example: Let's say we are running a local instance of the `regulatory/nitrate` service for region `be-wal` on port **48002**.

The URL endpoint of this service is declared in the envvar: `REMOTE_SCHEMA_URL_BE_WAL_REGULATORY_NITRATE`. When running the service locally, the value of this variable can be set for example to `https://localhost.fastplatform.eu:48002/graphql` (for https).

Add the following line to the `metadata.env` file:

```env
REMOTE_SCHEMA_URL_BE_WAL_REGULATORY_NITRATE=https://localhost.fastplatform.eu:48002/graphql
```

Now:

```bash
# Apply the changes to your local instance of Hasura:
make apply

# Restart the service
make stop
make starts
```

### Environment variables

- `HASURA_ADMIN_SECRET_KEY`:  Hasura admin secret key

## Development setup

### Prerequisites

- [Docker](https://www.docker.com/)
- [localhost](https://gitlab.com/fastplatform/ops/dev-local/-/tree/master/certificates/localhost/localhost.fastplatform.eu) SSL certificates (if HTTPS is required)


### Start the API Gateway

```bash
make start
```

With HTTPS:
```bash
make starts
```