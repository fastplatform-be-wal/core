from django.db import migrations
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from pathlib import Path


class Migration(migrations.Migration):

    dependencies = [
        ("farm", "0052_generate_fk_cascade_farm"),
    ]

    operations = [
        # Remove the regular columns
        migrations.RemoveField("plot", "area"),
        migrations.RemoveField("plot", "perimeter"),
        migrations.RemoveField("plot", "centroid"),
        migrations.RemoveField("plot", "bbox"),
        migrations.RemoveField("plot", "bbox_leaflet"),
        migrations.RemoveField("plot", "thumbnail"),
        # Recreate them using generated columns
        migrations.RunSQL(
            sql=(
                Path(__file__).parent / Path("create_plot_computed_columns.sql")
            ).read_text(),
            state_operations=[
                # Pretend that what we have done in SQL is actually equivalent
                # with regular Django AddField migrations
                migrations.AddField(
                    "plot",
                    "area",
                    models.FloatField(
                        null=True,
                        blank=True,
                        verbose_name=_("area"),
                        editable=False,
                    ),
                ),
                migrations.AddField(
                    "plot",
                    "perimeter",
                    models.FloatField(
                        null=True,
                        blank=True,
                        verbose_name=_("perimeter"),
                        editable=False,
                    ),
                ),
                migrations.AddField(
                    "plot",
                    "centroid",
                    models.PointField(
                        srid=settings.DEFAULT_SRID,
                        null=True,
                        blank=True,
                        verbose_name=_("centroid"),
                        editable=False,
                    ),
                ),
                migrations.AddField(
                    "plot",
                    "bbox",
                    models.PolygonField(
                        srid=settings.DEFAULT_SRID,
                        null=True,
                        blank=True,
                        verbose_name=_("bounding box"),
                        editable=False,
                    ),
                ),
                migrations.AddField(
                    "plot",
                    "bbox_leaflet",
                    models.PolygonField(
                        srid=settings.DEFAULT_LEAFLET_SRID,
                        null=True,
                        blank=True,
                        verbose_name=_("bounding box with Leaflet SRID"),
                        editable=False,
                    ),
                ),
                migrations.AddField(
                    "plot",
                    "thumbnail",
                    models.TextField(
                        null=True,
                        blank=True,
                        verbose_name=_("thumbnail"),
                        editable=False,
                    ),
                ),
            ],
        ),
    ]
