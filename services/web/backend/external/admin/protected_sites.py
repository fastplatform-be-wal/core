from django.utils.translation import gettext_lazy as _
from django.conf import settings

from import_export.admin import ImportExportMixin
from django_object_actions import DjangoObjectActions

from fastplatform.site import admin_site

from external.resources import (
    DesignationSchemeResource,
    DesignationResource,
    DesignationTypeResource,
    ProtectedSiteVersionResource,
    ProtectedSiteResource,
)
from external.models import (
    ProtectedSite,
    ProtectedSiteVersion,
    DesignationType,
    DesignationScheme,
    Designation,
)

from external.admin.version import VersionAdmin, VersionActionsMixin
from utils.admin import CommonModelAdmin


class DesignationTypeAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = DesignationTypeResource
    list_display = (
        "id",
        "designation_scheme",
        "designation",
        "percentage_under_designation",
    )
    list_select_related = ("designation", "designation_scheme")
    search_fields = ("id", "label", "description")


class DesignationAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = DesignationResource
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class DesignationSchemeAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = DesignationSchemeResource
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class ProtectedSiteVersionAdmin(VersionAdmin):
    hide_from_dashboard = True
    resource_class = ProtectedSiteVersionResource


class ProtectedSiteAdmin(VersionActionsMixin, DjangoObjectActions, CommonModelAdmin):
    hide_from_dashboard = False
    version_model = ProtectedSiteVersion
    resource_class = ProtectedSiteResource

    list_display = ("id", "version", "site_name", "site_protection_classification")
    list_display_links = (
        "id",
        "site_name",
    )
    list_select_related = ("version",)
    list_filter = ("version__is_active",)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["version"]
        return []

    def get_queryset(self, request):
        return super().get_queryset(request).select_related("version")

    autocomplete_fields = ("site_designation",)
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "authority_id",
                    "version",
                    "site_name",
                )
            },
        ),
        (
            _("Metadata"),
            {
                "fields": (
                    "site_designation",
                    "site_protection_classification",
                )
            },
        ),
        (
            _("Legal foundation"),
            {
                "fields": (
                    "legal_foundation_date",
                    "legal_foundation_document",
                )
            },
        ),
        (_("Geographic information"), {"fields": ("geometry",)}),
    )

    # Map parameters
    # --------------
    geometry_fields = ["geometry"]


if settings.ENABLE_EXTERNAL_PROTECTED_SITE:
    admin_site.register(DesignationScheme, DesignationSchemeAdmin)
    admin_site.register(Designation, DesignationAdmin)
    admin_site.register(DesignationType, DesignationTypeAdmin)
    admin_site.register(ProtectedSiteVersion, ProtectedSiteVersionAdmin)
    admin_site.register(ProtectedSite, ProtectedSiteAdmin)
