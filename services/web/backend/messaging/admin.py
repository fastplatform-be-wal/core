from datetime import datetime
from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django_object_actions import DjangoObjectActions
from django.db.models import Max, Count, Q
from django.db.models.functions import Coalesce
from django.template.loader import render_to_string
from django.utils.html import mark_safe
from django.template.defaultfilters import truncatechars

from import_export.admin import ExportMixin
from storages.backends.s3boto3 import S3Boto3Storage

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin
from messaging.models import (
    Broadcast,
    Queue,
    Ticket,
    TicketMessage,
)
from messaging.resources import (
    BroadcastResource,
    QueueResource,
    TicketResource,
    TicketMessageResource,
)

s3boto3storage = S3Boto3Storage()


class QueueAdmin(ExportMixin, CommonModelAdmin):
    list_display = ("title", "total_number_of_tickets", "number_of_open_tickets")
    list_display_links = ("title",)
    readonly_fields = ("total_number_of_tickets", "number_of_open_tickets")
    fields = ("title", "groups")
    autocomplete_fields = ("groups",)
    resource_class = QueueResource
    hide_from_dashboard = False

    def get_queryset(self, request):
        qs = super(QueueAdmin, self).get_queryset(request)
        qs = qs.annotate(
            total_number_of_tickets=Count("tickets"),
            number_of_open_tickets=Count(
                "tickets",
                filter=Q(
                    tickets__status__in=[Ticket.Status.OPEN, Ticket.Status.REOPENED]
                ),
            ),
        )
        return qs

    def total_number_of_tickets(self, obj):
        if obj.total_number_of_tickets is not None:
            return obj.total_number_of_tickets
        return 0

    def number_of_open_tickets(self, obj):
        if obj.number_of_open_tickets is not None:
            return obj.number_of_open_tickets
        return 0

    def has_change_permission(self, request, obj=None):
        return super().has_change_permission(request, obj) and (
            obj is None
            or request.user.is_superuser
            # A user can change a queue if he is member of this queue
            or obj.groups.filter(users=request.user).exists()
        )

    def has_delete_permission(self, request, obj=None):
        return (
            super().has_delete_permission(request, obj)
            # Prevent deleting Queue #1 (Default queue)
            and not (obj and obj.id == 1)
        )


admin_site.register(Queue, QueueAdmin)


class TicketMessageInline(admin.TabularInline):
    model = TicketMessage
    extra = 0

    fields = (
        (
            "created_at",
            "created_by",
        ),
        "body",
        "geo_tagged_photo",
        "_thumbnail_for_list",
    )

    readonly_fields = ("created_at", "created_by", "_thumbnail_for_list")
    raw_id_fields = ["geo_tagged_photo"]
    verbose_name = _("message")
    verbose_name_plural = _("messages")

    def _thumbnail_for_list(self, obj):
        size = 100
        if (
            obj
            and obj.geo_tagged_photo
            and obj.geo_tagged_photo.photo
            and obj.geo_tagged_photo.thumbnail
        ):
            return mark_safe(
                f'<a href="{s3boto3storage.url(name=obj.geo_tagged_photo.photo.name)}"><img src="{s3boto3storage.url(name=obj.geo_tagged_photo.thumbnail.name)}" style="max-width: {size}px; max-height: {size}px;"/></a>'
            )
        else:
            return "-"

    _thumbnail_for_list.short_description = _("Photo thumbnail")


class TicketMessageAdmin(ExportMixin, CommonModelAdmin):
    resource_class = TicketMessageResource
    hide_from_dashboard = True
    fields = (
        "ticket",
        "created_by",
        "created_at",
        "body",
        "geo_tagged_photo",
        "_thumbnail_for_detail",
    )
    autocomplete_fields = ("ticket", "created_by")
    readonly_fields = ("created_at", "_thumbnail_for_detail")
    raw_id_fields = ["geo_tagged_photo"]

    def has_add_permission(self, request):
        return super().has_add_permission(request) and (
            request.user.is_superuser
            # A user can add a ticket message only if he/she a member
            # of one queue
            or Queue.objects.filter(groups__users=request.user).exists()
        )

    def has_view_permission(self, request, obj=None):
        return super().has_view_permission(request) and (
            obj is None
            or request.user.is_superuser
            # A user can see a ticket message only if he is member of the queue of the ticket
            or Ticket.objects.filter(
                ticket_messages=obj, queue__groups__users=request.user
            ).exists()
        )

    def has_change_permission(self, request, obj=None):
        return super().has_change_permission(request, obj) and (
            obj is None
            or request.user.is_superuser
            # A user can change a ticket message only if he is member of the queue of the ticket
            or Ticket.objects.filter(
                ticket_messages=obj, queue__groups__users=request.user
            ).exists()
        )

    def has_delete_permission(self, request, obj=None):
        return super().has_delete_permission(request) and (
            obj is None
            or request.user.is_superuser
            # A user can delete a ticket message only if he is member of the queue of the ticket
            or Ticket.objects.filter(
                ticket_messages=obj, queue__groups__users=request.user
            ).exists()
        )

    def _thumbnail_for_detail(self, obj):
        size = 400
        if (
            obj
            and obj.geo_tagged_photo
            and obj.geo_tagged_photo.photo
            and obj.geo_tagged_photo.thumbnail
        ):
            return mark_safe(
                f'<a href="{s3boto3storage.url(name=obj.geo_tagged_photo.photo.name)}"><img src="{s3boto3storage.url(name=obj.geo_tagged_photo.thumbnail.name)}" style="max-width: {size}px; max-height: {size}px;"/></a>'
            )
        else:
            return "-"

    _thumbnail_for_detail.short_description = _("Photo thumbnail")


admin_site.register(TicketMessage, TicketMessageAdmin)


class TicketAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = TicketResource
    list_display = (
        "id",
        "_title",
        "created_at",
        "queue",
        "status",
        "priority",
        "holding",
        "created_by",
        "created_at",
        "last_updated",
    )
    list_display_links = ("_title",)
    fieldsets = [
        [
            None,
            {
                "fields": [
                    "id",
                    "holding",
                    "created_at",
                    "created_by",
                    "status",
                ]
            },
        ],
        [_("Triage"), {"fields": ["priority", "queue", "assigned_to"]}],
        [_("Content"), {"fields": ["title", "body", "resolution"]}],
    ]

    readonly_fields = ("id", "created_at", "created_by", "last_updated")
    inlines = [TicketMessageInline]
    date_hierarchy = "created_at"
    list_filter = (
        ("queue", admin.RelatedOnlyFieldListFilter),
        "status",
        "priority",
    )
    search_fields = ("title", "body", "created_by__username", "created_by__name")
    autocomplete_fields = (
        "holding",
        "assigned_to",
    )

    def has_add_permission(self, request):
        return super().has_add_permission(request) and (
            request.user.is_superuser
            # A user can add a ticket only if he/she is at least member
            # of one queue
            or Queue.objects.filter(groups__users=request.user).exists()
        )

    def has_view_permission(self, request, obj=None):
        return super().has_view_permission(request) and (
            obj is None
            or request.user.is_superuser
            # A user can see a ticket only if he is member of the queue of the ticket
            or Queue.objects.filter(tickets=obj, groups__users=request.user).exists()
        )

    def has_change_permission(self, request, obj=None):
        return super().has_change_permission(request, obj) and (
            obj is None
            or request.user.is_superuser
            # A user can change a ticket only if he is member of the queue of the ticket
            or Queue.objects.filter(tickets=obj, groups__users=request.user).exists()
        )

    def has_delete_permission(self, request, obj=None):
        return super().has_delete_permission(request) and (
            obj is None
            or request.user.is_superuser
            # A user can change a ticket only if he is member of the queue of the ticket
            or Queue.objects.filter(tickets=obj, groups__users=request.user).exists()
        )

    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)

    def render_change_form(self, request, context, *args, **kwargs):
        if not request.user.is_superuser:
            if "queue" in context["adminform"].form.fields:
                context["adminform"].form.fields["queue"].queryset = Queue.objects.filter(
                    groups__users=request.user
                )
        return super().render_change_form(request, context, *args, **kwargs)

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            if not instance.created_by:
                instance.created_by = request.user
                if instance.geo_tagged_photo:
                    instance.body = (
                        f"<<< FaST GeoTaggedPhoto id={instance.geo_tagged_photo.id} >>>"
                    )
                instance.save()
        formset.save_m2m()

    def get_queryset(self, request):
        qs = super(TicketAdmin, self).get_queryset(request)

        # users may see only tickets in queues associated to a group they belong to
        if not request.user.is_superuser:
            qs = qs.filter(queue__groups__users=request.user)

        qs = qs.annotate(
            last_updated=Coalesce(Max("ticket_messages__created_at"), "created_at"),
        ).order_by("-last_updated")
        return qs

    def last_updated(self, obj):
        if obj.last_updated is not None:
            return obj.last_updated
        return obj.created_at

    last_updated.admin_order_field = "last_updated"
    last_updated.short_description = _("last updated")

    def _title(self, obj):
        return truncatechars(obj.title, 35)

    _title.short_description = _("title")


admin_site.register(Ticket, TicketAdmin)


class BroadcastAdmin(ExportMixin, DjangoObjectActions, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = BroadcastResource
    list_display = ("title", "status", "sent_at")
    list_display_links = ("title",)
    date_hierarchy = "sent_at"
    list_filter = ("status",)
    search_fields = ("title", "body")

    fieldsets = (
        (_("Content"), {"fields": ("title", "body")}),
        (
            _("Status"),
            {
                "description": mark_safe(
                    render_to_string("broadcast_status_description.html")
                ),
                "fields": ("status", "sent_at", "sent_by"),
            },
        ),
    )

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super(BroadcastAdmin, self).get_form(request, obj, **kwargs)
        if "status" in form.base_fields:
            status_field = form.base_fields["status"]
            status_field.choices = Broadcast.UserModifiableStatus.choices
        return form

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ["sent_at", "sent_by"]

        # Fields may only be edited before sending the broadcast
        if obj is not None and obj.status in [
            Broadcast.Status.SENDING,
            Broadcast.Status.SENT,
        ]:
            readonly_fields += ["title", "body"]

        # Only the existing broadcasts that are draft or cancelled can have their status changed
        if obj is None or obj.status not in [
            Broadcast.Status.CANCELED,
            Broadcast.Status.DRAFT,
        ]:
            readonly_fields += ["status"]

        return readonly_fields

    # Action to send the broadcast (only available when status is DRAFT)
    def send_broadcast_action(self, request, obj):
        # Re-check permissions
        if (
            request.user.has_perm("messaging.send_broadcast")
            or request.user.is_superuser
        ) and obj.status == Broadcast.Status.DRAFT:
            # Setting the status to 'sending' will trigger the broadcast push
            # event from the event-messaging service
            obj.status = Broadcast.Status.SENDING
            obj.sent_at = datetime.now()
            obj.sent_by = request.user
            obj.is_sending_triggered = True
            obj.save()

    send_broadcast_action.label = _("Send this broadcast")

    # Fix for this: https://github.com/crccheck/django-object-actions/issues/25
    change_actions = ["send_broadcast_action"]

    def get_change_actions(self, request, object_id, form_url):
        # Only show the 'send broadcast' action if the user has the right
        # permission and the broadcast is DRAFT
        obj = Broadcast.objects.get(pk=object_id)
        if (
            request.user.has_perm("messaging.send_broadcast")
            or request.user.is_superuser
        ) and obj.status == Broadcast.Status.DRAFT:
            return ["send_broadcast_action"]
        return []


admin_site.register(Broadcast, BroadcastAdmin)