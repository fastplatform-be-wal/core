import os
import sys

from pathlib import Path, PurePath

from pydantic import BaseSettings

from typing import Dict, Union

class Settings(BaseSettings):

    APP_DIR: Path = PurePath(__file__).parent

    LOG_LEVEL: str = "info"

    MAPPROXY_CONFIGURATION_FILE_PATH: str = "./mapproxy.yaml"

config = Settings()