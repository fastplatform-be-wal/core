# 05-computes_null_constraint_if_not_exists_and_plot_not_intersects
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 protected_site that has **no intersection** with plot
- No precomputed protected_site constraint.

## Computed
An empty constraint, with description = null.

## Returns
Null constraint (`"computed_constraints_protected_site": null`)