# `core/mapproxy/api`: MapProxy dynamic configuration

The [core/mapproxy/api](./) service keeps the configuration of the [core/mapproxy/server](../server) in sync with the desired state declared in the [Administration portal](../../web/backend).

## Architecture

The service is composed of a [FastAPI](https://fastapi.tiangolo.com/)/[Starlette](https://www.starlette.io/) server, behind a [uvicorn](https://www.uvicorn.org/) WSGI server.

It exposes a single endpoint `/conf/update` where it expects to receive a `POST` request from the [core/api_gateway](../../api_gateway/) when any configuration of the layers has been edited. The body of the request is expected to contain a Hasura event object, as per the [Hasura specification](https://hasura.io/docs/latest/graphql/core/event-triggers/payload/).

## Environment variables

API gateway connection:
- `API_GATEWAY_FASTPLATFORM_URL`: URL of the [core/api_gateway](../../api_gateway)
- `API_GATEWAY_SERVICE_KEY`: Secret service key to access the [core/api_gateway](../../api_gateway)

MapProxy:
- `MAPPROXY_STANDARD_PROJ`: Standard target projection. Defaults to `"EPSG:3857"`.
- `MAPPROXY_DEFAULT_PROJ`: Default projection
- `MAPPROXY_DEFAULT_PROJ_BBOX`: Default projection bounding box
- `MAPPROXY_CONF_FILE_PATH`: Path to local configuration file. Defaults to `"../data/dev/mapproxy/config/mapproxy.yaml"` in local development mode. Set it to the correct mount path of the ConfigMap when deployed.
- `MAPPROXY_CONF_FROM_K8S`: Use configuration from a Kubernetes configmap, else use local file
- `MAPPROXY_CONF_ATTR_REGIONAL_GRID_NAME`: Default name of the regional grid. Defaults to `"regional-grid"`.

Caching:
- `MAPPROXY_CONF_CACHE_TYPE`: Type of cache, `"s3"` or `"file"`. Defaults to `"file"`.
- `MAPPROXY_CONF_CACHE_S3_BUCKET_NAME`: S3 bucket of the cache. Only needed if `MAPPROXY_CONF_CACHE_TYPE == "s3"`. No default value.
- `MAPPROXY_CONF_CACHE_S3_LOCATION`: Root directory/key of the cache on the S3 endpoint.  Only needed if `MAPPROXY_CONF_CACHE_TYPE == "s3"`. Defaults to `"cache/mapproxy"`.
- `MAPPROXY_CONF_CACHE_S3_URL`: URL of the S3 server endpoint.  Only needed if `MAPPROXY_CONF_CACHE_TYPE == "s3"`. No default value.
- `MAPPROXY_CONF_CACHE_FILE_DIR`: Directory of the cache. Only needed if `MAPPROXY_CONF_CACHE_TYPE == "file"`. Defaults to `"/tmp/cache/mapproxy"`.

Kubernetes:
- `MAPPROXY_K8S_NAMESPACE`: Kubernetes namespace
- `MAPPROXY_K8S_CONFIGMAP_NAME`: Kubernetes ConfigMap name
- `MAPPROXY_K8S_CONFIGMAP_KEY`: Kubernetes ConfigMap key to store MapProxy configuration
- `MAPPROXY_K8S_USE_INCLUSTER_CONFIG`: Use default injected Kubernetes service account
- `MAPPROXY_SERVER_LABELS_SELECTOR`: Kubernetes selector for MapProxy servers currently running on the cluster. Defaults to `"serving.knative.dev/service=mapproxy-server"`.

Service:
- `INIT_CONFIGMAP_ON_STARTUP`: Whether to initialize the MapProxy configuration (reading it from the API gateway) when the service starts. Defaults to `true`.
- `LOG_LEVEL`: logging level (trace, debug, info, warn, error, fatal)

## Development

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

Create a Python virtualenv and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```bash
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

Start the service:
```bash
make start
```

The API server is now started and available at http://localhost:7010.
