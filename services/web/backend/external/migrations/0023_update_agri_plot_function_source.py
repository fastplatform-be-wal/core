from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [('external', '0022_auto_20210127_1354')]

    operations = [
        migrations.RunSQL((Path(__file__).parent / Path(
            'create_agri_plot_function_source.sql'
        )).read_text())
    ]