```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "photos.GeoTaggedPhoto <Photos>" as photos.GeoTaggedPhoto #eef4d6 {
    geo-tagged photo
    ..
    Photo with geolocation information
    As per 6.2.5 of
    https://publications.jrc.ec.europa.eu/repository/bitstream/JRC108816/cts-201
7pubsy-online.pdf
    Geo-tagged photos
    =================
    Geotagging is the process of adding and embedding geographical information,
    and possibly additional temporal and/or textual information into the
metadata
    file of a photograph. Usually the location and time of acquisition will be
obtained
    through the GNSS antenna of the photo camera or smartphone.
    Member State Administrations should strongly consider the utility of this
lowcost,
    readily available, and easy to use technology. They could integrate, in
their
    IACS, geo-tagged photos provided by farmers as evidence of compliance to
    specific conditions or rules. For instance, a farmer could send a geo-tagged
    photo of his Durum wheat field(s) being part of a Voluntary Couple Support.
    Also, a Geo-tagged photo could be sent of a grassland just mowed to show
    compliance with a mowing date imposed by the legislation. The provision of
such
    geo-tagged information will avoid the field visit that otherwise would be
    necessary.
    The minimum information to be embedded or stored, for each image taken,
    should consist of:
    - Its geographical location (longitude, latitude);
    - Its date and time of shooting;
    - The direction the camera was pointing. This could be obtain, among
    other possibilities, directly from a compass system embedded in the device,
or
    by pointing the camera toward a feature/ a landscape element that could be
    easily depicted afterwards on imagery maps (see example below);
    Example of photo containing elements allowing determining the direction the
    camera was pointing when shooting the photo.
    - It is also recommended to register the elevation and the Dilution of
    precision (DOP). This last parameter gives a qualitative indication on the
    positional precision.
    - At last, a procedure should exist to identify the farmer providing the
    photo (e.g. image uploaded in the IACS after an authentication through
personal
    login and password).
    Administrations should develop a standard operating procedure to ensure the
    information integrity and security of received geo-tagged photos; in other
    words, ensure that the whole information content has, in no manner, been
    falsified.
    Some administration have already developed such dedicated smartphone/tablet
    Applets to be used by farmers (or by inspectors).
    Geo-tagged images should be stored in their original file formats. Critical
    image information may be lost and artefacts introduced as a result of a
lossy
    compression process. Files should also be as ‘read-only’ to avoid
modifications
    or deletions.
    --
    - id (AutoField) - 
    + shot_at (DateTimeField) - 
    + description (TextField) - 
    + uploaded_at (DateTimeField) - 
    ~ uploaded_by (ForeignKey) - 
    ~ holding_campaign (ForeignKey) - 
    ~ plot (ForeignKey) - 
    + location (PointField) - 
    + heading (FloatField) - 
    + elevation (FloatField) - 
    + horizontal_dilution_of_precision (FloatField) - 
    + vertical_dilution_of_precision (FloatField) - 
    + photo (ImageField) - 
    + photo_width (IntegerField) - pixels
    + photo_height (IntegerField) - pixels
    + thumbnail (ImageField) - 
    + thumbnail_width (IntegerField) - 
    + thumbnail_height (IntegerField) - 
    --
}


@enduml
```