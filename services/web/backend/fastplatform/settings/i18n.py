import os

from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ImproperlyConfigured

USE_I18N = True
USE_L10N = True

LOCALE_PATHS = [
    os.path.join(
        os.path.dirname((os.path.dirname(os.path.dirname(os.path.abspath(__file__))))),
        "locale",
    )
]

LANGUAGE_CODE = os.environ.get("LOCALE", "en")

LANGUAGES = (
    ("bg", "Bulgarian / Български"),
    ("de", "German / Deutsch"),
    ("el", "Greek / Ελληνική"),
    ("en", "English"),
    ("es", "Spanish / Castellano"),
    ("et", "Estonian / Eesti"),
    ("fr", "French / Français"),
    ("it", "Italian / Italiano"),
    ("ro", "Romanian / Română"),
    ("sk", "Slovak / Slovenčina"),
)

MODEL_TRANSLATIONS_LANGUAGES = os.environ.get(
    "MODEL_TRANSLATIONS_LANGUAGES", "fr,de"
).split(",")

if any([l for l in MODEL_TRANSLATIONS_LANGUAGES if l not in dict(LANGUAGES)]):
    raise ImproperlyConfigured(
        "MODEL_TRANSLATIONS_LANGUAGES must be a subset of LANGUAGES"
    )
