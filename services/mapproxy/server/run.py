import os

from app.main import app
from gunicorn.app.base import BaseApplication


class GunicornApplicationWrapper(BaseApplication):

    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super().__init__()

    def load_config(self):
        config = {key: value for key, value in self.options.items()
                  if key in self.cfg.settings and value is not None}
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application

GunicornApplicationWrapper(
    app,
    {
        "host": os.environ.get('HOST','127.0.0.1'),
        "port": int(os.environ.get('PORT',8000)),
        'workers': int(os.environ.get('WORKERS',2)),
        'threads': int(os.environ.get('THREADS',4)),
    }
).run()
