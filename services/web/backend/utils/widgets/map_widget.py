import json
from pathlib import Path

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import gettext as _
from django.templatetags.static import static

from leaflet.admin import LeafletWidget

from configuration.models import (
    Configuration,
    MapOverlay,
    MapBaseLayer,
)


class MapWidget(LeafletWidget):
    template_name = "widgets/map_widget/widget.html"
    include_media = True
    srid = None

    protected_site_overlay_default_visible = True
    management_restriction_or_regulation_zone_overlay_default_visible = True
    hydrography_overlay_default_visible = False
    agri_plot_overlay_default_visible = False
    farm_plots_overlay_default_visible = True

    extent = None
    session_key = None
    holding_id = None
    holding_campaign_id = None
    site_id = None
    plot_id = None

    def __init__(
        self,
        geom_type=False,
        modifiable=None,
        static=False,
        extent=None,
        holding_id=None,
        holding_campaign_id=None,
        site_id=None,
        plot_id=None,
        session_key=None,
        *args,
        **kwargs,
    ):
        self.geom_type = geom_type
        self.modifiable = bool(modifiable)
        self.read_only = not modifiable

        self.session_key = session_key
        self.extent = extent
        self.static = static
        self.holding_id = holding_id
        self.holding_campaign_id = holding_campaign_id
        self.site_id = site_id
        self.plot_id = plot_id

        super().__init__(*args, **kwargs)

    def _get_base_layers(self):
        base_layers = []
        for map_base_layer in (
            MapBaseLayer.objects.filter(is_active_in_admin_portal=True)
            .order_by("id")
            .all()
        ):
            layer = map_base_layer.to_json()
            layer["zIndex"] = 0
            base_layers += [layer]
        return base_layers

    def _get_overlays(self):
        configuration = Configuration.load()
        overlays = []

        # User defined overlays go below the FaST overlays
        for map_overlay in (
            MapOverlay.objects.filter(is_active_in_admin_portal=True)
            .order_by("layer_order")
            .all()
        ):
            layer = map_overlay.to_json()
            # if map_overlay.layer_order:
            #     layer["zIndex"] = map_overlay.layer_order
            # else:
            #     layer["zIndex"] = 1
            overlays += [layer]

        # Now the FaST overlays
        if settings.ENABLE_EXTERNAL_PROTECTED_SITE:
            overlays += [
                {
                    "title": _("Protected sites (Natura2000)"),
                    "url": settings.VECTOR_TILES_EXTERNAL_URL
                    + "/public.protected_site_function_source/{z}/{x}/{y}.pbf",
                    "type": "vector_tile",
                    "style": configuration.protected_site_overlay_style,
                    "tile_layer": "protected_site",
                    "display": self.protected_site_overlay_default_visible,
                    "attribution": get_overlay_attribution(
                        configuration.protected_site_overlay_style
                    ),
                }
            ]

        if settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE:
            overlays += [
                {
                    "title": _("Regulation zones (NVZ)"),
                    "url": settings.VECTOR_TILES_EXTERNAL_URL
                    + "/public.management_restriction_or_regulation_zone_function_source/{z}/{x}/{y}.pbf",
                    "type": "vector_tile",
                    "style": configuration.management_restriction_or_regulation_zone_overlay_style,
                    "tile_layer": "management_restriction_or_regulation_zone",
                    "display": self.management_restriction_or_regulation_zone_overlay_default_visible,
                    "attribution": get_overlay_attribution(
                        configuration.management_restriction_or_regulation_zone_overlay_style
                    ),
                }
            ]

        if settings.ENABLE_EXTERNAL_SURFACE_WATER:
            overlays += [
                {
                    "title": _("Hydrography (surface water)"),
                    "url": settings.VECTOR_TILES_EXTERNAL_URL
                    + "/public.surface_water_function_source/{z}/{x}/{y}.pbf",
                    "type": "vector_tile",
                    "style": configuration.hydrography_overlay_style,
                    "tile_layer": "surface_water",
                    "display": self.hydrography_overlay_default_visible,
                    "attribution": get_overlay_attribution(
                        configuration.hydrography_overlay_style
                    ),
                },
            ]

        if settings.ENABLE_EXTERNAL_WATER_COURSE:
            overlays += [
                {
                    "title": _("Hydrography (water courses)"),
                    "url": settings.VECTOR_TILES_EXTERNAL_URL
                    + "/public.water_course_function_source/{z}/{x}/{y}.pbf",
                    "type": "vector_tile",
                    "style": configuration.hydrography_overlay_style,
                    "tile_layer": "water_course",
                    "display": self.hydrography_overlay_default_visible,
                    "attribution": get_overlay_attribution(
                        configuration.hydrography_overlay_style
                    ),
                },
            ]

        if settings.ENABLE_EXTERNAL_AGRI_PLOT:
            overlays += [
                {
                    "title": _("Agricultural parcels (all)"),
                    "url": settings.VECTOR_TILES_EXTERNAL_URL
                    + "/public.agri_plot_function_source/{z}/{x}/{y}.pbf",
                    "type": "vector_tile",
                    "style": configuration.agri_plot_overlay_style,
                    "tile_layer": "agri_plot",
                    "display": self.agri_plot_overlay_default_visible,
                    "attribution": get_overlay_attribution(
                        configuration.agri_plot_overlay_style
                    ),
                },
            ]

        if any(
            [
                self.holding_id,
                self.holding_campaign_id,
                self.site_id,
                self.plot_id,
            ]
        ):
            if not self.session_key:
                raise ImproperlyConfigured("Missing session key for farm map layer")
            url = (
                settings.VECTOR_TILES_FASTPLATFORM_URL
                + f"/public.plot_function_source/{{z}}/{{x}}/{{y}}.pbf"
            )
            if self.holding_id is not None:
                url += f"?p_holding_id={self.holding_id}"
                title = _("Plots (of this farm)")
            elif self.holding_campaign_id is not None:
                url += f"?p_holding_campaign_id={self.holding_campaign_id}"
                title = _("Plots (of this campaign)")
            elif self.site_id is not None:
                url += f"?p_site_id={self.site_id}"
                title = _("Plots (of this site)")
            else:
                url += f"?p_plot_id={self.plot_id}"
                title = _("This plot")

            if self.session_key is not None:
                url += f"&p_session_key={self.session_key}"

            overlays += [
                {
                    "title": title,
                    "url": url,
                    "type": "vector_tile",
                    "style": configuration.plots_overlay_style,
                    "tile_layer": "plot",
                    "display": self.farm_plots_overlay_default_visible,
                    "attribution": get_overlay_attribution(
                        configuration.plots_overlay_style
                    ),
                }
            ]

        return overlays

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        configuration = Configuration.load()

        if configuration.rgb_ndvi_overlay_style is not None:
            rgb_ndvi_attribution = configuration.rgb_ndvi_overlay_style.get(
                        "attribution", ""
                )
        else:
            rgb_ndvi_attribution = None

        context.update(
            {
                "geom_type": self.geom_type,
                "map_width": configuration.backend_leaflet_map_width,
                "map_height": configuration.backend_leaflet_map_height,
                "djoptions": json.dumps(
                    {
                        "extent": self.extent,
                        "static": self.static,
                        "fitextent": True,
                        "center": [
                            configuration.backend_leaflet_default_center_latitude,
                            configuration.backend_leaflet_default_center_longitude,
                        ],
                        "zoom": configuration.backend_leaflet_default_zoom,
                        "precision": 10,
                        "minzoom": configuration.backend_leaflet_min_zoom,
                        "maxzoom": configuration.backend_leaflet_max_zoom,
                        "base_layers": self._get_base_layers(),
                        "overlays": self._get_overlays(),
                        "attributionprefix": "FaST",
                        "scale": "metric",
                    }
                ),
                # Satellite imagery context
                "api_fastplatform_public_url": settings.API_GATEWAY_FASTPLATFORM_PUBLIC_URL,
                "catalogue_graphql_query": (
                    Path(settings.BASE_DIR)
                    / "utils/graphql/ndvi_rgb_catalogue_query.graphql"
                ).read_text(),
                "p_session_key": self.session_key,
                "map_tile_service_url": settings.MAP_TILE_SERVICE_URL,
                "default_leaflet_srid": settings.DEFAULT_LEAFLET_SRID,
                "rgb_ndvi_attribution": rgb_ndvi_attribution,
            }
        )

        return context

    class Media:
        js = [
            "https://unpkg.com/leaflet@1.6.0/dist/leaflet.js",
            "https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js",
            "https://unpkg.com/leaflet.vectorgrid@latest/dist/Leaflet.VectorGrid.bundled.js",
            "https://api.mapbox.com/mapbox.js/plugins/leaflet-minimap/v1.0.0/Control.MiniMap.js",
            "https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.min.js",
            "https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.umd.js",
            "https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.6.2/proj4.js",
            static("leaflet/draw/leaflet.draw.js"),
            static("leaflet/leaflet.forms.js"),
            static("leaflet/leaflet.zoomhome.min.js"),
            static("leaflet/leaflet-fastplatform.js"),
            static("leaflet/proj4leaflet.js"),
        ]
        css = {
            "all": [
                "https://unpkg.com/leaflet@1.6.0/dist/leaflet.css",
                "https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css",
                "https://api.mapbox.com/mapbox.js/plugins/leaflet-minimap/v1.0.0/Control.MiniMap.css",
                "https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.mapbox.css",
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
                "https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.css",
                "https://cdnjs.cloudflare.com/ajax/libs/leaflet-groupedlayercontrol/0.6.1/leaflet.groupedlayercontrol.css",
                static("leaflet/draw/leaflet.draw.css"),
                static("leaflet/leaflet.zoomhome.css"),
                static("leaflet/leaflet-fastplatform.css"),
                static("leaflet/leaflet-django-fixes.css"),
            ]
        }


def get_overlay_attribution(configuration_style):
    if "attribution" in configuration_style:
        return configuration_style["attribution"]
    return ""
