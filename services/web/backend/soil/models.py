import numbers

from django.contrib.gis.db import models
from django.contrib.postgres.fields import ranges
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator

from common.models import AbstractCodeList
from utils.models import GetAdminURLMixin, TranslatableModel, validate_latin1


class ObservationValidateResultMixin:
    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)

        value = self.result.get("value")
        label = self.result.get("label")
        validation_rules = self.observed_property.validation_rules

        if value is None or value == "":
            raise ValidationError(_('Result must have a non-empty "value" property.'))

        if label is not None and not isinstance(label, str):
            raise ValidationError(
                _('Result "label" property must be a string, if present.')
            )

        if validation_rules is not None:
            if validation_rules.get("type") == "numeric":
                if not isinstance(value, numbers.Number):
                    raise ValidationError(
                        _(
                            'Result value must be a number, e.g. {"value": 123, "label": "m"}'
                        )
                    )
                if validation_rules.get(
                    "min"
                ) is not None and value < validation_rules.get("min"):
                    raise ValidationError(
                        _(
                            "Result value must be greater than or equal to {min}."
                        ).format(min=validation_rules.get("min"))
                    )
                if validation_rules.get(
                    "max"
                ) is not None and value > validation_rules.get("max"):
                    raise ValidationError(
                        _("Result value must be less than or equal to {max}.").format(
                            max=validation_rules.get("max")
                        )
                    )
            elif validation_rules.get("type") == "text":
                if not isinstance(value, str):
                    raise ValidationError(
                        _(
                            'Result value must be a string, e.g. {"value": "abc", "label": "m"}'
                        )
                    )
            elif validation_rules.get("type") == "boolean":
                if not isinstance(value, bool):
                    raise ValidationError(
                        _(
                            'Result value must be a boolean, e.g. {"value": true, "label": "m"}'
                        )
                    )
            elif validation_rules.get("type") == "category":
                if not isinstance(value, str):
                    raise ValidationError(
                        _(
                            'Result value must be a string, e.g. {"value": "abc", "label": "m"}'
                        )
                    )
                if validation_rules.get(
                    "choices"
                ) and value not in validation_rules.get("choices"):
                    raise ValidationError(
                        _(
                            'Result value must be one of {values}, e.g. {"value": "{value_0}", "label": "m"}'
                        ).format(
                            values=validation_rules.get("choices"),
                            value_0=validation_rules.get("choices")[0],
                        )
                    )


class SoilOrigin(models.TextChoices):
    """Origin of the soil site : public, private or none if computed but nothing found"""

    private = "private", _("private")
    public = "public", _("public")
    none = "none", _("none")


class SoilDerivedObject(GetAdminURLMixin, models.Model):
    """A synthetic collection of soil observations computed from other soil sites
    and attached to a specific plot
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    is_derived_from = models.JSONField(
        verbose_name=_("derived from"), null=True, blank=True, default=dict
    )

    soil_sample_origin = models.CharField(
        max_length=20,
        null=False,
        blank=False,
        choices=SoilOrigin.choices,
        verbose_name=_("soil sample origin"),
    )

    soil_sites_count = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_("count of soil samples"),
        validators=[MinValueValidator(0)],
        help_text=_("Number of soil samples used to estimate the soil properties"),
    )

    plot = models.ForeignKey(
        to="farm.Plot",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="soil_derived_objects",
        verbose_name=_("plot"),
    )

    def __str__(self):
        return f"SoilDerivedObject ({self.id}) of plot {self.plot}"

    class Meta:
        verbose_name = _("estimated soil properties")
        verbose_name_plural = _("estimated soil properties")
        db_table = "soil_derived_object"
        constraints = (
            # There can be only one soil derived object per plot
            # (constraint needed for upserts)
            models.UniqueConstraint(
                fields=["plot"], name="soil_derived_object_plot_unique"
            ),
        )


def derived_observation_default_result():
    return {"value": None}


class DerivedObservation(
    ObservationValidateResultMixin, GetAdminURLMixin, models.Model
):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    soil_derived_object = models.ForeignKey(
        to="soil.SoilDerivedObject",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("estimated soil properties"),
        related_name="derived_observations",
    )

    observed_property = models.ForeignKey(
        to="soil.ObservableProperty",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("observed property"),
        related_name="derived_observation",
    )

    result = models.JSONField(
        null=False,
        blank=False,
        verbose_name=_("result"),
        default=derived_observation_default_result,
    )

    def __str__(self):
        return f"{self.observed_property} observation"

    class Meta:
        verbose_name = _("estimated soil properties observation")
        verbose_name_plural = _("estimated soil properties observations")
        db_table = "derived_observation"
        constraints = (
            # There can be only one observation per property in a soil derived object
            # (otherwise we cannot know which one is the right one)
            models.UniqueConstraint(
                fields=["soil_derived_object", "observed_property"],
                name="derived_observation_soil_derived_object_observed_property_unique",
            ),
        )


class SoilSite(GetAdminURLMixin, models.Model):
    """A private soil site of the holding

    (public soil sites are stored in the external app/database)

    Origin: INSPIRE Annex III - Soil
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    authority_id = models.CharField(
        null=True, blank=True, max_length=100, verbose_name=_("authority identifier")
    )

    holding = models.ForeignKey(
        to="farm.Holding",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="soil_sites",
        verbose_name=_("farm"),
    )

    geometry = models.GeometryField(
        srid=settings.DEFAULT_SRID, verbose_name=_("geometry")
    )

    soil_investigation_purpose = models.ForeignKey(
        to="SoilInvestigationPurpose",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("soil investigation purpose"),
        related_name="soil_sites",
    )

    valid_from = models.DateTimeField(
        null=True, blank=True, verbose_name=_("valid from")
    )
    valid_to = models.DateTimeField(null=True, blank=True, verbose_name=_("valid to"))

    begin_lifespan_version = models.DateTimeField(
        null=True, blank=True, verbose_name=_("begin of lifespan version")
    )

    end_lifespan_version = models.DateTimeField(
        null=True, blank=True, verbose_name=_("end of lifespan version")
    )

    source = models.JSONField(
        null=True, blank=True, verbose_name=_("source"), default=dict
    )

    label = models.CharField(
        null=True, blank=True, max_length=255, verbose_name=(_("label"))
    )

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("soil sample")
        verbose_name_plural = _("soil samples")
        db_table = "soil_site"
        constraints = [
            # authority_id must be unique or null
            models.UniqueConstraint(
                fields=["authority_id"],
                name="soil_site_authority_id_unique_or_null",
                condition=models.Q(authority_id__isnull=False),
            ),
        ]


class Observation(ObservationValidateResultMixin, GetAdminURLMixin, models.Model):
    """A soil observation of a private soil site

    Origin: INSPIRE Annex III - Soil
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    soil_site = models.ForeignKey(
        to="soil.SoilSite",
        verbose_name=_("soil site"),
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="observations",
    )

    observed_property = models.ForeignKey(
        to="soil.ObservableProperty",
        verbose_name=_("observed property"),
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="observations",
    )

    # NOTE OGC SWE Common provides a model suitable for describing many kinds
    #  of observation results.
    # The type of the observation result shall be consistent with the observed
    # property, and the scale or scope for the value shall be consistent with
    # the quantity or category type. If the observed property (6.2.2.8) is a
    # spatial operation or function, the type of the result may be a coverage.
    # https://www.opengeospatial.org/standards/swecommon
    result = models.JSONField(
        null=False, blank=False, verbose_name=_("result"), default=dict
    )

    # The phenomenon time cannot be null, we *need* to know the date of the observation
    phenomenon_time = models.DateTimeField(
        null=False, blank=False, verbose_name=_("phenomenom time")
    )

    result_time = models.DateTimeField(
        null=True, blank=True, verbose_name=_("result time")
    )

    valid_time = ranges.DateTimeRangeField(
        null=True, blank=True, verbose_name=_("valid time")
    )

    source = models.JSONField(
        null=True, blank=True, verbose_name=_("source"), default=dict
    )

    def __str__(self):
        return f"{self.observed_property} observation"

    class Meta:
        verbose_name = _("soil sample observation")
        verbose_name_plural = _("soil sample observations")
        db_table = "observation"
        constraints = (
            # There can be only one observation per property in a soil site at the same time
            # (otherwise we cannot know which one is the right one)
            models.UniqueConstraint(
                fields=["soil_site", "observed_property", "phenomenon_time"],
                name="observation_soil_site_observed_property_phenomenon_time_unique",
            ),
        )


class ObservableProperty(GetAdminURLMixin, TranslatableModel, models.Model):
    """A property that can be observed in private soil sites and in derived soil objects

    Origin: INSPIRE Annex III - Soil
    """

    id = models.CharField(
        primary_key=True,
        max_length=100,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    label = models.CharField(
        null=False, blank=False, max_length=100, verbose_name=_("label")
    )

    base_phenomenon = models.ForeignKey(
        to="soil.PhenomenonType",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="observable_properties",
        verbose_name=_("base phenomenon"),
    )

    uom = models.ForeignKey(
        to="common.UnitOfMeasure",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measure"),
    )

    validation_rules = models.JSONField(
        verbose_name=_("validation rules"), null=True, blank=True
    )

    def __str__(self):
        return self.label

    def clean(self, *args, **kwargs) -> None:
        super().clean(*args, **kwargs)

        if self.validation_rules:
            value_type = self.validation_rules.get("type")
            if value_type == "numeric":
                value_min = self.validation_rules.get("min")
                value_max = self.validation_rules.get("max")
                if value_min is not None and not isinstance(value_min, numbers.Number):
                    raise ValidationError(_("Min must be a number"))
                if value_max is not None and not isinstance(value_max, numbers.Number):
                    raise ValidationError(_("Max must be a number"))

                extra_keys = [
                    k
                    for k in self.validation_rules.keys()
                    if k not in ["type", "min", "max"]
                ]
                if extra_keys:
                    raise ValidationError(
                        _(
                            "Unknown keys in validation rules: {}. Only 'min' and 'max' keys are supported for type 'numeric'."
                        ).format(extra_keys)
                    )
            elif value_type == "category":
                value_choices = self.validation_rules.get("choices")
                if value_choices is not None:
                    if not isinstance(value_choices, list):
                        raise ValidationError(
                            _("Choices must be a list of dictionaries")
                        )
                    for choice in value_choices:
                        if not isinstance(choice, dict):
                            raise ValidationError(
                                _("Choices must be a list of dictionaries")
                            )
                        if "value" not in choice:
                            raise ValidationError(
                                _("Each choice must have a 'value' field")
                            )
                        if "label" not in choice or not isinstance(
                            choice["label"], str
                        ):
                            raise ValidationError(
                                _("Each choice must have a text 'label' field")
                            )

                        extra_keys = [
                            k for k in choice.keys() if k not in ["value", "label"]
                        ]
                        if extra_keys:
                            raise ValidationError(
                                _(
                                    "Unknown keys in choice: {}. Only 'value' and 'label' keys are supported for each choice."
                                ).format(extra_keys)
                            )

                extra_keys = [
                    k
                    for k in self.validation_rules.keys()
                    if k not in ["type", "choices"]
                ]
                if extra_keys:
                    raise ValidationError(
                        _(
                            "Unknown keys in validation rules: {}. Only 'choices' key is supported for type 'category'."
                        ).format(extra_keys)
                    )

            elif value_type == "text":
                extra_keys = [
                    k for k in self.validation_rules.keys() if k not in ["type"]
                ]
                if extra_keys:
                    raise ValidationError(
                        _(
                            "Unknown keys in validation rules: {}. No extra keys are supported for type 'text'."
                        ).format(extra_keys)
                    )
            elif value_type == "boolean":
                extra_keys = [
                    k for k in self.validation_rules.keys() if k not in ["type"]
                ]
                if extra_keys:
                    raise ValidationError(
                        _(
                            "Unknown keys in validation rules: {}. No extra keys are supported for type 'boolean'."
                        ).format(extra_keys)
                    )
            else:
                raise ValidationError(
                    _("Unknown type: '{}'. Available types are: {}").format(
                        value_type, ["numeric", "category", "text", "boolean"]
                    )
                )

    class Meta:
        verbose_name = _("observable property")
        verbose_name_plural = _("observable properties")
        db_table = "observable_property"


class PhenomenonType(AbstractCodeList):
    """
    http://inspire.ec.europa.eu/codelist/SoilSiteParameterNameValue

    Origin: INSPIRE Annex III - Soil
    """

    class Meta:
        verbose_name = _("soil phenomenon type")
        verbose_name_plural = _("soil phenomenon types")
        db_table = "phenomenon_type"


class SoilInvestigationPurpose(AbstractCodeList):
    """
    https://inspire.ec.europa.eu/codelist/SoilInvestigationPurposeValue

    Origin: INSPIRE Annex III - Soil
    """

    class Meta:
        verbose_name = _("soil investigation purpose")
        verbose_name_plural = _("soil investigation purposes")
        db_table = "soil_investigation_purpose"
