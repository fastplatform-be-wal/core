# Generated by Django 3.2 on 2021-12-07 16:19

from django.db import migrations, models
import external.models.soil


class Migration(migrations.Migration):

    dependencies = [
        ('external', '0026_auto_20211026_1151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agribuilding',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='agriplot',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='designationtype',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='managementrestrictionorregulationzone',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='observation',
            name='result',
            field=models.JSONField(default=external.models.soil.default_value_none, help_text='Must be an JSON object with a "value" property, e.g. {"value": 123}', verbose_name='result'),
        ),
        migrations.AlterField(
            model_name='observation',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='protectedsite',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='soilsite',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='surfacewater',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='watercourse',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='watermeasurement',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
        migrations.AlterField(
            model_name='watermeasurementobservation',
            name='result',
            field=models.JSONField(default=dict, verbose_name='result'),
        ),
        migrations.AlterField(
            model_name='watermeasurementobservation',
            name='source',
            field=models.JSONField(blank=True, default=dict, help_text='The source data from which this object was derived.', null=True, verbose_name='source'),
        ),
    ]
