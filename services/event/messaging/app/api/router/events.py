from pydantic import BaseModel, Field
from datetime import datetime

# {
#   "id": "85558393-c75d-4d2f-9c15-e80591b83894",
#   "created_at": "2018-09-05T07:14:21.601701Z",
#   "trigger": {
#       "name": "test_trigger"
#   },
#   "table": {
#       "schema": "public",
#       "name": "users"
#   },
#   "event": {
#       "session_variables": {
#           "x-hasura-role": "admin",
#           "x-hasura-allowed-roles": "['user', 'boo', 'admin']",
#           "x-hasura-user-id": "1"
#       },
#       "op": "INSERT",
#       "data": {
#         "old": null,
#         "new": {
#             "id":"42",
#             "name": "john doe"
#         }
#       }
#   }
# }


class HasuraEventTrigger(BaseModel):
    name: str


class HasuraEventTable(BaseModel):
    scheme: str = Field(None, alias='schema')
    name: str


class HasuraEventData(BaseModel):
    old: dict = None
    new: dict = None


class HasuraEventEvent(BaseModel):
    session_variables: dict = None
    op: str
    data: HasuraEventData


class HasuraEvent(BaseModel):
    id: str
    created_at: datetime
    trigger: HasuraEventTrigger
    table: HasuraEventTable
    event: HasuraEventEvent


class HasuraActionAction(BaseModel):
    name: str


class HasuraAction(BaseModel):
    action: HasuraActionAction
    input: dict = None
    session_variables: dict = None
