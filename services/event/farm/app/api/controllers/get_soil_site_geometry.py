import logging

from opentelemetry import trace
from gql import gql
from app.settings import config
from app.db.graphql_clients import fastplatform

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def get_soil_site_geometry(soil_site_id: int):
    """Return geometry of a given soil_site, in the geojson format.

    Args:
        soil_site_id (int): the id of the soil_site

    Returns:
        geometry (geojson): geometry of the soil_site
    """

    with tracer().start_as_current_span("get_soil_site_geometry"):
        logger.info("get_soil_site_geometry %s", soil_site_id)
        mutation = (
            config.API_DIR
            / "controllers"
            / "graphql"
            / "query_soil_site_geometry.graphql"
        ).read_text()
        variables = {"soil_site_id": soil_site_id}

        result = await fastplatform.execute(gql(mutation), variables)

        if not result["soil_site_by_pk"]:
            return None

        return result["soil_site_by_pk"]["geometry"]
