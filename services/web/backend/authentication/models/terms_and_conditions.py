from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from utils.models import GetAdminURLMixin
from utils.models import TranslatableModel

class TermsAndConditionsConsentRequest(GetAdminURLMixin, TranslatableModel):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    activate_at = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("activation date"),
        help_text=_(
            "Consent will not be requested from users before that date (can be left blank)."
        ),
    )

    deactivate_at = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("deactivation date"),
        help_text=_(
            "Consent will not be requested from users after that date (can be left blank)."
        ),
    )

    title = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_("title")
    )

    short_description = models.TextField(
        null=True, blank=True, verbose_name=_("short description")
    )

    url = models.URLField(null=True, blank=True, verbose_name=_("URL"))

    html_content = models.TextField(
        null=True, blank=True, verbose_name=_("HTML content")
    )

    in_farmer_app = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name=_("In the farmer app"),
        help_text=_("Consent will be requested in the farmer mobile and web app"),
    )

    in_administration_portal = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name=_("In the Administration Portal"),
        help_text=_("Consent will be requested in the Administration Portal"),
    )

    def __str__(self):
        return self.title

    class Meta:
        db_table = "terms_and_conditions_consent_request"
        ordering = ["activate_at"]
        verbose_name = _("terms & conditions consent request")
        verbose_name_plural = _("terms & conditions consent requests")
        help_text = _(
            "Legal consents or notices (e.g. privacy policy, terms and conditions) that should be requested to the user"
        )
        constraints = (
            # If activate_at and deactivate_at are noth non null, then activate_at must
            # be before de activate_at
            models.CheckConstraint(
                name="tcs_request_deactivate_at_after_activate_at",
                check=~(
                    models.Q(activate_at__isnull=False)
                    & models.Q(deactivate_at__isnull=False)
                    & models.Q(activate_at__gt=models.F("deactivate_at"))
                ),
            ),
        )


class UserTermsAndConditionsConsent(GetAdminURLMixin, models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    user = models.ForeignKey(
        "authentication.User",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="user_terms_and_conditions_consents",
        verbose_name=_("user"),
    )

    consent_request = models.ForeignKey(
        "authentication.TermsAndConditionsConsentRequest",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        related_name="user_terms_and_conditions_consents",
        verbose_name=_("consent request"),
    )

    consent_received_at = models.DateTimeField(
        editable=False,
        auto_now_add=True,
        null=False,
        blank=False,
        verbose_name=_("date received"),
    )

    metadata = models.JSONField(null=True, blank=True, verbose_name=_("metadata"))

    def __str__(self):
        return f"{self.consent_request.title}: {self.user.username} {self.consent_received_at.isoformat()}"

    class Meta:
        db_table = "user_terms_and_conditions_consent"
        verbose_name = _("user terms & conditions consent")
        verbose_name_plural = _("user terms & conditions consents")
        help_text = _("Dates terms & conditions consents were given by users")
        constraints = (
            # A user can't consent twice on the same request
            models.UniqueConstraint(
                name="user_tcs_consent_user_request_unique",
                fields=["user", "consent_request"],
            ),
        )


class UserIACSConsent(GetAdminURLMixin, models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    user = models.ForeignKey(
        "authentication.User",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="user_iacs_consents",
        verbose_name=_("user"),
    )

    consent_received_at = models.DateTimeField(
        editable=False,
        auto_now_add=True,
        null=False,
        blank=False,
        verbose_name=_("date received"),
    )

    metadata = models.JSONField(null=True, blank=True, verbose_name=_("metadata"))

    def __str__(self):
        return f"{self.user.username} {self.consent_received_at.isoformat()}"

    class Meta:
        db_table = "user_iacs_consent"
        verbose_name = _("user IACS consent")
        verbose_name_plural = _("user IACS consents")
        help_text = _("Dates IACS access consents were given by users")
