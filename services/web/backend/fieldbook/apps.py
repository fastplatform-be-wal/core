from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class FieldbookConfig(AppConfig):
    name = 'fieldbook'
    verbose_name = _('Fieldbook & Agriculture')

    help_text = _('Elements describing the agricultural practices of a farm')
