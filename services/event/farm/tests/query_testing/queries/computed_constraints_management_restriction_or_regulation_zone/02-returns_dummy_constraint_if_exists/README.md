# 02-returns_dummy_constraint_if_exists

## Setup
- 1 holding containing 1 plot.
- 1 constraint, with description set to dummy value.

## Computed
Nothing.

## Returns
The constraint from setup.