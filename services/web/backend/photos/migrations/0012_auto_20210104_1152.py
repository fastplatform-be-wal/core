# Generated by Django 3.0.7 on 2021-01-04 10:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0024_auto_20210104_1150'),
        ('photos', '0011_reset_sql_sequences'),
    ]

    operations = [
        migrations.AlterField(
            model_name='geotaggedphotorequest',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='Groups that can view the photos associated with this request', related_name='geo_tagged_photo_requests', to='authentication.Group', verbose_name='groups'),
        ),
    ]
