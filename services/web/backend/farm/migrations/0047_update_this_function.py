from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farm', '0046_set_db_comments_farm')
    ]

    operations = [
        migrations.RunSQL((Path(__file__).parent / Path('create_plot_function_source.sql')).read_text())
    ]
