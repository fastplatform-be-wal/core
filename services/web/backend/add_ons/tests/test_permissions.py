import csv
import subprocess

import requests
from datetime import datetime
import time
import os

from pathlib import Path

from django.apps import apps
from django.test import TransactionTestCase
from django.conf import settings
from django.contrib.auth.models import Permission

from model_bakery.recipe import baker
from authentication.models import *
from common.models import *
from configuration.models import *
from soil.models import *
from fieldbook.models import *
from add_ons.models import *
from oidc_provider.models import *
from farm.models import *
from messaging.models import *
from photos.models import *


# Test constants
ADD_ON_ID_SUBSCRIBED_TO_AND_NOT_AUTO = "ADD_ON_ID_SUBSCRIBED_TO_AND_NOT_AUTO"
ADD_ON_ID_NOT_SUBSCRIBED_TO_AND_NOT_AUTO = "ADD_ON_ID_NOT_SUBSCRIBED_TO_AND_NOT_AUTO"
ADD_ON_ID_AUTO = "ADD_ON_ID_AUTO"
HOLDING_ID_SUBSCRIBED = "HOLDING_ID_SUBSCRIBED"
HOLDING_ID_NOT_SUBSCRIBED = "HOLDING_ID_NOT_SUBSCRIBED"
POSTGRES_USER = os.environ.get("POSTGRES_USER")
POSTGRES_PWD = os.environ.get("POSTGRES_PASSWORD")
POSTGRES_PORT = os.environ.get("POSTGRES_PORT")
POSTGRES_HOST = os.environ.get("POSTGRES_HOST")
POSTGRES_DATABASE = os.environ.get("POSTGRES_DATABASE")

MANY_TO_MANY_TABLES = [
    "user_group",
    "user_user_permissions",
]

# Prevent random recipe values from breaking specific field constraints
FLOAT_GTE_0 = 0.1

# Hasura params
HASURA_PORT_TEST = 18087
HASURA_DOCKER_CONTAINER_TEST = "hasura-fastplatform-test"

# DEBUG
RESTART_HASURA_ON_SETUPCLASS = True
STOP_HASURA_ON_TEARDOWNCLASS = True


class AddOnPermissionsTestCase(TransactionTestCase):
    @classmethod
    def setUpClass(cls):
        super(AddOnPermissionsTestCase, cls).setUpClass()

        cls.load_resources()
        cls.create_view_permissions()

        if RESTART_HASURA_ON_SETUPCLASS:
            print(
                f"\nFirst make sure {HASURA_DOCKER_CONTAINER_TEST} container is stopped and removed..."
            )
            cls.stop_hasura()
            time.sleep(15)
            cls.start_hasura()

            # Give it some time to finish launching
            wait = 120
            print(
                f"Wait {wait} seconds for {HASURA_DOCKER_CONTAINER_TEST} container to finish launching..."
            )
            time.sleep(wait)

    @classmethod
    def tearDownClass(cls):
        super(AddOnPermissionsTestCase, cls).tearDownClass()
        if STOP_HASURA_ON_TEARDOWNCLASS:
            cls.stop_hasura()

    @classmethod
    def load_resources(cls):
        resources_path = Path(settings.BASE_DIR) / "add_ons/tests/resources"

        with open(resources_path / "select_permissions.csv", "r") as f:
            reader = csv.DictReader(f)
            cls.columns_readable = {}
            cls.columns_not_readable = {}
            cls.tables = []
            for row in reader:
                table = row["table_name"]
                column = row["column_name"]
                if table not in cls.columns_readable:
                    cls.columns_readable[table] = []
                    cls.columns_not_readable[table] = []
                    cls.tables.append(table)

                if row["readable_by_add_on"]:
                    cls.columns_readable[table].append(column)

                if not row["readable_by_add_on"]:
                    cls.columns_not_readable[table].append(column)

        with open(resources_path / "tables_private_to_holding.csv", "r") as f:
            reader = csv.DictReader(f)
            cls.tables_private_to_holding = [
                row["table_name"] for row in reader if bool(row["private_to_holding"])
            ]

    @classmethod
    def create_view_permissions(cls):
        cls.view_permissions = {}
        for app in apps.get_app_configs():

            app_models = list(app.get_models())
            app_models = [m for m in app_models if not m._meta.proxy]
            app_models = sorted(app_models, key=lambda m: m.__name__)
            if not app_models:
                continue

            for model in app_models:
                model_anchor = model.__name__.lower()
                cls.view_permissions[model._meta.db_table] = f"view_{model_anchor}"

    @classmethod
    def start_hasura(cls):

        # cf. core/services/api_gateway/fastplatform/Makefile
        # cmd: start_test

        service_path = Path(settings.BASE_DIR).parent.parent

        command = [
            "docker",
            "run",
            "--name",
            HASURA_DOCKER_CONTAINER_TEST,
            "--restart",
            "on-failure",
            "-e",
            f"HASURA_GRAPHQL_DATABASE_URL=postgres://{POSTGRES_USER}:{POSTGRES_PWD}@{POSTGRES_HOST}:{POSTGRES_PORT}/test_{POSTGRES_DATABASE}",
            "-e",
            "HASURA_GRAPHQL_ENABLE_CONSOLE=true",
            "-e",
            "HASURA_GRAPHQL_CONSOLE_ASSETS_DIR=/srv/console-assets",
            "--env-file",
            f"{service_path}/api_gateway/fastplatform/metadata/metadata.env",
            "-p",
            f"{HASURA_PORT_TEST}:8080",
            "-v",
            f"{service_path}/api_gateway/fastplatform/metadata/:/hasura-metadata",
            "-d",
            "hasura/graphql-engine:v2.3.1.cli-migrations-v3",
        ]

        hasura_process = subprocess.Popen(
            command,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        hasura_process.wait(500)
        hasura_error = hasura_process.stderr.readline().decode("utf-8")

        print(
            f"(Re)started {HASURA_DOCKER_CONTAINER_TEST} container -> {hasura_error if hasura_error else 'ok'}"
        )

    @classmethod
    def stop_hasura(cls):
        # cf. core/services/api_gateway/fastplatform/Makefile
        # cmd: stop_test

        stop_command = ["docker", "stop", HASURA_DOCKER_CONTAINER_TEST]
        stop_process = subprocess.Popen(
            stop_command,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        stop_process.wait(500)
        stop_error = stop_process.stderr.readline().decode("utf-8")
        print(
            f"Stopped {HASURA_DOCKER_CONTAINER_TEST} container -> {stop_error if stop_error else 'ok'}"
        )

        rm_command = ["docker", "rm", HASURA_DOCKER_CONTAINER_TEST]
        rm_process = subprocess.Popen(
            rm_command,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        rm_process.wait(500)
        rm_error = rm_process.stderr.readline().decode("utf-8")

        print(
            f"Removed {HASURA_DOCKER_CONTAINER_TEST} container -> {rm_error if rm_error else 'ok'}"
        )

    def setUp(self):
        self.create_test_add_ons()
        self.create_not_private_to_holding_test_objects()
        self.create_private_to_holding_test_objects()

    def create_test_add_ons(self):
        self.fake_view_permission = baker.make(Permission, name="fake_view_permission")

        self.group = baker.make(Group, permissions=[self.fake_view_permission], id=1)

        self.provider = baker.make(Provider, name="Test provider", is_active=True)

        self.add_on_subscribed_to_and_not_auto, _ = AddOn.objects.update_or_create(
            id=ADD_ON_ID_SUBSCRIBED_TO_AND_NOT_AUTO,
            defaults=dict(
                name=ADD_ON_ID_SUBSCRIBED_TO_AND_NOT_AUTO,
                provider=self.provider,
                auto_subscribe=False,
                is_active=True,
                callback="https://test.com/callback/",
                # callback_secret_key NOT SET: automatically created on save
            ),
        )

        self.add_on_subscribed_to_and_not_auto.groups.set([self.group])  # add_on_group
        self.add_on_subscribed_to_and_not_auto.save()

        add_on_not_subscribed_to_and_not_auto, _ = AddOn.objects.update_or_create(
            id=ADD_ON_ID_NOT_SUBSCRIBED_TO_AND_NOT_AUTO,
            defaults=dict(
                name=ADD_ON_ID_NOT_SUBSCRIBED_TO_AND_NOT_AUTO,
                provider=self.provider,
                auto_subscribe=False,
                is_active=True,
                callback="https://test.com/callback/",
                # callback_secret_key NOT SET: automatically created on save
            ),
        )

        add_on_auto, _ = AddOn.objects.update_or_create(
            id=ADD_ON_ID_AUTO,
            defaults=dict(
                name=ADD_ON_ID_AUTO,
                provider=self.provider,
                auto_subscribe=True,
                is_active=True,
                callback="https://test.com/callback/",
                # callback_secret_key NOT SET: automatically created on save
            ),
        )

        add_on_with_fake_permission, _ = AddOn.objects.update_or_create(
            id="add_on_with_fake_view_permission"
        )

        add_on_with_fake_permission.permissions.set([self.fake_view_permission])

    def create_not_private_to_holding_test_objects(self):
        map_base_layer = baker.make(MapBaseLayer, id=1, is_active_in_farmer_app=True)
        map_overlay = baker.make(MapOverlay, id=1, is_active_in_farmer_app=True)
        configuration = baker.make(Configuration, id=1)
        add_on_api_key = baker.make(
            AddOnAPIKey, id=1, generated_at=datetime.now(tz=timezone.utc)
        )
        self.broadcast = baker.make(Broadcast, id=1)
        self.campaign = baker.make(Campaign, id=1)

        self.unit_of_measure = baker.make(UnitOfMeasure, id=1)

        language = baker.make(Language, iso_code="fr")
        country = baker.make(
            Country,
            iso_code="be",
            default_language=language,
            languages=[language],  # country_language
        )

        region = baker.make(
            Region,
            id="wa-be",
            iso_code="wa",
            country=country,
            default_language=language,
            languages=[language],  # region_language
        )

        # plot
        self.plant_species_group = baker.make(PlantSpeciesGroup, id=1)
        self.plant_species = baker.make(PlantSpecies, id=1)
        self.plant_variety = baker.make(
            PlantVariety, plant_species=self.plant_species, id=1
        )

        self.irrigation_method = baker.make(IrrigationMethod, id=1)
        self.chemical_element = baker.make(ChemicalElement, id=1)
        self.determination_method = baker.make(DeterminationMethod, id=1)

        self.fertilizer_type = baker.make(FertilizerType, id=1)
        self.phenomenon_type = baker.make(PhenomenonType, id=1)

        self.queue = baker.make(Queue, groups=[self.group], id=1)  # queue_groups
        registered_fertilizer = baker.make(
            RegisteredFertilizer,
            fertilizer_type=self.fertilizer_type,
            recommended_quantity_unit_of_measure=self.unit_of_measure,
            id=1,
        )

        registered_fertilizer_element = baker.make(
            RegisteredFertilizerElement,
            fertilizer=registered_fertilizer,
            chemical_element=self.chemical_element,
            id=1,
        )

        self.soil_investigation_purpose = baker.make(SoilInvestigationPurpose, id=1)

    def create_private_to_holding_test_objects(self):
        holding_ids = [HOLDING_ID_SUBSCRIBED, HOLDING_ID_NOT_SUBSCRIBED]

        running_id = 1
        for holding_id in holding_ids:

            user = baker.make(
                User,
                groups=[self.group],  # user_group
                user_permissions=[self.fake_view_permission],  # user_user_permissions
                is_active=True,
                id=running_id,
            )

            if holding_id == HOLDING_ID_SUBSCRIBED:
                self.subscribed_user_id = user.id

            self.provider.members.set([user])  # provider_member

            broadcast_user_read_status = baker.make(
                BroadcastUserReadStatus,
                user=user,
                id=running_id,
                broadcast=self.broadcast,
            )
            user_iacs_consent = baker.make(UserIACSConsent, user=user, id=running_id)
            user_terms_and_conditions_consent = baker.make(
                UserTermsAndConditionsConsent, user=user, id=running_id
            )

            holding = baker.make(
                Holding,
                id=holding_id,
                name=holding_id,
            )
            holding_campaign = baker.make(
                HoldingCampaign, id=running_id, campaign=self.campaign, holding=holding
            )

            site = baker.make(Site, holding_campaign=holding_campaign, id=running_id)
            plot = baker.make(Plot, site=site, id=running_id)

            plot_plant_variety = baker.make(
                PlotPlantVariety,
                plant_variety=self.plant_variety,
                irrigation_method=self.irrigation_method,
                plot=plot,
                id=running_id,
            )
            constraint = baker.make(Constraint, plot=plot, id=running_id)

            fertilization_plan = baker.make(
                FertilizationPlan,
                algorithm=self.add_on_subscribed_to_and_not_auto,
                plot=plot,
                id=running_id,
            )

            user_related_party = baker.make(
                UserRelatedParty, holding=holding, user=user, id=running_id
            )

            custom_fertilizer = baker.make(
                CustomFertilizer,
                created_by=user,
                holding=holding,
                fertilizer_type=self.fertilizer_type,
                recommended_quantity_unit_of_measure=self.unit_of_measure,
                id=running_id,
            )

            custom_fertilizer_element = baker.make(
                CustomFertilizerElement,
                fertilizer=custom_fertilizer,
                chemical_element=self.chemical_element,
                determination_method=self.determination_method,
                id=running_id,
            )

            soil_site = baker.make(
                SoilSite,
                holding=holding,
                soil_investigation_purpose=self.soil_investigation_purpose,
                id=running_id,
            )

            observable_property = baker.make(
                ObservableProperty,
                uom=self.unit_of_measure,
                base_phenomenon=self.phenomenon_type,
                id=running_id,
            )
            observation = baker.make(
                Observation,
                observed_property=observable_property,
                soil_site=soil_site,
                id=running_id,
            )
            soil_derived_object = baker.make(
                SoilDerivedObject, plot=plot, id=running_id
            )
            derived_observation = baker.make(
                DerivedObservation,
                observed_property=observable_property,
                soil_derived_object=soil_derived_object,
                id=running_id,
            )

            ticket = baker.make(
                Ticket,
                created_by=user,
                assigned_to=user,
                holding=holding,
                queue=self.queue,
                id=running_id,
            )

            geo_tagged_photo = baker.make(
                GeoTaggedPhoto,
                uploaded_by=user,
                holding_campaign=holding_campaign,
                plot=plot,
                photo_width=1,  # required to prevent attempt to open it
                photo_height=1,
                id=running_id,
            )
            ticket_message = baker.make(
                TicketMessage,
                ticket=ticket,
                created_by=user,
                geo_tagged_photo=geo_tagged_photo,
                id=running_id,
            )

            ticket_user_read_status = baker.make(
                TicketUserReadStatus,
                ticket=ticket,
                user=user,
                last_read_ticket_message=ticket_message,
                id=running_id,
            )

            # User subscribes only one holding to add_on to compare query results
            if holding_id == HOLDING_ID_SUBSCRIBED:
                add_on_subscription = baker.make(
                    AddOnSubscription,
                    add_on=self.add_on_subscribed_to_and_not_auto,
                    holding=holding,
                    subscribed_by=user,
                    id=running_id,
                )

            running_id += 1

    def test_all_tables_filled(self):
        """
        First test the test: some test assert that no objects are returned.
        Make sure no rows returned doesn't simply mean no objects in db.
        """
        models = [
            AddOnAPIKey,
            AddOnSubscription,
            Broadcast,
            BroadcastUserReadStatus,
            Campaign,
            ChemicalElement,
            Configuration,
            Constraint,
            Country,
            CustomFertilizer,
            CustomFertilizerElement,
            DerivedObservation,
            DeterminationMethod,
            FertilizationPlan,
            FertilizerType,
            GeoTaggedPhoto,
            Group,
            Holding,
            HoldingCampaign,
            IrrigationMethod,
            Language,
            MapBaseLayer,
            MapOverlay,
            ObservableProperty,
            Observation,
            Permission,
            PhenomenonType,
            PlantSpecies,
            PlantSpecies,
            PlantSpeciesGroup,
            PlantVariety,
            Plot,
            PlotPlantVariety,
            Provider,
            Queue,
            Region,
            RegisteredFertilizer,
            RegisteredFertilizerElement,
            Site,
            SoilDerivedObject,
            SoilInvestigationPurpose,
            SoilSite,
            Ticket,
            TicketMessage,
            TicketUserReadStatus,
            UnitOfMeasure,
            User,
            UserIACSConsent,
            UserRelatedParty,
            UserTermsAndConditionsConsent,
        ]

        for o in models:
            self.assertIsNotNone(o.objects.count())
            # DEBUG
            # print(f"{o}: {o.objects.count()} objects")

    def test_autosubscribed_addon_sees_all_private_to_holding(self):
        """
        Each object of type `private_to_holding` (as per resources/tables_private_to_holding.csv)
        has been instanciated twice, once for a subscribed addon, and once for an unsubscribed addon.

        Since an `autosubscribed` addon has automatically access to all data private to holding,
        check here we get back systematically 2 objects per model queried.
        """

        for table in self.tables_private_to_holding:
            with self.subTest(table=table):
                response = self.query_private_table(
                    add_on_id=ADD_ON_ID_AUTO, table=table
                )
                # DEBUG
                # if "errors" in response:
                #     print(f"{table} errors: {response['errors']}")
                # else:
                #     print(
                #         f"{table} has {len(response['data'][table])} object(s): {response['data'][table]}"
                #     )

                self.assertNotIn("errors", response)
                self.assertTrue(response["data"][table])
                id_column = self.get_id_column(table)

                if table == "add_on_subscription":
                    self.assertListEqual(response["data"][table], [{id_column: 1}])
                elif table == "user":
                    self.assertEquals(len(response["data"][table]), 3)
                elif table == "holding":
                    self.assertListEqual(
                        response["data"][table],
                        [
                            {id_column: HOLDING_ID_SUBSCRIBED},
                            {id_column: HOLDING_ID_NOT_SUBSCRIBED},
                        ],
                    )
                elif table in MANY_TO_MANY_TABLES:
                    self.assertEquals(len(response["data"][table]), 2)
                else:
                    self.assertListEqual(
                        response["data"][table], [{id_column: 1}, {id_column: 2}]
                    )

    def test_not_subscribed_addon_sees_none_of_private_objects(self):
        """
        Check that an add_on to which no holding has subscribed
        cannot access any private object of any holding.
        """

        for table in self.tables_private_to_holding:
            with self.subTest(table=table):
                response = self.query_private_table(
                    add_on_id=ADD_ON_ID_NOT_SUBSCRIBED_TO_AND_NOT_AUTO, table=table
                )

                # DEBUG
                # if "errors" in response:
                #     print(f"{table} errors: {response['errors']}")
                # else:
                #     print(f"{table}: {len(response['data'][table])} object(s)")

                self.assertNotIn("errors", response)
                self.assertListEqual(response["data"][table], [])

    def test_subscribed_add_on_only_sees_own_private_objects(self):
        """
        Check that an add_on to which exactly one holding subscribed
        can access the private object of the holding, and only those.
        """

        for table in self.tables_private_to_holding:
            with self.subTest(table=table):
                response = self.query_private_table(
                    add_on_id=ADD_ON_ID_SUBSCRIBED_TO_AND_NOT_AUTO, table=table
                )
                # DEBUG
                # if "errors" in response:
                #     print(f"{table} errors: {response['errors']}")
                # else:
                #     print(f"{table}: {len(response['data'][table])} object(s)")
                #     print(f"{table}: {response['data'][table]}")

                self.assertNotIn("errors", response)
                self.assertTrue(response["data"][table])

                if table in MANY_TO_MANY_TABLES:
                    self.assertTrue(response["data"][table])
                elif table == "user":
                    self.assertListEqual(
                        response["data"][table], [{"id": self.subscribed_user_id}]
                    )
                elif table == "holding":
                    self.assertListEqual(
                        response["data"][table], [{"id": HOLDING_ID_SUBSCRIBED}]
                    )
                else:
                    id_column = self.get_id_column(table)
                    self.assertListEqual(response["data"][table], [{id_column: 1}])

    def test_columns_readable(self):
        """Check for every readable column of every table that a query does not return `[]`"""
        for table in self.columns_readable:
            view_permission = self.format_view_permission(table)

            for column in self.columns_readable[table]:
                with self.subTest(table=table, column=column):
                    query = f"query MyQuery {{ {table} {{ {column} }} }}"
                    response = requests.post(
                        f"http://localhost.fastplatform.eu:{HASURA_PORT_TEST}/v1/graphql",
                        headers={
                            "X-Hasura-AddOn-Id": ADD_ON_ID_AUTO,
                            "X-Hasura-Role": "add_on",
                            "X-Hasura-AddOn-Permissions": f"{{{view_permission}}}",
                        },
                        json={"query": query, "variables": {}},
                    )
                    response = response.json()

                    # DEBUG
                    # print(f"{table}:{column}:{view_permission}:{response['data'][table]}")

                    self.assertNotIn("errors", response)
                    self.assertTrue(response["data"][table])

    def test_columns_not_readable(self):
        """For every non-readable column of every table, check we get a `error`-type response"""
        for table in self.columns_not_readable:

            view_permission = self.format_view_permission(table)

            for column in self.columns_not_readable[table]:
                with self.subTest(table=table, column=column):
                    query = f"query MyQuery {{ {table} {{ {column} }} }}"
                    response = requests.post(
                        f"http://localhost.fastplatform.eu:{HASURA_PORT_TEST}/v1/graphql",
                        headers={
                            "X-Hasura-AddOn-Id": ADD_ON_ID_AUTO,
                            "X-Hasura-Role": "add_on",
                            "X-Hasura-AddOn-Permissions": f"{{{view_permission}}}",
                        },
                        json={"query": query, "variables": {}},
                    )
                    response = response.json()

                    self.assertTrue("errors" in response)
                    self.assertTrue(response["errors"])
                    self.assertTrue(response["errors"][0])
                    self.assertTrue(response["errors"][0]["message"])
                    self.assertTrue(
                        f'field "{column}" not found in type'
                        in response["errors"][0]["message"]
                        or f'field "{table}" not found in type'
                        in response["errors"][0]["message"]
                    )

    def test_view_permission_missing(self):
        """Check no addon permission returns no data on every model"""
        for table in self.tables:
            with self.subTest(table=table):
                # If not one readable column, already covered by test_columns_not_readable
                if not self.columns_readable[table]:
                    continue

                first_readable_column = self.columns_readable[table][0]
                query = f"query MyQuery {{ {table} {{ {first_readable_column} }} }}"
                response = requests.post(
                    f"http://localhost.fastplatform.eu:{HASURA_PORT_TEST}/v1/graphql",
                    headers={
                        "X-Hasura-AddOn-Id": ADD_ON_ID_AUTO,
                        "X-Hasura-Role": "add_on",
                        "X-Hasura-AddOn-Permissions": f"{{}}",  # no permission artificially set here
                    },
                    json={"query": query, "variables": {}},
                )
                response = response.json()

                # DEBUG
                # print(f"{table}:{first_readable_column}:{response['data'][table]}")
                self.assertListEqual(response["data"][table], [])

    # Helper functions
    def format_view_permission(self, table):

        if table in self.view_permissions:
            return self.view_permissions[table]

        view_permission = f'view_{table.lower().replace("_", "")}'

        if (
            view_permission.endswith("group")
            or view_permission.endswith("groups")
            or view_permission.endswith("permission")
            or view_permission.endswith("permissions")
            or view_permission.endswith("language")
        ):
            view_permission = (
                view_permission.replace("addon", "")
                .replace("auth", "")
                .replace("plot", "")
                .replace("user", "")
                .replace("groups", "group")
                .replace("geotaggedphoto", "")
                .replace("request", "")
                .replace("queue", "")
                .replace("permissions", "permission")
                .replace("country", "")
                .replace("region", "")
            )

        return view_permission

    def get_id_column(self, table):
        return "id"

    def query_private_table(self, add_on_id, table):

        view_permission = self.format_view_permission(table)
        id_column = self.get_id_column(table)

        # DEBUG
        # print(f"{table}: view_permission={view_permission}, id_column={id_column}")

        query = f"query MyQuery {{ {table} {{ {id_column} }} }}"
        response = requests.post(
            f"http://localhost.fastplatform.eu:{HASURA_PORT_TEST}/v1/graphql",
            headers={
                "X-Hasura-AddOn-Id": add_on_id,
                "X-Hasura-Role": "add_on",
                "X-Hasura-AddOn-Permissions": f"{{{view_permission}}}",
            },
            json={"query": query, "variables": {}},
        )
        response = response.json()

        # DEBUG
        # print(f"{table}: {response}")

        return response
