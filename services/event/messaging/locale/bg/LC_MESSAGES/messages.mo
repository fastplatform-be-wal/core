��          �                 #     I   A  3   �  @   �  ,      4   -  A   b  5   �  &   �  A     4   C     x  	        �     �  �  �  2   %  b   X  @   �  T   �  7   Q  D   �  X   �  [   '  ?   �  h   �  W   ,     �     �     �     �   %(user_display_name)s sent a photo. %(user_link)s created ticket %(ticket_link)s for holding %(holding_link)s %(user_link)s replied to the ticket %(ticket_link)s %(user_link)s replied to the ticket %(ticket_link)s with a photo %(user_name)s created ticket %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Request %(ticket_link)s status changed to %(status)s. The request status has been changed to This email has been generated automatically, please do not reply. Ticket %(ticket_link)s status changed to %(status)s. closed duplicate open resolved Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-25 19:03+0100
PO-Revision-Date: 2021-12-14 09:36+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: bg
Language-Team: bg <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 %(user_display_name)s изпрати снимка. %(user_link)s създаде билет %(ticket_link)s за стопанство %(holding_link)s %(user_link)s отговори на билета %(ticket_link)s %(user_link)s отговори на билета %(ticket_link)s със снимка %(user_name)s създаде билет %(ticket_link)s %(user_name)s отговори на заявката %(ticket_link)s %(user_name)s отговори на заявката %(ticket_link)s със снимка Статусът на заявката %(ticket_link)s е променен на %(status)s. Статусът на заявката е променен на Този имейл е генериран автоматично, моля, не отговаряйте. Статусът на билета %(ticket_link)s е променен на %(status)s. затворен дублиране на отворен разрешен 