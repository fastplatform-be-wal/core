from django.contrib.auth.middleware import MiddlewareMixin
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.urls import reverse, resolve
from django.db.models import Exists, OuterRef

from authentication.models import (
    TermsAndConditionsConsentRequest,
    UserTermsAndConditionsConsent,
)


class TermsAndConditionsConsentRequestMiddleware(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user and request.user.is_authenticated:
            resolver_match = resolve(request.path)

            if (
                resolver_match.namespace in ["admin", "docs"]
                and resolver_match.view_name not in ["admin_login"]
            ) or (resolver_match.view_name in ["all_objects"]):
                # Find all the requests that are currently active for the admin portal
                # and that have not yet been consented to by the user
                now = timezone.now()
                pending_request = (
                    TermsAndConditionsConsentRequest.objects.exclude(
                        Exists(
                            UserTermsAndConditionsConsent.objects.filter(
                                consent_request_id=OuterRef("pk"),
                                user=request.user,
                            )
                        )
                    )
                    .exclude(activate_at__gt=now)
                    .exclude(deactivate_at__lt=now)
                    .exclude(in_administration_portal=False)
                    .order_by("activate_at", "id")
                    .first()
                )
                if pending_request is not None:
                    return HttpResponseRedirect(
                        reverse(
                            "authentication:terms_and_conditions",
                            kwargs={
                                "consent_request_id": pending_request.id,
                            },
                        )
                        + "?next="
                        + request.path
                    )
        return self.get_response(request)
