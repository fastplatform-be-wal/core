import logging
from functools import partial

from opentelemetry import trace
from gql import gql
from pydantic.types import constr
from shapely.geometry import shape


from app.api.types.computed_constraint import ComputedConstraint
from app.api.lib.gis import Projection, add_crs
from app.settings import config
from app.db.graphql_clients import fastplatform
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError

# Log
logger = logging.getLogger(__name__)

CONSTRAINT_TYPE = "protected_site"

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class ComputedConstraintsProtectedSiteResolver:
    async def resolve(self, plot_id: int):
        """
        For a given plot, compute its 'protected_site' constraints.

        Args:
            plot_id (int): the id of the plot whose constraints of type
            'protected_site' are calculated here.

        Raises:
            GraphQLHoldingPlotNotFoundError: No plot associated with the id provided.
            GraphQLUnexpectedDataError: The graphql operation could not be performed successfully.

        Returns:
            affected_rows
        """
        logger.info("protected_site constraints: resolve()")
        try:
            constraints = await self.get_plot_protected_site_constraints(plot_id)

            if not constraints == []:
                logger.info("constraints already computed for plot_id %s", plot_id)
                return constraints

            logger.info("Computing protected_site constraints for plot_id %s", plot_id)

            plot_details = await self.get_plot_details(plot_id)

            constraints = await self.compute_protected_site_constraints(plot_details)

            if constraints == []:
                return None

            await self.insert_protected_site_constraints(constraints)

            return await self.get_plot_protected_site_constraints(plot_id)

        except GraphQLHoldingPlotNotFoundError as ex:
            raise GraphQLHoldingPlotNotFoundError() from ex

        except Exception as ex:
            logger.exception("protected_site_constraints error")
            raise GraphQLUnexpectedDataError() from ex

    async def get_plot_protected_site_constraints(self, plot_id: int):
        logger.info("protected_site constraints: get_plot_protected_site_constraints()")

        with tracer().start_as_current_span("get_constraint") as span:
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_plot_constraints_of_type.graphql"
            ).read_text()

            variables = {"plot_id": plot_id, "constraint_type": CONSTRAINT_TYPE}

            response = await fastplatform.execute(gql(query), variables)

            constraints = response["constraint"]
            if constraints == []:
                logger.info("No protected_site constraint found for plot: %s", plot_id)
                return []

            constraints = [
                ComputedConstraint(
                    id=int(constraint["id"]),
                    name=constraint["name"],
                    description=constraint["description"],
                    plot_id=constraint["plot_id"],
                )
                for constraint in constraints
            ]

            if len(constraints) == 1 and constraints[0].description == None:
                constraints = None

            return constraints

    async def get_plot_details(self, plot_id: int):
        logger.info("protected_site constraints: get_plot_details()")

        with tracer().start_as_current_span("get_plot_details") as span:
            query = (
                config.API_DIR / "resolvers" / "graphql" / "query_plot_by_pk.graphql"
            ).read_text()
            variables = {"plot_id": plot_id}

            response = await fastplatform.execute(gql(query), variables)

            plot_details = response["plot_by_pk"]

            if plot_details == None or plot_details == {}:
                logger.error(
                    "No plot found for id: %s. They will now be computed.", plot_id
                )
                raise GraphQLHoldingPlotNotFoundError()

            return plot_details

    async def compute_protected_site_constraints(self, plot_details):
        logger.info("protected_site constraints: compute_protected_site_constraints()")
        plot_id = int(plot_details["id"])
        geometry = shape(plot_details["geometry"])

        with tracer().start_as_current_span("get_intersecting_protected_sites") as span:

            # Query the protected sites that intersect the plot geometry
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_external__protected_site_intersect_plot.graphql"
            ).read_text()

            variables = {
                "geometry": add_crs(geometry.__geo_interface__, config.EPSG_SRID_ETRS89)
            }

            response = await fastplatform.execute(gql(query), variables)

            protected_sites = response["external__protected_site"]

            span.add_event("{} protected sites found".format(len(protected_sites)))

        with tracer().start_as_current_span(
            "compute_intersections_with_protected_sites"
        ) as span:

            def _constraint_from_protected_site(target, target_area, site):
                """Convert a protected sites dict as returned by GraphQL
                to a FaST constraint object
                """

                geometry_site_projected = Projection.etrs89_to_etrs89_laea(
                    shape(site["geometry"])
                )

                intersection = geometry_site_projected.intersection(target)
                intersection_area = intersection.area

                return {
                    "name": "protected_site",
                    "plot_id": plot_id,
                    "description": {
                        "protected_site_id": site["id"],
                        "protected_site_name": site["site_name"],
                        "plot_pct_in_protected_site": (intersection_area / target_area),
                        "plot_area_in_protected_site": intersection_area,
                    },
                }

            geometry_projected = Projection.etrs89_to_etrs89_laea(geometry)

            constraints = map(
                partial(
                    _constraint_from_protected_site,
                    geometry_projected,
                    geometry_projected.area,
                ),
                protected_sites,
            )
            constraints = list(constraints)

            # This `empty constraint` acknowledges that computation took place but turned out nothing.
            if constraints == []:
                constraints = [
                    {
                        "name": CONSTRAINT_TYPE,
                        "plot_id": plot_id,
                        # no description json node set
                    }
                ]

            return constraints

    async def insert_protected_site_constraints(self, constraints):
        logger.info("protected_site constraints: insert_protected_site_constraints()")

        with tracer().start_as_current_span("insert_constraints"):
            # Build the query to insert constraints
            mutation = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "mutation_insert_constraint.graphql"
            ).read_text()
            variables = {"objects": constraints}

            response = await fastplatform.execute(gql(mutation), variables)

        return response
