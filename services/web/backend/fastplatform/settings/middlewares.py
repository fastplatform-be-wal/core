from .debug import DEBUG, DEBUG_TOOLBAR

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "user_sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "axes.middleware.AxesMiddleware",
    "fastplatform.middlewares.graphql.InjectGraphQLClientsMiddleware",
    "oidc_provider.middleware.SessionManagementMiddleware",
    "authentication.middlewares.TermsAndConditionsConsentRequestMiddleware",
]

if DEBUG:
    MIDDLEWARE += ["corsheaders.middleware.CorsMiddleware"]

if DEBUG and DEBUG_TOOLBAR:
    MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE
