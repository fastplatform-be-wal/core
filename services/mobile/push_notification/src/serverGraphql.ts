import { GraphQLServer as gqlServer, Options } from 'graphql-yoga'
import resolvers from './graphql/resolvers'

export class GraphQLServer {
  server: gqlServer
  constructor () {
    this.server = new gqlServer({
      typeDefs: __dirname + '/graphql/schema.graphql',
      resolvers: resolvers,
      context: ({req, request}: any) => {
        // set the headers in the "context" variable. Required for the authorization middleware
        let headers = null
        if (req) {
          headers = req.headers
        } else if (request) {
          headers = request.headers
        } else {
          throw new Error('Unauthorized request');
        }
        return {headers: headers}
      }
    })
  }
  start (options: Options) {
    this.server.start(options).then(() => console.log('GraphQL server is started'))
  }
}
