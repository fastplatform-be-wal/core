from django.conf import settings

from django_json_widget.widgets import JSONEditorWidget


class JSONEditorWidgetUpdated(JSONEditorWidget):
    class Media:
        js = (getattr(settings, "JSON_EDITOR_JS", "dist/jsoneditor.js"),)
        css = {"all": (getattr(settings, "JSON_EDITOR_CSS", "dist/jsoneditor.css"),)}
