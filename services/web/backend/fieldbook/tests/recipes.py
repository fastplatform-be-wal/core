from itertools import cycle

from model_bakery.recipe import Recipe, foreign_key

from fieldbook.models import (
    CustomFertilizer,
    RegisteredFertilizer,
    CustomFertilizerElement,
    RegisteredFertilizerElement,
    ChemicalElement,
    FertilizerType,
    FertilizationPlan,
    PlantSpeciesGroup,
    PlantSpecies,
    PlantVariety,
)

from farm.tests.test_admin import plot_recipe

fertilization_plan_recipe = Recipe(
    FertilizationPlan, parameters=lambda: {"fake": True}, plot=foreign_key(plot_recipe)
)

fertilizer_type_recipe = Recipe(FertilizerType)

chemical_element_recipe = Recipe(ChemicalElement)

custom_fertilizer_recipe = Recipe(CustomFertilizer)

registered_fertilizer_recipe = Recipe(RegisteredFertilizer)

custom_fertilizer_element_recipe = Recipe(
    CustomFertilizerElement,
    percentage=0.1,
    fertilizer=foreign_key(custom_fertilizer_recipe),
    chemical_element=foreign_key(chemical_element_recipe),
)

registered_fertilizer_element_recipe = Recipe(
    RegisteredFertilizerElement,
    percentage=0.1,
    fertilizer=foreign_key(registered_fertilizer_recipe),
    chemical_element=foreign_key(chemical_element_recipe),
)

plant_species_group_recipe = Recipe(PlantSpeciesGroup)

plant_species_recipe = Recipe(
    PlantSpecies, group=foreign_key(plant_species_group_recipe)
)

plant_variety_recipe = Recipe(PlantVariety, plant_species=foreign_key(plant_species_recipe))
