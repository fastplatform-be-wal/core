from itertools import cycle

from model_bakery.recipe import Recipe, foreign_key, related

from messaging.models import (
    Broadcast,
    TicketMessage,
    Queue,
    Ticket,
)

from authentication.tests.recipes import group_recipe


broadcast_recipe = Recipe(Broadcast)

queue_recipe = Recipe(Queue, groups=related(group_recipe, group_recipe))

ticket_recipe = Recipe(Ticket, queue=foreign_key(queue_recipe))

ticket_message_recipe = Recipe(
    TicketMessage, body="test body", ticket=foreign_key(ticket_recipe)
)
