from import_export.tmp_storages import CacheStorage


class MemcachedCacheStorage(CacheStorage):
    CACHE_LIFETIME = 60 * 10  # 10 minutes


IMPORT_EXPORT_USE_TRANSACTIONS = False
IMPORT_EXPORT_SKIP_ADMIN_LOG = False
IMPORT_EXPORT_TMP_STORAGE_CLASS = "fastplatform.settings.MemcachedCacheStorage"
IMPORT_EXPORT_IMPORT_PERMISSION_CODE = "add"
IMPORT_EXPORT_EXPORT_PERMISSION_CODE = "view"
IMPORT_EXPORT_CHUNK_SIZE = 100
