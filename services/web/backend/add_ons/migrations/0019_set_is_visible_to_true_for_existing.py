from django.db import migrations

from add_ons.models import AddOn


def set_existing_add_ons_is_visible_true(apps, schema_editor):
    AddOn.objects.update(is_visible=True)


class Migration(migrations.Migration):

    dependencies = [
        ("add_ons", "0018_auto_20210119_2313"),
    ]

    operations = [
        migrations.RunPython(set_existing_add_ons_is_visible_true),
    ]
