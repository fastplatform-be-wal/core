from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [("messaging", "0021_generate_fk_cascade_messaging")]

    operations = [
        migrations.RunSQL(
            sql="INSERT INTO queue(id, title) VALUES (setval(pg_get_serial_sequence('queue', 'id'), 1), 'Default') ON CONFLICT DO NOTHING;",
        ),
    ]

    operations = [
        migrations.RunSQL(
            sql="UPDATE ticket SET queue_id = 1 WHERE queue_id IS NULL;",
        ),
    ]