import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.header import Header
from email.utils import formataddr, formatdate, make_msgid
from html2text import html2text
import logging

logger = logging.getLogger(__name__)

SMTP_HOST = os.environ.get("SMTP_HOST")
SMTP_PORT = os.environ.get("SMTP_PORT")
SMTP_SENDER = os.environ.get("SMTP_SENDER")
SMTP_LOGIN = os.environ.get("SMTP_LOGIN")
SMTP_PASSWORD = os.environ.get("SMTP_PASSWORD")

SENDER_NAME = "FaST"


def send_email(to_addrs, subject, html_content, bcc=False):
    try:
        if to_addrs and len(to_addrs) > 0:
            _send_email(to_addrs, subject, html_content, bcc)
    except:
        logger.exception(f"Failed to send email {subject} to {','.join(to_addrs)}")


def _send_email(to_addrs, subject, html_content, bcc):
    smtp_server = smtplib.SMTP_SSL(host=SMTP_HOST, port=SMTP_PORT)
    smtp_server.login(SMTP_LOGIN, SMTP_PASSWORD)
    message = _build_message(SMTP_SENDER, to_addrs, subject, html_content, bcc)
    smtp_server.sendmail(SMTP_SENDER, to_addrs, message.as_string())
    smtp_server.quit()
    logger.info(f"Sent email {subject} to {','.join(to_addrs)}")


def _build_message(from_addr, to_addrs, subject, html_content, bcc):
    message = MIMEMultipart("alternative")
    message["Subject"] = subject
    message["From"] = formataddr((str(Header(SENDER_NAME, "utf-8")), from_addr))
    to = "BCC" if bcc else "To"
    message[to] = ",".join(to_addrs)
    message["Date"] = formatdate(localtime=True)
    message["Message-ID"] = make_msgid(domain="fastplatform.com")

    text_content = html2text(html_content)
    message.attach(MIMEText(text_content, "plain"))
    message.attach(MIMEText(html_content, "html"))

    """  Do not send single image as it lowers the spam score
    logo_path = os.path.join(os.path.dirname(__file__), "images/fast-logo.png")
    with open(logo_path, "rb") as img:
        msg_image = MIMEImage(img.read())
    msg_image.add_header("Content-ID", "<logo>")
    message.attach(msg_image) """

    return message
