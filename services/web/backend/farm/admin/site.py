from django.conf import settings
from django.contrib import admin
from django.db import models
from django.contrib.gis.db.models import Extent, Count
from django.utils.translation import gettext_lazy as _

from import_export.admin import ExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin
from utils.widgets.map_widget import MapWidget

from farm.models import (
    Site,
    Plot,
)
from farm.resources import SiteResource


class PlotInSiteInline(admin.TabularInline):
    model = Plot
    extra = 0
    fields = [
        "_link",
        "name",
        "_area",
        "_plant_species",
        "_number_of_fertilization_plans",
    ]
    readonly_fields = [
        "_link",
        "_number_of_fertilization_plans",
        "_plant_species",
        "_area",
    ]

    def has_add_permission(self, request, obj):
        return False

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .select_related("site")
            .select_related("site__holding_campaign")
            .select_related("site__holding_campaign__holding")
            .prefetch_related("plot_plant_varieties__plant_variety__plant_species")
            .annotate(
                number_of_fertilization_plans=Count("fertilization_plans"),
                area=models.expressions.RawSQL(
                    "area", [], output_field=models.FloatField()
                ),
            )
        )

    def _link(self, obj):
        if obj:
            return obj.get_admin_link()
        return ""

    _link.short_description = ""

    def _number_of_fertilization_plans(self, obj):
        return obj.number_of_fertilization_plans

    _number_of_fertilization_plans.short_description = _(
        "number of fertilization plans"
    )

    def _plant_species(self, obj):
        return ", ".join(
            [
                ppv.plant_variety.plant_species.name
                for ppv in obj.plot_plant_varieties.all()
            ]
        )

    _plant_species.short_description = _("plant species")

    def _area(self, obj):
        if obj.area is not None and obj.area > 0:
            if obj.area < 10000:
                return f"{round(obj.area, 0)} m2"
            return f"{round(obj.area / 10000, 2)} ha"
        return "-"

    _area.short_description = _("area")


class SiteAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = SiteResource
    search_fields = ("id", "authority_id", "name")
    list_filter = ("holding_campaign__campaign",)
    ordering = ("name",)
    inlines = [PlotInSiteInline]
    list_display_links = ("id", "name")

    def get_list_display(self, request):
        list_display = [
            "id",
            "name",
            "holding_campaign",
            "_area",
            "number_of_plots",
            "number_of_plots_with_fertilization_plan",
        ]
        return list_display

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ["id", "authority_id", "name", "holding_campaign"]}]
        ]
        if obj:
            fieldsets += [
                [
                    _("Statistics"),
                    {
                        "fields": [
                            "_area",
                            "number_of_plots",
                            "number_of_plots_with_fertilization_plan",
                        ]
                    },
                ]
            ]
        return fieldsets

    def get_readonly_fields(self, request, obj):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields += (
            "id",
            "_area",
            "number_of_plots",
            "number_of_plots_with_fertilization_plan",
        )
        return readonly_fields

    autocomplete_fields = ["holding_campaign"]

    def get_queryset(self, request):
        queryset = (
            super()
            .get_queryset(request)
            .prefetch_related("plots")
        )
        return queryset

    def _area(self, obj):
        if obj.area is not None and obj.area > 0:
            if obj.area < 10000:
                return f"{round(obj.area, 0)} m2"
            return f"{round(obj.area / 10000, 2)} ha"
        return "-"

    _area.short_description = _("area")

    # Map parameters
    # --------------
    static_map = True

    def get_map_extent(self, obj):
        return Plot.objects.filter(site=obj).aggregate(Extent("geometry"))[
            "geometry__extent"
        ]

    def get_holding_campaign_id(self, obj):
        if obj:
            return obj.holding_campaign.id
        return None

    class Media(MapWidget.Media):
        pass


admin_site.register(Site, SiteAdmin)
