from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [("authentication", "0032_set_db_comments_authentication")]

    operations = [
        migrations.RunSQL(
            sql="ALTER TABLE user_iacs_consent ALTER COLUMN consent_received_at SET DEFAULT now();",
        ),
    ]
