import { Request, Response } from 'express'
import { pushNotifications } from '../config/push-notification'
import { Data } from 'node-pushnotifications'
import { FastPlatformKeySeparator, getMatchingKeys } from './database'
import { APP_BUNDLE_ID_IOS } from '../config/env'

export type SendNotificationRequestBody = {
  user_ids: string[],
  message_payload: Data
}

type SendNotificationResponseDataDetailsMessageError = {
  name: string,
  message: string,
  stack?: string
}
type SendNotificationResponseDataDetailsMessage = {
  regId: string,
  originalRegId?: string,
  messageId?: string,
  error?: SendNotificationResponseDataDetailsMessageError | null
  errorMsg?: string
}
type SendNotificationResponseDataDetails = {
  method: string,
  success: number,
  failure: number,
  message: SendNotificationResponseDataDetailsMessage[],
  multicastId?: string[]
}
type SendNotificationResponseData = {
  details: SendNotificationResponseDataDetails[],
  usersNotFound: string[]
}
type SendNotificationResponse = {
  status: string
  data: SendNotificationResponseData | null,
  error?: {
    name: string,
    message: string
  },
  httpCode: number
}

function _getUsersDevices(userIds: string[]): Promise<{usersNotFound: string[], devices: string[]}> {
  let devices: string[] = []
  let usersNotFound: string[] = []
  let promises: Promise<any>[] = []
  userIds.forEach(userId => {
    const match = `${userId}${FastPlatformKeySeparator}*`
    promises.push(getMatchingKeys(match)
      .then(keys => {
        const userDevices = keys.map(key => key.substring(key.indexOf(FastPlatformKeySeparator) + FastPlatformKeySeparator.length))
        if (userDevices.length > 0) {
          userDevices.forEach(userDevice => {
            if (!devices.includes(userDevice)) {
              devices.push(userDevice)
            }
          })
        } else {
          usersNotFound.push(userId)
        }
      }).catch(() => {
        usersNotFound.push(userId)
      })
    )
  })
  return new Promise((resolve, reject) => { 
    Promise.all(promises)
    .then(() => {
      resolve({
        devices: devices,
        usersNotFound: usersNotFound
      })
    })
    .catch(() => {
      resolve({
        devices: devices,
        usersNotFound: usersNotFound
      })
    })
  })
}

function _sendNotification({ user_ids, message_payload }: SendNotificationRequestBody): Promise<SendNotificationResponse> {
  return new Promise(async (resolve, reject) => {
    const { devices, usersNotFound } = await _getUsersDevices(user_ids)
    if (!Object.keys(message_payload).includes('icon')) {
      message_payload.icon = 'fast-farmer-app'
    }
    if (!Object.keys(message_payload).includes('topic')) {
      message_payload.topic = APP_BUNDLE_ID_IOS
    }
    pushNotifications.send(devices, message_payload)
      .then(res => {
        resolve({
          status: 'SENT',
          data: {
            details: res,
            usersNotFound: usersNotFound
          },
          httpCode: 200
        })
      })
      .catch(err => {
        console.error(`[SEND_NOTIFICATION] Failed to send notification (title: ${message_payload.title}`, err)
        resolve({
          status: 'ERROR',
          data: null,
          error: {
            name: err.name,
            message: err.message
          },
          httpCode: 500
        })
      })
  })
}

export async function sendNotification(request: Request, response: Response) {
  const { user_ids, message_payload }: SendNotificationRequestBody = request.body.input || request.body
  if (user_ids == null || message_payload == null) {
    response.status(400).json({
      status: "ERROR",
      error: {
        name: 'INVALID_PARAMETER',
        message: 'Missing parameter(s) in the request body.'
      }
    })
  } else {
    let res = await _sendNotification({user_ids, message_payload})
    const httpCode = res.httpCode
    delete res['httpCode']
    response.status(httpCode).json(res)
  }
}

export async function graphqlSendNotification({ user_ids, message_payload }: SendNotificationRequestBody): Promise<SendNotificationResponse> {
  const res = await _sendNotification({user_ids, message_payload})
  return res
}