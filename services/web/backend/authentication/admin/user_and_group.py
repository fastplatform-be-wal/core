from django.contrib import admin
from django.contrib.auth.admin import (
    UserAdmin as DjangoUserAdmin,
    GroupAdmin as DjangoGroupAdmin,
)
from django.contrib.auth.models import Permission
from django.contrib.postgres.fields import JSONField
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from django_object_actions import DjangoObjectActions
from import_export.admin import ExportMixin, ImportExportMixin

from fastplatform.site import admin_site

from utils.admin import CommonModelAdmin, TruncateDeleteObjectsMixin
from utils.widgets.tabular_permissions.widget import TabularPermissionsWidget
from utils.widgets.json_editor import JSONEditorWidget

from authentication.models import (
    User,
    Country,
    Region,
    Language,
    Group,
    TermsAndConditionsConsentRequest,
    UserTermsAndConditionsConsent,
    UserIACSConsent,
)
from add_ons.models import AddOn
from authentication.resources import (
    GroupResource,
    UserResource,
    CountryResource,
    RegionResource,
    LanguageResource,
    TermsAndConditionsConsentRequestResource,
    UserTermsAndConditionsConsentResource,
    UserIACSConsentResource,
)
from authentication.actions import (
    insert_demo_holding,
    sync_holdings,
    remove_user_password,
)

from farm.models import UserRelatedParty


class UserRelatedPartyInline(admin.TabularInline):
    fields = ("holding", "role")
    model = UserRelatedParty
    extra = 0
    ordering = ("holding__name",)
    verbose_name = _("farm membership")
    verbose_name_plural = _("farm memberships")
    autocomplete_fields = ("holding",)

    def get_queryset(self, request):
        return super().get_queryset(request).select_related("holding")


class UserTermsAndConditionsConsentInline(admin.TabularInline):
    model = UserTermsAndConditionsConsent
    extra = 0
    fields = ("consent_request", "consent_received_at")
    readonly_fields = ("consent_received_at",)
    verbose_name = _("consent granted")
    verbose_name_plural = _("consents granted")

    def has_add_permission(self, request, obj):
        return False


class UserAdmin(
    ImportExportMixin, DjangoObjectActions, TruncateDeleteObjectsMixin, DjangoUserAdmin
):
    model = User
    hide_from_dashboard = False

    # List view settings
    list_display = (
        "username",
        "name",
        "last_login",
        "is_active",
        "is_staff",
        "is_superuser",
        "_has_usable_password",
    )
    list_filter = ("is_active", "is_staff", "is_superuser")
    list_select_related = ()

    def _has_usable_password(self, obj):
        return obj.has_usable_password()

    _has_usable_password.boolean = True
    _has_usable_password.short_description = _("Password access enabled")

    def get_ordering(self, request):
        if settings.ENABLE_MULTIPLE_REGIONS:
            return ("region", "username")
        return ("username",)

    def get_autocomplete_fields(self, request, obj=None):
        autocomplete_fields = ["groups", "user_permissions"]
        if settings.ENABLE_MULTIPLE_REGIONS:
            autocomplete_fields += ["region"]
        if settings.ENABLE_USER_PREFERRED_LANGUAGES:
            autocomplete_fields += ["preferred_language"]
        return autocomplete_fields

    search_fields = ("email", "username", "first_name", "last_name", "name")

    def get_fieldsets(self, request, obj=None):
        if obj:
            fieldsets = [
                (None, {"fields": ("email", "username", "password")}),
                (
                    _("User Information"),
                    {
                        "fields": (
                            "first_name",
                            "last_name",
                            "name",
                            "picture",
                        )
                    },
                ),
            ]

            if settings.ENABLE_MULTIPLE_REGIONS:
                fieldsets += [(_("Region Information"), {"fields": ("region",)})]

            if settings.ENABLE_USER_PREFERRED_LANGUAGES:
                fieldsets += [
                    (_("Language Information"), {"fields": ("preferred_language",)})
                ]

            fieldsets += [
                (
                    _("Permissions"),
                    {
                        "fields": (
                            "is_active",
                            "is_staff",
                            "is_superuser",
                            "groups",
                            "user_permissions",
                        )
                    },
                )
            ]
            if request.user.is_superuser:
                fieldsets += [
                    (
                        _("Miscellaneous"),
                        {"fields": ("additional_data",), "classes": ("collapse",)},
                    )
                ]
        else:
            fieldsets = (
                (
                    None,
                    {
                        "classes": ("wide",),
                        "fields": (
                            "username",
                            "password1",
                            "password2",
                        ),
                    },
                ),
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = set()

        if obj:
            # Cannot edit the username once the user has been created
            readonly_fields |= {
                "username",
            }

        # Only superusers can:
        # - set others are superusers
        # - change individual permissions (the default is to use groups instead)
        # - change usernames (this can break things and should not be done)
        if not request.user.is_superuser:
            readonly_fields |= {"is_superuser", "user_permissions"}

        # A non-superuser user cannot change his own permissions
        if not request.user.is_superuser and obj is not None and obj == request.user:
            readonly_fields |= {
                "is_staff",
                "is_superuser",
                "groups",
                "user_permissions",
            }

        return readonly_fields

    formfield_overrides = {
        JSONField: {"widget": JSONEditorWidget(width="50%", height="200px")}
    }

    def get_inlines(self, request, obj=None):
        # Do not display inlines on the 'add' form (when obj is None)
        if obj:
            return [UserRelatedPartyInline, UserTermsAndConditionsConsentInline]
        return []

    resource_class = UserResource

    def get_queryset(self, request):
        queryset = super().get_queryset(request)

        if settings.ENABLE_MULTIPLE_REGIONS:
            queryset = queryset.select_related("region")

        if settings.ENABLE_USER_PREFERRED_LANGUAGES:
            queryset = queryset.select_related("preferred_language")

        queryset = (
            queryset.prefetch_related("groups")
            .prefetch_related("user_related_parties")
            .prefetch_related("user_related_parties__holding")
            .prefetch_related("user_related_parties__user")
        )
        return queryset

    # Actions
    # -------

    # Action to manually remove the password of a given user
    def remove_user_password(self, request, obj):
        if (
            request.user.has_perm("authentication.change_user")
            or request.user.is_superuser
        ):
            remove_user_password(request, obj)

    remove_user_password.label = _("Remove this user's password")
    remove_user_password.short_description = _(
        "Remove this user's password to prevent him/her from logging in using a FaST password"
    )

    # Action to manually insert the demo farm of a given user
    def insert_demo_holding_action(self, request, obj):
        if (
            request.user.has_perm("farm.insert_demo_holding")
            or request.user.is_superuser
        ):
            insert_demo_holding(request, obj)

    insert_demo_holding_action.label = _("Create / update a demo farm")
    insert_demo_holding_action.short_description = _(
        "This will create (or update) a demo farm for the current user."
    )

    # Action to manually synchronize the farms of a given user
    def sync_holdings_action(self, request, obj):
        if request.user.has_perm("farm.sync_holdings") or request.user.is_superuser:
            sync_holdings(request, obj)

    sync_holdings_action.label = _("Synchronize farms with IACS")
    sync_holdings_action.short_description = _(
        "This will download the access rights of the user from the IACS system"
    )

    # Fix for this bug: https://github.com/crccheck/django-object-actions/issues/25
    change_actions = [
        "remove_user_password",
        "insert_demo_holding_action",
        "sync_holdings_action",
    ]

    def get_change_actions(self, request, object_id, form_url):
        actions = []
        # Only show the 'remove user password' action if the user has the permission to
        # edit users
        if (
            request.user.has_perm("authentication.change_user")
            or request.user.is_superuser
        ):
            actions += ["remove_user_password"]
        # Only show the 'insert demo holding' action if the user has the right
        # permission
        if settings.ENABLE_INSERT_DEMO_HOLDING_FROM_ADMIN and (
            request.user.has_perm("farm.insert_demo_holding")
            or request.user.is_superuser
        ):
            actions += ["insert_demo_holding_action"]
        if settings.ENABLE_SYNC_HOLDINGS_FROM_ADMIN and (
            request.user.has_perm("farm.sync_holdings") or request.user.is_superuser
        ):
            actions += ["sync_holdings_action"]
        return actions


admin_site.register(User, UserAdmin)


class LanguageAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    list_display = ("iso_code", "name")
    list_filter = ("iso_code",)
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "iso_code",
                    "name",
                )
            },
        ),
    )
    add_fieldsets = (
        None,
        {
            "classes": ("wide",),
            "fields": (
                "iso_code",
                "name",
            ),
        },
    )
    search_fields = (
        "iso_code",
        "name",
    )
    resource_class = LanguageResource


if settings.ENABLE_USER_PREFERRED_LANGUAGES:
    admin_site.register(Language, LanguageAdmin)


class CountryAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    list_display = ("iso_code", "name")
    search_fields = (
        "iso_code",
        "name",
    )
    resource_class = CountryResource

    def get_fields(self, request, obj=None):
        fields = ["iso_code", "name"]
        if settings.ENABLE_USER_PREFERRED_LANGUAGES:
            fields += ["default_language", "languages"]
        return fields


if settings.ENABLE_MULTIPLE_REGIONS:
    admin_site.register(Country, CountryAdmin)


class RegionAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    list_display = ("id", "iso_code", "name")
    search_fields = (
        "iso_code",
        "name",
    )
    resource_class = RegionResource

    def get_fields(self, request, obj=None):
        fields = ["id", "iso_code", "name", "country"]
        if settings.ENABLE_USER_PREFERRED_LANGUAGES:
            fields += ["default_language", "languages"]
        return fields


if settings.ENABLE_MULTIPLE_REGIONS:
    admin_site.register(Region, RegionAdmin)


class GroupUserInline(admin.TabularInline):
    model = User.groups.through
    extra = 0
    ordering = ("user__name",)
    raw_id_fields = ("user",)
    verbose_name = _("user")
    verbose_name_plural = _("users")


class GroupAddOnInline(admin.TabularInline):
    model = AddOn.groups.through
    extra = 0
    ordering = ("addon__name",)
    autocomplete_fields = ("addon",)
    verbose_name = _("add-on")
    verbose_name_plural = _("add-ons")


class GroupAdmin(ImportExportMixin, DjangoGroupAdmin):
    hide_from_dashboard = False
    list_display = ("name", "_number_of_users", "_number_of_add_ons")
    readonly_fields = ("_number_of_users", "_number_of_add_ons")
    search_fields = ("name",)
    filter_horizontal = ("permissions",)
    fields = ("name", "permissions")
    resource_class = GroupResource

    inlines = [GroupUserInline, GroupAddOnInline]

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        field = super().formfield_for_manytomany(db_field, request, **kwargs)
        if db_field.name == "permissions":
            field.widget = TabularPermissionsWidget(
                db_field.verbose_name,
                db_field.name in self.filter_vertical,
                "permissions",
            )
            field.help_text = ""
        return field

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related("permissions")
            .prefetch_related("users")
            .annotate(number_of_users=models.Count("users"))
            .annotate(number_of_add_ons=models.Count("add_ons"))
        )

    def _number_of_users(self, obj):
        return obj.number_of_users

    _number_of_users.short_description = _("Number of users")

    def _number_of_add_ons(self, obj):
        return obj.number_of_add_ons

    _number_of_users.short_description = _("Number of users")

    def has_add_permission(self, request):
        return request.user.is_superuser or request.user.has_perm("auth.add_group")

    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser or request.user.has_perm("auth.view_group")

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser or request.user.has_perm("auth.change_group")

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser or request.user.has_perm("auth.delete_group")


admin_site.register(Group, GroupAdmin)


class PermissionAdmin(CommonModelAdmin):
    hide_from_dashboard = True
    short_description = _(
        "List of all the available permissions within the management portal."
    )
    search_fields = ("name",)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset

    def has_add_permission(self, *args, **kwargs):
        return False


admin_site.register(Permission, PermissionAdmin)


class TermsAndConditionsConsentRequestAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = TermsAndConditionsConsentRequestResource
    list_display = [
        "title",
        "activate_at",
        "deactivate_at",
        "in_farmer_app",
        "in_administration_portal",
    ]

    fieldsets = [
        (None, {"fields": ["title"]}),
        (
            _("Activation"),
            {
                "fields": [
                    "activate_at",
                    "deactivate_at",
                    "in_farmer_app",
                    "in_administration_portal",
                ],
                "description": _(
                    "This section determines where and when this request will be visible to users"
                ),
            },
        ),
        (_("Content"), {"fields": ("short_description", "url", "html_content")}),
        (_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}),
    ]


admin_site.register(
    TermsAndConditionsConsentRequest, TermsAndConditionsConsentRequestAdmin
)


class UserTermsAndConditionsConsentAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = UserTermsAndConditionsConsentResource

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False


admin_site.register(UserTermsAndConditionsConsent, UserTermsAndConditionsConsentAdmin)


class UserIACSConsentAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = UserIACSConsentResource

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False


admin_site.register(UserIACSConsent, UserIACSConsentAdmin)
