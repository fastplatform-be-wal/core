from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [("messaging", "0018_auto_20201130_1840")]

    operations = [
        migrations.RunSQL(
            sql="ALTER TABLE ticket ALTER COLUMN created_at SET DEFAULT now();",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE ticket_message ALTER COLUMN created_at SET DEFAULT now();",
        )
    ]
