import os

VECTOR_TILES_FASTPLATFORM_URL = os.environ.get(
    "VECTOR_TILES_FASTPLATFORM_URL", "https://localhost.fastplatform.eu:43000"
)
VECTOR_TILES_EXTERNAL_URL = os.environ.get(
    "VECTOR_TILES_EXTERNAL_URL", "https://localhost.fastplatform.eu:43001"
)