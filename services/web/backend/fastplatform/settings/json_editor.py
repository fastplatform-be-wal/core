import os

JSON_EDITOR_JS = os.environ.get(
    "JSON_EDITOR_JS",
    "https://cdnjs.cloudflare.com/ajax/libs/jsoneditor/9.0.3/jsoneditor.js",
)
JSON_EDITOR_CSS = os.environ.get(
    "JSON_EDITOR_CSS",
    "https://cdnjs.cloudflare.com/ajax/libs/jsoneditor/9.0.3/jsoneditor.css",
)
