# Generated by Django 3.0.7 on 2020-12-14 15:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('external', '0010_auto_20201214_1554'),
    ]

    operations = [
        migrations.RenameField(
            model_name='agriplot',
            old_name='plant_species',
            new_name='plant_species_id',
        ),
        migrations.RenameField(
            model_name='agriplot',
            old_name='plant_variety',
            new_name='plant_variety_id',
        ),
    ]
