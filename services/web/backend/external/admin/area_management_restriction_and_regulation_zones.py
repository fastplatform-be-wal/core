from django.utils.translation import gettext_lazy as _
from django.conf import settings

from import_export.admin import ImportExportMixin
from django_object_actions import DjangoObjectActions

from fastplatform.site import admin_site

from external.models import (
    ManagementRestrictionOrRegulationZone,
    ManagementRestrictionOrRegulationZoneVersion,
    ZoneTypeCode,
    SpecialisedZoneTypeCode,
    EnvironmentalDomain,
)
from external.resources import (
    ZoneTypeCodeResource,
    EnvironmentalDomainResource,
    SpecialisedZoneTypeCodeResource,
    ManagementRestrictionOrRegulationZoneVersionResource,
    ManagementRestrictionOrRegulationZoneResource,
)

from external.admin.version import VersionAdmin, VersionActionsMixin
from utils.admin import CommonModelAdmin


class SpecialisedZoneTypeCodeAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = SpecialisedZoneTypeCodeResource
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class ZoneTypeCodeAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = ZoneTypeCodeResource
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class EnvironmentalDomainAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = EnvironmentalDomainResource
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class ManagementRestrictionOrRegulationZoneVersionAdmin(VersionAdmin):
    hide_from_dashboard = True
    model = ManagementRestrictionOrRegulationZoneVersion
    resource_class = ManagementRestrictionOrRegulationZoneVersionResource


class ManagementRestrictionOrRegulationZoneAdmin(
    VersionActionsMixin, DjangoObjectActions, CommonModelAdmin
):
    model = ManagementRestrictionOrRegulationZone
    version_model = ManagementRestrictionOrRegulationZoneVersion
    resource_class = ManagementRestrictionOrRegulationZoneResource

    autocomplete_fields = (
        "zone_type",
        "specialized_zone_type",
        "environmental_domain",
    )

    # Override the model verbose name for clarity
    verbose_name = _("Nitrate Vulnerable Zone")
    verbose_name_plural = _("Nitrate Vulnerable Zones")

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["version"]
        return []

    def get_queryset(self, request):
        return super().get_queryset(request).select_related("version")

    list_display = (
        "authority_id",
        "version",
        "name",
    )
    list_display_links = (
        "authority_id",
        "name",
    )
    list_select_related = ("version",)
    list_filter = ("version__is_active",)

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "authority_id",
                    "thematic_id",
                    "version",
                    "name",
                )
            },
        ),
        (
            _("Metadata"),
            {
                "fields": (
                    "environmental_domain",
                    "zone_type",
                    "specialized_zone_type",
                )
            },
        ),
        (
            _("Lifespan"),
            {
                "fields": (
                    ("begin_lifespan_version", "end_lifespan_version"),
                    # "designation_period",
                )
            },
        ),
        (
            _("Geographic information"),
            {"fields": ("geometry",)},
        ),
    )

    # Map parameters
    # --------------
    geometry_fields = ["geometry"]


if settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE:
    admin_site.register(EnvironmentalDomain, EnvironmentalDomainAdmin)
    admin_site.register(SpecialisedZoneTypeCode, SpecialisedZoneTypeCodeAdmin)
    admin_site.register(ZoneTypeCode, ZoneTypeCodeAdmin)
    admin_site.register(
        ManagementRestrictionOrRegulationZoneVersion,
        ManagementRestrictionOrRegulationZoneVersionAdmin,
    )
    admin_site.register(
        ManagementRestrictionOrRegulationZone,
        ManagementRestrictionOrRegulationZoneAdmin,
    )
