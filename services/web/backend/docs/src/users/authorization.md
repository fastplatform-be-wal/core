# Authorization

Authorization is the mechanism by which a certain set of "rights" or "permissions" are assigned to a user. It determines what a user can see and do within FaST.

FaST has two different policies for managing permissions, depending on which interface the user is connected to:

- authorizations when connected to the **Administration Portal**
- authorizations when connected to the **FaST mobile or web application**

> Note that the _same_ user can connect to both interfaces: you do _not_ need to have two different users, one for connecting to the **Administration Portal** and one for connecting to the **FaST mobile application**; the same user can be used for both.

```plantuml
skinparam defaultFontSize 12

actor "User" as user

usecase "Mobile app" as app
usecase "Administration\nPortal" as portal

user --> app
user --> portal

card portal_perms [
  Can see the data
  of all users
  ---
  Only for the objects
  and actions in his/her
  permissions
]

card portal_perms_superuser [
  Can see the data
  of all users
  ---
  For all objects
  and actions
]

card portal_perms_not_staff [
  Can see nothing
  ---
  User will be refused
  access
]

card mobile_perms [
  Can see **only the**
  **farms he/she is**
  **member of**
  ---
  For all the objects
  and actions in the
  mobile app
]

portal --> portal_perms : User is\n**staff**
portal --> portal_perms_superuser : User is\n**superuser**
portal --> portal_perms_not_staff : User is\n not **staff**\nnor **superuser**
app --> mobile_perms

caption
The same user can connect to both the app and the Administration Portal,
but with different sets of rights
endcaption
```

## Authorizations in the Administration Portal

To get access to the **Administration Portal**, a user must have the **staff** status. You can check (and possibly edit) which users have this status on the [user profile pages](/admin/authentication/user/).

<img src="/services/web/backend/docs/static/docs/img/users/staff_status.png" title="Staff status" class="img-docs" height="300"/>

Users that are not **staff** cannot log into the **Administration Portal**.

Only members of a public institution should likely have the **staff** status, since we only want them to be granted access to the **Administration Portal**.

Once a user has the **staff** status and is logged in to the **Administration Portal**, what the user can see is determined by the **permissions** that have been set up for him/her. These **permissions** are derived from the **Groups** that this user belongs to, as well as from individual **permissions** that have been granted to him/her. Authorizations do not depend on the user's membership to farms, but instead depend on an activity to be performed, or objects to be viewed.

### Permissions

The activity-based permissions of the Admin Portal are built according to the following semantics. A permission is the combination of:

- an action from `ADD`, `VIEW`, `CHANGE`, `DELETE`
- a FaST object

For example, a user could have the following permissions:

- `ADD` Ticket
- `ADD` Ticket Message
- `VIEW` Ticket
- `VIEW` Ticket Message
- `CHANGE` Ticket

With these permissions, the user **can**:
- view all tickets and their conversation, 
- create a new ticket or a new message in a conversation, 
- or change the ticket's metadata. 

With these permissions, the user **cannot**:
- delete any ticket or item of conversation,
- or change an item of a conversation.

The permissions system in the **Administration Portal** is _additive_: this means that users initially have zero permissions, and permissions are then _added_ as necessary, one-by-one, either through group membership, or through individual permissions (see below).

### Using groups to set permissions

Even though a user can be attached individual permissions if necessary, it is better to define _roles_ and attach permissions to these roles. In FaST, the concept of _role_ is captured by the **Group** object. A **Group** is an object that has a set of permissions and a set of members:

- if a user is member of a **Group**, then the user inherits the permissions of this **Group**.
- if a user is a member of several **Groups**, then the user will get as permissions the _union_ of the permissions of all the **Groups** the user is member of.

```plantuml
skinparam defaultFontSize 12

actor User

rectangle view_farm_description [
    **Group 1**
    ---
    //can VIEW Holding//
    //can VIEW Campaign//
    //can VIEW HoldingCampaign//
    //can VIEW Site//
    //can VIEW Plot//
    //can VIEW PlotPlantVariety//
]

User --> view_farm_description : is member\nof

rectangle view_fertilization_plans [
    **Group 2**
    ---
    //can VIEW FertilizationPlan//
    //can VIEW Plot//
]

User --> view_fertilization_plans : is also\nmember of

rectangle final [
    **Resulting user permissions**
    ---
    //can VIEW Holding//
    //can VIEW Campaign//
    //can VIEW HoldingCampaign//
    //can VIEW Site//
    //can VIEW Plot//
    //can VIEW PlotPlantVariety//
    //can VIEW FertilizationPlan//
]

view_farm_description --> final
view_fertilization_plans --> final

caption
A user's **permissions** are determined as
the union of the **groups** this user belongs to
endcaption
```

> Note: permissions manually added to a user will work in the same _additive_ way, in combination with group permissions.

The **groups** and their **permissions** are defined on the [Groups page](/admin/authentication/group/). You can add or remove **groups**. There is no limit to the number of **groups** you can create, nor are there limits to the number of groups any given user can belong to. Once a **group** is created, you can add **permissions** to the group, as well as members. As soon as a user is member of a **group**, he/she inherits the **group**'s **permissions**. Inversely, as soon as a user is removed from a **group**, or as soon as a **group** is deleted, the **group**'s members lose the associated **permissions**, unless it is still provided through membership in another **group**.

<img src="/services/web/backend/docs/static/docs/img/users/group_permissions.png" title="Setting group permissions" class="img-docs" height="400" />

### Manual permissions for specific users

FaST provides a way to add specific permissions one-by-one to a specific user.

This is however discouraged, as it can become difficult to maintain. Using **groups** make for a much easier way to set permissions, and improves their readability and maintainability.

### Superusers

Some users can be set as **superusers**. When a user is **superuser**, _he/she gets all the existing FaST permissions_, without needing any specific permission to be granted to them beforehand. They can basically access everything, and do anything they want. 

<img src="/services/web/backend/docs/static/docs/img/users/superuser.png" title="Superuser" class="img-docs" />

Because when a user is superuser he/she can see and change everything in the portal, this is a very **dangerous** situation. Some system objects of FaST will be exposed to the user, and changing those objects can lead to breaking situations.

Note that only current **superusers** can set another user as **superuser**.

> Superuser accounts should be reserved to one or two technical users. For all other users, only permission-based accounts should ever be used.

### Special permissions

Some extra ad-hoc permissions are defined in FaST, that do not respect the `CRUD` (ADD, VIEW, CHANGE, DELETE) semantics explained above. This is for example the case for the `farm | Can insert/update demo holding for a given user` permission, which allows the user to generate demo farms for other users. These permissions are also available on the **Group** permission configuration page.

### Special case of the ticketing system

The FaST ticketing system has the concept of `Queue` which is a way to categorize incoming tickets by categories. A queue is attached to a certain number of **groups** (this is configurable), and therefore a user can only see the tickets assigned to queues he/she is member of. This behavior _supersedes_ the VIEW permissions on the Ticket and TicketMessage objects.

## Authorizations in the FaST mobile application

At the heart of the permissions system for the mobile application is the concept _"being member of a farm"_.

Once a user is _member_ of a farm, they can view and edit _all the private data of this farm_.

> FaST does not currently support more fine-grained permissions in the farmer mobile application. _"Being member of a farm"_ in FaST is very binary: if you are member of a farm, you can do everything on this farm's data. If you are not member of a farm you cannot do anything, and can't even access the farm's data.

#### Initial farm memberships

The farm memberships of users are initially derived from the regional IACS system. When a user first logs in, he/she is not member of any farm; the memberships are then pulled from the regional IACS APIs and mapped to FaST farm memberships.

#### Administering memberships

The farm memberships can be edited from the **Administration Portal** (either on the page of a [farm](/admin/farm/holding/), or on the page of a [user](/admin/authentication/user/)).

<img src="/services/web/backend/docs/static/docs/img/users/farm_farm_members_2.png" title="Superuser" class="img-docs" />
