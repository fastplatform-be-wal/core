from django.utils.translation import gettext_lazy as _
from django.contrib.admin import StackedInline
from django.utils.html import mark_safe
from django.conf import settings
from django.utils import timezone

from storages.backends.s3boto3 import S3Boto3Storage
from import_export.admin import ExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin

from photos.models import GeoTaggedPhoto
from photos.resources import GeoTaggedPhotoResource


s3boto3storage = S3Boto3Storage()


# Register your models here.
class GeoTaggedPhotoAdmin(ExportMixin, CommonModelAdmin):
    model = GeoTaggedPhoto
    hide_from_dashboard = False
    resource_class = GeoTaggedPhotoResource

    list_display = [
        "id",
        "shot_at",
        "uploaded_at",
        "uploaded_by",
        "holding_campaign",
        "_thumbnail_for_list",
    ]

    readonly_fields = [
        "id",
        "_thumbnail_for_list",
        "_thumbnail_for_detail",
        "uploaded_by",
        "uploaded_at",
        "_latitude",
        "_longitude",
    ]

    raw_id_fields = ["holding_campaign", "plot"]

    def get_fieldsets(self, request, obj=None):
        if obj:
            # If change
            fieldsets = [
                [None, {"fields": ["id"]}],
                [
                    _("Photo"),
                    {
                        "fields": [
                            "photo",
                            "shot_at",
                            ("photo_width", "photo_height"),
                            "description",
                            "_thumbnail_for_detail",
                        ]
                    },
                ],
                [
                    _("Geographical information"),
                    {
                        "fields": [
                            ("heading", "elevation"),
                            (
                                "horizontal_dilution_of_precision",
                                "vertical_dilution_of_precision",
                            ),
                            "_latitude",
                            "_longitude",
                            "location",
                        ]
                    },
                ],
                [
                    _("Linkage"),
                    {"fields": [("holding_campaign", "plot")]},
                ],
                [_("Uploading"), {"fields": ["uploaded_at", "uploaded_by"]}],
            ]
        else:
            # If add
            fieldsets = [
                [
                    None,
                    {"fields": ["photo", "shot_at", "description"]},
                ],
                [
                    _("Geographical information"),
                    {
                        "fields": [
                            ("heading", "elevation"),
                            (
                                "horizontal_dilution_of_precision",
                                "vertical_dilution_of_precision",
                            ),
                            "location",
                        ]
                    },
                ],
                [
                    _("Linkage"),
                    {"fields": [("holding_campaign", "plot")]},
                ],
            ]

        return fieldsets

    # Map parameters
    # --------------
    geometry_fields = ["location"]

    def get_holding_campaign_id(self, obj):
        if obj:
            return obj.holding_campaign.id

    # Extra fields
    # ------------

    def _thumbnail_for_list(self, obj):
        size = 100
        if obj and obj.photo and obj.thumbnail:
            return mark_safe(
                f'<a href="{s3boto3storage.url(name=obj.photo.name)}"><img src="{s3boto3storage.url(name=obj.thumbnail.name)}" style="max-width: {size}px; max-height: {size}px;"/></a>'
            )
        else:
            return "-"

    def _thumbnail_for_detail(self, obj):
        size = 400
        if obj and obj.photo and obj.thumbnail:
            return mark_safe(
                f'<a href="{s3boto3storage.url(name=obj.photo.name)}"><img src="{s3boto3storage.url(name=obj.thumbnail.name)}" style="max-width: {size}px; max-height: {size}px;"/></a>'
            )
        else:
            return "-"

    _thumbnail_for_list.short_description = _("thumbnail")
    _thumbnail_for_list.allow_tags = True

    _thumbnail_for_detail.short_description = _("thumbnail")
    _thumbnail_for_detail.allow_tags = True

    def _latitude(self, obj):
        return obj.location.coords[1]

    def _longitude(self, obj):
        return obj.location.coords[0]

    _latitude.short_description = _("Latitude")
    _longitude.short_description = _("Longitude")

    def save_model(self, request, obj, form, change):
        # If it's a new photo that has been uploaded through the admin portal
        # then set the uploaded fields
        if not change:
            obj.uploaded_by = request.user
            obj.uploaded_at = timezone.now()
        if "photo" in form.changed_data:
            obj.thumbnail = None
        super().save_model(request, obj, form, change)


if settings.ENABLE_GEOTAGGED_PHOTO:
    admin_site.register(GeoTaggedPhoto, GeoTaggedPhotoAdmin)
