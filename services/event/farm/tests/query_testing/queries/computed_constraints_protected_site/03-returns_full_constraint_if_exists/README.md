# 03-returns_full_constraint_if_exists
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 protected_site that has some intersection with plot
- 1 protected_site constraint, with description containing **inaccurate** `plot_pct_in_protected_site` and `plot_area_in_protected_site`.

## Computed
Nothing.

## Returns
The constraint from setup, with inaccurate `plot_pct_in_protected_site` and `plot_area_in_protected_site`, proving *it has not been recalculated.*