from django.test import TestCase, override_settings
from django.conf import settings

from utils.tests.test_admin import AdminTestCaseMixin

from messaging.models import (
    Broadcast,
    TicketMessage,
    Queue,
    Ticket,
)

from messaging.tests.recipes import (
    broadcast_recipe,
    queue_recipe,
    ticket_recipe,
    ticket_message_recipe,
)


@override_settings(AXES_ENABLED=False)
class BroadcastAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Broadcast
    recipe = broadcast_recipe
    enable = settings.ENABLE_BROADCAST


@override_settings(AXES_ENABLED=False)
class TicketMessageAdminTestCase(AdminTestCaseMixin, TestCase):
    model = TicketMessage
    recipe = ticket_message_recipe
    enable = settings.ENABLE_TICKET


@override_settings(AXES_ENABLED=False)
class TicketAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Ticket
    recipe = ticket_recipe
    enable = settings.ENABLE_TICKET


@override_settings(AXES_ENABLED=False)
class QueueAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Queue
    recipe = queue_recipe
    enable = settings.ENABLE_TICKET
