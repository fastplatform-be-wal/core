This widget is derived from the original django-tabular-permissions widget developed by Ramez Ashraf and available under BSD-2 license (in this directory).

See: [https://github.com/RamezIssac/django-tabular-permissions](https://github.com/RamezIssac/django-tabular-permissions)
