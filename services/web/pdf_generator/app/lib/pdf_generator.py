from opentelemetry import trace
from pyppeteer import launch

from app.lib.template import get_footer, get_header
from app.settings import config


def tracer():
    return trace.get_tracer(__name__)

async def generate_pdf(content, data):
    browser = None
    try:
        with tracer().start_as_current_span("launch_chromium_browser"):
            if config.CHROMIUM_EXECUTABLE_PATH:
                browser = await launch(
                    executablePath=config.CHROMIUM_EXECUTABLE_PATH,
                    args=['--no-sandbox']
                )
            else:
                browser = await launch(
                    args=['--no-sandbox'],
                )
        with tracer().start_as_current_span("load_browser_page"):
            page = await browser.newPage()
            await page.setContent(content)
            await page.emulateMedia('screen')
        with tracer().start_as_current_span("get_pdf_from_browser_page"):
            pdf = await page.pdf({
                'format': 'A4',
                'margin': {'top': '0.8in', 'right': '0.3in', 'left': '0.3in', 'bottom': '0.6in'},
                'displayHeaderFooter': True,
                'footerTemplate': get_footer(),
                'headerTemplate': get_header(data)
            })
        with tracer().start_as_current_span("close_chromium_browser"):
            await browser.close()
        return pdf
    except Exception as e:
        if browser:
            browser.close()
        return None

