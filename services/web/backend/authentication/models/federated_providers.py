from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

import ipware.ip
from colorfield.fields import ColorField

from utils.models import GetAdminURLMixin, TranslatableModel, validate_latin1



class FederatedProvider(GetAdminURLMixin, TranslatableModel):
    """A identity provider that FaST can delegate to for guaranteeing
    user identity and logging into FaST
    """

    id = models.CharField(
        primary_key=True,
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    name = models.CharField(
        unique=True,
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("display name"),
        help_text=_(
            "The name of the federated identity provider, as displayed on the "
            "login screen. Try to keep it short."
        ),
    )

    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("description"),
        help_text=_("A description of the identity provider. Keep it short as well."),
    )

    logo = models.ImageField(
        null=True,
        blank=True,
        verbose_name=_("logo"),
        upload_to=("federated_provider/logo/"),
    )

    url = models.URLField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_("URL"),
        help_text=_(
            "The URL of the login page of the identity provider, that FaST will "
            "redirect the user to"
        ),
    )

    background_color = ColorField(
        default="#DDDDDD",
        null=True,
        blank=True,
        verbose_name=_("background color"),
        help_text=_("The background color of the button on the login screen"),
    )

    text_color = ColorField(
        default="#333333",
        null=True,
        blank=True,
        verbose_name=_("text color"),
        help_text=_("The text color of the button on the login screen"),
    )

    is_active = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name=_("is active"),
        help_text=_(
            "If this box is not checked, the identity provider will not be proposed to the users."
        ),
    )

    def __str__(self):
        if self.name:
            return f"{self.name} ({self.id})"
        else:
            return self.id

    class Meta:
        db_table = "federated_provider"
        verbose_name = _("federated identity provider")
        verbose_name_plural = _("federated identity providers")
        help_text = _(
            "Third-party federated identity providers are used by FaST to delegate authentication of users into FaST"
        )


class FederatedProviderLogEntryType(models.TextChoices):
    LOGIN = "login", _("login")
    LOGOUT = "logout", _("logout")
    FAILURE = "failure", _("login failure")
    OTHER = "other", _("other")


class FederatedProviderLogEntry(models.Model):
    """
    Audit trail entries for the federated identity providers
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    federated_provider = models.ForeignKey(
        to=FederatedProvider,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("federated provider"),
    )

    entry_time = models.DateTimeField(
        blank=False, null=False, verbose_name=_("entry time")
    )

    entry_type = models.CharField(
        max_length=8,
        blank=False,
        null=False,
        choices=FederatedProviderLogEntryType.choices,
        verbose_name=_("entry type"),
    )

    username = models.CharField(
        max_length=255, blank=True, null=True, db_index=True, verbose_name=_("username")
    )

    ip_address = models.GenericIPAddressField(
        blank=True, null=True, db_index=True, verbose_name=_("IP address")
    )

    user_agent = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=_("user agent")
    )

    path = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=_("path")
    )

    data = models.JSONField(blank=True, null=True, verbose_name=_("data"))

    def log(
        cls,
        entry_type,
        federated_provider_id,
        request,
        path=None,
        username=None,
        data=None,
    ):
        ip_address, _ = ipware.ip.get_client_ip(request)
        user_agent = request.META.get("HTTP_USER_AGENT", "")[:255]
        path = (path or "")[:255]
        federated_provider = FederatedProvider.objects.get(id=federated_provider_id)
        entry_time = timezone.now()

        cls.objects.create(
            federated_provider=federated_provider,
            entry_time=entry_time,
            entry_type=entry_type,
            username=username,
            ip_address=ip_address,
            user_agent=user_agent,
            path=path,
            data=data,
        )

    @classmethod
    def log_login(cls, *args, **kwargs):
        cls.log(FederatedProviderLogEntryType.LOGIN, *args, **kwargs)

    @classmethod
    def log_logout(cls, *args, **kwargs):
        cls.log(FederatedProviderLogEntryType.LOGOUT, *args, **kwargs)

    @classmethod
    def log_failure(cls, *args, **kwargs):
        cls.log(FederatedProviderLogEntryType.FAILURE, *args, **kwargs)

    @classmethod
    def log_other(cls, *args, **kwargs):
        cls.log(FederatedProviderLogEntryType.OTHER, *args, **kwargs)

    class Meta:
        db_table = "federated_provider_log_entry"
        ordering = ("-entry_time",)
        verbose_name = _("federated provider log entry")
        verbose_name_plural = _("federated provider log entries")
        help_text = _("Logging activity for third-party federated identity providers")
