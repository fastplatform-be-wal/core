from django.conf import settings

from storages.backends.s3boto3 import S3Boto3Storage


class StaticStorage(S3Boto3Storage):
    access_key= settings.STATIC_AWS_ACCESS_KEY_ID
    secret_key = settings.STATIC_AWS_SECRET_ACCESS_KEY
    bucket_name = settings.STATIC_AWS_STORAGE_BUCKET_NAME
    endpoint_url = settings.STATIC_AWS_S3_ENDPOINT_URL
    object_parameters = settings.STATIC_AWS_S3_OBJECT_PARAMETERS
    location = settings.STATIC_AWS_LOCATION
    querystring_auth = settings.STATIC_AWS_QUERYSTRING_AUTH
    gzip = settings.STATIC_AWS_IS_GZIPPED
    default_acl = settings.STATIC_AWS_DEFAULT_ACL
