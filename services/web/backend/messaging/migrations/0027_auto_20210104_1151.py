# Generated by Django 3.0.7 on 2021-01-04 10:51

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('messaging', '0026_set_read_at_default_now'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conversation',
            name='members',
            field=models.ManyToManyField(blank=True, through='messaging.ConversationMember', to=settings.AUTH_USER_MODEL, verbose_name='members'),
        ),
    ]
