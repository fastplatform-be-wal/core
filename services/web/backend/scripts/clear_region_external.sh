files=`ls ../../../../custom/$1/init/external/*.csv`

tables_to_truncate=""
for filename in $files;
do
    base_name=`basename "$filename" .csv`;
    table_name=`echo "$base_name" | awk -F. '{print $2}'`
    tables_to_truncate=`echo "public.$table_name,$tables_to_truncate"`
done
tables_to_truncate=`echo "${tables_to_truncate%?}"`
echo "TRUNCATE external $tables_to_truncate"
PGPASSWORD=${POSTGRES_EXTERNAL_PASSWORD} psql -U ${POSTGRES_EXTERNAL_USER} -h localhost -p ${POSTGRES_EXTERNAL_PORT} -d ${POSTGRES_EXTERNAL_DATABASE} -c "TRUNCATE $tables_to_truncate CASCADE"
echo "-----------------------------------------"
