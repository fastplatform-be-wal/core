# Dependencies managment
pip-tools==6.5.1

# Linting
pylint==2.4.4
pylint-plugin-utils==0.6

# Formatting
black==22.3.0

# Remote debugging
debugpy==1.4.1
