from django.apps import apps
from django.core.management.base import BaseCommand
from django.db import models


class Command(BaseCommand):
    help = "List all the models/apps that have untranslated strings"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.verbosity = 1

    def handle(self, *args, **kwargs):
        for app_label, all_models in apps.all_models.items():
            print()
            print(app_label)
            print("---------------")
            app_config = apps.get_app_config(app_label)
            if (
                not hasattr(app_config, "verbose_name")
                or app_config.verbose_name is None
                or isinstance(app_config.verbose_name, str)
            ):
                print("[!] app >>", app_label)

            for modelname, model in all_models.items():
                if (
                    not hasattr(model._meta, "verbose_name")
                    or model._meta.verbose_name is None
                    or isinstance(model._meta.verbose_name, str)
                    or not hasattr(model._meta, "verbose_name_plural")
                    or model._meta.verbose_name_plural is None
                    or isinstance(model._meta.verbose_name_plural, str)
                ):
                    print("[!] model >>", app_label, modelname)

                fields = model._meta.get_fields(
                    include_parents=False, include_hidden=True
                )
                for field in fields:
                    if not isinstance(
                        field,
                        (models.ManyToOneRel, models.OneToOneRel, models.ManyToManyRel),
                    ):
                        if (
                            not hasattr(field, "verbose_name")
                            or field.verbose_name is None
                            or isinstance(field.verbose_name, str)
                        ):
                            print("[!] field >>", app_label, modelname, field.name)
