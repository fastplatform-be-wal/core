from django.contrib import admin
from django.utils.translation import gettext_lazy as _


class IsActiveFilter(admin.SimpleListFilter):
    title = _("Active version")

    parameter_name = "active"

    def lookups(self, request, model_admin):
        return (
            (None, _("All")),
            ("yes", _("Yes")),
            ("no", _("No")),
        )

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                "selected": self.value() == lookup,
                "query_string": cl.get_query_string(
                    {
                        self.parameter_name: lookup,
                    },
                    [],
                ),
                "display": title,
            }

    def queryset(self, request, queryset):
        if self.value() == "no":
            return queryset.filter(version__is_active=False)
        elif self.value() == "yes":
            return queryset.filter(version__is_active=True)
        else:
            return queryset
