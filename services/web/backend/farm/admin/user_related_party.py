from django.utils.translation import gettext_lazy as _

from import_export.admin import ExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin

from farm.models import UserRelatedParty
from farm.resources import UserRelatedPartyResource


class UserRelatedPartyAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = UserRelatedPartyResource

    ordering = ("user__name",)
    fields = ("user", "holding", "role")
    raw_id_fields = (
        "user",
        "holding",
    )
    list_display = ("user", "role")


admin_site.register(UserRelatedParty, UserRelatedPartyAdmin)
