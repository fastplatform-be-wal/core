import logging

from opentelemetry import trace
from gql import gql
from graphql import GraphQLError

from app.settings import config
from app.i18n import _
from app.email.send_email import send_email
from app.email.template import (
    get_subject,
    get_body_html,
    get_ticket_admin_link_html,
    get_ticket_webapp_link_html,
)

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def handler_push_notification_on_ticket_status_change(
    ticket_id: int, graphql_clients: dict
):
    try:
        return await _handler_push_notification_on_ticket_status_change(
            ticket_id, graphql_clients
        )
    except GraphQLError:
        logger.exception(
            f"A GraphQL error occured while sending notification for ticket #{ticket_id}"
        )


async def _handler_push_notification_on_ticket_status_change(
    ticket_id: int, graphql_clients: dict
):
    with tracer().start_as_current_span("query_ticket"):
        # In case the ticket has changed since the event was emitted
        # fetch it again
        query = (config.API_DIR / "graphql/query_ticket_by_pk.graphql").read_text()
        variables = {"id": ticket_id}
        client = graphql_clients["fastplatform"]
        response = await client.execute(gql(query), variables)
        ticket = response["ticket_by_pk"]
        if not ticket:
            return f"ticket {ticket_id} does not exist"
        ticket_id = ticket["id"]
        title = ticket["title"]
        status = ticket["status"]
        body = _get_message(status)

    with tracer().start_as_current_span("determine_user_to_notify"):
        query = (
            config.API_DIR / "graphql/query_holding_user_active.graphql"
        ).read_text()
        variables = {
            "user_id": "",
            "holding_id": ticket["holding"]["id"],
        }
        client = graphql_clients["fastplatform"]
        response = await client.execute(gql(query), variables)
        user_ids = [user["id"] for user in response["user"]]
        user_emails = set([user["email"] for user in response["user"] if user["email"]])

    with tracer().start_as_current_span("determine_staff_users_to_email"):
        query = (config.API_DIR / "graphql/query_queue_user.graphql").read_text()
        variables = {
            "user_id": "",
            "queue_id": ticket["queue"]["id"],
        }
        client = graphql_clients["fastplatform"]
        response = await client.execute(gql(query), variables)
        staff_emails = set(
            [user["email"] for user in response["user"] if user["email"]]
        )
        # remove staff emails from user emails to avoid notifying them twice
        user_emails = user_emails - staff_emails

    with tracer().start_as_current_span("send_email"):
        email_data = _get_email_data(ticket_id, title, status)
        _send_email(staff_emails, email_data, staff=True)
        _send_email(user_emails, email_data)

    with tracer().start_as_current_span("send_push_notification") as span:
        mutation = (
            config.API_DIR / "graphql/mutation_send_notification.graphql"
        ).read_text()
        variables = {
            "user_ids": user_ids,
            "message_payload": {
                "title": title,
                "body": body,
            },
        }
        client = graphql_clients["fastplatform"]
        try:
            response = await client.execute(gql(mutation), variables)
        except Exception as err:
            logger.exception(
                f"And error occured on the push notification server for ticket #{ticket_id}"
            )
            span.add_event(str(err))

    return response


def _get_message(status):
    return (
        _("The request status has been changed to")
        + " "
        + _get_status_i18n(_(status.upper()))
    )


def _get_status_i18n(status):
    if status == "OPEN":
        return _("open")
    elif status == "REOPENED":
        return _("reopened")
    elif status == "RESOLVED":
        return _("resolved")
    elif status == "CLOSED":
        return _("closed")
    elif status == "DUPLICATE":
        return _("duplicate")


def _get_email_subject(ticket_id, title):
    return get_subject(ticket_id, title, re=True)


def _send_email(emails, data, staff=False):
    subject = get_subject(data["ticket_id"], data["ticket_title"], re=True)
    template_prefix = "staff" if staff else "user"
    template_name = f"{template_prefix}_ticket_status_change.html"
    content = get_body_html(template_name, data)
    send_email(emails, subject, content, bcc=not (staff))


def _get_email_data(ticket_id, ticket_title, status):
    return {
        "ticket_id": ticket_id,
        "ticket_title": ticket_title,
        "ticket_admin_link": get_ticket_admin_link_html(ticket_id, ticket_title),
        "ticket_webapp_link": get_ticket_webapp_link_html(ticket_id, ticket_title),
        "status_html": f"<b>{_get_status_i18n(status)}</b>",
    }
