# 04-computes_accurate_constraint_if_not_exists_and_plot_intersects
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 management_restriction_or_regulation_zone that has some intersection with plot
- No precomputed management_restriction_or_regulation_zone constraint.

## Computed
A new constraint with accurate `plot_pct_in_management_restriction_or_regulation_zone` and `plot_area_in_management_restriction_or_regulation_zone`.

## Returns
The constraint with the correct distance.