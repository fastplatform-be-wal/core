# Generated by Django 3.2.7 on 2022-02-10 16:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0027_remove_configuration_agri_building_overlay_style'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='rgb_ndvi_overlay_style',
            field=models.JSONField(blank=True, null=True, verbose_name='style for RGB/NDVI overlay'),
        ),
    ]
