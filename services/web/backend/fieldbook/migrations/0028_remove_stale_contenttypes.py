from django.db import migrations


def remove_stale_contenttypes(apps, schema_editor):
    ContentType = apps.get_model("contenttypes.ContentType")
    ContentType.objects.filter(
        model__in=[
            "fertilizationplanrecommendation",
            "fertilizationplanrecommendationrotation",
            "fertilizationplanrecommendationrotationadvice",
            "fertilizationplanrecommendationrotationresult",
            "fertilizationplanrecommendationrotationwarning",
        ],
        app_label="fieldbook",
    ).delete()


class Migration(migrations.Migration):

    dependencies = [
        ("fieldbook", "0027_set_db_comments_fieldbook"),
    ]

    operations = [
        migrations.RunPython(remove_stale_contenttypes),
    ]
