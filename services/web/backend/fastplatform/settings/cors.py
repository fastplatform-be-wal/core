import os


CORS_ORIGIN_ALLOW_ALL = (
    os.environ.get("CORS_ORIGIN_ALLOW_ALL", "TRUE").upper() == "TRUE"
)

CORS_ORIGIN_WHITELIST = os.environ.get(
    "CORS_ORIGIN_WHITELIST", "http://localhost.fastplatform.eu:8080"
).split(",")

CORS_ALLOW_METHODS = os.environ.get(
    "CORS_ALLOW_METHODS", "DELETE,GET,OPTIONS,PATCH,POST,PUT"
).split(",")

CORS_ALLOW_HEADERS = os.environ.get(
    "CORS_ALLOW_HEADERS",
    "accept,accept-encoding,authorization,cache-control,content-type,dnt,origin,user-agent,x-csrftoken,x-requested-with",
).split(",")

X_FRAME_OPTIONS = os.environ.get("X_FRAME_OPTIONS", "SAMEORIGIN")
