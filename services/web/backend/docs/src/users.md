# Managing users

- [Managing users](#managing-users)
  - [How do I...?](#how-do-i)
    - [Enroll/remove users](#enrollremove-users)
      - [Should I create users manually in FaST?](#should-i-create-users-manually-in-fast)
      - [How do I enroll a new farmer?](#how-do-i-enroll-a-new-farmer)
      - [How do I enroll a new Administration Portal user?](#how-do-i-enroll-a-new-administration-portal-user)
      - [Should I have 2 accounts to log in to the Administration Portal and to the app?](#should-i-have-2-accounts-to-log-in-to-the-administration-portal-and-to-the-app)
      - [How do I remove a user?](#how-do-i-remove-a-user)
      - [How do I edit a user's details?](#how-do-i-edit-a-users-details)
    - [Authentication](#authentication)
      - [I have an account with a custom username like `john.doe`, how can I log into this account using a federated provider (like CSAM, etc)?](#i-have-an-account-with-a-custom-username-like-johndoe-how-can-i-log-into-this-account-using-a-federated-provider-like-csam-etc)
      - [How do I reset my FaST password?](#how-do-i-reset-my-fast-password)
      - [How can I prevent a user from logging in using their FaST password?](#how-can-i-prevent-a-user-from-logging-in-using-their-fast-password)
      - [How do I reset my CSAM, APIA, APA [or any other non-FaST provider] password?](#how-do-i-reset-my-csam-apia-apa-or-any-other-non-fast-provider-password)
      - [How do I add / remove / edit a Federated Identity Provider?](#how-do-i-add--remove--edit-a-federated-identity-provider)
    - [Authorization](#authorization)
      - [How do I tell the difference between an Administration Portal user and a user of the mobile app?](#how-do-i-tell-the-difference-between-an-administration-portal-user-and-a-user-of-the-mobile-app)
      - [How do I give new permissions to an Administration Portal user?](#how-do-i-give-new-permissions-to-an-administration-portal-user)
      - [How do I remove permissions from an administration portal user?](#how-do-i-remove-permissions-from-an-administration-portal-user)
      - [How do I allow a user A to see the farm of user B in the mobile application?](#how-do-i-allow-a-user-a-to-see-the-farm-of-user-b-in-the-mobile-application)

FaST maintains an internal list of users that are currently registered in the system. You can access the user list [here](/admin/authentication/user/).

There is only *one type* of FaST user, but each user has a different set of *permissions* (or authorizations) under which they can operate in FaST. A user can be a farmer, or an advisor, or a member of public institution, or a combination of these.

Users can log in to FaST using a variety of methods, which are collectively named *authentication*: this process is explained in more details in the [Authentication](/services/web/backend/docs/en/users/authentication.md) section of this documentation.

Users that log in using a Federated Identity Provider such as CSAM (Wallonia), Ο.Π.Ε.Κ.Ε.Π.Ε. (Greece), APIA (Romania), etc… do not need to be created in advance; a new FaST account will be automatically created when the user logs in for the first time.

FaST users can be *deactivated*: a deactivated user cannot log in anymore in the app or in the Administration Portal. The user can later be reactivated and then be able to log in again. This feature can be used as a replacement for deleting users or as a way to ban users intentionally.

<img src="/services/web/backend/docs/static/docs/img/users/is_active.png" title="Active status" class="img-docs"/>

For more details, you can read:

- [Authentication](/services/web/backend/docs/en/users/authentication.md): How does FaST authenticate users
- [Authorization](](/services/web/backend/docs/en/users/authorization.md)): Who can do what and where?

## How do I...?

### Enroll/remove users

#### Should I create users manually in FaST?

When a user logs in using a **Federated Identity Provider**, FaST will automatically create a new user if the user does not already exist. So in that case, you do not need to create the user in advance.

This is not the case with the **FaST Internal Identity Provider**, where new users have to be manually created in the [user management page](/admin/authentication/user/) before they can connect to FaST. To create a new user and assign them a password they can use against the **FaST Internal Identity Provider**:

- Open the [User list](/admin/authentication/user/)
- Click on <kbd>Add user</kbd> on the top-right of the screen
<img src="/services/web/backend/docs/static/docs/img/users/add_user_button.png" title="Add user" class="img-docs" height="80"/>
- Fill in a username and an initial password
- Click on <kbd>Save</kbd>

The user is now created. You can further edit the user's profile (like setting a first and last name) by clicking on their username in the list.

<img src="/services/web/backend/docs/static/docs/img/users/username.png" title="Username" class="img-docs" height="50"/>

#### How do I enroll a new farmer?

If the farmer is able to log in using one of the **Federated Identity Providers** of your region, then you have nothing to do. The farmer can just install the mobile application or go to [https://app.beta.`<ms-iso-code>`.fastplatform.eu](https://app.beta.ms-iso-code.fastplatform.eu), log in, and a new account will be automatically created for them by FaST.

If you do not have a **Federated Identity Provider** that is able to authenticate farmers, then you will need to first create the new user manually in FaST (see above) and send users their credentials.

#### How do I enroll a new Administration Portal user?

A new user account has to be created first for the user: this is automatic if the user logs in using a **Federated Identity Provider**, and manual if the user uses the **FaST Internal Identity Provider**.

Once the user is created, you need to grant them the **staff** status. To do this:

- Open the user's profile page from the [User list](/admin/authentication/user/)
- Check the *Staff status* box
<img src="/services/web/backend/docs/static/docs/img/users/is_staff.png" title="Staff status" class="img-docs" height="40"/>
- Click <kbd>Save</kbd>

#### Should I have 2 accounts to log in to the Administration Portal and to the app?

No, you only need one account. All FaST users can log into the **mobile application**. If you have the **staff** status, then you will also be able to log into the **Administration Portal**.

#### How do I remove a user?

To remove a user, you just have to delete their account:

- Open the user's profile page from the [User list](/admin/authentication/user/)
- Click <kbd>Delete</kbd>

This action is <u>irreversible</u>.

Deleting a user will cascade to all the FaST objects that are directly linked to this user. Note that FaST might require you to delete some objects *before* you can delete the user (for example, ticket messages). This is to prevent accidental deletion of data.

> Caution: Deleting a user will *not* delete a user's farm(s). You have to delete the farm(s) separately. This is because a user might not be the only member of a farm, and therefore deleting the user does not necessarily imply deleting the farm which might still be used by other members.

Sometimes you just want to "deactivate" a user, without deleting them. FaST provides you a way to do just that:

- Open the user's profile page from the [User list](/admin/authentication/user/)
- Uncheck the *Active* checkbox
- Click <kbd>Save</kbd>

The user will still exist in the system but will be prevented from logging in (from the **mobile application** and from the **Administration Portal**). The user will also become invisible to [add-ons](/services/web/backend/docs/en/add_ons.md).

#### How do I edit a user's details?

To edit a user's details:

- Open the user's profile page from the [User list](/admin/authentication/user/)
- Edit the fields you wish to modify
- Click <kbd>Save</kbd>

### Authentication

#### I have an account with a custom username like `john.doe`, how can I log into this account using a federated provider (like CSAM, etc)?

The short answer is: you cannot. When you log in with CSAM (or another **Federated Identity Provider**) a new FaST account will be created for you _with the username returned by the provider_. This new account will be different from the `john.doe` account. So in the end you will have 2 accounts: one with `john.doe` as username, and one with the username returned by the identity provider.

Note that FaST currently does not provide a way to change a user's username.

#### How do I reset my FaST password?

If you have previously been provided a FaST password, then you can reset it by following the _Forgot your password or username?_ link on the login page.

<img src="/services/web/backend/docs/static/docs/img/users/forgot_password.png" title="Forgot your password or username?" class="img-docs" style="height: 220px" />

The password reset workflow assumes that your email address has been input into your user profile. If it is not the case, you need to contact your local support to fill it in for you.

If you had **not** been provided a FaST password for your account, then the password reset workflow will silently fail (i.e. you will not receive any email, and will not be able to create a new password). This is intentional, for security reasons. You will need to contact your local support to reset you password for you.

#### How can I prevent a user from logging in using their FaST password?

You can remove a user's FaST password by clicking on _Remove this user's password_ from the user's profile page.

<img src="/services/web/backend/docs/static/docs/img/users/remove_user_password.png" title="Remove this user's password" class="img-docs" style="height: 220px" />

Once a user's password is removed, they cannot log in using the **FaST Internal Identity Provider** anymore and *must* use one of the proposed **Federated Identity Providers**.

#### How do I reset my CSAM, APIA, APA [or any other non-FaST provider] password?

You cannot do that in FaST, as these systems are not part of FaST. You have to directly contact the relevant provider and/or follow their documentation.

#### How do I add / remove / edit a Federated Identity Provider?

Adding a **Federated Identity Provider** is not an automatic process, as a custom connector will likely have to be developed. Hence these actions can only be performed by the [development team](support@fastplatform.eu).

### Authorization

#### How do I tell the difference between an Administration Portal user and a user of the mobile app?

In the [User list](/admin/authentication/user/), the users that can connect to the **Administration Portal** have their **staff** indicated with a green checkmark.

<img src="/services/web/backend/docs/static/docs/img/users/staff_status.png" title="Staff status" class="img-docs" height="300"/>

All the users (**staff** and not staff) can connect to FaST using the **farmer mobile application**.

#### How do I give new permissions to an Administration Portal user?

You can either:

- Add permissions to groups that this user belongs to
- Create a new group with the desired permissions and assign this user to this group
- Add individual permissions to the user (however, this is discouraged)

To add new permissions to a group:

- Open the group's page from the [Group list](/admin/authentication/group/)
- Check the new permissions from the permissions list
<img src="/services/web/backend/docs/static/docs/img/users/group_permissions.png" title="Group permissions" class="img-docs" height="200" />
- Click <kbd>Save</kbd>

To create a new group:

- Open the [Group list](/admin/authentication/group/)
- Click on <kbd>Add group</kbd>
- Fill in the group name
- Check the desired permissions from the permissions list
- Add the user to the group's members by clicking <kbd>Add another group member</kbd>
- Click <kbd>Save</kbd>

To add individual permissions to the user:

- Open the user's profile page from the [User list](/admin/authentication/user/)
- Add the desired permission in the *User permissions* box (there is an autocomplete mechanism)
<img src="/services/web/backend/docs/static/docs/img/users/user_permissions.png" title="User permissions" class="img-docs" height="60" />
- Click <kbd>Save</kbd>


#### How do I remove permissions from an administration portal user?

You first have to identify where the user gets this permission from: is it a group permission or an individual permission on their profile?

If the permission comes from a group, then you will have to remove the user from this group. Note that this will also remove all the other permissions of the group from the user's permissions. If you do not want that, and want to only remove one permission while keeping the others, then you will have no other option than to create a new group with the remaining permissions, and assign the user to this group.

To remove a user from a group:

* Through the user's profile:
    - Open the user's profile page from the [User list](/admin/authentication/user/)
    - Remove the group from the user's Groups list
    <img src="/services/web/backend/docs/static/docs/img/users/user_groups.png" title="User groups" class="img-docs" height="45" />
    - Click <kbd>Save</kbd>

* Alternatively, through the group's page:
    - Open the group's page from the [Group list](/admin/authentication/group/)
    - From the *Group members* list, check the *Delete* checkbox for the user you want to remove
    <img src="/services/web/backend/docs/static/docs/img/users/delete_group_member.png" title="Delete a group member" class="img-docs" height="300" />
    - Click <kbd>Save</kbd>

If the permission is an individual permission, then you can remove it from user's profile page:

- Open the user's profile page from the [User list](/admin/authentication/user/)
- Remove the permission from the *User permissions* box
<img src="/services/web/backend/docs/static/docs/img/users/user_permissions.png" title="User permissions" class="img-docs" height="60" />
- Click <kbd>Save</kbd>

#### How do I allow a user A to see the farm of user B in the mobile application?

In the **mobile application**, permissions are based on the farm membership. A user can see another user's farm *only if they is member of the other user's farm*.

Therefore, you need to add user A as a member of user B's farm. To do this:

* From the farm configuration page:
    - Open user B's farm configuration from the [Farm list](/admin/farm/holding/)
    - Scroll down to <kbd>Farm members</kbd>
    <img src="/services/web/backend/docs/static/docs/img/users/farm_farm_members.png" title="Farm members" class="img-docs" height="160" />
    - Click on <kbd>Add another farm member</kbd>
    - Select user A using the search icon
    - Set a role from the list (note the role is purely indicative)
    - Click <kbd>Save</kbd>
    
* Alternatively, from user A's profile page
    - Open the user A's profile page from the [User list](/admin/authentication/user/)
    - In the *Farm membership* section, click on <kbd>Add another farm membership</kbd>
    <img src="/services/web/backend/docs/static/docs/img/users/farm_farm_members_2.png" title="Farm members" class="img-docs" height="200" />
    - Select user B's farm
    - Set a role
    - Click <kbd>Save</kbd>
