from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ExternalConfig(AppConfig):
    name = "external"
    verbose_name = _("Imported GIS data sources")

    help_text = _("Public geographical data sources that have been imported into the platform")
