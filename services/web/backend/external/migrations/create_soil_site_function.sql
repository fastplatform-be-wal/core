CREATE OR REPLACE FUNCTION public.nearest_soil_site(distance double precision, geom geometry, nb_limit integer = 5)
RETURNS SETOF soil_site AS $$
	SELECT
		*
	FROM
		soil_site
	WHERE
		ST_DWithin(
			soil_site.geometry,
			geom,
			distance)
	ORDER BY
		soil_site.geometry <->  geom
	LIMIT nb_limit
$$ LANGUAGE sql STABLE STRICT PARALLEL SAFE;