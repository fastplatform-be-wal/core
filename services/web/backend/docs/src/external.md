# Imported GIS data

- [Imported GIS data](#imported-gis-data)
  - [Area Management Restriction or Regulation Zones](#area-management-restriction-or-regulation-zones)
  - [Protected Sites](#protected-sites)
  - [Surface Waters and Water Courses](#surface-waters-and-water-courses)
  - [Soil Sites](#soil-sites)

Some external GIS data sources are imported into FaST and used to display maps and compute constraints on agricultural plots. The semantics adopted for these sources are extracted from the INSPIRE data model .

These GIS data sources comprise:

- **<x>Area Management Restriction or Regulation Zones</x>**, *INSPIRE Annex III*: spatial objects that represent zones or areas established to protect the environment via management, restriction or regulation. In FaST, this object type is used to store Nitrate Vulnerable Zones. These objects are for example used to compute the nitrogen limitations for a given plot.
- **<x>Protected Sites</x>**, *INSPIRE Annex I*: areas designated or managed within a framework of international, Community and Member States' legislation to achieve specific conservation objectives. In FaST, this object type is mainly used to store `Natura2000` areas. Those areas are used to compute specific constraints applicable to a given plot.
- **<x>Surface Waters</x>** and **<x>Water Courses</x>**, *INSPIRE Annex I Hydrography*: inland waterway bodies (e.g. lake/pond, reservoir, river/stream, etc.). Specific constraints may apply to a plot that intersects or is in proximity of such a body.
- **<x>Soil Sites</x>**, *INSPIRE Annex III*: areas within a larger survey, study or monitored area, where a specific soil investigation is carried out. In FaST this object type is used to store regional soil data. They are notably used to interpolate soil sample estimates for a given plot, when no specific private soil sample has been provided by the farmer. Those estimates are in turn used in certain regions (like Greece) to compute Nitrogen Limitations. They can also be used as default inputs for the fertilization algorithms.

## Area Management Restriction or Regulation Zones

The Nitrate Vulnerable Zones are stored in <x>Area Management Restriction or Regulation Zone</x> objects. They are used to compute constraints (to compute legal limits), and are displayed on the maps in purple.

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/external/nvz.png" height="500"/>
<figcaption>An agricultural plot (in red) is located inside a Nitrate Vulnerable Zone (in purple), as seen from the Administration Portal.</figcaption>
</figure>

## Protected Sites

The Natura2000 areas are stored in the <x>Protected Site</x> objects, and are displayed on the maps in green.

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/external/natura2000.png" height="500"/>
<figcaption>An agricultural plot (in red) is located close to several Natura2000 protected sites (in green), as seen from the Administration Portal.</figcaption>
</figure>

## Surface Waters and Water Courses

The lakes, rivers, ponds and other water bodies are loaded into FaST under the <x>Surface Water</x> and <x>Water Course</x> objects. They are used to compute water proximity constraints that are then displayed to the user. They are also displayed on the maps, in light blue.

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/external/hydro.png" height="500"/>
<figcaption>An agricultural plot (in red) is located in close proximity to several water courses (in blue), as seen from the Administration Portal.</figcaption>
</figure>

## Soil Sites

The regional soil data is stored in the <x>Soil Site</x> objects (and sub-objects) are derived from regional data and are used 
- to compute the soil estimates that are displayed to the user,
- and as default inputs for the fertilization algorithms.

```plantuml
skinparam defaultFontSize 12

entity SoilSite
entity Observation
entity ObservableProperty
entity PhenomenonType

SoilSite ||--o{ Observation
Observation ||--|| ObservableProperty
ObservableProperty ||--|| PhenomenonType
```

More information on Observations and ObservableProperties can be found [here](/services/web/backend/docs/en//observations.md)