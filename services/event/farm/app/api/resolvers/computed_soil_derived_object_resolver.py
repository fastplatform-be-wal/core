import logging

from opentelemetry import trace
from gql import gql
from shapely.geometry import shape
from app.api.types.computed_soil_derived_object import (
    ComputedSoilDerivedObject,
    ComputedDerivedObservation,
    ObservableProperty,
    UnitOfMeasure,
)

from app.api.lib.gis import Projection, add_crs
from app.settings import config
from app.db.graphql_clients import fastplatform
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class ComputedSoilDerivedObjectResolver:
    async def resolve(self, plot_id: int):
        """
        For a given plot, compute its soil_derived_object.

        Args:
            plot_id (int): the id of the plot whose soil_derived_object are calculated here.

        Raises:
            GraphQLHoldingPlotNotFoundError: No plot associated with the id provided.
            GraphQLUnexpectedDataError: The graphql operation could not be performed successfully.

        Returns:
            affected_rows
            ids
        """
        logger.debug("ComputedSoilDerivedObjectResolver.resolve()")
        try:
            computed_soil_derived_object = (
                await self.get_computed_plot_soil_derived_object(plot_id)
            )

            if not computed_soil_derived_object == []:
                logger.info(
                    "soil_derived_object already computed for plot_id %s", plot_id
                )
                return computed_soil_derived_object

            logger.info("Computing soil_derived_object for plot_id %s", plot_id)

            plot_details = await self.get_plot_details(plot_id)

            # First try to compute soil_derived_object from private soil site (fastplatform db)
            soil_derived_object = await self.get_private_soil_sites(plot_details)

            # If no private soil site, compute soil_derived_object from public soil site (external db)
            if soil_derived_object == []:
                soil_derived_object = await self.get_public_soil_sites(plot_details)

            # If no private soil sites for this plot and no public ones nearby, create "empty" sdo
            if soil_derived_object == []:
                soil_derived_object = {
                    "plot_id": plot_id,
                    "is_derived_from": {},
                    "soil_sample_origin": "none",
                    "soil_sites_count": 0,
                    "derived_observations": [],
                }

            await self.insert_soil_derived_object(soil_derived_object)

            # TODO: avoid second query and return results immediately
            return await self.get_computed_plot_soil_derived_object(plot_id)

        except GraphQLHoldingPlotNotFoundError as ex:
            raise GraphQLHoldingPlotNotFoundError() from ex

        except Exception as ex:
            logger.exception("resolve_soil_derived_object error")
            raise GraphQLUnexpectedDataError() from ex

    async def get_computed_plot_soil_derived_object(self, plot_id: int):
        logger.debug(
            "ComputedSoilDerivedObjectResolver.get_computed_plot_soil_derived_object()"
        )

        with tracer().start_as_current_span(
            "get_computed_plot_soil_derived_object"
        ) as span:
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_soil_derived_object_by_plot_id.graphql"
            ).read_text()
            variables = {"plot_id": plot_id}
            response = await fastplatform.execute(gql(query), variables)

            soil_derived_object = response["soil_derived_object"]
            if soil_derived_object == []:
                logger.info(
                    "No soil_derived_object found for plot: %s, it will now be computed.",
                    plot_id,
                )
                return []

            # Fastplatform ontology allows for several soil_derived_object per plot,
            # but only zero or one per plot is ever used by app.
            soil_derived_object = soil_derived_object[0]

            # If soil_sample_origin == "none", soil_derived_object already computed
            # and there are none. Return None
            if soil_derived_object["soil_sample_origin"] == "none":
                logger.info(
                    "soil_derived_object already computed, but found none for plot: %s",
                    plot_id,
                )
                return None

            # Now map to ComputedSoilDerivedObject
            computed = ComputedSoilDerivedObject(
                id=soil_derived_object["id"],
                is_derived_from=soil_derived_object["is_derived_from"],
                soil_sample_origin=soil_derived_object["soil_sample_origin"],
                soil_sites_count=soil_derived_object["soil_sites_count"],
                plot_id=soil_derived_object["plot_id"],
                derived_observations=[
                    ComputedDerivedObservation(
                        id=obs["id"],
                        result=obs["result"],
                        observed_property_id=obs["observed_property_id"],
                        observable_property=ObservableProperty(
                            id=obs["observed_property_id"],
                            label=obs["observable_property"]["label"],
                            i18n=obs["observable_property"]["i18n"],
                            unit_of_measure=UnitOfMeasure(
                                id=obs["observable_property"]["unit_of_measure"]["id"],
                                name=obs["observable_property"]["unit_of_measure"][
                                    "name"
                                ],
                                symbol=obs["observable_property"]["unit_of_measure"][
                                    "symbol"
                                ],
                                i18n=obs["observable_property"]["unit_of_measure"][
                                    "i18n"
                                ],
                            ),
                        ),
                    )
                    for obs in soil_derived_object["derived_observations"]
                ],
            )

        return [computed]

    async def get_plot_details(self, plot_id: int):
        logger.debug("ComputedSoilDerivedObjectResolver.get_plot_details()")

        with tracer().start_as_current_span("get_plot_details") as span:
            query = (
                config.API_DIR / "resolvers" / "graphql" / "query_plot_by_pk.graphql"
            ).read_text()
            variables = {"plot_id": plot_id}

            response = await fastplatform.execute(gql(query), variables)

            plot_details = response["plot_by_pk"]

            if plot_details == None or plot_details == {}:
                logger.error("No plot found for id: %s", plot_id)
                raise GraphQLHoldingPlotNotFoundError()

            return plot_details

    async def get_private_soil_sites(self, plot_details):
        """
        Get soil_sites and observations from **fastplatform** db that intersect plot geometry.
        They are private because they are attached to a specific holding.

        If no data found here, interpolate with existing public data in next step:
        -> get_public_soil_sites()

        Args:
            plot_details (dict): the result of the query performed in get_plot_details(),
                used here to retrieve plot.geometry.

        Returns:
            soil_derived_object (dict): the object that needs to be inserted, if any.
        """
        logger.debug("ComputedSoilDerivedObjectResolver.get_private_soil_sites()")

        with tracer().start_as_current_span("get_private_soil_sites") as span:
            plot_id = int(plot_details["id"])
            plot_geometry = shape(plot_details["geometry"])
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_soil_site_private_intersect_plot.graphql"
            ).read_text()

            plot_campaign_end_at = plot_details["site"]["holding_campaign"]["campaign"][
                "end_at"
            ]
            variables = {
                "plot_id": plot_id,
                "plot_geometry": add_crs(
                    plot_geometry.__geo_interface__, config.EPSG_SRID_ETRS89
                ),
                "plot_campaign_end_at": plot_campaign_end_at,
            }

            response = await fastplatform.execute(gql(query), variables)

            private_soil_sites = response["soil_site"]

            span.add_event(
                "{} private soil sites found".format(len(private_soil_sites))
            )

            if len(private_soil_sites) == 0:
                return []

            # Flatten the list of observations
            private_observations = [
                observation
                for private_soil_site in private_soil_sites
                for observation in private_soil_site["observations"]
            ]
            # Sort in reverse chronological order
            private_observations = sorted(
                private_observations,
                key=lambda x: x["phenomenon_time"],
                reverse=True,
            )
            # Coalesce observations into a single one, taking only the most recent measurement
            # for each property
            derived_observations = {}
            for observation in private_observations:
                observed_property_id = observation["observable_property"]["id"]
                if observed_property_id not in derived_observations:
                    derived_observations[observed_property_id] = {
                        "observed_property_id": observed_property_id,
                        "result": observation["result"],
                    }
            derived_observations = derived_observations.values()
            soil_derived_object = {
                "plot_id": plot_id,
                "is_derived_from": {
                    # TODO
                },
                "soil_sample_origin": "private",
                "soil_sites_count": len(private_soil_sites),
                "derived_observations": derived_observations,
            }

            return soil_derived_object

    async def get_public_soil_sites(self, plot_details):
        """
        Get soil_sites and observations from **external** db that intersect plot geometry.

        If no private soil_sites found in previous step (get_private_soil_sites),
        find here public soil_sites not too far from plot geometry, and interpolate results.

        Args:
            plot_details (dict): the result of the query performed in get_plot_details(),
                used here to retrieve:
                - plot.geometry
                - plot -> site -> holding_campaign -> campaign.end_at

        Returns:
            soil_derived_object (dict): the object that needs to be inserted, if any.
        """
        logger.debug("ComputedSoilDerivedObjectResolver.get_public_soil_sites()")

        with tracer().start_as_current_span("get_public_soil_sites") as span:
            plot_id = int(plot_details["id"])
            plot_geometry = shape(plot_details["geometry"])
            plot_campaign_end_at = plot_details["site"]["holding_campaign"]["campaign"][
                "end_at"
            ]

            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_soil_site_public_near_plot.graphql"
            ).read_text()
            variables = {
                "plot_geometry": add_crs(
                    plot_geometry.__geo_interface__, config.EPSG_SRID_ETRS89
                ),
                "plot_campaign_end_at": plot_campaign_end_at,
                "distance": config.API_PLOT_COMPUTE_SOIL_DERIVED_OBJECT_PUBLIC_SOIL_SITE_SEARCH_DISTANCE,
            }

            response = await fastplatform.execute(gql(query), variables)

            public_soil_sites = response["nearest_soil_site"]

            span.add_event("{} public soil sites found".format(len(public_soil_sites)))

            if len(public_soil_sites) == 0:
                return []

            with tracer().start_as_current_span("process_public_soil_sites") as span:
                # We found some public soil sites in a reasonable neighborhood
                with tracer().start_as_current_span("compute_distance") as span:
                    # Compute the distance to the plot for each public soil site
                    plot_geometry_projected = Projection.etrs89_to_etrs89_laea(
                        plot_geometry
                    )

                    for public_soil_site in public_soil_sites:
                        g_soil_site_projected = Projection.etrs89_to_etrs89_laea(
                            shape(public_soil_site["geometry"])
                        )
                        public_soil_site["distance"] = plot_geometry_projected.distance(
                            g_soil_site_projected
                        )

                with tracer().start_as_current_span("sort_by_distance") as span:
                    # Order the public soil sites in reverse order and keep only the N closest ones
                    public_soil_sites = sorted(
                        public_soil_sites,
                        key=lambda x: x["distance"],
                        reverse=True,
                    )

                with tracer().start_as_current_span("aggregate_values") as span:
                    # Aggregate the values (weighted average inverse to the distance)
                    derived_observations = {}
                    for public_soil_site in public_soil_sites:
                        weight = (
                            1 / public_soil_site["distance"]
                            if public_soil_site["distance"] > 0
                            else 1000
                        )

                        for observation in public_soil_site["external__observations"]:
                            observed_property_id = observation[
                                "external__observable_property"
                            ]["id"]
                            if observed_property_id not in derived_observations:
                                derived_observations[observed_property_id] = {
                                    "weights": [],
                                    "results": [],
                                }

                            derived_observations[observed_property_id][
                                "weights"
                            ].append(weight)

                            derived_observations[observed_property_id][
                                "results"
                            ].append(
                                (
                                    observation["result"].get("value"),
                                    observation["result"].get("label"),
                                )
                            )

                    derived_observations_objects = []
                    for (
                        observed_property_id,
                        obj,
                    ) in derived_observations.items():
                        if not obj:
                            continue
                        try:
                            agg_value = sum(
                                [
                                    result[0] * weight
                                    for result, weight in zip(
                                        obj["results"], obj["weights"]
                                    )
                                ]
                            ) / sum(obj["weights"])
                            agg_result = (agg_value, None)
                        except:
                            agg_result = max(
                                set(obj["results"]), key=obj["results"][0].count
                            )

                        result = {"value": agg_result[0]}
                        if agg_result[1] is not None:
                            result.update({"label": agg_result[1]})

                        derived_observations_objects.append(
                            {
                                "observed_property_id": observed_property_id,
                                "result": result,
                            }
                        )

                soil_derived_object = {
                    "plot_id": plot_id,
                    "is_derived_from": {
                        # TODO
                    },
                    "soil_sample_origin": "public",
                    "soil_sites_count": len(public_soil_sites),
                    "derived_observations": derived_observations_objects,
                }

                return soil_derived_object

    async def insert_soil_derived_object(self, soil_derived_object):
        logger.debug("ComputedSoilDerivedObjectResolver.insert_soil_derived_object()")

        with tracer().start_as_current_span("insert_soil_derived_object") as span:
            # Convert the soil_derived_object to a an upsert
            objects = [
                {
                    "plot_id": soil_derived_object["plot_id"],
                    "is_derived_from": soil_derived_object["is_derived_from"],
                    "soil_sample_origin": soil_derived_object["soil_sample_origin"],
                    "soil_sites_count": soil_derived_object["soil_sites_count"],
                    "derived_observations": {
                        "data": [
                            {
                                "observed_property_id": observation[
                                    "observed_property_id"
                                ],
                                "result": observation["result"],
                            }
                            for observation in soil_derived_object[
                                "derived_observations"
                            ]
                        ],
                        "on_conflict": {
                            "constraint": "derived_observation_soil_derived_object_observed_property_uniqu",
                            "update_columns": ["result"],
                        },
                    },
                }
            ]

            mutation = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "mutation_insert_soil_derived_object.graphql"
            ).read_text()

            variables = {
                "objects": objects,
            }

            # TODO: better handle eventual disconnection from server
            response = await fastplatform.execute(gql(mutation), variables)
