import json
import logging

from fastapi import APIRouter, Request
from functools import partial

from gql import gql
from opentelemetry import trace

from app.settings import config
from app.api.router.events import HasuraEvent
from app.api.lib.broadcast.push_notification_broadcast_sending_triggered import (
    handler_push_notification_broadcast_sending_triggered,
)


# Log
logger = logging.getLogger(__name__)

# FastAPI
router = APIRouter()

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


@router.post("/push_notification_broadcast_sending_triggered")
async def push_notification_broadcast_sending_triggered(
    request: Request, event: HasuraEvent
):

    broadcast_id = event.event.data.new["id"]

    return await handler_push_notification_broadcast_sending_triggered(
        broadcast_id, graphql_clients=request.state.graphql_clients
    )
