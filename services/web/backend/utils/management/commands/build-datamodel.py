from django.core.management.base import BaseCommand
from django.core.management import call_command

APPS_WITH_CUSTOM_MODELS = [
    "add_ons",
    "authentication",
    "common",
    "configuration",
    "external",
    "farm",
    "fieldbook",
    "messaging",
    "photos",
    "soil",
]


class Command(BaseCommand):
    help = ""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def handle(self, *args, **kwargs):
        for app_label in APPS_WITH_CUSTOM_MODELS:
            call_command(
                "generate_puml",
                "--file",
                f"{app_label}/data_model.md",
                "--include",
                app_label,
                "--add-help",
                "--add-legend",
            )

            new_line = "```plantuml\n"

            with open(f"{app_label}/data_model.md", "r+") as file:
                content = file.read()
                file.seek(0)
                file.write(new_line + content + "```")
