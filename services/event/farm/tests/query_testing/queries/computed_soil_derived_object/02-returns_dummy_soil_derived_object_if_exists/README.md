# 02-returns_dummy_soil_derived_object_if_exists

## Setup
- 1 holding containing 1 plot.
- 1 private soil_site
- 1 public soil_site
- 1 dummy sdo

## Computed
Finds existing sdo, and does not compute it from private/public soil site

## Returns
Returns existing sdo