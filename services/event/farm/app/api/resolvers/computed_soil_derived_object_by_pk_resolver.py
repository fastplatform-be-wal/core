import logging

from opentelemetry import trace
from gql import gql
from shapely.geometry import shape
from app.api.types.computed_soil_derived_object import (
    ComputedSoilDerivedObject,
    ComputedDerivedObservation,
    ObservableProperty,
    UnitOfMeasure,
)
import json

from app.settings import config
from app.db.graphql_clients import fastplatform
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class ComputedSoilDerivedObjectByPKResolver:
    async def resolve(self, id: int):
        """
        Resolve a soil_derived_object by its primary key ("id"),
        and return a graphene ObjectType of type ComputedSoilDerivedObject.

        Args:
            id (int): the id of the soil_derived_object

        Raises:
            GraphQLHoldingPlotNotFoundError: No plot associated with the id provided.
            GraphQLUnexpectedDataError: The graphql operation could not be performed successfully.

        Returns:
            (graphene ObjectType):  the ComputedSoilDerivedObject associated to the id provided.
        """
        logger.info("ComputedSoilDerivedObjectByPKResolver.resolve()")
        try:
            return await self.get_computed_plot_soil_derived_object_by_pk(id)

        except GraphQLHoldingPlotNotFoundError as ex:
            raise GraphQLHoldingPlotNotFoundError() from ex

        except Exception as ex:
            logger.exception("resolve_soil_derived_object_by_pk error")
            raise GraphQLUnexpectedDataError() from ex

    async def get_computed_plot_soil_derived_object_by_pk(self, id: int):
        logger.info(
            "ComputedSoilDerivedObjectByPKResolver.get_computed_plot_soil_derived_object_by_pk()"
        )

        with tracer().start_as_current_span(
            "get_computed_plot_soil_derived_object_by_pk"
        ) as span:
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_soil_derived_object_by_pk.graphql"
            ).read_text()
            variables = {"id": id}

            response = await fastplatform.execute(gql(query), variables)

            soil_derived_object = response["soil_derived_object_by_pk"]

            if soil_derived_object == None:
                logger.info("No soil_derived_object found for id: %s", id)
                return None

            # Now map to ComputedSoilDerivedObject
            computed = ComputedSoilDerivedObject(
                id=soil_derived_object["id"],
                is_derived_from=soil_derived_object["is_derived_from"],
                soil_sample_origin=soil_derived_object["soil_sample_origin"],
                soil_sites_count=soil_derived_object["soil_sites_count"],
                plot_id=soil_derived_object["plot_id"],
                derived_observations=[
                    ComputedDerivedObservation(
                        id=obs["id"],
                        result=obs["result"],
                        observed_property_id=obs["observed_property_id"],
                        observable_property=ObservableProperty(
                            id=obs["observable_property"]["id"],
                            label=obs["observable_property"]["label"],
                            i18n=obs["observable_property"]["i18n"],
                            unit_of_measure=UnitOfMeasure(
                                id=obs["observable_property"]["unit_of_measure"]["id"],
                                name=obs["observable_property"]["unit_of_measure"][
                                    "name"
                                ],
                                symbol=obs["observable_property"]["unit_of_measure"][
                                    "symbol"
                                ],
                                i18n=obs["observable_property"]["unit_of_measure"][
                                    "i18n"
                                ],
                            ),
                        ),
                    )
                    for obs in soil_derived_object["derived_observations"]
                ],
            )

        return computed
