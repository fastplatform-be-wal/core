# Generated by Django 3.2 on 2021-12-07 16:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0015_auto_20210729_0930'),
    ]

    operations = [
        migrations.AlterField(
            model_name='geotaggedphoto',
            name='exif',
            field=models.JSONField(blank=True, default=dict, null=True, verbose_name='EXIF data'),
        ),
    ]
