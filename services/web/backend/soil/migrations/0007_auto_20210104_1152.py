# Generated by Django 3.0.7 on 2021-01-04 10:52

import django.contrib.postgres.fields.jsonb
from django.db import migrations
import soil.models


class Migration(migrations.Migration):

    dependencies = [
        ('soil', '0006_reset_sql_sequences'),
    ]

    operations = [
        migrations.AlterField(
            model_name='derivedobservation',
            name='result',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=soil.models.derived_observation_default_result, verbose_name='result'),
        ),
    ]
