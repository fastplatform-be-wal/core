from django.utils import timezone

from model_bakery.recipe import Recipe

from external.models import (
    AgriPlot,
    AgriPlotVersion,
    ManagementRestrictionOrRegulationZone,
    ManagementRestrictionOrRegulationZoneVersion,
    ZoneTypeCode,
    SpecialisedZoneTypeCode,
    EnvironmentalDomain,
    WaterCourseVersion,
    WaterCourse,
    SurfaceWaterVersion,
    SurfaceWater,
    ProtectedSite,
    ProtectedSiteVersion,
    DesignationType,
    DesignationScheme,
    Designation,
    SoilSite,
    SoilSiteVersion,
    Observation,
    PhenomenonType,
    SoilInvestigationPurpose,
    ObservableProperty,
)


agri_plot_version_recipe = Recipe(
    AgriPlotVersion,
    import_started_at=timezone.now(),
    is_active=True,
    status="COMPLETED",
)
agri_plot_recipe = Recipe(AgriPlot)

management_restriction_or_regulation_zone_version_recipe = Recipe(
    ManagementRestrictionOrRegulationZoneVersion,
    import_started_at=timezone.now(),
    is_active=True,
    status="COMPLETED",
)

zone_type_code_recipe = Recipe(ZoneTypeCode)

specialised_zone_type_code_recipe = Recipe(SpecialisedZoneTypeCode)

environmental_domain_recipe = Recipe(EnvironmentalDomain)

management_restriction_or_regulation_zone_recipe = Recipe(
    ManagementRestrictionOrRegulationZone
)

water_course_version_recipe = Recipe(
    WaterCourseVersion,
    import_started_at=timezone.now(),
    is_active=True,
    status="COMPLETED",
)
water_course_recipe = Recipe(WaterCourse)

surface_water_version_recipe = Recipe(
    SurfaceWaterVersion,
    import_started_at=timezone.now(),
    is_active=True,
    status="COMPLETED",
)
surface_water_recipe = Recipe(SurfaceWater)

protected_site_version_recipe = Recipe(
    ProtectedSiteVersion,
    import_started_at=timezone.now(),
    is_active=True,
    status="COMPLETED",
)

designation_type_recipe = Recipe(DesignationType)

designation_scheme_recipe = Recipe(DesignationScheme)

designation_recipe = Recipe(Designation)

protected_site_recipe = Recipe(ProtectedSite)

soil_site_version_recipe = Recipe(
    SoilSiteVersion,
    import_started_at=timezone.now(),
    is_active=True,
    status="COMPLETED",
)

observation_recipe = Recipe(Observation, result={"value": 1.0})

phenomenon_type_recipe = Recipe(PhenomenonType)

soil_investigation_purpose_recipe = Recipe(SoilInvestigationPurpose)

observable_property_recipe = Recipe(ObservableProperty)

soil_site_recipe = Recipe(SoilSite)
