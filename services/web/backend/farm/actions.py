from pathlib import Path

from gql import gql

from django.conf import settings
from django.contrib import messages
from django.utils.translation import gettext_lazy as _

from farm.models import Campaign
from authentication.models import User

ROOT_PATH = Path(__file__).parent


def sync_holding(request, obj):

    if not settings.ENABLE_SYNC_HOLDING_FROM_ADMIN:
        messages.add_message(
            request,
            messages.ERROR,
            _('This feature is not activated.'),
        )
        return

    # Find the current active campaign
    active_campaign = Campaign.objects.filter(is_current=True).first()

    if active_campaign is None:
        messages.add_message(
            request,
            messages.ERROR,
            f"{_('No campaign has been set as the current campaign.')}",
        )
        return

    # Find a user related party to use to query the IACS
    user = User.objects.filter(user_related_parties__holding=obj).first()

    if user is None:
        messages.add_message(
            request,
            messages.ERROR,
            f"{_('No user has been added as a related to this farm. You need to add a user that has access rights to this farm in the IACS system, to be able to synchronize the data on his/her behalf.')}",
        )
        return

    try:
        mutation = (ROOT_PATH / "graphql/mutation_sync_holding.graphql").read_text()
        variables = {
            "holding_id": obj.id,
            "campaign_id": active_campaign.id,
            "iacs_year": None,
        }
        client = request.graphql_clients["fastplatform"]
        extra_headers = {"X-Hasura-User-Id": user.username, "X-Hasura-Role": "farmer"}
        
        response = client.execute(gql(mutation), variables, extra_headers=extra_headers)

        messages.add_message(
            request,
            messages.INFO,
            f"{_('The following plots have been created / updated')}: {', '.join(response['sync_holding']['plot_ids'])}.",
        )
    except Exception as exc:
        messages.add_message(
            request,
            messages.ERROR,
            f"{_('There was a error when synchronizing the plots of this farm from IACS')}: {str(exc)}",
        )
