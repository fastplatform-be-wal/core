from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings


from common.models import AbstractCodeList
from utils.models import GetAdminURLMixin
from external.models import AbstractModelVersion

class AgriPlotVersion(AbstractModelVersion):
    class Meta:
        db_table = "agri_plot_version"
        verbose_name = _("agricultural plot version")
        verbose_name_plural = _("agricultural plot versions")
        help_text = _("Versions of public agricultural plots objects")


class AgriPlot(GetAdminURLMixin, models.Model):
    """Agricultural plots

    These are not the plots of the farms of each farmer, but the general flat
    layer of plots that the farmer can pick from to add to his farm/campaign

    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    authority_id = models.CharField(
        null=False,
        max_length=100,
        verbose_name=_("authority identifier"),
        help_text=_("Must be unique within each version"),
    )

    version = models.ForeignKey(
        to="external.AgriPlotVersion",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="items",
        verbose_name=_("version"),
    )

    geometry = models.MultiPolygonField(
        srid=settings.DEFAULT_SRID, verbose_name=_("geometry")
    )

    # These 2 cannot be foreign keys, as the plant species and varieties in the other
    # database (fastplatform instead of external)
    plant_species_id = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("plant species")
    )
    plant_variety_id = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("plant variety")
    )

    area = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("area"),
        help_text=_("In square meters (m2)"),
    )

    valid_from = models.DateField(null=True, blank=True, verbose_name=_("valid from"))

    valid_to = models.DateField(null=True, blank=True, verbose_name=_("valid to"))

    source = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("source"),
        default=dict,
        help_text=_("The source data from which this object was derived."),
    )

    def __str__(self):
        return self.authority_id

    class Meta:
        db_table = "agri_plot"
        verbose_name = _("public agricultural plot")
        verbose_name_plural = _("public agricultural plots")
        help_text = _(
            "Base layer of all the agricultural plots, that the farmers can use to add to their campaigns in FaST."
        )

        constraints = [
            models.UniqueConstraint(
                fields=["authority_id", "version_id"],
                name="agri_plot_authority_id_version_id_unique",
            )
        ]

