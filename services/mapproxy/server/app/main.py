# WSGI module for use with Apache mod_wsgi or gunicorn

import os

from app.settings import config

from logging.config import fileConfig
from mapproxy.wsgiapp import make_wsgi_app


# logging configuration
# `mapproxy-util create -t log-ini`
fileConfig(
    config.APP_DIR / "logs/log.ini",
    defaults = {
        "log_level": config.LOG_LEVEL.upper(),
    }
)

file = config.APP_DIR / config.MAPPROXY_CONFIGURATION_FILE_PATH
app = make_wsgi_app(file, reloader=True)
