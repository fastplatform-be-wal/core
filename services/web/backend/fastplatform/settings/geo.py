import os

# Coordinate reference system for PostGIS columns
DEFAULT_SRID = int(os.environ.get("DEFAULT_SRID", 4258))  # ETRS89
DEFAULT_PROJECTED_SRID = int(
    os.environ.get("DEFAULT_PROJECTED_SRID", 3035)
)  # ETRS89-LAEA
DEFAULT_LEAFLET_SRID = int(
    os.environ.get("DEFAULT_LEAFLET_SRID", 3857)
)  # WGS 84 / Pseudo-Mercator

# Map server URL for TMS and WMS services used in Leaflet maps
MAP_SERVER_URL = os.environ.get("MAPPROXY_URL", "")
MAP_TILE_SERVICE_URL = MAP_SERVER_URL + "/tiles"
