from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings

from external.models import AbstractModelVersion
from utils.models import GetAdminURLMixin


class AbstractPhysicalWater(GetAdminURLMixin, models.Model):
    """Base class for SurfaceWater and WaterCourse"""

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    authority_id = models.CharField(
        null=False,
        blank=False,
        max_length=256,
        verbose_name=_("authority identifier"),
        help_text=_("Must be unique within each version"),
    )

    hydro_id = models.CharField(
        null=True, blank=True, max_length=256, verbose_name=_("hydrological identifier")
    )

    geometry = models.GeometryField(
        null=False, blank=False, srid=settings.DEFAULT_SRID, verbose_name=_("geometry")
    )

    geographical_name = models.CharField(
        null=True, blank=True, max_length=512, verbose_name=_("geographical name")
    )

    begin_lifespan_version = models.DateTimeField(
        null=True, blank=True, verbose_name=_("begin date/time of version")
    )

    end_lifespan_version = models.DateTimeField(
        null=True, blank=True, verbose_name=_("end date/time of version")
    )

    buffer = models.IntegerField(
        null=True, blank=True, default=0, verbose_name=_("buffer band width")
    )

    local_type = models.CharField(
        null=True, blank=True, max_length=256, verbose_name=_("local type")
    )

    source = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("source"),
        default=dict,
        help_text=_("The source data from which this object was derived."),
    )

    def __str__(self):
        return (
            self.authority_id
            if self.geographical_name is None
            else self.geographical_name
        )

    class Meta:
        abstract = True


class SurfaceWaterVersion(AbstractModelVersion):
    """A version of the surface water data"""

    class Meta:
        db_table = "surface_water_version"
        verbose_name = _("surface water version")
        verbose_name_plural = _("surface water versions")
        help_text = _("Versions of surface water objects")


class SurfaceWater(AbstractPhysicalWater):
    """A surface water body (ie still water)

    Inspire - Annex I - Hydrography - Physical Waters
    """

    version = models.ForeignKey(
        to="external.SurfaceWaterVersion",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="items",
        verbose_name=_("version"),
    )

    class Meta:
        db_table = "surface_water"
        verbose_name = _("surface water")
        verbose_name_plural = _("surface water")
        help_text = _("Any known inland waterway body")

        constraints = [
            models.UniqueConstraint(
                fields=["authority_id", "version_id"],
                name="surface_water_authority_id_version_id_unique",
            )
        ]


class WaterCourseVersion(AbstractModelVersion):
    """A version of the water course data"""

    class Meta:
        db_table = "water_course_version"
        verbose_name = _("water course version")
        verbose_name_plural = _("water course versions")
        help_text = _("Versions of water course objects")


class WaterCourse(AbstractPhysicalWater):
    """A water course (ie running water)

    Inspire - Annex I - Hydrography - Physical Waters
    """

    version = models.ForeignKey(
        to="external.WaterCourseVersion",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="items",
        verbose_name=_("version"),
    )

    class Meta:
        db_table = "water_course"
        verbose_name = _("water course")
        verbose_name_plural = _("water courses")
        help_text = _("A natural or man-made flowing watercourse or stream")

        constraints = [
            models.UniqueConstraint(
                fields=["authority_id", "version_id"],
                name="water_course_authority_id_version_id_unique",
            )
        ]
