files=`ls ../../../../$1/init/external/*$2.csv`

for filename in $files;
do
    base_name=`basename "$filename" .csv`;
    table_name=`echo "$base_name" | awk -F. '{print $2}'`
    echo "table_name: external $table_name"
    table_headers=`head -n 1 $filename`
    copy_sql="\\COPY public.$table_name($table_headers) FROM '$filename' DELIMITER ',' CSV HEADER;"
    PGPASSWORD=${POSTGRES_EXTERNAL_PASSWORD} psql -U ${POSTGRES_EXTERNAL_USER} -h localhost -p ${POSTGRES_EXTERNAL_PORT} -d ${POSTGRES_EXTERNAL_DATABASE} -c "$copy_sql"
    echo "-----------------------------------------"
done