# Generated by Django 3.2.7 on 2022-01-26 16:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farm', '0063_auto_20220126_1743'),
        ('common', '0015_unitofmeasure_i18n'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='administrativeunit',
            name='country',
        ),
        migrations.RemoveField(
            model_name='administrativeunit',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='unitofmeasurelinearconversion',
            name='unit_from',
        ),
        migrations.RemoveField(
            model_name='unitofmeasurelinearconversion',
            name='unit_to',
        ),
        migrations.DeleteModel(
            name='Address',
        ),
        migrations.DeleteModel(
            name='AdministrativeUnit',
        ),
        migrations.DeleteModel(
            name='UnitOfMeasureLinearConversion',
        ),
    ]
