# Generated by Django 3.0.5 on 2020-06-29 17:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0004_auto_20200626_1139'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(blank=True, db_table='user_group', to='authentication.Group'),
        ),
    ]
