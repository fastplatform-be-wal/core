import os

from . import *

DATABASES["default"]["HOST"] = os.environ.get(
    "POSTGRES_SUPER_HOST", DATABASES["default"]["HOST"]
)
DATABASES["default"]["PORT"] = os.environ.get(
    "POSTGRES_SUPER_PORT", DATABASES["default"]["PORT"]
)
DATABASES["default"]["USER"] = os.environ.get(
    "POSTGRES_SUPER_USER", DATABASES["default"]["USER"]
)
DATABASES["default"]["PASSWORD"] = os.environ.get(
    "POSTGRES_SUPER_PASSWORD", DATABASES["default"]["PASSWORD"]
)

DATABASES["external"]["HOST"] = os.environ.get(
    "POSTGRES_EXTERNAL_SUPER_HOST", DATABASES["external"]["HOST"]
)
DATABASES["external"]["PORT"] = os.environ.get(
    "POSTGRES_EXTERNAL_SUPER_PORT", DATABASES["external"]["PORT"]
)
DATABASES["external"]["USER"] = os.environ.get(
    "POSTGRES_EXTERNAL_SUPER_USER", DATABASES["external"]["USER"]
)
DATABASES["external"]["PASSWORD"] = os.environ.get(
    "POSTGRES_EXTERNAL_SUPER_PASSWORD", DATABASES["external"]["PASSWORD"]
)
