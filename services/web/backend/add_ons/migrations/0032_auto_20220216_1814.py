# Generated by Django 3.2.7 on 2022-02-16 17:14

from django.db import migrations, models
import utils.models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('add_ons', '0031_auto_20220126_1111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addon',
            name='i18n',
            field=models.JSONField(blank=True, null=True, verbose_name='Translations'),
        ),
        migrations.AlterField(
            model_name='addon',
            name='id',
            field=models.CharField(default=uuid.uuid4, max_length=50, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='provider',
            name='i18n',
            field=models.JSONField(blank=True, null=True, verbose_name='Translations'),
        ),
        migrations.AlterField(
            model_name='provider',
            name='id',
            field=models.CharField(default=uuid.uuid4, max_length=50, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
    ]
