-- use public schema by default
ALTER ROLE fastplatform SET search_path = public, fastplatform;

-- connect to 'external' database
\c external;

-- create pgcrypto extension, required for UUID
CREATE EXTENSION IF NOT EXISTS pgcrypto schema public;

-- Hasura requirements
-- create the schemas required by the hasura system
CREATE SCHEMA IF NOT EXISTS hdb_catalog;
CREATE SCHEMA IF NOT EXISTS hdb_views;
-- make the user an owner of system schemas
ALTER SCHEMA hdb_catalog OWNER TO fastplatform;
ALTER SCHEMA hdb_views OWNER TO fastplatform;

-- grant all privileges on all tables in the public schema
ALTER DEFAULT PRIVILEGES FOR USER postgres IN SCHEMA public GRANT ALL ON TABLES TO fastplatform;
ALTER DEFAULT PRIVILEGES FOR USER postgres IN SCHEMA public GRANT ALL ON FUNCTIONS TO fastplatform;
ALTER DEFAULT PRIVILEGES FOR USER postgres IN SCHEMA public GRANT ALL ON SEQUENCES TO fastplatform;
