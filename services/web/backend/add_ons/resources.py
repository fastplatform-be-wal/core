from common.resources import ModelResourceWithColumnIds
from add_ons.models import (
    Provider,
    ProviderMember,
    AddOn,
    AddOnAPIKey,
    AddOnSubscription,
    APIAccess,
)


class ProviderResource(ModelResourceWithColumnIds):
    class Meta:
        model = Provider


class ProviderMemberResource(ModelResourceWithColumnIds):
    class Meta:
        model = ProviderMember


class AddOnResource(ModelResourceWithColumnIds):
    class Meta:
        model = AddOn


class AddOnAPIKeyResource(ModelResourceWithColumnIds):
    class Meta:
        model = AddOnAPIKey


class AddOnSubscriptionResource(ModelResourceWithColumnIds):
    class Meta:
        model = AddOnSubscription


class APIAccessResource(ModelResourceWithColumnIds):
    class Meta:
        model = APIAccess