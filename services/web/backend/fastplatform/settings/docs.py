import os

DOCS_PLANTUML_SERVER_URL = os.environ.get(
    "DOCS_PLANTUML_SERVER_URL", "https://www.plantuml.com/plantuml"
)

DOCS_CACHE_MAX_AGE = int(os.environ.get("DOCS_CACHE_MAX_AGE", 3600))

DOCS_DEFAULT_LANGUAGE = os.environ.get("DOCS_DEFAULT_LANGUAGE", "en")
