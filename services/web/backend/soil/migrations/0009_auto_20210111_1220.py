# Generated by Django 3.0.7 on 2021-01-11 11:20

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('farm', '0043_generate_fk_cascade_farm'),
        ('soil', '0008_generate_fk_cascade_soil'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='derivedobservation',
            options={'verbose_name': 'estimated soil properties observation', 'verbose_name_plural': 'estimated soil properties observations'},
        ),
        migrations.AlterModelOptions(
            name='observation',
            options={'verbose_name': 'soil sample observation', 'verbose_name_plural': 'soil sample observations'},
        ),
        migrations.AlterModelOptions(
            name='phenomenontype',
            options={'verbose_name': 'soil phenomenon type', 'verbose_name_plural': 'soil phenomenon types'},
        ),
        migrations.AlterModelOptions(
            name='soilderivedobject',
            options={'verbose_name': 'estimated soil properties', 'verbose_name_plural': 'estimated soil properties'},
        ),
        migrations.AlterModelOptions(
            name='soilsite',
            options={'verbose_name': 'soil sample', 'verbose_name_plural': 'soil samples'},
        ),
        migrations.AlterField(
            model_name='derivedobservation',
            name='soil_derived_object',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='derived_observations', to='soil.SoilDerivedObject', verbose_name='estimated soil properties'),
        ),
        migrations.AlterField(
            model_name='soilderivedobject',
            name='is_derived_from',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default=dict, null=True, verbose_name='derived from'),
        ),
        migrations.AlterField(
            model_name='soilderivedobject',
            name='plot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='soil_derived_objects', to='farm.Plot', verbose_name='plot'),
        ),
        migrations.AlterField(
            model_name='soilderivedobject',
            name='soil_sites_count',
            field=models.IntegerField(help_text='Number of soil samples used to estimate the soil properties', verbose_name='count of soil samples'),
        ),
    ]
