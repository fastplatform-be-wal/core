# `core/mapproxy`: Map raster tiles proxy and cache

The [core/mapproxy](./) service computes the raster tiles that are displayed in the FaST maps (in the farmer mobile application and in the Administration Portal). These raster tiles are used for the *base layers* and *overlays* in the maps.

<div align="center">
    <img src="docs/img/map-raster-layers.png" width="420">
    <div>Raster (base) layers</div>
    <br>
</div>

The service is a *proxy* between the FaST map raster tiles and the *original source of the tiles*. It performs the following operations:
- tile re-projection (if necessary)
- tile caching: for performance, cost and to respect hit rate limits that might be imposed by the original source

As the raster layers are configurable by a FaST administrator (in the Administration Portal), the service dynamically reacts to changes in the configuration: layers can be added, removed, re-ordered, and their properties can be modified ; the proxy service will automatically react to these changes and start (re)computing tiles automatically.

The tiles are computed and served under the [XYZ/Slippy Map tile scheme](https://wiki.openstreetmap.org/wiki/Slippy_Map) and under the EPSG:3857 projection. They are computed *live* when requested and stored in the cache.

This service serves only **raster tiles**, not vector tiles. It is the [core/web/vector-tiles](../web/vector_tiles/) service that serves vector tiles in FaST. In addition, if it is installed, the [addons/sobloo](https://gitlab.com/fastplatform/addons/sobloo) add-on serves tiles for the RGB and NDVI satellite imagery (that are independent from the base layers and overlays).

The [core/mapproxy](./) service is composed of 2 sub-services:
- [core/mapproxy/server](./server): the MapProxy server
- [core/mapproxy/api](./api): the event handler that dynamically updates the MapProxy server configuration

## Architecture

The raster tiles are requested by the map components instantiated in both the farmer mobile application and the Administration Portal. All the map components use the same underlying library: [Leaflet](https://leafletjs.com/). The requested tiles come in the form of HTTP requests, where the URL defines the layer and the geographical extent of the requested tile.

For example, the below query will request the tile column `4207`, row `2675` of zoom level `13` of layer `spw_orthophotos_2021` (with projection `EPSG:3857`):

```http
https://map.beta.be-wal.fastplatform.eu/tiles/spw_orthophotos_2021_EPSG3857/EPSG3857/13/4207/2765.png
```

The query is received by the [MapProxy](https://mapproxy.org/) server, which then:
1. looks for the tile in the S3 cache and returns it if found
2. if not found, download the necessary data from the original source (in the example: the [OrthoPhoto 2021 WMS service of the Service Public de Wallonie](https://geoportail.wallonie.be/catalogue/7608c4c6-1434-4291-940c-8b9c8da64484.html))
3. generates the tile from the downloaded data
4. reprojects the tile to the EPSG:3857 projection
5. caches the tile in the S3 cache
6. returns the tile

Because the tiles are computed on-the-fly and then cached, when a new layer is added to the configuration, the first tiles being requested will take a few seconds to be processed and displayed on the client maps: the reactivity depends on the current load of the FaST system, on the performance of the distant original source (the WMS/TMS server) and on network conditions. FaST currently performs no seeding of the cache, i.e. the cache is not pre-filled with computed tiles, but only when the first tiles are requested.

The following diagram shows the architecture of the service (when requesting tiles):

```plantuml
actor "User" as user
actor "Admin" as admin

component "FaST app" as app {
    component "Leaflet maps" as leaflet_app
    component "Browser cache" as browser_cache_app #line.dashed
}
component "FaST Administration Portal" as portal {
    component "Leaflet maps" as leaflet_portal
    component "Browser cache" as browser_cache_portal #line.dashed
}

rectangle "FaST backend" as backend {
    component "Map raster tiles proxy\n""core/mapproxy/server""" as mapproxy << MapProxy >>  #yellow {
        file "Layer configuration" as conf #yellow
    }
    storage "Cache storage" as cache  << S3 >> #yellow
    
}

cloud "OpenStreetMap" as osm <<TMS endpoint>>
cloud "Google Satellite" as google_sat <<TMS endpoint>>
cloud "..." as another
cloud "OrthoPhotos 2021" as ortho <<WMS endpoint>>

user --> app
admin --> portal

leaflet_app --> browser_cache_app
browser_cache_app --> mapproxy

leaflet_portal --> browser_cache_portal
browser_cache_portal --> mapproxy

mapproxy <-> cache

mapproxy --> osm
mapproxy --> google_sat
mapproxy --> another
mapproxy --> ortho
```

### Dynamic layer configuration

The raster layers in the FaST maps (base layers and overlays) are configurable in the Administration Portal:

<div align="center">
    <img src="docs/img/admin-layers-index.png" width="420">
    <br>
</div>
<div align="center">
    <img src="docs/img/admin-base-layers-list.png" width="420">
    <div>Base layers and overlays configuration in the Admin Portal</div>
    <br>
</div>

The following types of layers can added, modified, removed:
- *base layers*: are displayed in the background of the map, *only one base layer can be active at a time*
- *overlays*: are displayed above the base layers and below the vector layers, multiple overlays can be active at the same time

The configuration options for the base layers and the overlays are the same:
- *id*: unique identifier
- *name*: display name of the layer
- Display parameters:
    - *minimum zoom*: minimum zoom level at which the layer is displayed
    - *maximum zoom*: maximum zoom level at which the layer is displayed
    - *active in farmer app*: whether the layer is displayed in the farmer mobile application
    - *active in admin portal*: whether the layer is displayed in the Administration Portal
    - *display by default*:
        - if the layer is a base layer: this is the base layer that is displayed by default, all other base layers are hidden
        - if the layer is an overlay: this overlay is displayed by default
- Source parameters:
    - *source type*: the type of the source: WMS and TMS are currently supported
    - *URL of the source*: the URL of the source
        - in case of WMS source, the URL is the base URL of the WMS server,w ith no parameters, e.g. https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2021/MapServer/WMSServer
        - in case of a TMS source, the URL must contain `{z}`, `{x}`, `{y}` (and optionally `{s}`) placeholders, as per OGC TMS, e.g. [https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png]()
    - *maximum native zoom*: the maximum zoom level at which the source is available (for zoom levels above this level and below the *maximum zoom* parameter above, the tiles will be interpolated on the client)
    - *attribution*: the attribution string of the source, e.g. `&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors`, that xwill be displayed at the bottom of the maps when the layer is displayed
- WMS-specific parameters: to be filled only when the *source type* is WMS
    - *source layer(s)*: the name of the layer(s) to be displayed, separated by a comma, e.g. `0` for the first layer, `0,1` for the first and second layer, `0,1,2` for the first, second and third layer
    - *styles*: the style of the layer(s) to be displayed, separated by a comma, to be left blank to use the default style of the layer
    - *source format*: the format of the images being requested, e.g. `image/png`
    - *server version*: the version of the WMS server, e.g. `1.3.0`
    - *transparent*: whether the layer contains any alpha transparency (alpha channel)
    - *projection name*: the name of the source projection, in `EPSG:XXXX` format

Every time base layer or an overlay is added, modified or removed, the following chain reaction happens:
1. a row is modified on the `map_base_layer` or `map_overlay` table of the FaST database
2. this triggers an event from the FaST API gateway (see [core/api_gateway](../api_gateway/))
3. this event is forwarded to the [core/mapproxy/api](./api) service
4. the service handles the event and re-generates the MapProxy configuration file based on the updated layer configuration
5. the updated configuration is applied on the [core/mapproxy/server](./server) service through an update to the corresponding Kubernetes ConfigMap
6. the updated configuration is picked up by the MapProxy server and reloaded

This process takes approximately 30 seconds.

```plantuml
actor "Admin" as admin
component "FaST Administration Portal\n""core/web/backend""" as portal {
    component "Map base layer &\noverlay configuration" as configuration
}

database postgis << PostGIS >> [
    Database
    ===
    Tables
    ---
    map_base_layer
    map_overlay
    ...
]

component "API Gateway\n""core/api_gateway""" as api_gateway << Hasura >>

component "Map raster tiles proxy\n""core/mapproxy/server""" as mapproxy << MapProxy >>  #yellow {
    file "Configuration" as conf #yellow
}

component "Event handler\n""core/mapproxy/api""" as mapproxy_api << Python >>  #yellow

admin --> configuration : Edit configuration
portal --> postgis : Table row change
postgis -> api_gateway : Trigger event
api_gateway --> mapproxy_api : Forward event
mapproxy_api -> conf : Update configuration
```

## Development

Further documentation on development is available for each of the sub-services:
- [core/mapproxy/server](./server)
- [core/mapproxy/api](./api)
