```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "messaging.Queue <Messaging>" as messaging.Queue #d6f4eb {
    Queue
    ..
    A collection of tickets
    --
    - id (AutoField) - 
    + title (CharField) - 
    # groups (ManyToManyField) - 
    --
}


class "messaging.Ticket <Messaging>" as messaging.Ticket #d6f4eb {
    ticket
    ..
    A ticket is a two-way communication between the Paying Agency (users with
"staff" status) and
    a **holding** (not a specific user, but all the users that are related
parties to this holding)
    --
    - id (AutoField) - 
    ~ queue (ForeignKey) - Queues you are a member of
    + created_at (DateTimeField) - 
    ~ created_by (ForeignKey) - 
    ~ holding (ForeignKey) - Only members of this holding will be able to view and
reply.
    ~ assigned_to (ForeignKey) - 
    + title (CharField) - 
    + body (TextField) - The text content of the ticket. Will appear at the top of
the conversation in the app.
    + status (CharField) - 
    + resolution (TextField) - (Optional) The resolution provided to the ticket.
Will appear at the top of the conversation in the app.
    + priority (IntegerField) - 1 = Highest Priority, 5 = Low Priority
    --
}
messaging.Ticket *-- messaging.Queue


class "messaging.TicketMessage <Messaging>" as messaging.TicketMessage #d6f4eb {
    ticket message
    ..
    A message of a conversation/ticket
    --
    - id (AutoField) - 
    ~ ticket (ForeignKey) - 
    + created_at (DateTimeField) - 
    ~ created_by (ForeignKey) - 
    + body (TextField) - The body is overridden with '<<< FaST GeoTaggedPhoto
id=XXX >>>' when a geo-tagged photo is attached to the message
    + file (FileField) - 
    + file_name (CharField) - 
    + file_type (CharField) - 
    + file_size (IntegerField) - 
    ~ geo_tagged_photo (ForeignKey) - (Optional) The photo attached to the message
(mutually exclusive with the body)
    --
}
messaging.TicketMessage *-- messaging.Ticket


class "messaging.Broadcast <Messaging>" as messaging.Broadcast #d6f4eb {
    broadcast
    ..
    A one-way notification sent by a a Paying Agency staff user to all holdings
    --
    - id (AutoField) - 
    + title (CharField) - Limited to 48 characters.
    + body (TextField) - Limited to 178 characters to fit in a push notification.
    + icon (ImageField) - Try to have an image as square as possible. If not
provided, FaST default icon will be used.
    + status (CharField) - 
    + is_sending_triggered (BooleanField) - 
    + sent_at (DateTimeField) - 
    ~ sent_by (ForeignKey) - 
    --
}


class "messaging.BroadcastUserReadStatus <Messaging>" as messaging.BroadcastUserReadStatus #d6f4eb {
    Broadcast user read status
    ..
    A joined model to set read status between a User and a Broadcast
    --
    - id (AutoField) - 
    ~ broadcast (ForeignKey) - 
    ~ user (ForeignKey) - 
    + read_at (DateTimeField) - The date/time (UTC) at which the broadcast was
read.
    --
}
messaging.BroadcastUserReadStatus *-- messaging.Broadcast


class "messaging.TicketUserReadStatus <Messaging>" as messaging.TicketUserReadStatus #d6f4eb {
    Ticket user read status
    ..
    A joined model to set read status between a User and a Ticket
    --
    - id (AutoField) - 
    ~ ticket (ForeignKey) - 
    ~ user (ForeignKey) - 
    + read_at (DateTimeField) - The date/time (UTC) at which the ticket was read.
    ~ last_read_ticket_message (ForeignKey) - 
    --
}
messaging.TicketUserReadStatus *-- messaging.Ticket
messaging.TicketUserReadStatus *-- messaging.TicketMessage


@enduml
```