from django import forms
from django.utils.translation import gettext_lazy as _

from soil.models import DerivedObservation



class DerivedObservationInlineForm(forms.ModelForm):
    """
    Override the default form for the Observation inline
    to deal with the result JSONField
    """

    class Meta:
        model = DerivedObservation
        fields = ("id", "observed_property", "result")
        widgets = {
            "result": forms.Textarea(attrs={"rows": 1, "cols": 60}),
        }