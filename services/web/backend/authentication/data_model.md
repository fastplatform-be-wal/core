```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "authentication.FederatedProvider <Authentication & Authorization>" as authentication.FederatedProvider #ded6f4 {
    federated identity provider
    ..
    A identity provider that FaST can delegate to for guaranteeing
    user identity and logging into FaST
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - The name of the federated identity provider, as displayed
on the login screen. Try to keep it short.
    + description (TextField) - A description of the identity provider. Keep it
short as well.
    + logo (ImageField) - 
    + url (URLField) - The URL of the login page of the identity provider, that
FaST will redirect the user to
    + background_color (ColorField) - The background color of the button on the
login screen
    + text_color (ColorField) - The text color of the button on the login screen
    + is_active (BooleanField) - If this box is not checked, the identity provider
will not be proposed to the users.
    --
}


class "authentication.FederatedProviderLogEntry <Authentication & Authorization>" as authentication.FederatedProviderLogEntry #ded6f4 {
    federated provider log entry
    ..
    Audit trail entries for the federated identity providers
    --
    - id (AutoField) - 
    ~ federated_provider (ForeignKey) - 
    + entry_time (DateTimeField) - 
    + entry_type (CharField) - 
    + username (CharField) - 
    + ip_address (GenericIPAddressField) - 
    + user_agent (CharField) - 
    + path (CharField) - 
    + data (JSONField) - 
    --
}
authentication.FederatedProviderLogEntry *-- authentication.FederatedProvider


class "authentication.Country <Authentication & Authorization>" as authentication.Country #ded6f4 {
    country
    ..
    Country(iso_code, name, default_language)
    --
    + iso_code (CharField) - The ISO code of the country as per ISO 3166-1 alpha-2
    + name (TextField) - The name of the country in its local language
    ~ default_language (ForeignKey) - The default/official language of the country
    # languages (ManyToManyField) - The acceptable languages for this country
    --
}
authentication.Country *-- authentication.Language
authentication.Country *--* authentication.Language


class "authentication.Region <Authentication & Authorization>" as authentication.Region #ded6f4 {
    region
    ..
    Region(id, iso_code, name, country, default_language)
    --
    + id (CharField) - The region identifier, as XX-YYY, where XX is the ISO 3166-1
alpha 2 code of the country and YY is the ISO 3166-2 alpha 2 code of the region.
    + iso_code (CharField) - The ISO 3166-2 alpha 2 code of the region
    + name (CharField) - The region name in its local language
    ~ country (ForeignKey) - The country this region belongs in
    ~ default_language (ForeignKey) - The default/official language of the region
    # languages (ManyToManyField) - The acceptable languages of the region
    --
}
authentication.Region *-- authentication.Country
authentication.Region *-- authentication.Language
authentication.Region *--* authentication.Language


class "authentication.Language <Authentication & Authorization>" as authentication.Language #ded6f4 {
    language
    ..
    A language available in the system
    --
    + iso_code (CharField) - The ISO 639-1 code of the language
    + name (CharField) - The name of the language
    --
}


class "authentication.User <Authentication & Authorization>" as authentication.User #ded6f4 {
    User
    ..
    A user of the FaST platform
    Can be a farmer, advisor or PA staff, or all at once
    --
    + password (CharField) - 
    + last_login (DateTimeField) - 
    + is_superuser (BooleanField) - Designates that this user has all permissions
without explicitly assigning them.
    + is_staff (BooleanField) - Designates whether the user can log into this admin
site.
    + is_active (BooleanField) - Designates whether this user should be treated as
active. Unselect this instead of deleting accounts.
    + date_joined (DateTimeField) - 
    + id (CharField) - 
    + username (CharField) - Required. 255 characters or fewer. Letters, digits and
@/./+/-/_ only. For users authenticated through a third party service, this
username is determined by the the third party service. The username MUST be
unique accross the FaST environment.
    + first_name (CharField) - The first name(s) of the user
    + last_name (CharField) - The last name(s) of the user
    + name (CharField) - The full name of the user. This field is not computed
based on first name and last name, and must be filled manually.
    ~ region (ForeignKey) - The region this user belongs to
    + picture (ImageField) - 
    ~ preferred_language (ForeignKey) - 
    + additional_data (JSONField) - Additional data stored for the user, as a key-
value JSON, usually to store region-specific authentication data.
    + email (EmailField) - Email address of the user, if provided by the third-
party authentication system or by the user
    # user_permissions (ManyToManyField) - Specific permissions for this user.
    # groups (ManyToManyField) - 
    --
}
authentication.User *-- authentication.Region
authentication.User *-- authentication.Language


class "authentication.Group <Authentication & Authorization>" as authentication.Group #ded6f4 {
    group
    ..
    Group(id, name)
    --
    - id (AutoField) - 
    + name (CharField) - 
    # permissions (ManyToManyField) - 
    --
}


class "authentication.TermsAndConditionsConsentRequest <Authentication & Authorization>" as authentication.TermsAndConditionsConsentRequest #ded6f4 {
    terms & conditions consent request
    ..
    TermsAndConditionsConsentRequest(i18n, id, activate_at, deactivate_at, title,
short_description, url, html_content, in_farmer_app, in_administration_portal)
    --
    + i18n (JSONField) - 
    - id (AutoField) - 
    + activate_at (DateTimeField) - Consent will not be requested from users before
that date (can be left blank).
    + deactivate_at (DateTimeField) - Consent will not be requested from users
after that date (can be left blank).
    + title (CharField) - 
    + short_description (TextField) - 
    + url (URLField) - 
    + html_content (TextField) - 
    + in_farmer_app (BooleanField) - Consent will be requested in the farmer mobile
and web app
    + in_administration_portal (BooleanField) - Consent will be requested in the
Administration Portal
    --
}


class "authentication.UserTermsAndConditionsConsent <Authentication & Authorization>" as authentication.UserTermsAndConditionsConsent #ded6f4 {
    user terms & conditions consent
    ..
    UserTermsAndConditionsConsent(id, user, consent_request, consent_received_at,
metadata)
    --
    - id (AutoField) - 
    ~ user (ForeignKey) - 
    ~ consent_request (ForeignKey) - 
    + consent_received_at (DateTimeField) - 
    + metadata (JSONField) - 
    --
}
authentication.UserTermsAndConditionsConsent *-- authentication.User
authentication.UserTermsAndConditionsConsent *-- authentication.TermsAndConditionsConsentRequest


class "authentication.UserIACSConsent <Authentication & Authorization>" as authentication.UserIACSConsent #ded6f4 {
    user IACS consent
    ..
    UserIACSConsent(id, user, consent_received_at, metadata)
    --
    - id (AutoField) - 
    ~ user (ForeignKey) - 
    + consent_received_at (DateTimeField) - 
    + metadata (JSONField) - 
    --
}
authentication.UserIACSConsent *-- authentication.User


class "authentication.OIDCClient <Authentication & Authorization>" as authentication.OIDCClient #ded6f4 {
    OIDC Client
    ..
    OIDCClient(id, name, owner, client_type, client_id, client_secret, jwt_alg,
date_created, website_url, terms_url, contact_email, logo, reuse_consent,
require_consent, require_pkce, _redirect_uris, _post_logout_redirect_uris,
_scope)
    --
    + id (BigAutoField) - 
    + name (CharField) - 
    ~ owner (ForeignKey) - 
    + client_type (CharField) - <b>Confidential</b> clients are capable of
maintaining the confidentiality of their credentials. <b>Public</b> clients are
incapable.
    + client_id (CharField) - 
    + client_secret (CharField) - 
    + jwt_alg (CharField) - Algorithm used to encode ID Tokens.
    + date_created (DateField) - 
    + website_url (CharField) - 
    + terms_url (CharField) - External reference to the privacy policy of the
client.
    + contact_email (CharField) - 
    + logo (FileField) - 
    + reuse_consent (BooleanField) - If enabled, server will save the user consent
given to a specific client, so that user won't be prompted for the same
authorization multiple times.
    + require_consent (BooleanField) - If disabled, the Server will NEVER ask the
user for consent.
    + require_pkce (BooleanField) - Require PKCE (RFC7636) for public clients. You
should only disable this if you know that the client does not support it. Has no
effect on confidential clients.
    + _redirect_uris (TextField) - Enter each URI on a new line. For native
applications using looback addresses the port will be ignored to support
applications binding to ephemeral ports.
    + _post_logout_redirect_uris (TextField) - Enter each URI on a new line.
    + _scope (TextField) - Specifies the authorized scope values for the client
app.
    # response_types (ManyToManyField) - 
    --
}
authentication.OIDCClient *-- authentication.User


class "authentication.OIDCCode <Authentication & Authorization>" as authentication.OIDCCode #ded6f4 {
    OIDC Authorization code
    ..
    OIDCCode(id, client, _scope, user, code, nonce, is_authentication,
code_challenge, code_challenge_method, expires_at)
    --
    + id (BigAutoField) - 
    ~ client (ForeignKey) - 
    + _scope (TextField) - 
    ~ user (ForeignKey) - 
    + code (CharField) - 
    + nonce (CharField) - 
    + is_authentication (BooleanField) - 
    + code_challenge (CharField) - 
    + code_challenge_method (CharField) - 
    + expires_at (DateTimeField) - 
    --
}
authentication.OIDCCode *-- authentication.User


class "authentication.OIDCToken <Authentication & Authorization>" as authentication.OIDCToken #ded6f4 {
    OIDC Token
    ..
    OIDCToken(id, client, _scope, issued_at, user, access_token, access_expires_at,
refresh_token, _id_token)
    --
    + id (BigAutoField) - 
    ~ client (ForeignKey) - 
    + _scope (TextField) - 
    + issued_at (DateTimeField) - 
    ~ user (ForeignKey) - 
    + access_token (CharField) - 
    + access_expires_at (DateTimeField) - 
    + refresh_token (CharField) - 
    + _id_token (TextField) - 
    --
}
authentication.OIDCToken *-- authentication.User


class "authentication.LoginAccessAttempt <Authentication & Authorization>" as authentication.LoginAccessAttempt #ded6f4 {
    Login attempt
    ..
    LoginAccessAttempt(id, user_agent, ip_address, username, http_accept, path_info,
attempt_time, get_data, post_data, failures_since_start)
    --
    - id (AutoField) - 
    + user_agent (CharField) - 
    + ip_address (GenericIPAddressField) - 
    + username (CharField) - 
    + http_accept (CharField) - 
    + path_info (CharField) - 
    + attempt_time (DateTimeField) - 
    + get_data (TextField) - 
    + post_data (TextField) - 
    + failures_since_start (PositiveIntegerField) - 
    --
}


class "authentication.LoginAccessLog <Authentication & Authorization>" as authentication.LoginAccessLog #ded6f4 {
    Login access log
    ..
    LoginAccessLog(id, user_agent, ip_address, username, http_accept, path_info,
attempt_time, logout_time)
    --
    - id (AutoField) - 
    + user_agent (CharField) - 
    + ip_address (GenericIPAddressField) - 
    + username (CharField) - 
    + http_accept (CharField) - 
    + path_info (CharField) - 
    + attempt_time (DateTimeField) - 
    + logout_time (DateTimeField) - 
    --
}


class "authentication.UserSession <Authentication & Authorization>" as authentication.UserSession #ded6f4 {
    User session
    ..
    UserSession(session_key, session_data, expire_date, user, user_agent,
last_activity, ip)
    --
    + session_key (CharField) - 
    + session_data (TextField) - 
    + expire_date (DateTimeField) - 
    ~ user (ForeignKey) - 
    + user_agent (CharField) - 
    + last_activity (DateTimeField) - 
    + ip (GenericIPAddressField) - 
    --
}
authentication.UserSession *-- authentication.User


@enduml
```