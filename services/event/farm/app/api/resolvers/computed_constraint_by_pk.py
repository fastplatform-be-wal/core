import logging
from opentelemetry import trace
from gql import gql

from app.api.types.computed_constraint import ComputedConstraint
from app.settings import config
from app.db.graphql_clients import fastplatform
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError

# Log
logger = logging.getLogger(__name__)


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class ComputedConstraintByPKResolver:
    async def resolve(self, id: int):
        """
        Resolve a constraint by its primary key ("id"),
        and return a graphene ObjectType of type ComputedConstraint.

        Args:
            id (int): the id of the constraint

        Raises:
            GraphQLHoldingPlotNotFoundError: No plot associated with the id provided.
            GraphQLUnexpectedDataError: The graphql operation could not be performed successfully.

        Returns:
            (graphene ObjectType):  the ComputedConstraint associated to the id provided.
        """
        logger.info("ComputedConstraintByPKResolver: resolve()")
        try:
            return await self.get_constraint_by_pk(id)

        except GraphQLHoldingPlotNotFoundError as ex:
            raise GraphQLHoldingPlotNotFoundError() from ex

        except Exception as ex:
            logger.exception(
                "ComputedConstraintByPKResolver unexpected error with id: ,%s", id
            )
            raise GraphQLUnexpectedDataError() from ex

    async def get_constraint_by_pk(self, id: int):
        logger.info("constraint by pk: get_plot_constraints() for id: %s", id)

        with tracer().start_as_current_span("get_plot_constraints"):
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_constraint_by_pk.graphql"
            ).read_text()

            variables = {"id": id}

            response = await fastplatform.execute(gql(query), variables)

            constraint = response["constraint_by_pk"]
            if constraint == None:
                logger.info("No constraint found for id: %s", id)
                return None

            return ComputedConstraint(
                id=int(constraint["id"]),
                name=constraint["name"],
                description=constraint["description"],
                plot_id=constraint["plot_id"],
            )
