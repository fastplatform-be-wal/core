import json
from graphql import GraphQLError


class GraphQLUnexpectedDataError(GraphQLError):
    """
    The graphql operation could not be performed successfully.

    Args:
        GraphQLError: A GraphQLError describes an Error found during the parse,
                        validate, or execute phases of performing a GraphQL operation.

    """

    message = "UNEXPECTED_DATA_ERROR"

    def __init__(self, *args, **kwargs):
        super().__init__(GraphQLUnexpectedDataError.message, *args, **kwargs)

    def __str__(self):
        return json.dumps(self.__dict__)


class GraphQLHoldingPlotNotFoundError(GraphQLError):
    """
    No plot associated with the id provided.

    Args:
        GraphQLError: A GraphQLError describes an Error found during the parse,
                        validate, or execute phases of performing a GraphQL operation.

    Returns:
        [type]: [description]
    """

    message = "HOLDING_PLOT_NOT_FOUND_ERROR"

    def __init__(self, *args, **kwargs):
        super().__init__(GraphQLHoldingPlotNotFoundError.message, *args, **kwargs)

    def __str__(self):
        return json.dumps(self.__dict__)
