from jinja2 import Environment, PackageLoader, select_autoescape
import logging
import io
import base64
import pytz
from datetime import datetime as dt
from app.settings import config
from staticmap import Polygon
from app.lib.static_map import StaticMapNoError

# Log
logger = logging.getLogger(__name__)


jinja_env = Environment(
  loader=PackageLoader("app", "templates"),
  autoescape=select_autoescape(["html"]),
  trim_blocks=True,
  lstrip_blocks=True,
)
# Jinja datetime format filter
def datetimeformat(value, format="%Y-%m-%d %H:%M:%S", value_format="%Y-%m-%dT%H:%M:%S.%f%z", value_timezone="UTC"):
  return dt.strptime(
    value, value_format
  ).replace(
    tzinfo=pytz.timezone(value_timezone)
  ).astimezone(
    pytz.timezone(config.TIME_ZONE)
  ).strftime(format)

jinja_env.filters['datetimeformat'] = datetimeformat


def get_fast_logo():
  template = jinja_env.get_template('fast_logo.html')
  return template.render()


def get_logo_base64(logo_base64):
  template = jinja_env.get_template('logo_base64.html')
  return template.render({'logo_base64': logo_base64})


def get_plot_map_image(map_image_data):
  template = jinja_env.get_template('plot_map.html')
  return template.render({'plot_map_image': map_image_data})


def get_footer():
  template = jinja_env.get_template('footer.html')
  return template.render()


def get_header(data):
  template = jinja_env.get_template('header.html')
  return template.render(data)


def generate_plot_map_images(data):
  for k, v in data.items():
    if k == "plot_preview":
      data[k] = generate_plot_map_image(v['plot_geometry']['coordinates'][0][0], v['base_layer_url'])
    elif isinstance(v, dict):
      generate_plot_map_images(v)


def generate_plot_map_image(plot_coordinates, base_layer_url="https://a.tile.osm.org/{z}/{x}/{y}.png"):
  base_layer_url = base_layer_url.replace("{s}", "a")
  m = StaticMapNoError(800, 350, url_template=base_layer_url)
  # plot_polygon = Polygon(plot_coordinates, '#1C9ACD96', '#1C9ACD') # blue
  plot_polygon = Polygon(plot_coordinates, '#F2340096', '#F23400') # orange
  m.add_polygon(plot_polygon)
  image = m.render()
  image_data = io.BytesIO()
  image.save(image_data, format="PNG")
  image_data.seek(0)
  return get_plot_map_image(base64.b64encode(image_data.getvalue()).decode())
