import json
from pathlib import Path
import glob
import os
import re
import shutil
import logging
import requests
from typing import List

from django.conf import settings
from django.core.management.base import BaseCommand

import markdown
from markdown.extensions.toc import TocExtension
from docs.management.commands.plantuml_markdown import PlantUMLMarkdownExtension
from mdx_linkify.mdx_linkify import LinkifyExtension

from whoosh.index import create_in
from whoosh.fields import TEXT, ID, Schema

from bs4 import BeautifulSoup

DOCS_ROOT_DIR: str = os.path.join(settings.BASE_DIR, "docs")

BUILD_DIR: str = os.path.join(DOCS_ROOT_DIR, "build")
INDEX_DIR: str = os.path.join(BUILD_DIR, "index")
HTML_DIR: str = os.path.join(BUILD_DIR, "html")

CACHE_DIR: str = os.path.join(DOCS_ROOT_DIR, "cache")

PLANTUML_CACHE_DIR: str = os.path.join(settings.BASE_DIR, "data", "plantuml")

LANGUAGE_CODES: List[str] = [settings.DOCS_DEFAULT_LANGUAGE] + [
    lc for (lc, _) in settings.LANGUAGES if lc != settings.DOCS_DEFAULT_LANGUAGE
]

DEEPL_API_URL: str = os.environ.get("DEEPL_API_URL")
DEEPL_API_KEY: str = os.environ.get("DEEPL_API_KEY")

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Build the documentation to html and index"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.verbosity = 1

    def _init_directories(self):
        # Initialize the directories
        if os.path.exists(BUILD_DIR):
            shutil.rmtree(BUILD_DIR)

        try:
            os.mkdir(BUILD_DIR)
            os.mkdir(INDEX_DIR)
            os.mkdir(HTML_DIR)
            os.mkdir(CACHE_DIR)
        except FileExistsError:
            pass

    def _init_index(self):
        index_schema = Schema(
            title=TEXT(stored=True),
            language_code=ID(stored=True),
            url=ID(stored=True),
            content=TEXT(stored=True),
        )
        index = create_in(INDEX_DIR, index_schema)
        self.writer = index.writer()

    def _markdown_to_html(self, md):
        """Parse a markdown file and convert it to HTML

        Args:
            md (str): Markdown content

        Returns:
            BeautifulSoup: HTML content
        """

        markdown_processor = markdown.Markdown(
            extensions=[
                # Convert the PlantUML diagrams to inline SVGs
                PlantUMLMarkdownExtension(
                    format="svg",
                    cachedir=PLANTUML_CACHE_DIR,
                    priority=1000,
                ),
                # Extract the document table of contents
                TocExtension(
                    toc_depth="1-3",
                    slugify=lambda value, separator: "toc-header",
                ),
                # Standard Markdown post-processors
                "tables",
                "footnotes",
                LinkifyExtension(linker_options={"parse_email": True}),
                "fenced_code",
                "codehilite",
            ],
            extension_configs={"codehilite": {}},
        )

        # Convert to HTML (this will inline all the images and diagrams)
        html = markdown_processor.convert(md)

        # Parse the HTML file to extract the TOC and index the content
        return BeautifulSoup(html, features="html.parser"), getattr(
            markdown_processor, "toc", ""
        )

    def _post_process_html(self, soup):
        """Fix some of the links from the HTML

        The links (for images and anchors) in the markdown files are designed
        for the documentation to be readable in GitLab. Of course, once
        converted to HTML and public in the Admin Portal of FaST, the URLs have
        to be changed to match the portal URL scheme.

        Args:
            soup (BeautifulSoup): The current parsed HTML

        Returns:
            BeautifulSoup: The parsed HTML with edited links
        """
        for img in soup.find_all("img"):
            # Replace the hardcoded path to the static
            # with the corresponding {% static xx %} template tag
            static_prefix = "/services/web/backend/docs/static/"
            if img["src"].startswith(static_prefix):
                img["src"] = "{{% static '{}' %}}".format(
                    img["src"][len(static_prefix) :]
                )

        for a in soup.find_all("a"):
            # Same for the links
            link_prefix = "/services/web/backend/docs/en/"
            if a["href"].startswith(link_prefix):
                a["href"] = "/docs/" + a["href"][len(link_prefix) :]
            if a["href"].endswith(".md"):
                a["href"] = a["href"][: -len(".md")] + "/"

        return soup

    def _path_to_url(self, relative_path):
        """Convert the relative path to a markdown file into a valid URL

        Args:
            relative_path (str): Relative path

        Returns:
            str: URL
        """
        # This relative path determines the URL
        # (note that the URL does *not* contain the language anymore ;
        # the language will be determined when view is rendered)
        url, ext = os.path.splitext(relative_path)
        tokens = url.split("/")

        # If the doc is named xx/index.md then assign it to URL xx/
        if tokens[-1] == "index":
            tokens = tokens[:-1]
        if tokens:
            no_index_url = "/".join(tokens) + "/"
        else:
            no_index_url = ""

        return no_index_url

    def _translate_html(self, soup, language):
        """Translate HTML into another language using the DeepL API

        Args:
            soup (BeautifulSoup): HTML source
            language (str): ISO code of target language

        Returns:
            BeautifulSoup: HTML translated
        """

        # DeepL has a limit for the size of the payload, so remove our heaviest
        # items: the inline images, and store them into a separate array to
        # reinstate them later
        SVG_B64_PREFIX = "data:image/svg+xml;base64,"
        srcs = []
        for img in soup.find_all("img"):
            if img["src"].startswith(SVG_B64_PREFIX):
                srcs.append(img["src"])
                img["src"] = "__to_replace__"

        # Translate the HTML using DeepL API
        # See https://www.deepl.com/en/docs-api/
        # And especially https://www.deepl.com/docs-api/handling-xml/
        response = requests.post(
            os.environ.get("DEEPL_API_URL"),
            data={
                "auth_key": os.environ.get("DEEPL_API_KEY"),
                "source_lang": settings.DOCS_DEFAULT_LANGUAGE.upper(),
                "target_lang": language.upper(),
                "text": str(soup),
                "tag_handling": "xml",
                "split_sentences": "nonewlines",
                #"outline_detection": "0",
                #"splitting_tags": "p,ul,li,h1,h2,h3,h4,h5,h6,figure,figcaption",
                "ignore_tags": "code,x",
            },
            headers={"User-Agent": "FaST https://fastplatform.eu"},
        )

        response.raise_for_status()
        data = response.json()
        html = data["translations"][0]["text"]
        soup = BeautifulSoup(html, features="html.parser")

        # Translate our diagrams as well
        # for idx, src in enumerate(srcs):
        #     logger.info(" + diagram %s/%s", idx + 1, len(srcs))
            
        #     # Decode the base64 content of the SVG tag
        #     decoded = base64.b64decode(src[len(SVG_B64_PREFIX) :]).decode("utf-8")

        #     # Parse it and extract texts
        #     diagram_soup = BeautifulSoup(decoded, features="xml")
        #     texts = [text.string for text in diagram_soup.find_all("text")]

        #     response = requests.post(
        #         os.environ.get("DEEPL_API_URL"),
        #         data={
        #             "auth_key": os.environ.get("DEEPL_API_KEY"),
        #             "source_lang": settings.DOCS_DEFAULT_LANGUAGE.upper(),
        #             "target_lang": language.upper(),
        #             "text": texts,
        #             "tag_handling": "xml",
        #             "split_sentences": "nonewlines",
        #             "ignore_tags": "code,x",
        #         },
        #         headers={"User-Agent": "FaST https://fastplatform.eu"},
        #     )
        #     response.raise_for_status()
        #     data = response.json()

        #     for text, translation in zip(
        #         diagram_soup.find_all("text"), data["translations"]
        #     ):
        #         text.string = translation["text"]

        #     encoded = base64.b64encode(
        #         bytes(str(diagram_soup), encoding="utf-8")
        #     ).decode("ascii")

        #     srcs[idx] = SVG_B64_PREFIX + encoded

        # Reinstate our heavy inline images
        for img in soup.find_all("img"):
            if img["src"] == "__to_replace__":
                img["src"] = srcs.pop()

        return soup

    def handle(self, *args, **kwargs):

        self._init_directories()
        self._init_index()

        # Global table of contents
        tocs = {
            language_code: [] for language_code, language_name in settings.LANGUAGES
        }

        # URL patterns for Django urls.py file
        urlpatterns = {}

        # Get all md in English
        paths = glob.glob(os.path.join(DOCS_ROOT_DIR, "src", "**/*.md"))
        paths += glob.glob(os.path.join(DOCS_ROOT_DIR, "src", "*.md"))

        # For each md file found
        for absolute_path in paths:

            # Extract the path relative to docs/<language>
            relative_path = os.path.relpath(
                absolute_path,
                os.path.join(DOCS_ROOT_DIR, "src"),
            )

            logger.info("Processing %s", relative_path)

            url = self._path_to_url(relative_path)
            tokens = url.split("/")

            # Store the URL to our urlpatterns if we do not have it already
            if url not in urlpatterns:
                urlpatterns[url] = {
                    "url": url,
                    "file_name": relative_path,
                    "name": url or "index",
                }

            md = Path(absolute_path).read_text()

            # Is the file in the cache?
            # (i.e. has it already been translated?)
            md_cache_filepath = Path(
                CACHE_DIR,
                relative_path,
            )

            try:
                from_cache = md_cache_filepath.read_text() == md
            except FileNotFoundError:
                from_cache = False

            if not from_cache:
                os.makedirs(md_cache_filepath.parent, exist_ok=True)
                md_cache_filepath.write_text(md)

            soup, toc = self._markdown_to_html(md)
            soup = self._post_process_html(soup)

            # For each declared language, read all the md files in the docs/<language> directory
            for language_code in LANGUAGE_CODES:

                html_filepath = Path(
                    HTML_DIR,
                    language_code,
                    relative_path + ".html",
                )
                toc_html_filepath = Path(
                    HTML_DIR,
                    language_code,
                    relative_path + ".toc.html",
                )
                html_title_filepath = Path(
                    HTML_DIR,
                    language_code,
                    relative_path + ".title.html",
                )

                if language_code == settings.DOCS_DEFAULT_LANGUAGE:
                    # If that's the English version we are producing, no need to translate
                    translated_soup = BeautifulSoup(str(soup), features="html.parser")
                    translated_toc = BeautifulSoup(str(toc), features="html.parser")
                elif (
                    from_cache
                    and html_filepath.exists()
                    and toc_html_filepath.exists()
                    and html_title_filepath.exists()
                ):
                    # If the .md file did not change, then just load the translated HTML
                    # and do not re-translate it
                    translated_soup = BeautifulSoup(
                        html_filepath.read_text(), features="html.parser"
                    )
                    translated_toc = BeautifulSoup(
                        toc_html_filepath.read_text(), features="html.parser"
                    )
                else:
                    # Else translate the HTLML using DeepL
                    logger.info("- translating to %s", language_code)
                    translated_soup = self._translate_html(
                        BeautifulSoup(str(soup), features="html.parser"), language_code
                    )
                    translated_toc = self._translate_html(
                        BeautifulSoup(str(toc), features="html.parser"), language_code
                    )

                translated_title = translated_soup.select("h1")[0].text.strip()
                translated_content = translated_soup.get_text()

                # Write the HTML to the build directory
                html_filepath.parent.mkdir(parents=True, exist_ok=True)
                html_filepath.write_text(
                    "{{% load static %}}{}".format(str(translated_soup))
                )

                toc_html_filepath.write_text(str(translated_toc))

                html_title_filepath.write_text(str(translated_title))

                # Add the file to the index for search
                self.writer.add_document(
                    title=translated_title,
                    url="/docs/" + url if tokens else "/docs/",
                    language_code=language_code,
                    content=translated_content,
                )

                # Add to the TOC for this language
                tocs[language_code].append(
                    {
                        "level": len(tokens),
                        "url": "/docs/" + url if url else "/docs/",
                        "title": translated_title,
                        "language_code": language_code,
                        "view_name": "docs:" + url,
                    }
                )

        for language_code in LANGUAGE_CODES:

            # Sort the entries by the url (#TODO this is not ideal)
            tocs[language_code] = sorted(tocs[language_code], key=lambda t: t["url"])

            # Compute the level difference with the previous item (this will
            # serve when we actually render the TOC)
            for i, t in enumerate(tocs[language_code]):
                if i > 0:
                    t["diff"] = tocs[language_code][i - 1]["level"] - t["level"]

        # Write the global TOCS to the build directory
        Path(BUILD_DIR, "tocs.json").write_text(json.dumps(tocs, indent=2))

        # Write all entries to the index and close it
        self.writer.commit()

        # Write the URL patterns to the build directory
        Path(BUILD_DIR, "urlpatterns.json").write_text(
            json.dumps(list(urlpatterns.values()), indent=2)
        )
