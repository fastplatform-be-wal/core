from django.db.models import JSONField

from utils.widgets.json_editor import JSONEditorWidget


class JSONEditorMixin:
    def __init__(self, obj, *args, **kwargs):

        super().__init__(obj, *args, **kwargs)

        self.formfield_overrides.update(
            {
                JSONField: {
                    "widget": JSONEditorWidget(
                        width="500px",
                        height="250px",
                        options={
                            "mainMenuBar": False,
                            "statusBar": False,
                            "navigationBar": False,
                        },
                    )
                }
            }
        )
