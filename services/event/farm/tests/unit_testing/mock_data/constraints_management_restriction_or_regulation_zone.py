from app.api.types import ComputedConstraint
from .shared import *

EXISTING_CONSTRAINT = {
    "description": {
        "management_restriction_or_regulation_zone_id": 1,
        "management_restriction_or_regulation_zone_name": "name",
        "plot_pct_in_management_restriction_or_regulation_zone": 0.16944617396772127,
        "plot_area_in_management_restriction_or_regulation_zone": 602.8421348452359,
    },
    "id": 1,
    "name": "protected_site",
    "plot_id": PLOT_ID,
}


EXISTING_CONSTRAINTS = [EXISTING_CONSTRAINT]

EXPECTED_EXISTING_CONSTRAINT = ComputedConstraint(**EXISTING_CONSTRAINT)

INTERSECTING_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE = [
    {"id": 1, "name": "name", "geometry": INTERSECTING_GEOMETRY_NODE}
]

COMPUTED_CONSTRAINT = {
    "name": "management_restriction_or_regulation_zone",
    "plot_id": PLOT_ID,
    "description": {
        "management_restriction_or_regulation_zone_id": 1,
        "management_restriction_or_regulation_zone_name": "name",
        "plot_pct_in_management_restriction_or_regulation_zone": 0.16944617396772127,
        "plot_area_in_management_restriction_or_regulation_zone": 602.8421348452359,
    },
}


EMPTY_CONSTRAINT = {
    "name": "management_restriction_or_regulation_zone",
    "plot_id": PLOT_ID,
}
