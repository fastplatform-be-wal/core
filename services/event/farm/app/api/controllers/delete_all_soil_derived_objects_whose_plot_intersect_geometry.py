import logging
from shapely.geometry import shape

from app.api.lib.gis import add_crs

from opentelemetry import trace
from gql import gql
from app.settings import config
from app.db.graphql_clients import fastplatform

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def delete_all_soil_derived_objects_whose_plot_intersect_geometry(
    soil_site_id: int,
    geometry: dict,
):
    """
    For a given geometry, delete soil_derived_objects that validate relation:
        soil_derived_object -> plot.geometry *intersects* $geometry
        ( "->" = "belongs to" )

    Args:
        soil_site_id (int): The id of the soil_site, passed in method for logging purposes
        geometry (dict): The geometry of the soil_site

    Returns:
        affected_rows
    """
    geometry = shape(geometry)
    with tracer().start_as_current_span(
        "delete_all_soil_derived_objects_whose_plot_intersect_geometry"
    ):
        logger.info(
            "Deleting all soil_derived_object whose plot intersects geometry of soil_site_id : %s",
            soil_site_id,
        )
        mutation = (
            config.API_DIR
            / "controllers"
            / "graphql"
            / "mutation_delete_all_soil_derived_objects_whose_plot_intersect_geometry.graphql"
        ).read_text()
        variables = {
            "geometry": add_crs(geometry.__geo_interface__, config.EPSG_SRID_ETRS89)
        }

        return await fastplatform.execute(gql(mutation), variables)
