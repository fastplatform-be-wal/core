from common.resources import ModelResourceWithColumnIds

from messaging.models import Queue, Ticket, TicketMessage, Broadcast


class QueueResource(ModelResourceWithColumnIds):
    class Meta:
        model = Queue


class TicketResource(ModelResourceWithColumnIds):
    class Meta:
        model = Ticket


class TicketMessageResource(ModelResourceWithColumnIds):
    class Meta:
        model = TicketMessage


class BroadcastResource(ModelResourceWithColumnIds):
    class Meta:
        model = Broadcast
