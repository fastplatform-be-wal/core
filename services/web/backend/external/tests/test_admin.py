from django.test import TestCase, override_settings
from django.conf import settings

from utils.tests.test_admin import AdminTestCaseMixin

from external.models import (
    AgriPlot,
    AgriPlotVersion,
    ManagementRestrictionOrRegulationZone,
    ManagementRestrictionOrRegulationZoneVersion,
    ZoneTypeCode,
    SpecialisedZoneTypeCode,
    EnvironmentalDomain,
    WaterCourseVersion,
    WaterCourse,
    SurfaceWaterVersion,
    SurfaceWater,
    ProtectedSite,
    ProtectedSiteVersion,
    DesignationType,
    DesignationScheme,
    Designation,
    SoilSite,
    SoilSiteVersion,
    Observation,
    PhenomenonType,
    SoilInvestigationPurpose,
    ObservableProperty,
)

from configuration.tests.test_admin import configuration_recipe

from external.tests.recipes import (
    agri_plot_version_recipe,
    agri_plot_recipe,
    management_restriction_or_regulation_zone_version_recipe,
    zone_type_code_recipe,
    specialised_zone_type_code_recipe,
    environmental_domain_recipe,
    management_restriction_or_regulation_zone_recipe,
    water_course_version_recipe,
    water_course_recipe,
    surface_water_version_recipe,
    surface_water_recipe,
    protected_site_version_recipe,
    designation_type_recipe,
    designation_scheme_recipe,
    designation_recipe,
    protected_site_recipe,
    soil_site_version_recipe,
    observation_recipe,
    phenomenon_type_recipe,
    soil_investigation_purpose_recipe,
    observable_property_recipe,
    soil_site_recipe,
)



@override_settings(AXES_ENABLED=False)
class AgriPlotVersionAdminTestCase(AdminTestCaseMixin, TestCase):
    model = AgriPlotVersion
    recipe = agri_plot_version_recipe
    enable = settings.ENABLE_EXTERNAL_AGRI_PLOT
    using = "external"
    databases = ["default", "external"]

    skip_add = True
    skip_change = True


@override_settings(AXES_ENABLED=False)
class AgriPlotAdminTestCase(AdminTestCaseMixin, TestCase):
    model = AgriPlot
    recipe = agri_plot_recipe
    enable = settings.ENABLE_EXTERNAL_AGRI_PLOT
    using = "external"
    databases = ["default", "external"]

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class ManagementRestrictionOrRegulationZoneVersionAdminTestCase(
    AdminTestCaseMixin, TestCase
):
    model = ManagementRestrictionOrRegulationZoneVersion
    recipe = management_restriction_or_regulation_zone_version_recipe
    enable = settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE
    using = "external"
    databases = ["default", "external"]

    skip_add = True
    skip_change = True


@override_settings(AXES_ENABLED=False)
class ZoneTypeCodeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = ZoneTypeCode
    recipe = zone_type_code_recipe
    enable = settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class SpecialisedZoneTypeCodeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = SpecialisedZoneTypeCode
    recipe = specialised_zone_type_code_recipe
    enable = settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class EnvironmentalDomainAdminTestCase(AdminTestCaseMixin, TestCase):
    model = EnvironmentalDomain
    recipe = environmental_domain_recipe
    enable = settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class ManagementRestrictionOrRegulationZoneAdminTestCase(AdminTestCaseMixin, TestCase):
    model = ManagementRestrictionOrRegulationZone
    recipe = management_restriction_or_regulation_zone_recipe
    enable = settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE
    using = "external"
    databases = ["default", "external"]

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class WaterCourseVersionAdminTestCase(AdminTestCaseMixin, TestCase):
    model = WaterCourseVersion
    recipe = water_course_version_recipe
    enable = settings.ENABLE_EXTERNAL_WATER_COURSE
    using = "external"
    databases = ["default", "external"]

    skip_add = True
    skip_change = True


@override_settings(AXES_ENABLED=False)
class WaterCourseAdminTestCase(AdminTestCaseMixin, TestCase):
    model = WaterCourse
    recipe = water_course_recipe
    enable = settings.ENABLE_EXTERNAL_WATER_COURSE
    using = "external"
    databases = ["default", "external"]

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class SurfaceWaterVersionAdminTestCase(AdminTestCaseMixin, TestCase):
    model = SurfaceWaterVersion
    recipe = surface_water_version_recipe
    enable = settings.ENABLE_EXTERNAL_SURFACE_WATER
    using = "external"
    databases = ["default", "external"]

    skip_add = True
    skip_change = True


@override_settings(AXES_ENABLED=False)
class SurfaceWaterAdminTestCase(AdminTestCaseMixin, TestCase):
    model = SurfaceWater
    recipe = surface_water_recipe
    enable = settings.ENABLE_EXTERNAL_SURFACE_WATER
    using = "external"
    databases = ["default", "external"]

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class ProtectedSiteVersionAdminTestCase(AdminTestCaseMixin, TestCase):
    model = ProtectedSiteVersion
    recipe = protected_site_version_recipe
    enable = settings.ENABLE_EXTERNAL_PROTECTED_SITE
    using = "external"
    databases = ["default", "external"]

    skip_add = True
    skip_change = True


@override_settings(AXES_ENABLED=False)
class DesignationTypeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = DesignationType
    recipe = designation_type_recipe
    enable = settings.ENABLE_EXTERNAL_PROTECTED_SITE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class DesignationSchemeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = DesignationScheme
    recipe = designation_scheme_recipe
    enable = settings.ENABLE_EXTERNAL_PROTECTED_SITE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class DesignationAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Designation
    recipe = designation_recipe
    enable = settings.ENABLE_EXTERNAL_PROTECTED_SITE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class ProtectedSiteAdminTestCase(AdminTestCaseMixin, TestCase):
    model = ProtectedSite
    recipe = protected_site_recipe
    enable = settings.ENABLE_EXTERNAL_PROTECTED_SITE
    using = "external"
    databases = ["default", "external"]

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class SoilSiteVersionAdminTestCase(AdminTestCaseMixin, TestCase):
    model = SoilSiteVersion
    recipe = soil_site_version_recipe
    enable = settings.ENABLE_EXTERNAL_SOIL_SITE
    using = "external"
    databases = ["default", "external"]

    skip_add = True
    skip_change = True


@override_settings(AXES_ENABLED=False)
class ObservationAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Observation
    recipe = observation_recipe
    enable = settings.ENABLE_EXTERNAL_SOIL_SITE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class PhenomenonTypeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = PhenomenonType
    recipe = phenomenon_type_recipe
    enable = settings.ENABLE_EXTERNAL_SOIL_SITE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class SoilInvestigationPurposeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = SoilInvestigationPurpose
    recipe = soil_investigation_purpose_recipe
    enable = settings.ENABLE_EXTERNAL_SOIL_SITE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class ObservablePropertyAdminTestCase(AdminTestCaseMixin, TestCase):
    model = ObservableProperty
    recipe = observable_property_recipe
    enable = settings.ENABLE_EXTERNAL_SOIL_SITE
    using = "external"
    databases = ["default", "external"]


@override_settings(AXES_ENABLED=False)
class SoilSiteAdminTestCase(AdminTestCaseMixin, TestCase):
    model = SoilSite
    recipe = soil_site_recipe
    enable = settings.ENABLE_EXTERNAL_SOIL_SITE
    using = "external"
    databases = ["default", "external"]

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()
