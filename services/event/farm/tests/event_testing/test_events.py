"""
This tests require an instance of hasura up, running and accessible.

No other requirements expected in terms of db data prefilling,
they can run on empty db (only clear-region, no init-region).

BEWARE: running this tests will erase all data in certain tables.
"""
import os
from pathlib import Path
import glob
import json
from unittest import TestCase
import asyncio
from gql import gql
import time
from pprint import pprint

from fastapi.testclient import TestClient

from app.main import app


from app.db.graphql_clients import GraphQLClient
from app.settings import config
from tests.constants import SHARED_VALUES


MAIN_DIR = Path(__file__).parent
CLEAR_DBS_QUERY_URI = Path(__file__).parent.parent / "query_clear_dbs.graphql"
HEADER_SCENARIO = (
    "\n\n-----------------------------------------\nEVENT:    {e}\nSCENARIO: {s}\n"
)

# RUN_ONLY = [
#     "delete_constraint_on_protected_site_version_is_active_updated",  # [event name]
#     "nominal",  # [scenario name]
# ]
RUN_ONLY = None

fastplatform = GraphQLClient(
    name="fastplatform",
    url=config.API_GATEWAY_FASTPLATFORM_URL,
    headers={
        "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
        "Content-type": "application/json",
    },
)


class EventsTest(TestCase):
    clear_dbs_query = None
    event_setup = None
    loop = None

    # Counters
    events_skipped = 0
    scenario_skipped = 0
    steps_skipped = 0

    @classmethod
    def setUpClass(cls) -> None:
        cls.clear_dbs_query = CLEAR_DBS_QUERY_URI.read_text()
        cls.event_setup = cls.load_query("event_setup")
        cls.loop = asyncio.get_event_loop()
        return super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        # cls.loop.close()
        print()
        print(f"{EventsTest.events_skipped} event(s) skipped")
        print(f"{EventsTest.scenario_skipped} scenario(s) skipped")
        print(f"{EventsTest.steps_skipped} step(s) skipped")

        return super().tearDownClass()

    @classmethod
    def load_json(self, relative_path):
        json_path = Path(str(MAIN_DIR / f"{relative_path}.json"))
        if not json_path.exists():
            return None

        content = json_path.read_text()

        # return actual json
        return json.loads(content)

    @classmethod
    def load_query(self, relative_path):
        query_path = Path(str(MAIN_DIR / f"{relative_path}.graphql"))
        if not query_path.exists():
            return None

        query = query_path.read_text()
        for place_holder, value in (
            (ph, v) for (ph, v) in SHARED_VALUES.items() if ph in query
        ):
            query = query.replace(f"{{{{ { place_holder } }}}}", str(value))

        return query

    def run_async(self, method):
        return self.loop.run_until_complete(method)

    def get_tested_events(self):
        tested_events = glob.glob(str(MAIN_DIR / "events" / "*"))
        tested_events.sort()
        for tested_event in tested_events:
            # For easier debug, adding a file simply called 'skip' (no extension) skips the test
            if Path(f"{tested_event}/skip").exists():
                EventsTest.events_skipped += 1
            else:
                yield Path(tested_event).name

    def get_scenarios(self, event):
        scenarios = [
            f.path for f in os.scandir(str(MAIN_DIR / "events" / event)) if f.is_dir()
        ]
        scenarios.sort()
        for scenario in scenarios:
            # For easier debug, adding a file simply called 'skip' (no extension) skips the scenario
            if Path(f"{scenario}/skip").exists():
                EventsTest.scenario_skipped += 1
            else:
                yield Path(scenario).name

    def get_steps(self, event, scenario):
        steps = [
            f.path
            for f in os.scandir(str(MAIN_DIR / "events" / event / scenario))
            if f.is_dir()
        ]

        # Steps are executed sequentially
        steps.sort()

        for step in steps:
            # For easier debug, adding a file simply called 'skip' (no extension) skips the step
            if Path(f"{step}/skip").exists():
                EventsTest.steps_skipped += 1
            else:
                yield Path(step).name

    def load_step(self, event, scenario, step):
        path = f"events/{event}/{scenario}/{step}"
        return (
            self.load_query(f"{path}/fastplatform_mutation"),
            self.load_query(f"{path}/service_query"),
            self.load_query(f"{path}/fastplatform_query"),
            self.load_json(f"{path}/expected"),
        )

    def test_events(self):
        try:
            self.run_async(fastplatform.connect())

            for event in self.get_tested_events():
                with self.subTest(event=event):
                    for scenario in self.get_scenarios(event):
                        with self.subTest(scenario=scenario):

                            if RUN_ONLY and not (
                                RUN_ONLY[0] == event and RUN_ONLY[1] == scenario
                            ):
                                EventsTest.events_skipped += 1
                                EventsTest.scenario_skipped += 1
                                continue

                            print(HEADER_SCENARIO.format(e=event, s=scenario))

                            # Clear db from all previous test values
                            print("Clearing db…")
                            self.run_async(fastplatform.execute(gql(self.clear_dbs_query)))

                            # Important: give events triggered by deletion the time to be fully processed
                            time.sleep(2)

                            print("Event setup…\n")
                            self.run_async(fastplatform.execute(gql(self.event_setup)))

                            # Wait for cascade events to finish
                            time.sleep(2)

                            for step in self.get_steps(event, scenario):
                                with self.subTest(step=step):
                                    response = None
                                    print(f"STEP: {step}\n--------------------------")

                                    # Load all available step files, None if not found
                                    (
                                        fp_mutation,
                                        service_query,
                                        fp_query,
                                        expected,
                                    ) = self.load_step(event, scenario, step)

                                    # If found in step, execute fastplatform mutation
                                    if fp_mutation:
                                        self.run_async(
                                            fastplatform.execute(gql(fp_mutation))
                                        )
                                        # Wait after the mutation so that events are processed by Hasura
                                        # and sent to the service for processing
                                        print("\tWait 2 seconds...")
                                        time.sleep(2)

                                    # If found in step, query fastplatform for assert
                                    if fp_query:
                                        response = self.run_async(
                                            fastplatform.execute(gql(fp_query))
                                        )
                                    
                                    # If found in step, trigger remote schema
                                    # by querying service graphql endpoint.
                                    if service_query:
                                        with TestClient(app) as client:
                                            response = client.post(
                                                "/graphql",
                                                json={
                                                    "query": service_query,
                                                    "variables": {},
                                                },
                                            )

                                            response = response.json()

                                    if not expected:
                                        print("\tNo assert performed at this step\n")

                                    else:
                                        # Keep for cleaner debugging
                                        print("\t> Response:")
                                        pprint(response, indent=2, compact=True)
                                        print("\t> Expected:")
                                        pprint(expected, indent=2, compact=True)

                                        # # Assert results are identical
                                        TestCase().assertDictEqual(response, expected)

                                        print("\t--> Assert: True\n")

        finally:
            self.run_async(fastplatform.close())
