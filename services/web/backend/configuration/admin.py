from fastplatform.site import admin_site
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.conf import settings

from utils.admin import CommonModelAdmin
from configuration.models import Configuration, MapBaseLayer, MapOverlay


class ConfigurationAdmin(CommonModelAdmin):
    def get_fieldsets(self, request, obj=None):
        map_fields = [
            "backend_leaflet_default_center_latitude",
            "backend_leaflet_default_center_longitude",
            "backend_leaflet_default_zoom",
            "backend_leaflet_min_zoom",
            "backend_leaflet_max_zoom",
            "backend_leaflet_map_width",
            "backend_leaflet_map_height",
            "backend_leaflet_display_raw",
            "backend_leaflet_map_srid",
        ]

        if settings.ENABLE_EXTERNAL_PROTECTED_SITE:
            map_fields += ["protected_site_overlay_style"]

        if (
            settings.ENABLE_EXTERNAL_WATER_COURSE
            or settings.ENABLE_EXTERNAL_SURFACE_WATER
        ):
            map_fields += ["hydrography_overlay_style"]

        if settings.ENABLE_EXTERNAL_SOIL_SITE:
            map_fields += ["soil_overlay_style"]

        if settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE:
            map_fields += ["management_restriction_or_regulation_zone_overlay_style"]

        if settings.ENABLE_EXTERNAL_AGRI_PLOT:
            map_fields += ["agri_plot_overlay_style"]

        map_fields += ["plots_overlay_style", "rgb_ndvi_overlay_style"]

        return (
            (_("Map configuration"), {"fields": map_fields}),
            (
                _("Surface units configuration"),
                {
                    "fields": [
                        "display_surface_unit_name",
                        "display_surface_unit_short_name",
                        "display_surface_unit_ratio_to_hectare",
                    ]
                },
            ),
        )

    def has_add_permission(self, *args):
        return False

    def has_delete_permission(self, *args):
        return False


admin_site.register(Configuration, ConfigurationAdmin)


class LayerAdmin(CommonModelAdmin):
    readonly_fields = ("proj_wkt",)

    def get_fieldsets(self, request, obj=None):
        display_fields = [
            "min_zoom",
            "max_zoom",
            "is_active_in_farmer_app",
            "is_active_in_admin_portal",
            "display",
        ]

        if isinstance(obj, MapOverlay):
            display_fields += ["layer_order"]

        fieldsets = [
            [
                None,
                {"fields": ["id", "title"]},
            ],
            [
                _("Display"),
                {
                    "description": _(
                        "These parameters will configure how the layer will be displayed in the app and in the Admin portal maps."
                    ),
                    "fields": display_fields,
                },
            ],
            [
                _("Source parameters"),
                {
                    "description": None,
                    "fields": [
                        "layer_type",
                        "url",
                        "max_native_zoom",
                        "attribution",
                    ],
                },
            ],
            [
                "WMS-specific parameters",
                {
                    "description": _("This set of fields is only for WMS Layers."),
                    "fields": [
                        "layers",
                        "styles",
                        "proj_format",
                        "wms_version",
                        "is_transparent",
                        "proj_name",
                        "proj_wkt",
                    ],
                },
            ],
        ]
        return fieldsets

    def get_readonly_fields(self, request, obj):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            readonly_fields += ("id",)
        return readonly_fields


class MapBaseLayerAdmin(LayerAdmin):
    model = MapBaseLayer
    list_display = (
        "title",
        "is_active_in_farmer_app",
        "is_active_in_admin_portal",
        "display",
        "url",
    )

    def save_model(self, request, obj, form, change):
        if obj.display:
            MapBaseLayer.objects.exclude(id=obj.id).update(display=False)

        super().save_model(request, obj, form, change)


admin_site.register(MapBaseLayer, MapBaseLayerAdmin)


class MapOverlayAdmin(LayerAdmin):
    model = LayerAdmin
    list_display = (
        "title",
        "is_active_in_farmer_app",
        "is_active_in_admin_portal",
        "display",
        "layer_order",
        "url",
    )
    ordering = ("layer_order",)


admin_site.register(MapOverlay, MapOverlayAdmin)
