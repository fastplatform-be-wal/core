from django.contrib import admin
from django.utils.translation import gettext_lazy as _


from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin

from farm.admin.forms import DerivedObservationInlineForm
from soil.models import DerivedObservation, SoilDerivedObject


class DerivedObservationInline(admin.TabularInline):
    model = DerivedObservation
    form = DerivedObservationInlineForm

    extra = 0

    verbose_name = _("estimate")
    verbose_name_plural = _("estimates")

    def get_queryset(self, request):
        return super().get_queryset(request).select_related("observed_property")


class SoilDerivedObjectAdmin(CommonModelAdmin):
    hide_from_dashboard = True
    raw_id_fields = ("plot",)
    fields = ["plot", "soil_sites_count", "soil_sample_origin", "is_derived_from"]
    inlines = [DerivedObservationInline]


admin_site.register(SoilDerivedObject, SoilDerivedObjectAdmin)
