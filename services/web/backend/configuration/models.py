import urllib.parse
import re

from django.core.cache import cache
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import (
    RegexValidator,
    URLValidator,
    MinValueValidator,
    MaxValueValidator,
)

import pyproj

from utils.models import GetAdminURLMixin, TranslatableModel, validate_latin1


def default_plots_overlay_style():
    return {
        "fill": True,
        "color": "#1C9ACD",
        "radius": 5,
        "weight": 1,
        "fillColor": "#1C9ACD",
        "fillOpacity": 0.5,
    }


class Configuration(GetAdminURLMixin, models.Model):
    id = models.CharField(
        max_length=50,
        editable=False,
        primary_key=True,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    backend_leaflet_default_center_latitude = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("default center latitude"),
        help_text=_("Default center latitude for maps in the admin backend"),
    )

    backend_leaflet_default_center_longitude = models.FloatField(
        null=False,
        blank=False,
        verbose_name=_("default center longitude"),
        help_text=_("Default center longitude for maps in the admin backend"),
    )

    backend_leaflet_default_zoom = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_("default zoom"),
        help_text=_("Default zoom level of maps in the admin backend"),
    )

    backend_leaflet_min_zoom = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_("minimum zoom"),
        help_text=_("Minimum zoom level of maps in the admin backend"),
    )

    backend_leaflet_max_zoom = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_("maximum zoom"),
        help_text=_("Maximum zoom level of maps in the admin backend"),
    )

    backend_leaflet_map_width = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_("map width"),
        help_text=_("Default width of maps in the admin backend, in CSS units"),
    )

    backend_leaflet_map_height = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_("map height"),
        help_text=_("Default height of maps in the admin backend, in CSS units"),
    )

    backend_leaflet_display_raw = models.BooleanField(
        null=False,
        blank=False,
        verbose_name=_("display raw geometries"),
        help_text=_(
            "Whether or not to display the current geometry as WKT in the admin backend"
        ),
    )

    backend_leaflet_map_srid = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_("map SRID"),
        help_text=_("SRID of the maps in the admin backend"),
    )

    protected_site_overlay_style = models.JSONField(
        null=False,
        blank=False,
        verbose_name=_("style for protected sites overlay"),
    )

    hydrography_overlay_style = models.JSONField(
        null=False,
        blank=False,
        verbose_name=_("style for hydrography overlay"),
    )

    soil_overlay_style = models.JSONField(
        null=False,
        blank=False,
        verbose_name=_("style for soil sites overlay"),
    )

    management_restriction_or_regulation_zone_overlay_style = models.JSONField(
        null=False,
        blank=False,
        verbose_name=_("style for management restriction or regulation zones overlay"),
    )

    agri_plot_overlay_style = models.JSONField(
        null=False,
        blank=False,
        verbose_name=_("style for all agricultural plots overlay"),
    )

    plots_overlay_style = models.JSONField(
        null=False,
        blank=False,
        verbose_name=_("style for all farm plots overlay"),
        default=default_plots_overlay_style,
    )

    rgb_ndvi_overlay_style = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("style for RGB/NDVI overlay"),
    )

    display_surface_unit_name = models.CharField(
        null=True,
        blank=True,
        verbose_name=_("Display surface unit name"),
        max_length=50,
        help_text=_("Will default to 'hectare' if not set"),
    )

    display_surface_unit_short_name = models.CharField(
        null=True,
        blank=True,
        verbose_name=_("Display surface unit short name"),
        max_length=50,
        help_text=_("Will default to 'ha' if not set"),
    )

    display_surface_unit_ratio_to_hectare = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("Ratio between one display surface unit and one hectare"),
        max_length=50,
        help_text=_("Will default to 1.0 if not set"),
    )

    # -------------------------------------------------------------
    # Singleton and cache management

    def delete(self, *args, **kwargs):  # pylint: disable=arguments-differ
        pass

    def save(self, *args, **kwargs):
        self.id = "CONFIG"
        super().save(*args, **kwargs)
        self.set_cache()

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        RE_COLOR = r"^#(?:[0-9a-fA-F]{3}){1,2}$"
        SUPPORTED_KEYS = [
            "fill",
            "color",
            "radius",
            "weight",
            "fillColor",
            "fillOpacity",
            "attribution",
        ]

        for attr in [
            "protected_site_overlay_style",
            "hydrography_overlay_style",
            "soil_overlay_style",
            "management_restriction_or_regulation_zone_overlay_style",
            "agri_plot_overlay_style",
            "plots_overlay_style",
        ]:
            value = getattr(self, attr)
            attr_name = self._meta.get_field(attr).verbose_name.capitalize()

            if not isinstance(value, dict):
                raise ValidationError(_("'{}' must be a dictionary").format(attr_name))
            if not isinstance(value.get("fill"), bool):
                raise ValidationError(
                    _("'fill' must be a boolean for '{}'").format(attr_name)
                )
            if not isinstance(value.get("color"), str) or not re.search(
                RE_COLOR, value["color"]
            ):
                raise ValidationError(
                    _("'color' must be a valid color (e.g. #12AB45) for '{}'").format(
                        attr_name
                    )
                )
            if not isinstance(value.get("radius"), int):
                raise ValidationError(
                    _("'radius' must be an integer for '{}'").format(attr_name)
                )
            if not isinstance(value.get("weight"), int):
                raise ValidationError(
                    _("'weight' must be an integer for '{}'").format(attr_name)
                )
            if not isinstance(value.get("fillColor"), str) or not re.search(
                RE_COLOR, value["fillColor"]
            ):
                raise ValidationError(
                    _(
                        "'fillColor' must be a valid color (e.g. #12AB45) for '{}'"
                    ).format(attr_name)
                )
            if (
                not isinstance(value.get("fillOpacity"), float)
                or not 0 <= value["fillOpacity"] <= 1
            ):
                raise ValidationError(
                    _("'fillOpacity' must be a float between 0 and 1 for '{}'").format(
                        attr_name
                    )
                )
            if value.get("attribution") and not isinstance(
                value.get("attribution"), str
            ):
                raise ValidationError(
                    _("'attribution' must be a string for '{}'").format(attr_name)
                )
            if [k for k in value.keys() if k not in SUPPORTED_KEYS]:
                raise ValidationError(
                    _("Unknown keys in '{}'. Supported keys are: '{}'.").format(
                        attr_name, SUPPORTED_KEYS
                    )
                )

        value = getattr(self, "rgb_ndvi_overlay_style")
        attr_name = self._meta.get_field(
            "rgb_ndvi_overlay_style"
        ).verbose_name.capitalize()

        if value:
            if not isinstance(value, dict):
                raise ValidationError(
                    _("'{}' must be a dictionary").format("rgb_ndvi_overlay_style")
                )
            if value.get("attribution") and not isinstance(
                value.get("attribution"), str
            ):
                raise ValidationError(
                    _("'attribution' must be a string for '{}'").format(attr_name)
                )
            if [k for k in value.keys() if k not in ["attribution"]]:
                raise ValidationError(
                    _("Unknown keys in '{}'. Supported keys are: '{}'.").format(
                        attr_name, ["attribution"]
                    )
                )

    @classmethod
    def load(cls):
        if cache.get(cls.__name__) is None:
            obj, created = cls.objects.get_or_create(pk="CONFIG")
            if not created:
                obj.set_cache()
                return obj
        return cache.get(cls.__name__)

    def set_cache(self):
        cache.set(self.__class__.__name__, self)

    class Meta:
        db_table = "configuration"
        verbose_name = _("configuration")
        verbose_name_plural = _("configurations")
        help_text = _(
            "A configuration panel that contains parameters for the maps and surface units on the portal and the mobile app."
        )


class AbstractMapLayer(GetAdminURLMixin, TranslatableModel):
    id = models.CharField(
        primary_key=True,
        max_length=50,
        validators=[
            RegexValidator(
                regex="[a-z0-9\_\-]+",
                message=_(
                    "Can contain only lowercase letters (a-z), digits (0-9), dash (-) and underscore (_)."
                ),
            )
        ],
        verbose_name=_("identifier"),
        help_text=_(
            "Can contain only lowercase letters (a-z), digits (0-9), dash (-) and underscore (_)."
        ),
    )

    title = models.CharField(
        max_length=255, null=False, blank=False, verbose_name=_("name")
    )

    min_zoom = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_("minimum zoom"),
        help_text=_("Below this zoom level, the layer will not be displayed."),
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30),
        ],
    )

    max_zoom = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_("maximum zoom"),
        help_text=_("Above this zoom level, the layer will not be displayed."),
        validators=[
            MinValueValidator(0),
            MaxValueValidator(30),
        ],
    )

    is_active_in_farmer_app = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name=_("active in Farmer App"),
        help_text=_(
            "Whether or not to display this layer in the layer selection menu in the Farmer (mobile) App"
        ),
    )

    is_active_in_admin_portal = models.BooleanField(
        null=False,
        blank=False,
        default=True,
        verbose_name=_("active in Admin Portal"),
        help_text=_(
            "Whether or not to display this layer in the layer selection menu in the Administration Portal"
        ),
    )

    # Source parameters

    class LayerType(models.TextChoices):
        TMS = "tms", "TMS (Tile Map Service)"
        WMS = "wms", "WMS (OGC Web Map Service)"

    layer_type = models.CharField(
        max_length=4,
        choices=LayerType.choices,
        default=LayerType.TMS,
        verbose_name=_("source type"),
    )

    url = models.CharField(max_length=256, null=True, blank=True, verbose_name=_("URL"))

    max_native_zoom = models.IntegerField(
        null=False,
        blank=False,
        verbose_name=_("source maximum native zoom"),
        help_text=_("Maximum zoom level provided by the source"),
    )

    attribution = models.CharField(
        max_length=255, null=False, blank=False, verbose_name=_("source attribution")
    )

    # WMS parameters

    class WMSFormat(models.TextChoices):
        png = "image/png", "image/png"
        png8 = "image/png8", "image/png8"
        jpeg = "image/jpeg", "image/jpeg"

    class WMSVersion(models.TextChoices):
        v100 = "1.0.0", "1.0.0"
        v110 = "1.1.0", "1.1.0"
        v111 = "1.1.1", "1.1.1"
        v130 = "1.3.0", "1.3.0"

    layers = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("source layer(s)"),
        help_text=_("Comma-separated list of source WMS layer(s) to display"),
    )

    proj_format = models.CharField(
        max_length=255,
        choices=WMSFormat.choices,
        null=True,
        blank=True,
        verbose_name=_("source format"),
    )

    proj_name = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("projection authority name"),
        help_text=_("Authority name of the source projection, in EPSG:XXXX format"),
    )

    proj_wkt = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("projection OGC WKT"),
    )

    wms_version = models.CharField(
        max_length=5,
        null=True,
        blank=True,
        choices=WMSVersion.choices,
        default=WMSVersion.v111,
        verbose_name=_("server version"),
    )

    styles = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_("Styles"),
        help_text=_("Comma-separated list of styles, leave blank to use default style"),
    )

    is_transparent = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name=_("is transparent"),
        help_text=_(
            "Whether the selected layer(s) contains any alpha transparency (alpha channel)"
        ),
    )

    def __str__(self):
        return f"{self.title} ({self.id})"

    def clean(self):
        super().clean()

        if self.url is None:
            raise ValidationError(
                str(self._meta.get_field("url").verbose_name)
                + " "
                + str(_("cannot be empty"))
                + "."
            )
        else:
            url = self.url.replace("{s}", "s")
            url = url.replace("{z}", "z")
            url = url.replace("{x}", "x")
            url = url.replace("{y}", "y")
            try:
                URLValidator(schemes=["https", "http"])(url)
            except:
                raise ValidationError(
                    str(self._meta.get_field("url").verbose_name)
                    + ": "
                    + str(_("invalid URL"))
                    + "."
                )

        if self.layer_type == self.LayerType.WMS:
            # Make sure that the mandatory parameters for WMS are present
            missing_for_wms = []
            if self.layers is None:
                missing_for_wms += [str(self._meta.get_field("layers").verbose_name)]
            if self.proj_format is None:
                missing_for_wms += [
                    str(self._meta.get_field("proj_format").verbose_name)
                ]
            if self.proj_name is None:
                missing_for_wms += [str(self._meta.get_field("proj_name").verbose_name)]
            if missing_for_wms:
                raise ValidationError(
                    _("The following fields must be filled for WMS")
                    + ": "
                    + ", ".join(missing_for_wms)
                )

            # Make sure that none of the WMS params managed by Mapproxy appears in the WMS querystring
            reserved_params = [
                "version",
                "crs",
                "srs",
                "layers",
                "styles",
                "transparent",
                "format",
            ]
            parsed_url = urllib.parse.urlparse(url)
            parsed_query_string = urllib.parse.parse_qs(parsed_url.query)
            if set(parsed_query_string.keys()) & set(reserved_params):
                raise ValidationError(
                    _(
                        "The WMS URL cannot contain any of the following querystring parameters:"
                    )
                    + " "
                    + ", ".join(reserved_params)
                )

        if self.proj_name is None or self.proj_name == "":
            self.proj_name = None
            self.proj_wkt = None
        else:
            try:
                crs = pyproj.CRS.from_string(self.proj_name)
                self.proj_name = crs.to_string()
                # Need to specify this WKT version, otherwise it cannot be parsed with proj4js
                self.proj_wkt = crs.to_wkt(
                    pyproj.enums.WktVersion.WKT1_GDAL, pretty=True
                )
            except Exception as e:
                raise ValidationError(e)

    def to_json(self):
        layer = {
            "id": self.id,
            "url": self.url,
            "type": self.layer_type,
            "title": self.title,
            "minZoom": self.min_zoom,
            "maxZoom": self.max_zoom,
            "maxNativeZoom": self.max_native_zoom,
            "attribution": self.attribution,
            "group": "Public data",
        }
        if self.layer_type == MapOverlay.LayerType.TMS:
            layer.update(inverted=False)
        elif self.layer_type == MapOverlay.LayerType.WMS:
            layer.update(
                layers=self.layers,
                format=self.proj_format,
                projName=self.proj_name,
                projWKT=self.proj_wkt,
            )
        return layer

    class Meta:
        abstract = True


class MapBaseLayer(AbstractMapLayer):

    display = models.BooleanField(
        default=False,
        verbose_name=_("display by default"),
        help_text=_(
            "Whether or not this is the default base layer. Only one base layer can be default at a time."
        ),
    )

    def to_json(self):
        layer = super().to_json()
        layer.update(display=self.display)
        return layer

    def clean(self):
        super().clean()

        if (
            self.pk
            and not self.display
            and not MapBaseLayer.objects.exclude(pk=self.pk)
            .filter(display=True)
            .exists()
        ):
            raise ValidationError(_("One base layer must be displayed by default"))

    class Meta:
        db_table = "map_base_layer"
        verbose_name = _("map base layer")
        verbose_name_plural = _("map base layers")
        help_text = _(
            "Map layers used as (mutually exclusive) background data sources for maps in the app"
        )


class MapOverlay(AbstractMapLayer):
    layer_order = models.IntegerField(
        null=True, verbose_name=_("order"), help_text=_("Stacking order of the layer")
    )

    display = models.BooleanField(
        default=False,
        verbose_name=_("display by default"),
        help_text=_("Whether this overlay is displayed by default."),
    )

    def to_json(self):
        layer = super().to_json()
        layer.update(layer_order=self.layer_order)
        return layer

    class Meta:
        ordering = ["layer_order"]
        db_table = "map_overlay"
        verbose_name = _("map overlay")
        verbose_name_plural = _("map overlays")
        help_text = _(
            "Map overlays used as optional overlaid data sources for maps in the app"
        )
