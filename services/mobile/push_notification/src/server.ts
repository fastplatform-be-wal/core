import assert from 'assert';
import fs from 'fs'
import {
  IS_GRAPHQL_SERVER_ELSE_REST,
  APNS_ENCRYPTION_KEY_PATH,
  APNS_ENCRYPTION_KEY_VALUE,
  APNS_KEY_ID,
  APNS_TEAM_ID,
  FCM_API_KEY_PATH,
  FCM_API_KEY_VALUE,
  APP_BUNDLE_ID_IOS,
  NODE_ENV,
  API_VERSION,
  SERVER_PORT
} from './config/env'

// Check that the required variables are defined.
if (APNS_ENCRYPTION_KEY_VALUE.toString() === '') {
  // Verify that the paths exist
  assert.ok(
    fs.existsSync(APNS_ENCRYPTION_KEY_PATH),
    `The path to the APNs encryption key file does not exist: ${APNS_ENCRYPTION_KEY_PATH}`
  )
}
if (FCM_API_KEY_VALUE.toString() === '') {
  // Verify that the paths exist
  assert.ok(
    fs.existsSync(FCM_API_KEY_PATH),
    `The path to the FCM API Key file does not exist: ${FCM_API_KEY_PATH}`
  )
}
assert.ok(APP_BUNDLE_ID_IOS, 'APP_BUNDLE_ID_IOS env variable is not set. Could not start the server without it.')
assert.ok(APNS_KEY_ID, 'APNS_KEY_ID env variable is not set. Could not start the server without it.')
assert.ok(APNS_TEAM_ID, 'APNS_TEAM_ID env variable is not set. Could not start the server without it.')


import { RestServer } from './serverRest'
import { GraphQLServer } from './serverGraphql'

// Note: if you change the base API url, you need to also update the routes rewriting for the REST server 
const baseAPIUrl = `/fastplatform/api/${API_VERSION || (process.env.npm_package_version ||  'latest')}`
const corsOptions = {
  origin: "*",
  methods: "GET,HEAD,POST,DELETE",
  allowedHeaders: "Origin,Content-Type,authorization,x-identity,ocp-apim-subscription-key",
  preflightContinue: false,
  optionsSuccessStatus: 200
}

if (IS_GRAPHQL_SERVER_ELSE_REST) {
  // start graphql server
  const graphqlServer = new GraphQLServer()
  console.log(`Starting server at 0.0.0.0:${SERVER_PORT}${baseAPIUrl}`)
  // TODO set https ? https://github.com/prisma-labs/graphql-yoga/blob/master/src/types.ts#L62-L65
  graphqlServer.start({
    port: SERVER_PORT,
    cors: corsOptions,
    endpoint: baseAPIUrl + '/graphql',
    subscriptions: baseAPIUrl + '/subscriptions',
    playground: baseAPIUrl + '/playground',
  })
} else {
  // start rest server
  const restServer = new RestServer(baseAPIUrl, {corsOptions: corsOptions})
  console.log(`Starting server at 0.0.0.0:${SERVER_PORT}${baseAPIUrl}`)
  restServer.start(SERVER_PORT)
}