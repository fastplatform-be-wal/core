
class FaSTRouter:
    """
    A router to control all database operations on models
    """
    def db_for_read(self, model, **hints):
        return 'external' if model._meta.app_label == 'external' else 'default'

    def db_for_write(self, model, **hints):
        return 'external' if model._meta.app_label == 'external' else 'default'

    def allow_relation(self, obj1, obj2, **hints):
        return (obj1._meta.app_label == 'external'
                and obj2._meta.app_label == 'external') or (
                    obj1._meta.app_label != 'external'
                    and obj2._meta.app_label != 'external')

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """Determine if a given model should be created in a given db

        Arguments:
            db {str} -- Database name as per Django settings
            app_label {app} -- App name

        Keyword Arguments:
            model_name {str} -- Model name (lowercase) (default: {None})

        Returns:
            bool -- True to create the model in the database
        """

        # Create all models from the 'external' app into the 'external' database
        if app_label == 'external':
            return db == 'external'

        # Create all models of all the other apps into the 'default' database
        else:
            return db == 'default'
