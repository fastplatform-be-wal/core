from django.utils.translation import gettext_lazy as _

from import_export.admin import ImportExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin
from farm.models import Campaign
from farm.resources import CampaignResource


class CampaignAdmin(ImportExportMixin, CommonModelAdmin):
    resource_class = CampaignResource
    search_fields = ("id", "name")
    list_display = ("id", "name", "is_current", "start_at", "end_at")
    fields = ("id", "name", "is_current", "start_at", "end_at")

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            readonly_fields += ("id",)
        return readonly_fields


admin_site.register(Campaign, CampaignAdmin)
