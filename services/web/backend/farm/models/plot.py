from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator

from utils.models import GetAdminURLMixin


class Plot(GetAdminURLMixin, models.Model):

    id = models.AutoField(
        primary_key=True, editable=False, verbose_name=_("identifier")
    )

    authority_id = models.CharField(
        max_length=256, null=True, blank=True, verbose_name=_("identifier (authority)")
    )

    site = models.ForeignKey(
        to="farm.Site",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="plots",
        verbose_name=_("site"),
    )

    name = models.CharField(
        max_length=256, null=True, blank=True, verbose_name=_("name")
    )

    geometry = models.MultiPolygonField(
        srid=settings.DEFAULT_SRID, null=False, blank=False, verbose_name=_("geometry")
    )

    valid_from = models.DateField(null=True, blank=True, verbose_name=_("valid from"))

    valid_to = models.DateField(null=True, blank=True, verbose_name=_("valid to"))

    # Autocalculated fields
    # ---------------------

    # The below fields are computed automatically by Postgres using generated columns
    # See migration "0053_plot_computed_columns.py" for details

    # area = models.FloatField(
    #     null=True,
    #     blank=True,
    #     verbose_name=_("area"),
    #     editable=False,
    # )

    # perimeter = models.FloatField(
    #     null=True, blank=True, verbose_name=_("perimeter"), editable=False
    # )

    # centroid = models.PointField(
    #     srid=settings.DEFAULT_SRID,
    #     null=True,
    #     blank=True,
    #     verbose_name=_("centroid"),
    #     editable=False,
    # )

    # bbox = models.PolygonField(
    #     srid=settings.DEFAULT_SRID,
    #     null=True,
    #     blank=True,
    #     verbose_name=_("bounding box"),
    #     editable=False,
    # )

    # bbox_leaflet = models.PolygonField(
    #     srid=settings.DEFAULT_LEAFLET_SRID,
    #     null=True,
    #     blank=True,
    #     verbose_name=_("bounding box with Leaflet SRID"),
    #     editable=False,
    # )

    # thumbnail = models.TextField(
    #     null=True,
    #     blank=True,
    #     verbose_name=_("thumbnail"),
    #     editable=False,
    # )

    def __str__(self):
        return self.name or self.authority_id or str(self.id)

    class Meta:
        db_table = "plot"
        verbose_name = _("plot")
        verbose_name_plural = _("plots")


class PlotPlantVariety(GetAdminURLMixin, models.Model):
    """A plant variety cultivated on a plot

    Note:
    - as a Plot object is local to a Site, which is local to a HoldingCampaign,
    a PlotPlantVariety is also local to a HoldingCampaign
    - we allow multiple plant varieties on the same plot during the
    same campaign (ie a many-to-many relationship)
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    plot = models.ForeignKey(
        to="farm.Plot",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="plot_plant_varieties",
        related_query_name="plot_plant_varieties",
        verbose_name=_("plot"),
    )

    plant_variety = models.ForeignKey(
        to="fieldbook.PlantVariety",
        null=False,
        blank=False,
        related_name="plot_plant_varieties",
        related_query_name="plot_plant_varieties",
        on_delete=models.PROTECT,
        verbose_name=_("plant variety"),
    )

    percentage = models.FloatField(
        verbose_name=_("Percentage"),
        null=False,
        blank=False,
        default=100.0,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage of the plot dedicated to this plant variety"),
    )

    irrigation_method = models.ForeignKey(
        to="fieldbook.IrrigationMethod",
        null=True,
        blank=True,
        verbose_name=_("irrigation method"),
        on_delete=models.PROTECT,
    )

    crop_yield = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("Crop yield in kg/ha"),
    )

    class Meta:
        db_table = "plot_plant_variety"
        verbose_name = _("plot plant variety")
        verbose_name_plural = _("plot plant varieties")

        constraints = (
            models.UniqueConstraint(
                name="plot_plant_variety_plot_id_plant_variety_id_unique",
                fields=("plot", "plant_variety"),
            ),
        )
