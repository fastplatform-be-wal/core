# `core/event/farm`: Events handler for farm updates

- [`core/event/farm`: Events handler for farm updates](#coreeventfarm-events-handler-for-farm-updates)
  - [Architecture](#architecture)
  - [Constraints](#constraints)
    - [Computation](#computation)
    - [Data model](#data-model)
  - [Soil Estimates](#soil-estimates)
    - [Computation](#computation-1)
    - [Data model](#data-model-1)
    - [Validation rules for observable properties](#validation-rules-for-observable-properties)
      - [Numeric](#numeric)
      - [Text](#text)
      - [Boolean](#boolean)
      - [Category](#category)
  - [Environment variables](#environment-variables)
  - [Development Setup](#development-setup)
  - [Running tests](#running-tests)
  - [In Altair (or any graphql ui)](#in-altair-or-any-graphql-ui)

The [core/event/farm](./) service is a [FastAPI](https://fastapi.tiangolo.com/) service that:
- **computes** plot constraints and soil derived objects
- **caches** the result in the `fastplatform` database for further use
- exposes several web hooks that are called by the [core/api_gateway](../../api_gateway/) when selected **events** triggered by Postgres signal that previously computed values in cache may not be accurate anymore and need to be recomputed.

Its main roles are to compute:
- *constraints* (e.g. intersections with NVZ and Natura2000) 
- and *soil estimates* (based on private samples and public data).

## Architecture

```plantuml
package "FaST" as fast {
    folder "\nGraphql hasura events" as api_gateway <<Api Gateway>>
    database "\nfarm, plots, plant_species,… " as pg_fastplatform << fastplatform >>
    database "\nhydro, NVZs,…" as pg_external << external >>

    folder "Event services" as event_services #yellow {
      component "core / event / farm" as core_event_farm
    }

    actor user
    component "mobile app" as mobile #lightblue
    component "\nregulatory/nitrate, etc…" as other_services << Other services  >> #lightpink
}
pg_fastplatform -down-> api_gateway: ""events"" on plots,\nobservation, soil_sites...
pg_external -down-> api_gateway: ""events"" on surface_water_version,\nprotected_site_version,…
api_gateway -right-> core_event_farm: API calls on ""event"" to disable existing values
api_gateway <-right-> core_event_farm: API calls on ""demand""

user <-right-> mobile
mobile <-right-> api_gateway: query
other_services <-up-> api_gateway: query

```


## Constraints

Constraints of a given plots can be found in the `constraint` object.

Constraints can be of 4 types:
- `management_restriction_or_regulation_zone`
- `protected_site`
- `water_course`
- `surface_water`

### Computation

The constraints are computed only when the farmer app or any other FasT service requires to have access to it for a certain plot. After they have been initially computed, the result is stored into the `fastplatform`.`constraint` object. Further request to access the constraints for a given plot will not trigger a recomputation of the constraints, unless no constraint could be determined previously. Certain events (database triggers) monitored by the service invalidate this caching mechanism by deleting the constraint object associated to a plot, when certain conditions arise.

- Constraints of type `management_restriction_or_regulation_zone` and `protected_site`
  They are computed by finding out which `external`.`management_restriction_or_regulation_zone` and `external`.`protected_site` **intersect** the geometry of the plot.
- Constraints of type `water_course` and `surface_water`
  They are computed by finding out which `external`.`surface_water` and `external`.`surface_water` are not too far from the geometry of the plot (see [API_PLOT_COMPUTE_CONSTRAINTS_SURFACE_WATER_SEARCH_DISTANCE](app/settings.py) and [API_PLOT_COMPUTE_CONSTRAINTS_WATER_COURSE_SEARCH_DISTANCE](app/settings.py)).
- Caching invalidation
  The previously computed constraints of the plot are deleted when any of the following events occur:
  
| database monitored | table/column monitored                                            | event    | objects deleted                                                                                  |
| ------------------ | ----------------------------------------------------------------- | -------- | ------------------------------------------------------------------------------------------------ |
| external           | `management_restriction_or_regulation_zone`.`version`.`is_active` | *UPDATE* | all `constraints` of type `management_restriction_or_regulation_zone` for all plots of all farms |
| external           | `protected_site`.`version`.`is_active`                            | *UPDATE* | all `constraints` of type `protected_site` for all plots of all farms                            |
| external           | `water_course`.`version`.`is_active`                              | *UPDATE* | all `constraints` of type `water_course` for all plots of all farms                              |
| external           | `surface_water`.`version`.`is_active`                             | *UPDATE* | all `constraints` of type `surface_water` for all plots of all farms                             |

### Data model
The Constraint object exposes mainly 3 fields: 
- `plot_id`: links the constraint to a specific plot;
- `name`: represents the type of constraint:
  - `water_course`
  - `protected_site`
  - `management_restriction_or_regulation_zone`
  - `surface_water`
- `description`: a json field that contains the specifics of one constraint of a certain type for a certain plot.

> Constraints are associated to plots through an array relationship, thus several constraints can be associated to the same plot.
> The same plot can also have several Constraints of the same type, ex: several `water_course` constraints, if the plot is located near more than one river.$
> A plot can also have no Constraint whatsoever of a given type, ex: no `water_course` constraint, if it located far from any river.

```plantuml
package "fastplatform database" as fastplatform_db <<Rectangle>> {
    class plot {
        id
    }

    class constraint {
        id
        plot_id
        name
        description
    }

    constraint::plot_id -up-> plot:id
}

package "external database" as external_db <<Rectangle>> {
      class management_restriction_or_regulation_zone {
          id
          name
          geometry
          version_id
      }

      class management_restriction_or_regulation_zone_version {
          id
          is_active
      }
          
      class protected_site {
          id
          name
          geometry
          version_id
      }

      class protected_site_version {
          id
          is_active
      }
      
      class water_course {
          id
          name
          geometry
          version_id
      }

      class water_course_version {
          id
          is_active
      }
          
      class surface_water {
          id
          name
          geometry
          version_id
      }

      class surface_water_version {
          id
          is_active
      }

      management_restriction_or_regulation_zone::version_id -up-> management_restriction_or_regulation_zone_version::id
      protected_site::version_id -up-> protected_site_version::id
      water_course::version_id -up-> water_course_version::id
      surface_water::version_id -up-> surface_water_version::id
}

management_restriction_or_regulation_zone "intersects" -down-> constraint
protected_site "intersects" -down-> constraint
surface_water "not too far" -down-> constraint
water_course "not too far" -down-> constraint

```

## Soil Estimates

### Computation

The soil estimate of a given plot can be found in the `soil_derived_object` (the sample). It holds any number of `derived_observations` (scientific measurement or determination) associated to it. 

The soil estimates are computed only when the farmer app or any other FasT service requires to have access to it for a certain plot.

Here is how we compute compute the `soil_derived_object` and `derived_observations` for a given plot:
1. We first check whether some private sample exists for the farm, that meets the criteria required to be associated to a given plot. 
   If such a **private** sample *intersects* with the plot, we copy the `fastplatform`.`soil_site` object (private sample) into a new `fastplatform`.`soil_derived_object`. We then do the same for all the `fastplatform`.`observations` associated to the `fastplatform`.`soil_site`, by copying them into new `fastplatform`.`derived_observations` linked to the newly created `fastplatform`.`soil_derived_object`.
2. If no private sample is available, we check whether any public sample exists *at a distance* not too great to the plot. 
   If such a **public** sample exists, we copy the `external`.`soil_site` object (public sample) into a new `fastplatform`.`soil_derived_object`. We then do the same for all the `external`.`observations` associated to the `external`.`soil_site`, by copying them into new `fastplatform`.`derived_observations` linked to the newly created `fastplatform`.`soil_derived_object`.
3. If no private and no public data are available, we create an **empty** `soil_derived_object`. 
   It signifies that the computation has been carried out successfully, but it did not turn out any value. Unless some exterior event happens (cache invalidation), there is no need to be compute it again. The "empty" object has the following structure:
    ```py
    # Structure of an "empty" soil_derived_object:
    soil_derived_object = {
        "plot_id": plot_id,
        "is_derived_from": {},
        "soil_sample_origin": "none",
        "soil_sites_count": 0,
        "derived_observations": [],
    }
    ```
4. Cache invalidation
   Finally, a series of events (database triggers) are monitored by the service. They determine the conditions under which the `soil_derived_object` and its associated `derived_observations` are going to be deleted for the plot. This deletion in effect invalidates the cache. It forces the present service to recompute the plot sample next time it is queried by some other service or by the farmer app.
   The previously computed constraints of the plot are deleted when any of the following events occur:

| database monitored | table/column monitored          | event    | objects deleted                                                                                       |
| ------------------ | ------------------------------- | -------- | ----------------------------------------------------------------------------------------------------- |
| fastplatform       | `campaign`.`end_at`             | *UPDATE* | all `soil derived objects` of the updated campaign                                                    |
| fastplatform       | `plot`.`geometry`               | *UPDATE* | all `soil derived objects` and `constraints` of updated plot                                          |
| fastplatform       | `soil_site`                     | *INSERT* | all `soil derived objects` whose plot intersect new `soil_site` geometry                              |
| fastplatform       | `soil_site`                     | *UPDATE* | 1. all `soil derived objects` whose plot intersect **old** `soil_site` geometry                       |
|                    |                                 |          | 2. all `soil derived objects` whose plot intersect **new** `soil_site` geometry                       |
| fastplatform       | `soil_site`                     | *DELETE* | all `soil derived objects` whose plot intersect old `soil_site` geometry                              |
| fastplatform       | `observation`                   | *DELETE* | all `soil derived objects` whose plot intersect the geometry of the `soil_site` for the `observation` |
| external           | `soil_site_version`.`is_active` | *UPDATE* | all `soil derived objects` for all plots of all farms                                                 |



### Data model

1. `fastplatform`.`soil_derived_object`
   Holds the computed sample object for the plot. Its source is either a private observation made on the farm from the `fastplatform` database, or public data from the `external` database. One sample can have many `derived_observation`.
2. `fastplatform`.`derived_observation`
   Holds one scientific measurement or determination associated to the sample. e.g: *pH*, *soil depth*, *plot slope*. Again, its source is either private of public.
3. `fastplatform`.`soil_site`
   Represents **private** samples that are made on a farm, at a specific `geometry` (location). They belong (= "foreign key") to the `holding` object. What ultimately allows them to be used to compute the `soil_derived_object` of a particular plot is:
    - their *intersection* with the `geometry` of the `plot`
    - the date at which the `soil_site` was made.
4. `fastplatform`.`observation`
   Any scientific measure or determination associated the **private** sample. They belong (= "foreign key") to the `fastplatform`.`soil_site` object. The type of measurement or determination is specified by their `observable_property`.
5. `fastplatform`.`observable_property`
   A type of scientific measurement or determination. Observable properties come with a set of **validation rules** to constraint the user input, and make sure it can be used for other computations by other services.
6. `external`.`soil_site`
   Represent **public** samples made during public survey campaigns. They have a specific `geometry` (location). What ultimately allows them to be used to compute the `soil_derived_object` of a particular plot is:
    - the fact that no private sample made before the end of the current campaign has been found for the plot, 
    - their *distance* to the `geometry` of the `plot` is smaller than some value (see [API_PLOT_COMPUTE_SOIL_DERIVED_OBJECT_PUBLIC_SOIL_SITE_SEARCH_DISTANCE](app/settings.py))
    - the `soil_site_version` is active
    - the date at which the `observations` were made.
7. `external`.`observation`
   Any scientific measure or determination associated the **public** sample. They belong (= "foreign key") to the `external`.`soil_site` object. The type of measurement or determination is specified by their `observable_property`.
8. `external`.`observable_property`
   A type of scientific measurement or determination.


```plantuml
package "fastplatform database" as fastplatform_db <<Rectangle>> {
    class holding {
        id
    }

    folder "farmer sample" #lightblue {
      class soil_site {
          id
          holding_id
          geometry
          valid_from
          valid_to
          source
      }

      class observation {
          id
          result
          result_time
          soil_site_id
          observed_property_id
      }
    }

    class observable_property {
        id
        name
        validation_rule
    }

    soil_site::holding_id -up-> holding::id
    observation::soil_site_id -up-> soil_site::id
    observation::observed_property_id -up-> observable_property::id

    class plot << A plot that belongs to a farm >> {
        id
    }

    folder "Computed sample" #yellow {
      class soil_derived_object << The sample computed for the plot >> {
          id
          plot_id
      }

      class derived_observation << The computed measure or determination >> {
          id
          soil_derived_object_id
          observed_property_id
      }
    }

    soil_derived_object::plot_id -up-> plot::id
    derived_observation::soil_derived_object_id -up-> soil_derived_object::id
    derived_observation::observed_property_id -up-> observable_property::id
}

package "external database" as external_db <<Rectangle>> {
    folder "public sample" #lightblue {
      class external__soil_site {
          id
          geometry
          version_id
      }
          
      class external__observation {
          id
          soil_site_id
          observed_property_id
      }
    }

    class external__soil_site_version {
        id
        is_active
    }

    class external__observable_property {
        id
        name
        validation_rule
    }
    external__soil_site::version_id -up-> external__soil_site_version::id
    external__observation::soil_site_id -up-> external__soil_site::id
    external__observation::observed_property_id -up-> external__observable_property::id
}
```

### Validation rules for observable properties

Since the result measured for a given observation can either come from public data or from **direct user input**, it is important to maintain some kind of validation rules, so as to make sure that the data can be used by various algorithms without any problem.

> If no validation rule is chosen, the result is always considered valid as long as it is defined.

When it comes to validating the result of an **<x>Observation<x>**, the same exact following logic is applied, irrespective of whether the observation is **<x>Private<x>** (user input) or a **<x>Public<x>** (public data) in nature.

Validation rules are expressed in the form of a **JSON object**.

Here is a straightforward example, typical for an observation whose unit is expressed as a percentage that cannot be negative or greater than 100%:

```json
{
    "min": 0,
    "max": 100,
    "type": "numeric"
}
```

Validation rules can be of 4 different types: **<g>numerical<g>**, **<g>textual<g>**, **<g>boolean<g>**, or a **<g>category<g>**.

#### Numeric
The result must be a number.

- If a `min` key is found inside the JSON object, the result must be greater than or equal to the value of this key,
- If a `max` key is found inside the JSON object, the result must be lower than or equal to the value of this key,
- If no `min` or `max` key are found, the result is considered valid as long as it is a number.

In the example below, we only check whether the result is greater than zero:
```json
{
    "min": 0,
    "type": "numeric"
}
```

#### Text
The result must be a text.

```json
{
    "type": "text"
}
```

#### Boolean
The result must be either true or false.

```json
{
    "type": "boolean"
}
```

#### Category
The result must be a specific text chosen amongst the available _choices_.

Here is an example of a validation rule for soil density:

```json
{
    "choices": ["light", "medium", "heavy"],
    "type": "category"
}
```



## Environment variables

- `API_GATEWAY_FASTPLATFORM_URL`: URL of the fastplatform API Gateway
- `API_GATEWAY_SERVICE_KEY`: secret servive key to access the fastplatform Hasura API Gateway
- `API_GATEWAY_TIMEOUT`: The timeout value in seconds used to access fastplatform gateway. Defaults to `120` (= 2 minutes), because some farms can have thousands of plots to insert.
- `LOG_LEVEL`: logging level (trace, debug, info, warn, error, fatal)
- `API_PLOT_COMPUTE_CONSTRAINTS_SURFACE_WATER_SEARCH_DISTANCE`: the maximum distance to a surface_water used to compute a constraint of type `surface_water`.
- `API_PLOT_COMPUTE_CONSTRAINTS_WATER_COURSE_SEARCH_DISTANCE`: the maximum distance to a water_course used to compute a constraint of type `water_course`.
- `API_PLOT_COMPUTE_SOIL_DERIVED_OBJECT_PUBLIC_SOIL_SITE_SEARCH_DISTANCE`: the maximum distance to a external.soil_site used to compute `soil_derived_object`.


## Development Setup

Create a Python virtualenv and activate it:
```sh
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```sh
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```sh
make start-tracing
```

Jaeger UI will be available at [http://localhost:16686]()

Start the service:
```sh
make start
```

The API server is now started and available at http://localhost.fastplatform.eu:7010/graphql.

**Other services (like `regulatory/nitrate`) need to access `core/event/farm` through a remote schema mounted in the API Gateway.**

In order to do that, make sure your local version of `core/services/api_gateway/fastplatform/metadata/remote_schemas.yaml` contains the following:
```yaml
- name: core-event-farm
  definition:
    url_from_env: REMOTE_SCHEMA_URL_CORE_EVENT_FARM
    timeout_seconds: 60
```

If needed, apply metadata:
```bash
cd …/core/services/api_gateway/fastplatform

make apply
```

Open Hasura UI, make sure the `core-event-farm` remote schema is visible on page `[hasura]/console/remote-schemas/manage/schemas`.

Troubleshooting:

If `core-event-farm` does not appear in list, try the following:
- Simply refresh page
- If still not working, open `[hasura]/console/settings/metadata-actions`, check `Reload all remote schemas`, and click `Reload`.


**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run 
```

## Running tests

Executing the tests located in `tests/...`, simply launch
```sh
make test
```

For code coverage:
```sh
make test
make report
```

BEWARE:
`query_testing` and `event_testing` require:
- an instance of hasura up and running
- to have `clear-region` and `init-region` both on `default` and `external` for a given region (eg: `be-wal`):

```bash
$ make clear-region REGION=be-wal; make init-region REGION=be-wal; make clear-region-external REGION=be-wal; make init-region-external REGION=be-wal
```

## In Altair (or any graphql ui)


```graphql
query computed_soil_derived_object {
  computed_soil_derived_object(plot_id: 1) {
    id
    is_derived_from
    soil_sites_count
    soil_sample_origin
    derived_observations {
      observed_property_id
      result
    }
  }
}

query computed_soil_derived_object_FULL {
  computed_soil_derived_object(plot_id: 1) {
    id
    is_derived_from
    plot_id
    soil_sample_origin
    soil_sites_count
    derived_observations {
      id
      observed_property_id
      result
      observable_property {
        id
        label
        unit_of_measure {
          id
          name
          symbol
        }
      }
    }
  }
}

query computed_constraints_water_course{
  computed_constraints_water_course(plot_id: 1){
    id
    name
    description
  } 
}

query computed_constraints_surface_water{
  computed_constraints_surface_water(plot_id: 1) {
    id
    name
    description
  }
}

query computed_constraints_protected_site{
  computed_constraints_protected_site(plot_id: 1) {
    id
    name
    description
  }
}

query computed_constraints_management_restriction_or_regulation_zone{
  computed_constraints_management_restriction_or_regulation_zone(plot_id: 1) {
    id
    name
    description
  }
}

query computed_plot_constraints{
  computed_plot_constraints(plot_id: 1) {
    id
    name
    description
  }
}


query computed_constraint_by_pk{
  computed_constraint_by_pk(id: 1146){
    id
    name
    description
    plot_id
  }
}

query computed_soil_derived_object_by_pk{
  computed_soil_derived_object_by_pk(id: 73){
    id
    is_derived_from
    soil_sample_origin
    soil_sites_count
    plot_id
    derived_observations {
      observed_property_id
      result
    }
  }
}
```
