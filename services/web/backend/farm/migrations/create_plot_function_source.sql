CREATE OR REPLACE FUNCTION public.plot_function_source(
		z integer,
		x integer,
		y integer,
		p_session_key text DEFAULT NULL::varchar(255),
		p_access_token text DEFAULT NULL::varchar(255),
		p_holding_campaign_id integer DEFAULT NULL::integer,
		p_site_id integer DEFAULT NULL::integer,
		p_plot_id integer DEFAULT NULL::integer,
		p_plot_group_id integer DEFAULT NULL::integer
	) RETURNS bytea LANGUAGE 'plpgsql' STABLE CALLED ON NULL INPUT PARALLEL SAFE LEAKPROOF AS $BODY$
DECLARE mvt bytea;
BEGIN
SELECT INTO mvt ST_AsMVT(tile, 'plot', 4096, 'geom')
FROM (
		SELECT ST_AsMVTGeom(
				ST_Transform(
					ST_SimplifyPreserveTopology(plot.geometry, 1 /(2 ^ z)),
					3857
				),
				TileBBox(z, x, y, 3857),
				4096,
				64,
				true
			) AS geom,
			plot.id,
			plot.name AS plot_name,
			site.id AS site_id,
			site.name AS site_name,
			holding_campaign.id AS holding_campaign_id,
			campaign.id AS campaign_id,
			campaign.name AS campaign_name,
			holding.id AS holding_id,
			holding.name AS holding_name
		FROM plot
			INNER JOIN site ON plot.site_id = site.id
			INNER JOIN holding_campaign ON site.holding_campaign_id = holding_campaign.id
			INNER JOIN campaign ON holding_campaign.campaign_id = campaign.id
			INNER JOIN holding ON holding_campaign.holding_id = holding.id
			LEFT JOIN user_related_party ON user_related_party.holding_id = holding.id
			LEFT JOIN public.user ON user_related_party.user_id = public.user.id
			LEFT JOIN oidc_provider_token ON oidc_provider_token.user_id = public.user.id
		WHERE --
			-- Check authentication 
			(
				(
					-- Authentication from a user token, check that the token is valid
					-- and that the user is related party to the holding
					p_access_token IS NOT NULL
					AND oidc_provider_token.access_token = encode(sha256(p_access_token::bytea), 'hex')
					AND oidc_provider_token.access_expires_at >= NOW()
					AND public.user.is_active
				)
				OR (
					-- Authentication from a user session, check that the session is valid
					-- and that the user is staff or superuser
					p_session_key IS NOT NULL
					AND EXISTS(
						SELECT *
						FROM user_sessions_session
							INNER JOIN public.user uu ON user_sessions_session.user_id = uu.id
						WHERE user_sessions_session.session_key = p_session_key
							AND user_sessions_session.expire_date >= NOW()
							AND (
								uu.is_staff
								OR uu.is_superuser
							)
							AND uu.is_active
					)
				)
			) --
			-- Constrain to tile bounds
			AND ST_Intersects(
				plot.geometry,
				ST_Transform(TileBBox(z, x, y, 3857), 4258)
			) --
			-- Constrain to farm selection
			AND (
				CASE
					WHEN p_holding_campaign_id IS NULL THEN TRUE
					ELSE holding_campaign.id = p_holding_campaign_id
				END
			)
			AND (
				CASE
					WHEN p_site_id IS NULL THEN TRUE
					ELSE site.id = p_site_id
				END
			)
			AND (
				CASE
					WHEN p_plot_id IS NULL THEN TRUE
					ELSE plot.id = p_plot_id
				END
			)
		GROUP BY plot.id,
			site.id,
			holding_campaign.id,
			campaign.id,
			holding.id
	) AS tile
WHERE geom IS NOT NULL;
RETURN mvt;
END $BODY$;