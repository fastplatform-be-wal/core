from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farm', '0002_auto_20200625_1901')
    ]

    operations = [
        migrations.RunSQL((Path(__file__).parent / Path('create_tilebbox_function.sql')).read_text()),
        migrations.RunSQL((Path(__file__).parent / Path('create_plot_function_source.sql')).read_text())
    ]
