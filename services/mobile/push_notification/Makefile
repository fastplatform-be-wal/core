# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

include secrets.env
export $(shell sed 's/=.*//' secrets.env)

.PHONY: start-redis
start-redis: ## 
	docker run --name push-notification-server \
		-e REDIS_PASSWORD=$(REDIS_AUTH_PASSWORD) \
		-p 6379:6379 \
		-h 0.0.0.0 \
		-v $(shell pwd)/_data:/bitnami/redis/data \
		-d bitnami/redis:latest

.PHONY: stop-redis
stop-redis: ## 
	-docker stop push-notification-server
	-docker rm push-notification-server

.PHONY: start-graphql
start-graphql: ## 
	IS_GRAPHQL_SERVER_ELSE_REST="true" \
	NODE_ENV="development" \
	API_VERSION="dev" \
	APNS_KEY_ID=$(APNS_KEY_ID) \
	APNS_TEAM_ID=$(APNS_TEAM_ID) \
	APP_BUNDLE_ID_IOS=$(APP_BUNDLE_ID_IOS) \
	REDIS_AUTH_PASSWORD=$(REDIS_AUTH_PASSWORD) \
	APNS_ENCRYPTION_KEY_PATH=$(APNS_ENCRYPTION_KEY_PATH) \
	APNS_ENCRYPTION_KEY_VALUE=$$(echo $$APNS_ENCRYPTION_KEY_VALUE) \
	FCM_API_KEY_VALUE=$(FCM_API_KEY_VALUE) \
	npm run dev

.PHONY: start-rest
start-rest: ## 
	IS_GRAPHQL_SERVER_ELSE_REST="false" \
	NODE_ENV="development" \
	API_VERSION="dev" \
	APNS_KEY_ID=$(APNS_KEY_ID) \
	APNS_TEAM_ID=$(APNS_TEAM_ID) \
	APP_BUNDLE_ID_IOS=$(APP_BUNDLE_ID_IOS) \
	REDIS_AUTH_PASSWORD=$(REDIS_AUTH_PASSWORD) \
	APNS_ENCRYPTION_KEY_PATH=$(APNS_ENCRYPTION_KEY_PATH) \
	APNS_ENCRYPTION_KEY_VALUE=$(APNS_ENCRYPTION_KEY_VALUE) \
	FCM_API_KEY_VALUE=$(FCM_API_KEY_VALUE) \
	npm run dev

.PHONY: bazel-run
bazel-run: ## Run the service locally with bazel
	bazel run .
	
