import logging
from shapely.geometry import shape

from app.api.lib.gis import add_crs

from opentelemetry import trace
from gql import gql
from app.settings import config
from app.db.graphql_clients import fastplatform

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def delete_all_soil_derived_object_whose_plot_belong_to_campaign(
    campaign_id: int,
):
    """
    For a single campaign, delete soil_derived_objects that validate the following relation:
        soil_derived_object -> plot -> site -> holding_campaign -> $campaign_id

        ( "->" = "belongs to" )

    Args:
        campaign_id (int): the id of the campaign

    Returns:
        affected_rows
    """
    with tracer().start_as_current_span(
        "delete_all_soil_derived_objects_whose_plot_intersect_geometry"
    ):

        logger.info(
            "Deleting all soil_derived_object whose plot belong to campaign_id : %s",
            campaign_id,
        )
        mutation = (
            config.API_DIR
            / "controllers"
            / "graphql"
            / "mutation_delete_all_soil_derived_object_whose_plot_belong_to_campaign.graphql"
        ).read_text()
        variables = {"campaign_id": campaign_id}

        return await fastplatform.execute(gql(mutation), variables)
