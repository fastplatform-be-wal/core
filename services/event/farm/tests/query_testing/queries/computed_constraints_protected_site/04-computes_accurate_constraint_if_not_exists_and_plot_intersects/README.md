# 04-computes_accurate_constraint_if_not_exists_and_plot_intersects
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 protected_site that has some intersection with plot
- No precomputed protected_site constraint.

## Computed
A new constraint with accurate `plot_pct_in_protected_site` and `plot_area_in_protected_site`.

## Returns
The constraint with the correct distance.