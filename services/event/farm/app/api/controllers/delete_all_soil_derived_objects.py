import logging

from opentelemetry import trace
from gql import gql
from app.settings import config
from app.db.graphql_clients import fastplatform

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def delete_all_soil_derived_objects():
    """
    Deletes all soil_derived_object in db.

    No arguments

    Returns:
        affected_rows
    """
    with tracer().start_as_current_span("delete_all_soil_derived_objects"):
        logger.info("Deleting all soil_derived_objects")
        mutation = (
            config.API_DIR
            / "controllers"
            / "graphql"
            / "mutation_delete_all_soil_derived_objects.graphql"
        ).read_text()

        return await fastplatform.execute(gql(mutation), {})
