import graphene
from graphene.types.generic import GenericScalar


class ComputedConstraint(graphene.ObjectType):
    id = graphene.Int(required=True)
    name = graphene.String()
    description = GenericScalar()
    plot_id = graphene.Int(required=True)

    class Meta:
        name = 'computed_constraint'
