from django.utils.translation import gettext_lazy as _
from django.contrib import admin
from django.conf import settings
from django import forms
from django.core.exceptions import ValidationError

from import_export.admin import ImportExportMixin
from django_object_actions import DjangoObjectActions

from fastplatform.site import admin_site

from external.resources import (
    PhenomenonTypeResource,
    SoilInvestigationPurposeResource,
    ObservablePropertyResource,
    ObservationResource,
    SoilSiteResource,
    SoilSiteVersionResource,
)
from external.models import (
    SoilSite,
    SoilSiteVersion,
    Observation,
    PhenomenonType,
    SoilInvestigationPurpose,
    ObservableProperty,
)

from external.admin.version import VersionAdmin, VersionActionsMixin
from utils.admin import CommonModelAdmin


class SoilInvestigationPurposeAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = SoilInvestigationPurposeResource
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class PhenomenonTypeAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = PhenomenonTypeResource
    autocomplete_fields = ("parent",)
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class ObservablePropertyAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    autocomplete_fields = ("base_phenomenon",)
    resource_class = ObservablePropertyResource
    list_display = ("id", "label", "base_phenomenon", "uom")
    search_fields = ("id", "label")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [
                None,
                {
                    "fields": (
                        "id",
                        "label",
                        "base_phenomenon",
                        "uom",
                        "validation_rules",
                    )
                },
            ]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class ObservationAdmin(CommonModelAdmin):
    hide_from_dashboard = True
    autocomplete_fields = (
        "soil_site",
        "observed_property",
    )
    exclude = ["source"]
    search_fields = ("id",)
    resource_class = ObservationResource


class ObservationInlineForm(forms.ModelForm):
    """
    Override the default form for the Observation inline
    to deal with the result JSONField
    """

    class Meta:
        model = Observation
        fields = ("id", "observed_property", "phenomenon_time", "result")
        widgets = {
            "result": forms.Textarea(attrs={"rows": 1, "cols": 60}),
        }


class ObservationInline(admin.TabularInline):
    model = Observation
    form = ObservationInlineForm

    extra = 0

    verbose_name = _("property")
    verbose_name_plural = _("properties")

    def get_queryset(self, request):
        return super().get_queryset(request).select_related("observed_property")


class SoilSiteAdmin(VersionActionsMixin, DjangoObjectActions, CommonModelAdmin):
    hide_from_dashboard = False
    model = SoilSite
    version_model = SoilSiteVersion
    resource_class = SoilSiteResource

    exclude = ["source"]
    list_display = (
        "authority_id",
        "version",
        "soil_investigation_purpose",
        "valid_from",
        "valid_to",
    )
    search_fields = ("_link", "label", "authority_id")
    list_select_related = ("version", "soil_investigation_purpose")
    list_filter = ("version__is_active",)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["version"]
        return []

    def get_queryset(self, request):
        return super().get_queryset(request).select_related("version")

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "authority_id",
                    "version",
                    "soil_investigation_purpose",
                )
            },
        ),
        (_("Validity"), {"fields": (("valid_from", "valid_to"),)}),
        (
            _("Lifespan"),
            {"fields": (("begin_lifespan_version", "end_lifespan_version"),)},
        ),
        (_("Geographic information"), {"fields": ("geometry",)}),
    )

    inlines = [ObservationInline]

    # Map parameters
    # --------------
    geometry_fields = ["geometry"]


class SoilSiteVersionAdmin(VersionAdmin):
    hide_from_dashboard = True
    model = SoilSiteVersion
    resource_class = SoilSiteVersionResource


if settings.ENABLE_EXTERNAL_SOIL_SITE:
    admin_site.register(SoilSite, SoilSiteAdmin)
    admin_site.register(SoilSiteVersion, SoilSiteVersionAdmin)
    admin_site.register(Observation, ObservationAdmin)
    admin_site.register(SoilInvestigationPurpose, SoilInvestigationPurposeAdmin)
    admin_site.register(PhenomenonType, PhenomenonTypeAdmin)
    admin_site.register(ObservableProperty, ObservablePropertyAdmin)
