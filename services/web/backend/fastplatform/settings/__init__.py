import os

BASE_DIR = os.path.dirname(
    (os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
)

# Overrides
# ---------

# Silence the "db_table 'X' is used by multiple models"
SILENCED_SYSTEM_CHECKS = ["models.W035", "admin.E410"]

# Add ability to add a help_text and a show_on_dashboard flag to a Model
# As per https://stackoverflow.com/questions/1088431/adding-attributes-into-django-models-meta-class
import django.db.models.options as options
options.DEFAULT_NAMES = options.DEFAULT_NAMES + ("help_text", "hide_from_dashboard")

MULTIPROCESSING_USE_FORK = os.environ.get('MULTIPROCESSING_USE_FORK', False)
if MULTIPROCESSING_USE_FORK:
    import multiprocessing
    multiprocessing.set_start_method('fork')

# Load the rest of the settings
from .add_ons import *
from .api_gateways import *
from .app_and_support import *
from .authentication import *
from .axes import *
from .cache import *
from .cors import *
from .databases import *
from .docs import *
from .email import *
from .features import *
from .geo import *
from .i18n import *
from .import_export import *
from .installed_apps import *
from .json_editor import *
from .logging import *
from .media import *
from .messages import *
from .middlewares import *
from .oidc_provider import *
from .security import *
from .static import *
from .templates import *
from .timezone import *
from .urls import *
from .vector_tiles import *
from .version import *
from .wsgi import *

from .debug import *
