import os
import io
import logging
import json

from ruamel.yaml import YAML
from app.api.k8s_interface import ConfigMapManager
from app.settings import config


# Log
logger = logging.getLogger(__name__)


def get_identifier(identifier: str) -> str:
    return identifier.lower()


def get_regional_layer_name(identifier: str) -> str:
    if identifier is None:
        raise ValueError("Layer identifier cannot by None.")

    proj = config.MAPPROXY_DEFAULT_PROJ.replace(":", "")

    return identifier.lower() + "_" + proj


def get_standard_layer_name(identifier: str) -> str:
    if identifier is None:
        raise ValueError("Layer identifier cannot by None.")

    proj = config.MAPPROXY_STANDARD_PROJ.replace(":", "")

    return identifier.lower() + "_" + proj


def generate_layer_name(name: str) -> str:
    return name.replace(" ", "_").lower()


def generate_mapproxy_tms_url(url: str) -> str:
    format_url = url

    # Mapproxy is not compatible with {s}
    format_url = format_url.replace("{s}", "a")

    format_url = format_url.replace("{", "%(")
    format_url = format_url.replace("}", ")s")
    return format_url


def parse_yaml(yaml_to_parse: str):
    """
    Parse string yaml formated and return a dictionnary with the input content

    Args:
        yaml_to_parse (str): The yaml to be parsed

    Returns:
        dict: The input string as a dict. None if exception.
    """
    yaml = YAML()
    parsed_yaml = yaml.load(yaml_to_parse)

    return parsed_yaml


class MapproxyConfig:

    base_proj: str

    def __init__(self, description: str):

        # get projection from environment
        self.base_proj = os.environ.get("FAST_MAPPROXY_BASE_POJECTION", "EPSG:3857")
        self.configuration = parse_yaml(description)

    def to_yaml(self):
        """Get the configuration at yaml format

        Returns:
            str: The yaml description
        """
        yaml = YAML()
        yaml.preserve_quotes = True
        buf = io.BytesIO()
        yaml.dump(self.configuration, buf)
        return buf.getvalue()

    def get_layers(self):
        return self.configuration["layers"]

    def get_sources(self):
        return self.configuration["sources"]

    def get_wms_srs(self):
        srs = None
        if "wms" in self.configuration["services"]:
            srs = self.configuration["services"]["wms"]["srs"]
        return srs

    def remove_source(self, source_name: str):
        """Remove source from configuration

        Args:
            source_name (str): The name of the source to remove
        """
        self.configuration["sources"].pop(source_name, None)

    def remove_layer(self, layer_name: str):
        """Remove layer from configuration

        Args:
            layer_name (str): The name of the layer to remove
        """
        new_layers = []
        for l in self.configuration["layers"]:
            if not l["name"] == layer_name:
                new_layers.append(l)

        self.configuration["layers"] = new_layers

    def add_grid(self, name: str, srs: str, bbox: str):
        """Add grid

        Args:
            name (str): grid name
            srs (str): grid SRS
            bbox (str): grid bbox
        """
        # Because of yaml library, we need to add a blank in the string
        # representing the srs to allow simple quote to not be escaped.
        # Note: we also need to do that on all srs already present in the
        # clean mapproxy configuration file
        if "wms" in self.configuration["services"]:
            new_srs = []
            for i in self.configuration["services"]["wms"]["srs"]:
                new_srs.append(i)
            new_srs.append(srs)
            self.configuration["services"]["wms"]["srs"] = new_srs

        new_grid = {"srs": srs, "bbox": json.loads(bbox)}
        self.configuration["grids"][name] = new_grid

    def add_source(self, source_name: str, source: dict):
        """Add or update source

        Args:
            source_name (str): The name of the source to add or update
            source (dict): The source to add or update

        Raises:
            ValueError: An error is raised if source is not complete
        """
        # Check source content
        if not "type" in source:
            raise ValueError("Type is missing.")

        if source["type"] == "tms":
            source["type"] = "tile"

        self.configuration["sources"][source_name] = source

    def add_cache(self, cache_name: str, cache: dict):
        if not "caches" in self.configuration:
            self.configuration["caches"] = {}

        self.configuration["caches"][cache_name] = cache

    def add_s3_url(self, s3_url: str):
        if not "globals" in self.configuration:
            self.configuration["globals"] = {}
        if not "cache" in self.configuration["globals"]:
            self.configuration["globals"]["cache"] = {}
        if not "s3" in self.configuration["globals"]["cache"]:
            self.configuration["globals"]["cache"]["s3"] = {}

        self.configuration["globals"]["cache"]["s3"]["endpoint_url"] = s3_url

    def add_layer(self, layer: dict) -> bool:
        """
        Add a layer to the configuration. The layer isd a dict with:
        - name
        - title
        - sources

        Sources is a list of existing sources. In the case the source does not exist, the layer
        is not added.

        Name is unique. In the case of duplication, the layer is not added.

        Args:
            layer (dict): The layer to be added to the configuration

        Returns:
            boolean: True if layer is added, False else.
        """

        # Check if layer already exists
        layers = self.get_layers()
        for l in layers:
            if layer["name"] == l["name"]:
                raise ValueError("Layer name already exists [" + l["name"] + "]")

        self.configuration["layers"].append(layer)

        return True

    def parse_layers(self, base_layers: list, overlays: list):
        """Parse layers

        Args:
            base_layers (list): [description]
            overlays (list): [description]

        Returns:
            [type]: [description]
        """

        # Add the default regional grid
        self.add_grid(
            config.MAPPROXY_CONF_ATTR_REGIONAL_GRID_NAME,
            config.MAPPROXY_DEFAULT_PROJ,
            config.MAPPROXY_DEFAULT_PROJ_BBOX,
        )

        # List of layers to be injected into mapproxy configuration
        layers = base_layers + overlays

        for bl in layers:
            regional_grid_name = config.MAPPROXY_CONF_ATTR_REGIONAL_GRID_NAME

            identifier = get_identifier(bl["id"])
            identifier_regional = get_regional_layer_name(bl["id"])
            identifier_standard = get_standard_layer_name(bl["id"])

            title = bl["title"]
            name_source = "source_" + identifier
            layer_type = bl["layer_type"]
            layers = bl["layers"]
            proj_name = bl["proj_name"]
            wms_version = bl["wms_version"]
            is_transparent = bl["is_transparent"]
            styles = bl["styles"]
            url = generate_mapproxy_tms_url(bl["url"])

            # Create caches
            cache_name_std_reg = "cache_std_reg_" + identifier
            cache_name_src_std = "cache_src_std_" + identifier
            cache_name_src_reg = "cache_src_reg_" + identifier
            cache_name_reg_std = "cache_reg_std_" + identifier
            cache_src_std = {
                "grids": ["GLOBAL_WEBMERCATOR"],
                "sources": [name_source],
                "concurrent_tile_creators": 4,
            }
            cache_std_reg = {
                "grids": [regional_grid_name],
                "sources": [cache_name_src_std],
                "bulk_meta_tiles": True,
                "meta_size": [2, 2],
            }
            cache_src_reg = {
                "grids": [regional_grid_name],
                "sources": [name_source],
                "concurrent_tile_creators": 4,
            }
            cache_reg_std = {
                "grids": ["GLOBAL_WEBMERCATOR"],
                "sources": [cache_name_src_reg],
                "bulk_meta_tiles": True,
                "meta_size": [2, 2],
            }

            if config.MAPPROXY_CONF_CACHE_TYPE == "s3":
                cache_src_std["cache"] = {
                    "type": config.MAPPROXY_CONF_CACHE_TYPE,
                    "bucket_name": config.MAPPROXY_CONF_CACHE_S3_BUCKET_NAME,
                    "directory": "{}/{}".format(
                        config.MAPPROXY_CONF_CACHE_S3_LOCATION,
                        cache_name_src_std,
                    ),
                }
                cache_std_reg["cache"] = {
                    "type": config.MAPPROXY_CONF_CACHE_TYPE,
                    "bucket_name": config.MAPPROXY_CONF_CACHE_S3_BUCKET_NAME,
                    "directory": "{}/{}".format(
                        config.MAPPROXY_CONF_CACHE_S3_LOCATION,
                        cache_name_std_reg,
                    ),
                }
                cache_src_reg["cache"] = {
                    "type": config.MAPPROXY_CONF_CACHE_TYPE,
                    "bucket_name": config.MAPPROXY_CONF_CACHE_S3_BUCKET_NAME,
                    "directory": "{}/{}".format(
                        config.MAPPROXY_CONF_CACHE_S3_LOCATION,
                        cache_name_src_reg,
                    ),
                }
                cache_reg_std["cache"] = {
                    "type": config.MAPPROXY_CONF_CACHE_TYPE,
                    "bucket_name": config.MAPPROXY_CONF_CACHE_S3_BUCKET_NAME,
                    "directory": "{}/{}".format(
                        config.MAPPROXY_CONF_CACHE_S3_LOCATION,
                        cache_name_reg_std,
                    ),
                }
            else:
                file_config = {"cache_dir": config.MAPPROXY_CONF_CACHE_FILE_DIR}
                cache_src_std["cache"] = file_config.copy()
                cache_std_reg["cache"] = file_config.copy()
                cache_src_reg["cache"] = file_config.copy()
                cache_reg_std["cache"] = file_config.copy()

            if is_transparent:
                cache_src_std["image"] = {"transparent": True, "mode": "RGBA"}
                cache_std_reg["image"] = {"transparent": True, "mode": "RGBA"}
                cache_src_reg["image"] = {"transparent": True, "mode": "RGBA"}
                cache_reg_std["image"] = {"transparent": True, "mode": "RGBA"}

            self.add_cache(cache_name_std_reg, cache_std_reg)
            self.add_cache(cache_name_src_std, cache_src_std)
            self.add_cache(cache_name_src_reg, cache_src_reg)
            self.add_cache(cache_name_reg_std, cache_reg_std)

            # Configuration format depends on source layer type
            if layer_type == "tms":
                # create the source (depends on layer_type)
                source = {"type": "tile", "grid": "GLOBAL_WEBMERCATOR", "url": url}
                self.add_source(name_source, source)

            elif layer_type == "wms":
                # create the source (depends on layer_type)
                source = {"type": layer_type, "req": {"url": url}}
                if layers is not None:
                    source["req"]["layers"] = layers
                if proj_name is not None:
                    source["supported_srs"] = [proj_name]
                if wms_version is not None:
                    source["wms_opts"] = {"version": wms_version}
                if is_transparent:
                    source["image"] = {"transparent": True, "mode": "RGBA"}
                    source["req"]["transparent"] = True
                if styles is not None:
                    source["req"]["styles"] = styles
                self.add_source(name_source, source)
            else:
                logger.error("Layer type is not WMS or TMS. layer_type=" + layer_type)
                continue

            # Layer in regional projection
            # source -> cache std -> cache regional -> layer
            layer = {
                "name": identifier_regional,
                "title": title,
                "sources": [cache_name_std_reg],
            }
            self.add_layer(layer)

            # Layer in standard projection
            if (
                proj_name is None
                or proj_name == "EPSG:3857"
                or proj_name == "EPSG:4326"
            ):
                # We assume that if the proj_name is not given it is standard proj
                layer_std = {
                    "name": identifier_standard,
                    "title": title,
                    "sources": [cache_name_src_std],
                }
                self.add_layer(layer_std)
            else:
                # need to be reprojected from reg to std
                layer_std = {
                    "name": identifier_standard,
                    "title": title,
                    "sources": [cache_name_reg_std],
                }
                self.add_layer(layer_std)

            if config.MAPPROXY_CONF_CACHE_TYPE == "s3":
                self.add_s3_url(config.MAPPROXY_CONF_CACHE_S3_URL)


if __name__ == "__main__":

    cmm = ConfigMapManager()
    desc = cmm.get_mapproxy_config()

    mapp_config = MapproxyConfig(desc)
    print(mapp_config.to_yaml())
