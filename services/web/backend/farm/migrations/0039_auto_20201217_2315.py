# Generated by Django 3.0.7 on 2020-12-17 22:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('farm', '0038_auto_20201130_1500'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userrelatedparty',
            options={'verbose_name': 'farm member', 'verbose_name_plural': 'farm members'},
        ),
        migrations.AlterField(
            model_name='site',
            name='holding_campaign',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sites', to='farm.HoldingCampaign', verbose_name='farm campaign'),
        ),
        migrations.AlterField(
            model_name='userrelatedparty',
            name='holding',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_related_parties', to='farm.Holding', verbose_name='farm'),
        ),
        migrations.AlterField(
            model_name='userrelatedparty',
            name='role',
            field=models.CharField(choices=[('farmer', 'farmer'), ('advisor', 'advisor'), ('farmer_association', 'farmer association'), ('public_administration', 'public administration')], default='farmer', max_length=30, verbose_name='role'),
        ),
    ]
