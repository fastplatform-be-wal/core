import json
import logging

from fastapi import APIRouter, Request
from functools import partial

from gql import gql
from opentelemetry import trace

from app.api.router.events import HasuraEvent
from app.api.lib.ticket.push_notification_on_new_ticket import (
    handler_push_notification_on_new_ticket,
)
from app.api.lib.ticket.push_notification_on_new_ticket_message import (
    handler_push_notification_on_new_ticket_message,
)
from app.api.lib.ticket.push_notification_on_ticket_status_change import (
    handler_push_notification_on_ticket_status_change,
)

# Log
logger = logging.getLogger(__name__)

# FastAPI
router = APIRouter()

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


@router.post("/push_notification_on_new_ticket")
async def push_notification_ticket_on_new_ticket(request: Request, event: HasuraEvent):

    ticket_id = event.event.data.new["id"]

    return await handler_push_notification_on_new_ticket(
        ticket_id,
        graphql_clients=request.state.graphql_clients,
        notify_only_staff_users=False,
    )


@router.post("/push_notification_on_new_ticket_message")
async def push_notification_ticket_on_new_ticket_message(
    request: Request, event: HasuraEvent
):

    ticket_message_id = event.event.data.new["id"]

    return await handler_push_notification_on_new_ticket_message(
        ticket_message_id, graphql_clients=request.state.graphql_clients
    )


@router.post("/push_notification_on_ticket_status_change")
async def push_notification_ticket_on_ticket_status_change(
    request: Request, event: HasuraEvent
):

    ticket_id = event.event.data.new["id"]

    return await handler_push_notification_on_ticket_status_change(
        ticket_id, graphql_clients=request.state.graphql_clients
    )


@router.post("/push_notification_on_ticket_queue_change")
async def push_notification_ticket_on_ticket_queue_change(
    request: Request, event: HasuraEvent
):

    ticket_id = event.event.data.new["id"]

    return await handler_push_notification_on_new_ticket(
        ticket_id,
        graphql_clients=request.state.graphql_clients,
        notify_only_staff_users=True,
    )
