"""
This tests require an instance of hasura up, running and accessible.

No other requirements expected in terms of db data prefilling,
they can run on empty db (only clear-region, no init-region).

BEWARE: running this tests will erase all data in certain tables.
"""
import os
from pathlib import Path
import glob
import json
from unittest import TestCase
import asyncio
from gql import gql
import time


from fastapi.testclient import TestClient

from app.main import app
from app.db.graphql_clients import GraphQLClient
from app.settings import config
from tests.constants import SHARED_VALUES


QUERIES_DIR = Path(__file__).parent / "queries"
CLEAR_DBS_QUERY_URI = Path(__file__).parent.parent / "query_clear_dbs.graphql"

# RUN_ONLY = [
#     "computed_soil_derived_object",  # [query name]
#     "05-computes_sdo_from_public_soil_sites_averages_if_any",  # [scenario name]
# ]
RUN_ONLY = []

fastplatform = GraphQLClient(
    name="fastplatform",
    url=config.API_GATEWAY_FASTPLATFORM_URL,
    headers={
        "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
        "Content-type": "application/json",
    },
)


class QueriesTest(TestCase):
    clear_dbs_query = None
    loop = None

    # Counters
    ran = 0
    succeeded = 0

    @classmethod
    def setUpClass(cls) -> None:
        cls.clear_dbs_query = CLEAR_DBS_QUERY_URI.read_text()
        cls.loop = asyncio.get_event_loop()
        return super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        # cls.loop.close()
        print()
        print(f"{QueriesTest.succeeded}/{QueriesTest.ran} query scenarios succeeded")

        return super().tearDownClass()

    def run_async(self, method):
        return self.loop.run_until_complete(method)

    def get_tested_queries(self):
        tested_queries = glob.glob(str(QUERIES_DIR / "*"))
        tested_queries.sort()
        for tested_query in tested_queries:
            # For easier debug, adding a file simply called 'skip' (no extension) skips the test
            if not Path(f"{tested_query}/skip").exists():
                yield Path(tested_query).name

    def get_scenarios(self, query):
        scenarios = [f.path for f in os.scandir(str(QUERIES_DIR / query)) if f.is_dir()]
        scenarios.sort()
        for scenario in scenarios:
            # For easier debug, adding a file simply called 'skip' (no extension) skips the scenario
            if not Path(f"{scenario}/skip").exists():
                yield Path(scenario).name

    def load_json(self, relative_path):
        json_uri = str(QUERIES_DIR / f"{relative_path}.json")
        content = Path(json_uri).read_text()

        # return actual json
        return json.loads(content)

    def load_query(self, relative_path):
        query_path = Path(str(QUERIES_DIR / f"{relative_path}.graphql"))
        if not query_path.exists():
            return None

        query = query_path.read_text()
        for place_holder, value in (
            (ph, v) for (ph, v) in SHARED_VALUES.items() if ph in query
        ):
            query = query.replace(f"{{{{ { place_holder } }}}}", str(value))

        return query

    def test_queries(self):
        try:
            self.run_async(fastplatform.connect())

            for query_name in self.get_tested_queries():
                with self.subTest(query_name=query_name):
                    query = self.load_query(f"{query_name}/query")
                    query_setup = self.load_query(f"{query_name}/query_setup")

                    for scenario in self.get_scenarios(query_name):
                        with self.subTest(scenario=scenario):

                            if RUN_ONLY and not (
                                RUN_ONLY[0] == query_name and RUN_ONLY[1] == scenario
                            ):
                                continue

                            print("-" * 60)
                            print(f"QUERY:    {query_name}")
                            print(f"SCENARIO: {scenario}")
                            print("-" * 60)

                            scenario_setup = self.load_query(
                                f"{query_name}/{scenario}/scenario_setup"
                            )

                            expected = self.load_json(
                                f"{query_name}/{scenario}/expected"
                            )

                            # Clear db from all previous test values
                            self.run_async(
                                fastplatform.execute(gql(self.clear_dbs_query))
                            )

                            # Important: give events triggered by deletion the time to be fully processed
                            time.sleep(2)

                            # Execute sequentially all setups
                            self.run_async(fastplatform.execute(gql(query_setup)))
                            if scenario_setup:
                                self.run_async(
                                    fastplatform.execute(gql(scenario_setup))
                                )

                        with TestClient(app) as client:
                            # Submit query

                            QueriesTest.ran += 1

                            response = client.post(
                                "/graphql",
                                json={"query": query, "variables": {}},
                            )

                        # Keep for easier debug
                        print("Response formatted: ", json.dumps(response.json()))
                        print()
                        print("Expected:           ", json.dumps(expected))

                        # # Assert results are identical
                        TestCase().assertDictEqual(
                            response.json(),
                            expected,
                            msg=f'Failed for query "{query_name}", scenario "{scenario}"',
                        )
                        QueriesTest.succeeded += 1
                        print("--> OK")
                        print()

        finally:
            self.run_async(fastplatform.close())
