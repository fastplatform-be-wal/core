from django.utils.translation import gettext_lazy as _

from oidc_provider.models import Client, Code, Token
from oidc_provider.admin import ClientAdmin, CodeAdmin, TokenAdmin

from fastplatform.site import admin_site


class OIDCClient(Client):
    class Meta:
        proxy = True
        verbose_name = _("OIDC Client")
        verbose_name_plural = _("OIDC Clients")
        help_text = _("OIDC clients registered on the platform")
        app_label = "authentication"


class OIDCCode(Code):
    class Meta:
        proxy = True
        verbose_name = _("OIDC Authorization code")
        verbose_name_plural = _("OIDC Authorization codes")
        help_text = _("OIDC authorization codes generated during the login process")
        app_label = "authentication"


class OIDCToken(Token):
    class Meta:
        proxy = True
        verbose_name = _("OIDC Token")
        verbose_name_plural = _("OIDC Tokens")
        help_text = _("OIDC tokens of users logged in using mobile access")
        app_label = "authentication"


admin_site.register(OIDCClient, ClientAdmin)
admin_site.register(OIDCCode, CodeAdmin)
admin_site.register(OIDCToken, TokenAdmin)
