from django.test import TestCase, override_settings

from utils.tests.test_admin import AdminTestCaseMixin

from configuration.models import Configuration, MapBaseLayer, MapOverlay

from configuration.tests.recipes import (
    configuration_recipe,
    map_base_layer_recipe,
    map_overlay_recipe,
)


@override_settings(AXES_ENABLED=False)
class ConfigurationAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Configuration
    recipe = configuration_recipe

    skip_add = True
    expected_status_code_add_view = 403
    expected_status_code_delete = 403


@override_settings(AXES_ENABLED=False)
class MapBaseLayerAdminTestCase(AdminTestCaseMixin, TestCase):
    model = MapBaseLayer
    recipe = map_base_layer_recipe


@override_settings(AXES_ENABLED=False)
class MapOverlayAdminTestCase(AdminTestCaseMixin, TestCase):
    model = MapOverlay
    recipe = map_overlay_recipe
