from app.api.types.computed_soil_derived_object import (
    ComputedDerivedObservation,
    ComputedSoilDerivedObject,
)

from .shared import *

DATA_UNIT_OF_MEASURE = {
    "id": "no_unit",
    "name": "no unit",
    "symbol": "-",
}

DATA_OBSERVABLE_PROPERTY = {
    "id": "ph",
    "base_phenomenon_id": "http://inspire.ec.europa.eu/codelist/SoilProfileParameterNameValue/chemicalParameter",
    "label": "pH",
    "unit_of_measure": DATA_UNIT_OF_MEASURE,
}

DATA_DERIVED_OBSERVATION = {
    "id": 1,
    "result": {"value": "default observation"},
    "observable_property": DATA_OBSERVABLE_PROPERTY,
}

DATA_PLOT = {"id": PLOT_ID, "name": "plot_name"}

DATA_SOIL_DERIVED_OBJECT = {
    "id": 1,
    "is_derived_from": {},
    "soil_sample_origin": "private",
    "soil_sites_count": 2,
    "plot_id": DATA_PLOT["id"],
    "derived_observations": [DATA_DERIVED_OBSERVATION],
}


EXPECTED_SOIL_DERIVED_OBJECT = ComputedSoilDerivedObject(
    id=DATA_SOIL_DERIVED_OBJECT["id"],
    is_derived_from=DATA_SOIL_DERIVED_OBJECT["is_derived_from"],
    soil_sample_origin=DATA_SOIL_DERIVED_OBJECT["soil_sample_origin"],
    soil_sites_count=DATA_SOIL_DERIVED_OBJECT["soil_sites_count"],
    plot_id=DATA_PLOT["id"],
    derived_observations=[
        ComputedDerivedObservation(
            result=DATA_DERIVED_OBSERVATION["result"],
            observed_property_id=DATA_OBSERVABLE_PROPERTY["id"],
        )
    ],
)

QUERY_SOIL_SITE_PRIVATE_INTERSECT_PLOT = [
    {
        "id": 1,
        "geometry": INTERSECTING_GEOMETRY_NODE,
        "observations": [
            {
                "observable_property": {"label": "pH", "id": "ph"},
                "phenomenon_time": "2020-01-01T00:00:00+00:00",
                "result": {"value": "default observation"},
            }
        ],
    },
    {
        "id": 2,
        "geometry": {
            "type": "MultiPolygon",
            "crs": {
                "type": "name",
                "properties": {"name": "urn:ogc:def:crs:EPSG::4258"},
            },
            "coordinates": [
                [
                    [
                        [4.70049402428607, 50.3328983692753],
                        [4.70111704105072, 50.3342926478598],
                        [4.6999701336121, 50.3350088900415],
                        [4.70049402428607, 50.3328983692753],
                    ]
                ]
            ],
        },
        "observations": [
            {
                "observable_property": {"label": "pH", "id": "ph"},
                "phenomenon_time": "2020-01-01T00:00:00+00:00",
                "result": {"value": "private observation"},
            }
        ],
    },
]

QUERY_SOIL_SITE_PUBLIC_NEAR_PLOT = [
    {
        "id": 38,
        "geometry": CLOSE_BY_GEOMETRY_NODE,
        "external__observations": [
            {
                "external__observable_property": {"label": "pH", "id": "ph"},
                "phenomenon_time": "2020-01-01T00:00:00+00:00",
                "result": {"value": "external observation"},
            }
        ],
    },
    {
        "id": 39,
        "geometry": {
            "type": "MultiPolygon",
            "crs": {
                "type": "name",
                "properties": {"name": "urn:ogc:def:crs:EPSG::4258"},
            },
            "coordinates": [
                [
                    [
                        [4.70049402428607, 50.3328983692753],
                        [4.70111704105072, 50.3342926478598],
                        [4.6999701336121, 50.3350088900415],
                        [4.70049402428607, 50.3328983692753],
                    ]
                ]
            ],
        },
        "external__observations": [
            {
                "external__observable_property": {"label": "pH", "id": "ph"},
                "phenomenon_time": "2020-01-01T00:00:00+00:00",
                "result": {"value": "external observation"},
            }
        ],
    },
    {
        "id": 41,
        "geometry": {
            "type": "MultiPolygon",
            "crs": {
                "type": "name",
                "properties": {"name": "urn:ogc:def:crs:EPSG::4258"},
            },
            "coordinates": [
                [
                    [
                        [4.70249402428607, 50.3318983692753],
                        [4.70241704105072, 50.3332926478598],
                        [4.7029701336121, 50.3340088900415],
                        [4.70249402428607, 50.3318983692753],
                    ]
                ]
            ],
        },
        "external__observations": [
            {
                "external__observable_property": {"label": "pH", "id": "ph"},
                "phenomenon_time": "2020-01-01T00:00:00+00:00",
                "result": {"value": "external observation"},
            }
        ],
    },
    {
        "id": 40,
        "geometry": {
            "type": "MultiPolygon",
            "crs": {
                "type": "name",
                "properties": {"name": "urn:ogc:def:crs:EPSG::4258"},
            },
            "coordinates": [
                [
                    [
                        [4.69649402428607, 50.3318983692753],
                        [4.69641704105072, 50.3332926478598],
                        [4.6969701336121, 50.3340088900415],
                        [4.69649402428607, 50.3318983692753],
                    ]
                ]
            ],
        },
        "external__observations": [
            {
                "external__observable_property": {"label": "pH", "id": "ph"},
                "phenomenon_time": "2020-01-01T00:00:00+00:00",
                "result": {"value": "external observation"},
            }
        ],
    },
]

VALID_SOIL_DERIVED_OBJECT_FOR_INSERT = {
    "plot_id": 1,
    "is_derived_from": {},
    "soil_sample_origin": "private",
    "soil_sites_count": 2,
    "derived_observations": [
        {"observed_property_id": "ph", "result": {"value": "default observation"}}
    ],
}

EXPECTED_NULL_SDO = {
    "plot_id": 1,
    "is_derived_from": {},
    "soil_sample_origin": "none",
    "soil_sites_count": 0,
    "derived_observations": [],
}
