from unittest import TestCase
from unittest.mock import MagicMock
import asyncio
import copy

from app.api.resolvers import ComputedSoilDerivedObjectResolver
from app.db.graphql_clients import fastplatform
from app.api.types.computed_soil_derived_object import ComputedSoilDerivedObject
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError
from .mock_data import soil_derived_object as md


async def async_magic():
    pass


MagicMock.__await__ = lambda x: async_magic().__await__()


class MockGraphQLSession:
    data_plot_by_pk: dict
    data_query_soil_derived_object_by_plot_id: dict
    data_query_soil_site_private_intersect_plot: dict
    data_query_soil_site_public_near_plot: dict

    async def execute(self, *args, **kwargs):
        query_name = [*args][0].definitions[0].name.value

        if query_name == "plot_by_pk":
            return {"plot_by_pk": self.data_plot_by_pk}

        if query_name == "soil_derived_object_by_plot_id":
            return {
                "soil_derived_object": self.data_query_soil_derived_object_by_plot_id
            }

        if query_name == "soil_site_private_intersect_plot":
            return {"soil_site": self.data_query_soil_site_private_intersect_plot}

        if query_name == "soil_site_public_near_plot":
            return {"nearest_soil_site": self.data_query_soil_site_public_near_plot}


class SoilDerivedObjectTest(TestCase):
    mock = MockGraphQLSession()
    sut = None
    loop = None

    @classmethod
    def setUpClass(cls) -> None:
        print("SoilDerivedObjectTest")
        cls.loop = asyncio.get_event_loop()

        # Replace fastplatform.session with our mock
        setattr(fastplatform, "session", cls.mock)
        return super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        # TODO: if following line uncommented, multiple async class test not working
        # cls.loop.close()
        return super().tearDownClass()

    def run_async(self, method):
        return self.loop.run_until_complete(method)

    def setUp(self) -> None:
        self.sut = ComputedSoilDerivedObjectResolver()
        return super().setUp()

    # TODO: correct following test once reverse remote relationship possible in Hasura
    # def test_returns_existing_soil_derived_object_if_exists(self):
    #     self.mock.data_query_soil_derived_object_by_plot_id = [
    #         md.DATA_SOIL_DERIVED_OBJECT
    #     ]

    #     result = self.run_async(self.sut.resolve(md.PLOT_ID))
    #     self.assertIsInstance(result, ComputedSoilDerivedObject)

    #     self.assertEqual(result, md.EXPECTED_SOIL_DERIVED_OBJECT)

    def test_returns_existing_private_soil_derived_object_if_none_already_computed(
        self,
    ):

        # Simulate no soil_derived_object associated to requested plot
        self.mock.data_query_soil_derived_object_by_plot_id = []
        self.mock.data_plot_by_pk = md.QUERY_PLOT_BY_PK
        self.mock.data_query_soil_site_private_intersect_plot = (
            md.QUERY_SOIL_SITE_PRIVATE_INTERSECT_PLOT
        )

        self.sut.get_public_soil_sites = MagicMock()
        self.sut.insert_soil_derived_object = MagicMock()

        self.run_async(self.sut.resolve(md.PLOT_ID))

        self.sut.get_public_soil_sites.assert_not_called()
        self.sut.insert_soil_derived_object.assert_called()

    def test_returns_existing_public_soil_derived_object_if_none_already_computed_and_no_private(
        self,
    ):
        # Simulate no soil_derived_object associated to requested plot
        # and no private data either.
        self.mock.data_query_soil_derived_object_by_plot_id = []
        self.mock.data_plot_by_pk = md.QUERY_PLOT_BY_PK
        self.mock.data_query_soil_site_private_intersect_plot = []
        self.mock.data_query_soil_site_public_near_plot = (
            md.QUERY_SOIL_SITE_PUBLIC_NEAR_PLOT
        )

        self.sut.insert_soil_derived_object = MagicMock()

        self.run_async(self.sut.resolve(md.PLOT_ID))
        self.sut.insert_soil_derived_object.assert_called()

    def test_no_data_available_returns_nothing(
        self,
    ):
        # Simulate no soil_derived_object associated to requested plot
        # and no private data either.
        self.mock.data_query_soil_derived_object_by_plot_id = []
        self.mock.data_plot_by_pk = md.QUERY_PLOT_BY_PK
        self.mock.data_query_soil_site_private_intersect_plot = []
        self.mock.data_query_soil_site_public_near_plot = []

        self.sut.insert_soil_derived_object = MagicMock()

        self.run_async(self.sut.resolve(md.PLOT_ID))
        sdo = self.sut.insert_soil_derived_object.call_args_list[0][0][0]

        self.sut.insert_soil_derived_object.assert_called()
        self.assertDictEqual(sdo, md.EXPECTED_NULL_SDO)

    def test_no_plot_details_raises_plot_not_found_error(self):
        self.mock.data_query_soil_derived_object_by_plot_id = []
        self.mock.data_plot_by_pk = None

        with self.assertRaises(GraphQLHoldingPlotNotFoundError):
            self.run_async(self.sut.resolve(md.PLOT_ID))

    def test_valid_soil_derived_object_insert(self):
        self.run_async(
            self.sut.insert_soil_derived_object(md.VALID_SOIL_DERIVED_OBJECT_FOR_INSERT)
        )

    def test_erroneous_data_raises_unexpected_data_error(self):
        erroneous_data = copy.deepcopy(md.DATA_SOIL_DERIVED_OBJECT)
        del erroneous_data["id"]
        self.mock.data_query_soil_derived_object_by_plot_id = [erroneous_data]

        with self.assertRaises(GraphQLUnexpectedDataError):
            self.run_async(self.sut.resolve(md.PLOT_ID))
