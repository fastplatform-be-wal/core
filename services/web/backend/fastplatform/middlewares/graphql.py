from django.utils.deprecation import MiddlewareMixin

from fastplatform.graphql.clients import fastplatform


class InjectGraphQLClientsMiddleware(MiddlewareMixin):
    def process_request(self, request):
        request.graphql_clients = {"fastplatform": fastplatform}
