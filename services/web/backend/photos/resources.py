from common.resources import ModelResourceWithColumnIds

from photos.models import GeoTaggedPhoto


class GeoTaggedPhotoResource(ModelResourceWithColumnIds):
    class Meta:
        model = GeoTaggedPhoto
