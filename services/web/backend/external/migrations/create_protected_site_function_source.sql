CREATE OR REPLACE FUNCTION public.protected_site_function_source(
        z integer,
        x integer,
        y integer
    ) RETURNS bytea AS $$
DECLARE mvt bytea;
BEGIN
SET enable_seqscan TO off;
SELECT INTO mvt ST_AsMVT(tile, 'protected_site', 4096, 'geom')
FROM (
        SELECT DISTINCT ON (p.id) ST_AsMVTGeom(
                ST_Transform(
                    ST_SimplifyPreserveTopology(p.geometry, 1 /(2 ^ z)),
                    3857
                ),
                TileBBox(z, x, y, 3857),
                4096,
                64,
                true
            ) AS geom,
            p.id,
            p.authority_id,
            p.site_name,
            p.site_protection_classification,
            STRING_AGG(
                DISTINCT d.label || ' / ' || ds.label || ': ' || COALESCE(dt.percentage_under_designation, 100.0)::text,
                '\n'
            ) AS designations
        FROM protected_site p
            INNER JOIN protected_site_version pv ON p.version_id = pv.id
            LEFT JOIN protected_site_site_designation psd ON psd.protectedsite_id = p.id
            LEFT JOIN designation_type dt ON psd.designationtype_id = dt.id
            LEFT JOIN designation d ON dt.designation_id = d.id
            LEFT JOIN designation_scheme ds ON dt.designation_scheme_id = ds.id
        WHERE pv.is_active
            AND ST_Intersects(
                p.geometry,
                ST_Transform(TileBBox(z, x, y, 3857), 4258)
            )
        GROUP BY p.id
    ) as tile
WHERE z >= 5
    AND geom IS NOT NULL;
RETURN mvt;
SET enable_seqscan TO on;
END $$ LANGUAGE plpgsql VOLATILE STRICT PARALLEL SAFE;