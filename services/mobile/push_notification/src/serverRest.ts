import {
  NODE_ENV,
} from './config/env'
import express, { Request, Response } from 'express';
import userDeviceRoutes from './routes/device';
import notificationRoutes from './routes/notification';
import bodyParser from 'body-parser';
import cors from 'cors'
// History mode for the router
import history from 'connect-history-api-fallback';

export class RestServer {
  app: express.Express
  constructor (baseAPIUrl: string, options: any) {
    // Create the server
    this.app = express();
    
    this.app.get('/', (request: Request, response: Response) => {
      response.send('FaST Farmer mobile application push notification server');
    });

    // Allow CORS
    this.app.use(cors(options.corsOptions))
    this.app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, Content-Type, authorization, x-identity, ocp-apim-subscription-key");
      res.header("Access-Control-Allow-Methods", 'OPTIONS, GET, POST, PUT, DELETE');
      next();
    });

    // Configure bodyparser to handle post requests
    this.app.use(bodyParser.urlencoded({
      extended: true
    }));
    this.app.use(bodyParser.json());

    // Handle history mode for the router.
    // Set a 'rewrites' config to avoid the API requests to be redirected to the 'index.html' file
    let rewrites = [
      {
        from: /\/fastplatform\/.*$/,
        to: function(context: any) {
          return context.parsedUrl.pathname
        }
      },
      {
        from: /\/health$/,
        to: function(context: any) {
          return context.parsedUrl.pathname
        }
      },
      {
        from: /\/health\/.*$/,
        to: function(context: any) {
          return context.parsedUrl.pathname
        }
      },
    ];

    this.app.use(history({
      verbose: NODE_ENV === 'development',
      htmlAcceptHeaders: ['text/html', 'application/json', '*/*'],
      rewrites: rewrites
    }));

    this.app.use(baseAPIUrl, userDeviceRoutes);
    this.app.use(baseAPIUrl, notificationRoutes);

  }
  start (port = 5000) {
    this.app.listen(port, function () {
      console.log(`Running server on port ${port}`);
    });
  }
}
