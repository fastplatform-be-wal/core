import os
import re
from fastplatform_graphql.hasura import QueryCollection, AllowList


def find_gql_files_in_folder(folder):

    gql_files = []
    for root, _, files in os.walk(folder):
        for filename in files:
            if filename.lower().endswith(('.gql', '.graphql')):
                gql_files.append(os.path.join(root,filename))

    return gql_files

current_dir = os.path.dirname(os.path.abspath(__file__))
web_backend_dir = '/'.join([current_dir, '..', '..', '..', 'web', 'backend'])
mobile_app_dir = '/'.join([current_dir, '..', '..', '..', '..', '..', 
                           'mobile', 'farmer-mobile-app'])
metadata_dir = '/'.join([current_dir, '..', 'metadata'])
query_collections = '/'.join([metadata_dir, 'query_collections.yaml'])
allow_list = '/'.join([metadata_dir, 'allow_list.yaml'])

non_external_folders = [
    '/'.join([web_backend_dir, d])
    for d in os.listdir(web_backend_dir) 
    if os.path.isdir(os.path.join(web_backend_dir, d))
    if d != 'external'
]
non_external_folders.append(mobile_app_dir)

gql_files = [gql_file 
             for folder in non_external_folders 
             for gql_file in find_gql_files_in_folder(folder)]

query_collection = QueryCollection()
for gql_file in gql_files:
    query_collection.add_query(filename=gql_file)
query_collection.write_to_yaml(filename=query_collections)


allow_list_hasura = AllowList()
allow_list_hasura.write_to_yaml(filename=allow_list)
