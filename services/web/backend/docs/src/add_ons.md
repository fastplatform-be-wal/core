# Add-ons & API accesses

- [Add-ons & API accesses](#add-ons--api-accesses)
  - [Authentication and authorization](#authentication-and-authorization)
  - [Add-on subscriptions](#add-on-subscriptions)
    - [The *Auto subscribe* flag](#the-auto-subscribe-flag)
  - [Object-level permissions](#object-level-permissions)
    - [Object-level permissions versus querying the object graph](#object-level-permissions-versus-querying-the-object-graph)
  - [Using the API](#using-the-api)
    - [Third-party services running outside FaST](#third-party-services-running-outside-fast)
    - [Researchers](#researchers)
    - [Examples configurations](#examples-configurations)
      - [Give access to a researcher to the geometries of all the farms and their crops](#give-access-to-a-researcher-to-the-geometries-of-all-the-farms-and-their-crops)
      - [A very simple add-on that just displays a web-page](#a-very-simple-add-on-that-just-displays-a-web-page)
      - [An add-on that displays a tailored web-page and does not need additional login](#an-add-on-that-displays-a-tailored-web-page-and-does-not-need-additional-login)
      - [Other examples](#other-examples)

The [<g>Add-Ons</g>](/admin/add_ons/addon/) and [<g>API accesses</g>](/admin/add_ons/apiaccess/) in FaST offer the possibility to remotely connect to the FaST data store and consume / query data to build new use cases or to compute statistics for research.

Both concepts are very similar in the way they provide access to the data; the only difference is that [<x>Add-Ons</x>](/admin/add_ons/addon/) are meant to be used by third-party services and proposed in the farmer mobile application, while [<x>API Accesses</x>](/admin/add_ons/apiaccess/) are meant to be used by researchers who query the data for aggregation purposes.

The FaST platform currently exposes a single way to consume its data, through an API Gateway based on the GraphQL standard. The purpose of this documentation is not to be a course about GraphQL querying, hence we invite the reader to first get familiar with this query language, for example [here](https://graphql.org/).

```plantuml
skinparam defaultFontSize 12

actor "Researcher" as researcher
actor "Third-party service" as add_on
component "FaST API Gateway" as api_gateway
database "FaST datastore" as db

researcher --> api_gateway
add_on --> api_gateway

api_gateway --> db
```

GraphQL is similar in principle to the more ancient REST semantics, in that it offers a way to query a data source over web technologies, and get back data in a standard text format (like JSON); the main difference is that the REST querying possibilities are fixed by what is already implemented in the server, while the GraphQL querying allows the users to query exactly the objects they need, without any need to change the server configuration beforehand.

Example query (*query the id and name of all the farms*):

```bash
curl -X POST \
     -H 'Content-Type: application/json' \
     -H 'Authorization: ApiKey <XXXXXXXXXX>' \
     -d '{"query": "query {holding {id name}}"}'
```

Example response (*returns a list of farms in JSON format*):

``` json
{
    "holding": [
        {
            "id": "DEMO-holding",
            "name": "Some demo farm"
        },
        {
            "id": "12694673F",
            "name": "The farm of Matus"
        }
    ]
}
```

## Authentication and authorization

Every request to the API gateway goes through an authorization flow that determines if the query should be allowed or rejected. The authentication of the caller is determined from the HTTP Authorization header, expected to be present on every POST request.
The main steps of the authorization flow are:

- we determine if the API key exists and find the corresponding [<x>Add-On</x>](/admin/add_ons/addon/) or [<x>API Access</x>](/admin/add_ons/apiaccess/)
- we determine if the [<x>Add-On</x>](/admin/add_ons/addon/) / [<x>API Access</x>](/admin/add_ons/apiaccess/) is still active (has not been deactivated)
- if [<x>Add-On</x>](/admin/add_ons/addon/), we determine if the [<x>Provider</x>](/admin/add_ons/provider/) of the [<x>Add-On</x>](/admin/add_ons/addons/) is still active (has not been deactivated)
- in case of an [<x>Add-On</x>](/admin/add_ons/addon/) and if the requested object type is private to a holding, we determine if it has the authorization to view the data of this holding (either because the holding is [subscribed](add-on-subscriptions) or because the [<x>Add-On</x>](/admin/add_ons/addon/) has the [`auto_subscribe` flag](the-auto-subscribe-flag) set)
- we determine if the [<x>Add-On</x>](/admin/add_ons/addon/) / [<x>API Access</x>](/admin/add_ons/apiaccess/) has the authorization to request this object type, either directly or through a [<x>Group</x>](/admin/authentication/group/) membership

The API gateway will do its best to return as many results as possible, but only results that match *all* the conditions above.

A summary of the authorization flow for accessing with ([<x>Add-Ons</x>](/admin/add_ons/addon/) and [<x>API Accesses</x>](/admin/add_ons/apiaccess/)) is presented in the diagram below. Some more details on the concepts of *subscription* and *object-level permissions* are proposed in the next paragraphs.

```plantuml
skinparam defaultFontSize 12

title Authorization flow for API key access

(*) --> "POST\nHeaders: //Authorization: ApiKey ABCDEF//\nPayload: //query {object}//" as query


query --> "API key ABCDEF exists?" as api_key

api_key -->[yes\nAPI access] "API access is active?" as is_api_access_active

api_key -->[yes\nadd-on] "Add-on is active?" as is_add_on_active
api_key -->[no] "Reject" as reject #red

is_api_access_active -->[yes] "Add-on has group view permission\nfor this object?" as view_group_permission
is_api_access_active -->[no] reject

is_add_on_active -->[yes] "Add-on provider is active?" as is_provider_active
is_add_on_active -->[no] reject

is_provider_active -->[yes] "Requested //object// type is\nprivate to a holding?" as is_private_object
is_provider_active -->[no] reject

is_private_object -->[yes] "Add-on is //auto_subscribe//?" as is_auto_subscribe
is_private_object --> [no] "Add-on has view permission\nfor this object?" as view_permission

is_auto_subscribe -->[no] "Holding is subscribed to add-on?" as is_subscribed

is_subscribed --> [yes] view_permission
is_subscribed --> [no] reject

view_permission -->[no] view_group_permission

is_auto_subscribe -->[yes] view_permission
view_permission -->[yes] "Return result" as result #lightgreen
view_group_permission -->[yes] result
                    
view_group_permission -->[no] reject

result --> (*)
reject --> (*)
```


## Add-on subscriptions

The [<x>Add-Ons</x>](/admin/add_ons/addon/) displayed in the **mobile application** are available for the user to consume. Because such a consumption potentially involves the user sharing his farm data with the [<x>Add-On</x>](/admin/add_ons/addon/), FaST provides the concept of subscription: subscriptions are basically *explicit consents from the user to accept to share their farm data with the [<x>Add-On</x>](/admin/add_ons/addon/)*.

This subscription is materialized by a <kbd>Subscribe</kbd> button on the [<x>Add-On</x>](/admin/add_ons/addon/)'s page in the app.

**Important: <u>a subscription is between a <x>Holding (farm)</x> and an [<x>Add-On</x>](/admin/add_ons/addon/)</u>.** Not between a <x>User</x> and an [<x>Add-On</x>](/admin/add_ons/addon/).

An [<x>Add-On</x>](/admin/add_ons/addon/) can only access the data of holdings that have subscribed to it. As soon as a holding has unsubscribed to a particular [<x>Add-On</x>](/admin/add_ons/addon/), their relevant data cannot be accessed anymore by the [<x>Add-On</x>](/admin/add_ons/addon/).

<img src="/services/web/backend/docs/static/docs/img/add_ons/subscribe.png" title="Subscribe" class="img-docs" height="400" />

### The *Auto subscribe* flag

This flag allows a system administrator to give to an [<x>Add-On</x>](/admin/add_ons/addon/) the right to query *all the holdings*, and not only the ones that have subscribed to it. It is used to provide every user with an access to some core functionalities of the FaST platform such as meteorology, satellite images, fertilization plans etc…

This flag is very powerful (and therefore you should be cautious when activating it) as it basically bypasses the user's consent to share their farm's data. 
## Object-level permissions

In addition to being constrained to the subscribed holdings, an [<x>Add-On</x>](/admin/add_ons/addon/) (and an [<x>API Access</x>](/admin/add_ons/apiaccess/)) are also explicitly constrained to object types they are allowed to query. These object-level permissions are implemented and function exactly in the same way as [permissions for users in the **Administration Portal**](/services/web/backend/docs/en/users/authorization.md).

Permissions for an [<x>Add-On</x>](/admin/add_ons/addon/) are computed as the union of the permissions of the groups this [<x>Add-On</x>](/admin/add_ons/addon/) is member of, as well as the direct permissions that are assigned to this [<x>Add-On</x>](/admin/add_ons/addon/).

**Important: as the FaST API Gateway is readonly for [<x>Add-Ons</x>](/admin/add_ons/addon/) and [<x>API Accesses</x>](/admin/add_ons/apiaccess/), any permission of type `add`, `change` or `delete` will be ignored. Only `view` permissions will be enforced.**

### Object-level permissions versus querying the object graph

The API Gateway will do its best effort to return as much data as possible within the permissions set for the [<x>Add-On</x>](/admin/add_ons/addon/) or the [<x>API Access</x>](/admin/add_ons/apiaccess/).

For example, assume an [<x>Add-On</x>](/admin/add_ons/addon/) that has the following view permissions:

- <x>Holding</x>
- <x>User Related Party</x>
- <x>Geo-tagged photos</x>

But does not have this view permission: <x>User</x>

And runs the following query:

```graphql
query {
    holding {
        name
        user_related_parties {
            role
            user {
                username
                geo_tagged_photos {
                    location
                }
            }
        }
    }
}
```

Because the add-on does not have the permission to view <x>User</x> objects, the API will return a JSON that looks like:

```json
{
    "holding": [
        {
            "name": "DEMO-holding",
            "user_related_parties": [
                {"role": "farmer", "user": null},
                {"role": "advisor", "user": null},
                {"etc...": "..."}
            ]
        },
        {
            "etc...": "..."
        }
    ]
}
```

Notice that, because the add-on does not have the permission to view the <x>User</x> object, the API returns `null` for all user entries and therefore does not return the <x>Geo-tagged photos</x> objects (even though the add-on did have the permission to view this object type).

**An add-on needs to have the permission to view all the object types between the root of the query and the query leaves.**

For example, if your query root is `holding` and the leaves are `plot`: 

```graphql
query {
  holding{
    name
	holding_campaigns{
      campaign_id
      sites{
        name
        plots{
          name
        }
      }
    }
  }
}
```

In this case, the [<x>Add-On</x>](/admin/add_ons/addon/) needs to have the view permissions on all the vertices from `holding` to `plot`, which are: `holding`, `holding_campaign`, `site`, `plot`.

On the other hand, you do not need the view permissions on all vertices if you are directly query the farm plot themselves. Let's say the only permission set for the [<x>Add-On</x>](/admin/add_ons/addon/) is `plot`, and if your query reads simply:

```graphql
query {
  plot {
    id
  }
}
```

In this case, the query is going to return all the plot ids belonging to farms that are subscribed to the [<x>Add-On</x>](/admin/add_ons/addon/), without having to add view permission on all the relevant vertices in between.

## Using the API

### Third-party services running outside FaST

The [<x>Add-On</x>](/admin/add_ons/addon/) mechanism proposed by FaST allows to extend the range of services proposed to the farmer in the application by integrating external services (i.e. not managed by FaST) that consume FaST public and private data to propose additional content to the farmer.

These integrations can be proposed to institutional or commercial actors; data-sharing is based on the authorization workflow described above.

The high-level process for registering a new external service goes as follows:

1. The provider of the external service must be registered in the **Administration Portal**
2. The external service must be registered as an [<x>Add-On</x>](/admin/add_ons/addon/) in the **Adminsitration Portal** under the provider defined above
3. Both the **provider** and the [<x>Add-On</x>](/admin/add_ons/addon/) must be defined as *active*
4. At least one API key must be created for the [<x>Add-On</x>](/admin/add_ons/addon/) and must be communicated to the external service provider. Note that FaST does not provide an automated way to communicate the API key, it has to be sent by other means such as email.
5. A callback URL must be defined for the [<x>Add-On</x>](/admin/add_ons/addon/). This is the URL that FaST will call when the user clicks to access the [<x>Add-On</x>](/admin/add_ons/addon/) in the app.
6. Permissions must be granted to the [<x>Add-On</x>](/admin/add_ons/addon/) to access FaST objects
7. The [<x>Add-On</x>](/admin/add_ons/addon/) must be set as visible (it will then immediately be available in the mobile app)

Once the [<x>Add-On</x>](/admin/add_ons/addon/) is available to users in the app, the users can interact with it in the <kbd>Add-ons</kbd> page:

1. First the user must *subscribe* their holding to the [<x>Add-On</x>](/admin/add_ons/addon/). This subscription determines whether the [<x>Add-On</x>](/admin/add_ons/addon/) has access to the private data of this holding (limited to the permitted objects).
2. Then the user will click on <kbd>Go to Add-on XXXX</kbd>, this will open the [<x>Add-On</x>](/admin/add_ons/addon/) callback URL in a new web page **outside the FaST mobile app**
3. The user is now on a page served by the [<x>Add-On</x>](/admin/add_ons/addon/), outside FaST. The [<x>Add-On</x>](/admin/add_ons/addon/) can implement whatever is necessary to provide its service, such as logging the user in (if the service requires a login), making queries to the FaST API (if the service requires FaST data) and displaying content to the user.

A token (JSON Web Token) gets appended to the callback url. It may contain in its payload any combination of the 3 parameters:
- `user_id`, if `Callback with user ID` is checked for the AddOn in the Administration Portal
- `holding_id`, if `Callback with holding ID` is checked for the AddOn in the Administration Portal
- `campaign_id`, if `Callback with campaign ID` is checked for the AddOn in the Administration Portal.

Those parameters can then be used by the third-party service to query the FaST API in return, using the API key provided by FaST (see above).

A Callback secret key is used to sign the token's payload, and ensure on the side of the third-party that the request legitimately emanates from FaST.

For more information on JWTs, as well as an easy way to check the content of a JWT, check for example [jwt.io](https://jwt.io/).

```plantuml
skinparam defaultFontSize 12

title "High-level sequence of user interaction\nwith third-party add-on"

skinparam responseMessageBelowArrow true

participant "User U\nHolding/farm H" as user
participant "FaST" as fast
box "Outside FaST" #lightblue
    participant "Add-on A" as add_on
endbox

user -> fast: Subscribe holding\nto add-on A
user -> fast: Click on add-on A link\nin mobile app
note over fast: Check that holding H\nis subscribed to add-on A
fast -> add_on: FaST redirects to\nadd-on A callback\nand appends the JWT
note over add_on: If necessary, add-on A\nlogs in user U\n(independent from FaST)
add_on -> fast: Add-on A queries\ndata of holding H\nfrom the FaST API\nusing its API key
fast -> add_on: FaST returns data of holding H
add_on --> user: Add-on A proposes services to user U for holding H\n(in its own UI, outside FaST)
```

It is important to note that FaST only provides a way to showcase third-party services in the app, but these services are always consumed *outside the FaST app*. Also, FaST provides the system administrators fined-grained permissions to determine which and whose objects are being shared with these services, to cater to most of the possible situations (see [examples](#example-configurations) below).

### Researchers

When researchers or official institutions are connecting to the API Gateway, they will also need to be provided an API key, and will go through the same authorization flow as described above. The main differences between an [<x>API Access</x>](/admin/add_ons/apiaccess/) from a researcher and an [<x>Add-On</x>](/admin/add_ons/addon/) access from a third-party service are:

- An [<x>API Access</x>](/admin/add_ons/apiaccess/) is never `visible`: it does not appear in the mobile application
- An [<x>API Access</x>](/admin/add_ons/apiaccess/) is always `auto subscribe`: it has automatically access to all the holdings, without the user needing to subscribe the holding first (anyway, as the [<x>API Access</x>](/admin/add_ons/apiaccess/) does not appear in the app, the user could not subscribe even if they wanted to)
- An [<x>API Access</x>](/admin/add_ons/apiaccess/) does not have a `Callback URL`, nor a `Callback secret key`
- An [<x>API Access</x>](/admin/add_ons/apiaccess/) does not have any `Provider`
- An [<x>API Access</x>](/admin/add_ons/apiaccess/) does not have any vanity metadata such as website or logo

The main similarities are:

- An [<x>API Access</x>](/admin/add_ons/apiaccess/) must be `active` to be able to query data
- An [<x>API Access</x>](/admin/add_ons/apiaccess/) is subject to the same object-level permissions as [<x>Add-Ons</x>](/admin/add_ons/addon/)

### Examples configurations

#### Give access to a researcher to the geometries of all the farms and their crops

A researcher (John) needs to retrieve the geometries of the farms for the current campaign to compute statistics. 

Let's give John access:

1. Create a new [<x>API Access</x>](/admin/add_ons/apiaccess/) with the following parameters:
    - Identifier: `john-doe`
    - Name: `John Doe`
    - Is active: `true`
    - Permissions: give permissions to view the following objects:
        - <x>HoldingCampaign</x>
        - <x>Campaign</x>
        - <x>Site</x>
        - <x>Plot</x>
        - <x>PlotPlantVariety</x>
        - <x>PlantVariety</x>
        - <x>PlantSpecies</x>
        - <x>PlantSpeciesGroup</x>
    - API keys:
        - Create a new API key
2. Give the API key you just created to John

Now John can get data from the FaST API by running a GraphQL query and authenticating with the API key.

```graphql
# query.graphql

query {
    holding_campaign(
        where: {
            campaign: {
                is_current: {
                    _eq: true
                }
            }
        }
    ) {
        sites {
            plots {
                geometry
                plot_plant_varieties {
                    plant_variety {
                        id
                        name
                        plant_species {
                            id
                            name
                            plant_species_group {
                                id
                                name
                            }
                        }
                    }
                }
            }
        }
    }
}
```

Now run:

```bash
curl -X POST \
     -H 'Content-Type: application/json' \
     -H 'Authorization: ApiKey <his API key>' \
     -d '{"query": "$(cat query.graphql)"}'
```

Which will return a JSON output containing all the plots and their geometries and corresponding plant varieties/species.

> Note: because we did not grant permission to view the <x>Holding</x> object or the <x>User</x> object, the received data does not contain any direct identification of the business or of the user.

#### A very simple add-on that just displays a web-page

Our first example [<x>Add-On</x>](/admin/add_ons/addon/) will be very basic, and will just display to the user a `Hydrology Alert Bulletin`, from an imaginary governmental agency called `Hydrology Agency`.


Let's start:

1. In the **Administration Portal**, we create a [<x>Provider</x>](/admin/add_ons/provider/) named `Hydrology Agency` with the following configuration:
    - Identifier: `hydrology-agency`
    - Name: `Hydrology Agency`
    - Is active: `true`
    - Optionally, add a logo, description and vanity web page
2. Next we create an [<x>Add-On</x>](/admin/add_ons/addon) named `Hydrology Alert Bulletin`, with the following configuration:
    - Identifier: `hydrology-alert-bulletin`
    - Name: `Hydrology Alert Bulletin`
    - Provider: `Hydrology Agency`
    - Is active: `true`
    - Is visible: `true` (will be visible in the app)
    - Optionally, add a logo, description and vanity web page
    - Callback:
        - Callback URL: `https://www.hydrologyagency.com/hydrology-alert-bulletin`
        - Callback with user ID: `false` (the bulletin does not need to know which FaST user is calling)
        - Callback with holding ID: `false` (the bulletin does not need to know which FaST holding is calling)
        - Callback with campaign ID: `false` (the bulletin does not need to know which FaST campaign is calling)
    - Permissions:
        - Auto subscribe: `true` (the user does not need to subscribe to the [<x>Add-On</x>](/admin/add_ons/addon/) before seeing the bulletin)
        - Groups / Permissions: leave all `blank` (the bulletin does not need access to any FaST object)
    - API keys:
        - Do not create any API key

Our [<x>Add-On</x>](/admin/add_ons/addon/) now appears in the list of add-ons in the mobile app.


If the user clicks on <kbd>Go to Hydrology Alert Bulletin</kbd> then the latest hydrologic alert bulletin will open in their browser.

#### An add-on that displays a tailored web-page and does not need additional login

We will build the connection for an [<x>Add-On</x>](/admin/add_ons/addon/) that is similar in principle to the first one above, but with a small difference: this time the displayed page will depend on the holding that is calling.

The page we want to display is a list of prices for all the crops that the farmer are currently growing on their farm.

This time we need to access the holding (farm) of the user, the plots of the farm, and more importantly the plant species that they are growing. The creation of this [<x>Add-On</x>](/admin/add_ons/addon) will involve setting up some backend service somewhere outside FaST. Below, we will provide some sample code to help understand the basics and bootstrap such a backend service.

1. Create and expose an endpoint that receives the FaST GET callback, retrieves the plant species that is currently grown on their farm, and displays a web page with the prices for each of them.

``` python
# Example pseudo-code

import urllib, json

FAST_API_URL = "<URL to the FaST API Gateway for your region>"

@router.get("/callback")
def handle_callback(callback):
    # The callback will be called by FaST as follows:
    # https://the-callback-url/callback?token=…
    holding_id = callback.GET.get("holding_id")
    campaign_id = callback.GET.get("campaign_id")

    # Request an estimate of the holding geometry (ie, a subset of the plots)
    # via a GraphQL query to the FaST API gateway (simplified query)
    query = """
        query query_all_plant_species_in_farm_during_campaign(
        $holding_id: String!
        $campaign_id: Int!
        ) {
        holding_by_pk(id: $holding_id) {
            name
        }

        campaign_by_pk(id: $campaign_id) {
            name
        }

        plot_plant_variety_aggregate(
            distinct_on: [plant_variety_id]
            where: {
            plot: {
                site: {
                holding_campaign: {
                    _and: {
                    holding_id: { _eq: $holding_id }
                    campaign_id: { _eq: $campaign_id }
                    }
                }
                }
            }
            }
        ) {
            nodes {
            plant_variety {
                plant_species {
                id
                name
                }
                plot_plant_varieties_aggregate(
                where: {
                    plot: {
                    site: {
                        holding_campaign: {
                        _and: {
                            holding_id: { _eq: $holding_id }
                            campaign_id: { _eq: $campaign_id }
                        }
                        }
                    }
                    }
                }
                ) {
                aggregate {
                    count
                }
                }
            }
            }
        }
        }
    """
    data = {
        "query": query,
        "variables": { "holding_id": holding_id, "campaign_id": campaign_id }
    }
    request = urllib.request(FAST_API_URL, json.dumps(data))
    request.add_header('Content-Type', 'application/json')
    request.add_header('Authorization', 'ApiKey <insert API key here>')
    response = urllib.request.urlopen(request)

    # Read the JSON response, compute the centroid of the plots
    # …

    # Compute the Alert Zone that intersects the centroid
    # …

    # Redirect
    # …

```

> Note: the security considerations of exposing such a callback on the internet are left to the implementer to determine. FaST currently does not provide any mechanism to guarantee that the callback is being called by FaST, nor that the callback was not intercepted and modified by a third party.

Now that we have our callback running somewhere, let's configure the [<x>Add-On</x>](/admin/add_ons/addon/).

1. We create a new provider called `Smart Farming`
2. Next we create an [<x>Add-On</x>](/admin/add_ons/addon) named `My Crop Prices`, with the following configuration:
    - Identifier: `crop-prices`
    - Name: `My Crop Prices`
    - Provider: `Smart Farming`
    - Is active: `true`
    - Is visible: `true` (it should be visible in the app)
    - Optionally, add a logo, description and description web page
    - Callback:
        - Callback URL: `https://the-callback-url/callback` (the URL that FaST will call)
        - Callback with user ID: `false` (the bulletin does not need to know which FaST user is calling)
        - Callback with holding ID: `true` (the bulletin needs to know which FaST holding is calling)
        - Callback with campaign ID: `true` (the bulletin needs to know which FaST campaign is calling)
    - Permissions:
        - Auto subscribe: `false` (the user *needs to subscribe* to the [<x>Add-On</x>](/admin/add_ons/addon) before seeing the bulletin, as he/she is sharing his/her farm geometry)
        - Groups / Permissions: give permissions to view the following objects:
            - <x>Farm</x>
            - <x>Campaign</x>
            - <x>Farm Campaign</x>
            - <x>Plot Plant Variety</x>
            - <x>Plant Variety</x>
            - <x>Plant Species</x>
            - <x>Site</x>
            - <x>Plot</x>
              These 8 permissions are needed as the GraphQL query we are sending above needs to request the holding and all the sites and all the plots, etc…
    - API keys:
        - Create an API key

Now, when the user will click on the <kbd>Go to My Crop Prices</kbd> in the app, they will be first redirected to FaST, then to the callback URL, then and finally to the right page the expected information.
#### Other examples

To finish this presentation, here is an example of a slightly more elaborate query, that will be used to retrieve the number of farms that grew a specific plant species in a specific campaign, the number of plots where it was planted, the average superficy of such plot, and the total surface of plot involved.

In this example, let's say we want to investigate this question for olive trees, and let's say in our IACS plant list "olive tree" has the id `909`.

``` graphql
query one_plant_species_during_campaign(
  $plant_species_id: String!
  $campaign_id: Int!
) {
  plant_species(where: { id: { _eq: $plant_species_id } }) {
    id
    name
  }

  campaign_by_pk(id: $campaign_id) {
    name
  }

  holding_aggregate(
    where: {
      holding_campaigns: {
        _and: {
          campaign_id: { _eq: $campaign_id }
          sites: {
            plots: {
              plot_plant_variety: {
                plant_variety: { plant_species_id: { _eq: $plant_species_id } }
              }
            }
          }
        }
      }
    }
  ) {
    aggregate {
      count
    }
  }
  plot_aggregate(
    where: {
      _and: {
        site: { holding_campaign: { campaign: { id: { _eq: $campaign_id } } } }
        plot_plant_variety: {
          plant_variety: { plant_species: { id: { _eq: $plant_species_id } } }
        }
      }
    }
  ) {
    aggregate {
      count
      sum {
        area
      }
      avg {
        area
      }
    }
  }
}

```

This query requires 2 variables to be set, for example:
```json
{
    "plant_species_id": "909", // olive tree
    "campaign_id": 2022 
}
```
