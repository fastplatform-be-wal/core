# Testing

| folder        | content                                         | launch            |
| ------------- | ----------------------------------------------- | ----------------- |
| unit_testing  | unit testing without Hasura access              | `make unit-test`  |
| query_testing | test queries using a running instance of Hasura | `make query-test` |
| event_testing | test events using a running instance of Hasura  | `make event-test` |

## query_testing 
For 1 query with 1 scenario:

`tests`
├── `query_testing`
│   ├── `queries`
│   │   ├── `query #1`: named after the tested *query*
│   │   │   ├── `scenario #1`: named after the tested *scenario* for the query
│   │   │   │   ├── `scenario_setup.graphql`: executed after query_setup, before assert
│   │   │   │   └── `expected.json`: what the query should return
│   │   │   ├── `query_setup.graphql`: executed after query_clear_dbs, before each scenario
│   │   │   └── `query.graphql`: the tested query
│   ├── `test_queries.py`: encapsulates all testing logic
├── `constants.py`: shared values for all tests
└── `query_clear_dbs.graphql`: deletes all objects from db between each scenario.

For each tested scenario, `test_queries.py` executes:
- `query_clear_dbs.graphql`: empties db from all objects
- `query_setup.graphql`: common setup for every scenario
- `scenario_setup.graphql`: specific setup for each scenario
- `query.graphql`: the tested query itself, whose return value should equal `expected.json`. 

## event_testing 
For 1 query with 1 scenario and 1 step:

`tests`
├── `event_testing`
│   ├── `events`
│   │   ├── `event #1`: named after the tested *event*
│   │   │   ├── `scenario #1`: named after the tested *scenario* for the event
│   │   │   │   ├── `01_step_1`: first of n steps for a tested scenario
│   │   │   │   │   ├── either:
│   │   │   │   │   │    - `fastplatform_mutation.graphql`: a mutation to be performed by the fastplatform client;
│   │   │   │   │   │    - `service_query.graphql`: a query to be performed by the tested client;
│   │   │   │   │   │    - `fastplatform_query.graphql`: a query to be performed by the fastplatform client.
│   │   │   │   │   └── `expected.json`: what the `fastplatform_query.graphql` should return.
│   ├── `test_events.py`: encapsulates all testing logic
│   └── `event_setup.graphql`: executed after query_clear_dbs, before each scenario
├── `constants.py`: shared values for all tests
└── `query_clear_dbs.graphql`: deletes all objects from db before each scenario.

For each scenario of a tested event, `test_events.py` executes:
- `query_clear_dbs.graphql`: empties db from all objects
- `event_setup.graphql`: common setup of every scenario

For each step of a scenario, `test_events.py` executes either
- `fastplatform_mutation.graphql`: change db to a new state.;
- `service_query.graphql`: change db to a new state by querying (and thus computing) an sdo or a constraint; 
- `fastplatform_query.graphql`: query db to assert that it matches the given `expected.json` state.

NB: `query_clear_dbs.graphql`, `event_setup.graphql` and `fastplatform_mutation.graphql` potentially trigger a cascade of events, so we systematically **wait 2 seconds** after each execution.
