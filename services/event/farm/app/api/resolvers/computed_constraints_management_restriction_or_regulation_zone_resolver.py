import logging
from functools import partial

from opentelemetry import trace
from gql import gql
from pydantic.types import constr
from shapely.geometry import shape


from app.api.types.computed_constraint import ComputedConstraint
from app.api.lib.gis import Projection, add_crs
from app.settings import config
from app.db.graphql_clients import fastplatform
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError

# Log
logger = logging.getLogger(__name__)

CONSTRAINT_TYPE = "management_restriction_or_regulation_zone"


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class ComputedConstraintsManagementRestrictionOrRegulationZoneResolver:
    async def resolve(self, plot_id: int):
        """
        For a given plot, compute its 'management_restriction_or_regulation_zone' constraints.

        Args:
            plot_id (int): the id of the plot whose constraints of type
            'management_restriction_or_regulation_zone' are calculated here.

        Raises:
            GraphQLHoldingPlotNotFoundError: No plot associated with the id provided.
            GraphQLUnexpectedDataError: The graphql operation could not be performed successfully.

        Returns:
            affected_rows
        """

        logger.info("management_restriction_or_regulation_zone constraints: resolve()")
        try:
            constraints = await self.get_plot_management_restriction_or_regulation_zone_constraints(
                plot_id
            )

            if not constraints == []:
                logger.info("constraints already computed for plot_id %s", plot_id)
                return constraints

            logger.info(
                "Computing management_restriction_or_regulation_zone constraints for plot_id %s",
                plot_id,
            )

            plot_details = await self.get_plot_details(plot_id)

            constraints = await self.compute_management_restriction_or_regulation_zone_constraints(
                plot_details
            )

            if constraints == []:
                return None

            await self.insert_management_restriction_or_regulation_zone_constraints(
                constraints
            )

            return await self.get_plot_management_restriction_or_regulation_zone_constraints(
                plot_id
            )

        except GraphQLHoldingPlotNotFoundError as ex:
            raise GraphQLHoldingPlotNotFoundError() from ex

        except Exception as ex:
            logger.exception(
                "management_restriction_or_regulation_zone_constraints error"
            )
            raise GraphQLUnexpectedDataError() from ex

    async def get_plot_management_restriction_or_regulation_zone_constraints(
        self, plot_id: int
    ):
        logger.info(
            "management_restriction_or_regulation_zone constraints: get_plot_management_restriction_or_regulation_zone_constraints()"
        )

        with tracer().start_as_current_span("get_constraint") as span:
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_plot_constraints_of_type.graphql"
            ).read_text()

            variables = {"plot_id": plot_id, "constraint_type": CONSTRAINT_TYPE}

            response = await fastplatform.execute(gql(query), variables)

            constraints = response["constraint"]
            if constraints == []:
                logger.info(
                    "No management_restriction_or_regulation_zone constraint found for plot: %s. They will now be computed.",
                    plot_id,
                )
                return []

            constraints = [
                ComputedConstraint(
                    id=int(constraint["id"]),
                    name=constraint["name"],
                    description=constraint["description"],
                    plot_id=constraint["plot_id"],
                )
                for constraint in constraints
            ]

            if len(constraints) == 1 and constraints[0].description == None:
                constraints = None

            return constraints

    async def get_plot_details(self, plot_id: int):
        logger.info(
            "management_restriction_or_regulation_zone constraints: get_plot_details()"
        )

        with tracer().start_as_current_span("get_plot_details") as span:
            query = (
                config.API_DIR / "resolvers" / "graphql" / "query_plot_by_pk.graphql"
            ).read_text()
            variables = {"plot_id": plot_id}

            response = await fastplatform.execute(gql(query), variables)

            plot_details = response["plot_by_pk"]

            if plot_details == None or plot_details == {}:
                logger.error("No plot found for id: %s", plot_id)
                raise GraphQLHoldingPlotNotFoundError()

            return plot_details

    async def compute_management_restriction_or_regulation_zone_constraints(
        self, plot_details
    ):
        logger.info(
            "management_restriction_or_regulation_zone constraints: compute_management_restriction_or_regulation_zone_constraints()"
        )
        plot_id = int(plot_details["id"])
        geometry = shape(plot_details["geometry"])

        with tracer().start_as_current_span("get_intersecting_nvz_zones") as span:

            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_external__management_restriction_or_regulation_zone_intersect_plot.graphql"
            ).read_text()

            variables = {
                "geometry": add_crs(geometry.__geo_interface__, config.EPSG_SRID_ETRS89)
            }

            response = await fastplatform.execute(gql(query), variables)

            zones = response["external__management_restriction_or_regulation_zone"]
            span.add_event("{} nvz zones found".format(len(zones)))

        with tracer().start_as_current_span("compute_intersection_with_nvz_zones"):

            def _constraint_from_zone(target, target_area, zone):
                """Convert a zone dict as returned by GraphQL
                to a FaST constraint object
                """

                geometry_zone_projected = Projection.etrs89_to_etrs89_laea(
                    shape(zone["geometry"])
                )

                intersection = geometry_zone_projected.intersection(target)
                intersection_area = intersection.area

                return {
                    "name": "management_restriction_or_regulation_zone",
                    "plot_id": plot_id,
                    "description": {
                        "management_restriction_or_regulation_zone_id": zone["id"],
                        "management_restriction_or_regulation_zone_name": zone["name"],
                        "plot_pct_in_management_restriction_or_regulation_zone": (
                            intersection_area / target_area
                        ),
                        "plot_area_in_management_restriction_or_regulation_zone": intersection_area,
                    },
                }

            geometry_projected = Projection.etrs89_to_etrs89_laea(geometry)
            constraints = map(
                partial(
                    _constraint_from_zone, geometry_projected, geometry_projected.area
                ),
                zones,
            )
            constraints = list(constraints)

            # This `empty constraint` acknowledges that computation took place but turned out nothing.
            if constraints == []:
                constraints = [
                    {
                        "name": CONSTRAINT_TYPE,
                        "plot_id": plot_id,
                        # no description json node set
                    }
                ]

            return constraints

    async def insert_management_restriction_or_regulation_zone_constraints(
        self, constraints
    ):
        logger.info(
            "management_restriction_or_regulation_zone constraints: insert_management_restriction_or_regulation_zone_constraints()"
        )

        with tracer().start_as_current_span("insert_constraints"):
            # Build the query to insert constraints
            mutation = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "mutation_insert_constraint.graphql"
            ).read_text()
            variables = {"objects": constraints}

            response = await fastplatform.execute(gql(mutation), variables)

        return response
