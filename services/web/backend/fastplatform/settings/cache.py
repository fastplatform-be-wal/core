import os


CACHES = {
    "default": {
        "BACKEND": "django_bmemcached.memcached.BMemcached",
        "LOCATION": os.environ.get("DJANGO_MEMCACHED_HOST", "127.0.0.1")
        + ":"
        + os.environ.get("DJANGO_MEMCACHED_PORT", "11211"),
        "OPTIONS": {
            "username": os.environ.get("DJANGO_MEMCACHED_USERNAME", "memcached"),
            "password": os.environ.get("DJANGO_MEMCACHED_PASSWORD"),
        },
    },
    "locmem": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "fastplatform",
    },
}
