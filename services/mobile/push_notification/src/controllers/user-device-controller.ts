import { Request, Response } from 'express'
import client, { FastPlatformKeySeparator, getMatchingKeys } from './database'
import { REDIS_KEY_EXPIRE_USER_DEVICE } from '../config/env'

type redisActionResponse = {
  status: string,
  message: string,
  data?: any
}

// ********* REGISTER USER DEVICE ***********
function _registerUserDevice({ user_id, device_token }: {user_id: string, device_token: string}): Promise<{res: redisActionResponse, httpCode: number}> {
  const key = `${user_id}${FastPlatformKeySeparator}${device_token}`  
  return new Promise((resolve, reject) => {
    // before registering the user device, first deslete the device for other users (see https://gitlab.com/fastplatform/core/-/issues/66)
    _deleteDevice(device_token)
      .then(count => {
        console.log(`Unregistered device token for ${count} user`)
      })
      .catch(err => {
        console.error(`Failed to unregister the device token ${device_token}`, err)
      })
      .finally(() => {
        client.set(key, new Date().toISOString(), 'EX', REDIS_KEY_EXPIRE_USER_DEVICE, (err, reply) => {
          if (err) {
            console.error(`Failed to register the device token ${device_token} for user ${user_id}`, err)
            resolve({
              res: {
                status: 'ERROR',
                message: 'Failed to register the device token'
              },
              httpCode: 500
            })
          } else {
            resolve({
              res: {
                status: 'OK',
                message: ''
              },
              httpCode: 200
            })
          }
        })
      })
  })
}

export async function registerUserDevice(request: Request, response: Response) {
  const user_id = request.params.user_id
  const { device_token } = request.body
  const { res, httpCode } = await _registerUserDevice({user_id, device_token})
  response.status(httpCode).json(res)
}

export async function graphqlRegisterUserDevice({user_id, device_token}: {user_id: string, device_token: string}) {
  const { res } = await _registerUserDevice({user_id, device_token})
  return res
}

// ********* GET USER DEVICES ***********
function _getUserDevices({ user_id }: {user_id: string}): Promise<{res: redisActionResponse, httpCode: number}> {
  return new Promise((resolve, reject) => {
    const match = `${user_id}${FastPlatformKeySeparator}*`
    getMatchingKeys(match).then(keys => {
      const devices = keys.map(key => key.substring(key.indexOf(FastPlatformKeySeparator) + FastPlatformKeySeparator.length))
      resolve({          
        res: {
          status: devices.length > 0 ? 'OK': 'UNKNOWN_OR_EMPTY',
          message: devices.length > 0 ? '': 'Unknown user id or no device token registered for this user',
          data: devices
        },
        httpCode: 200
      })
    }).catch(err => {
      console.error(`Cannot get device tokens for user ${user_id}`, err)
      resolve({
        res: {
          status: 'ERROR',
          message: `Cannot get device tokens for user ${user_id}`
        },
        httpCode: 500
      })
    })
  })
}
 
export async function getUserDevices(request: Request, response: Response) {
  const user_id = request.params.user_id
  const { res, httpCode } = await _getUserDevices({user_id})
  response.status(httpCode).json(res)
}

export async function graphqlGetUserDevices({user_id}: {user_id: string}) {
  const { res } = await _getUserDevices({user_id})
  return res
}

// ********* DELETE USER ***********
function _deleteUser({ user_id }: {user_id: string}): Promise<{res: redisActionResponse, httpCode: number}> {  
  return new Promise((resolve, reject) => {
    const match = `${user_id}${FastPlatformKeySeparator}*`
    getMatchingKeys(match).then(keys => {
      if (keys.length > 0) {
        client.del(keys, (err, reply) => {
          if (err) {
            throw err
          } else {
            resolve({
              res: {
                status: 'DELETED',
                message: `${reply} user devices deleted`
              },
              httpCode: 200
            })
          }
        })
      } else {
        resolve({
          res: {
            status: 'DELETED',
            message: `No user devices found`
          },
          httpCode: 200
        })
      }
    }).catch(err => {
      console.error(`Cannot delete user ${user_id}`, err)
      resolve({
        res: {
          status: 'ERROR',
          message: `Cannot delete user ${user_id}`
        },
        httpCode: 500
      })
    })
  })
}

export async function deleteUser(request: Request, response: Response) {
  const user_id = request.params.user_id;
  const { res, httpCode } = await _deleteUser({user_id})
  response.status(httpCode).json(res)
}

export async function graphqlDeleteUser({user_id}: {user_id: string}) {
  const { res } = await _deleteUser({user_id})
  return res
}

// ********* DELETE USER DEVICE ***********
function _deleteUserDevice({ user_id, device_token }: {user_id: string, device_token: string}): Promise<{res: redisActionResponse, httpCode: number}> {
  const key = `${user_id}${FastPlatformKeySeparator}${device_token}`
  return new Promise((resolve, reject) => {
    client.del(key, (err, reply) => {
      if (err) {
        console.error(`Cannot delete device token ${device_token} for user ${user_id}`, err)
        resolve({
          res: {
            status: 'ERROR',
            message: `Cannot delete device token ${device_token} for user ${user_id}`
          },
          httpCode: 500
        })
      } else {
        resolve({
          res: {
            status: 'DELETED',
            message: `${reply} user device deleted`
          },
          httpCode: 200
        })
      }
    })
  })
}

export async function deleteUserDevice(request: Request, response: Response) {
  const user_id = request.params.user_id;
  const device_token = request.params.device_token;
  const { res, httpCode } = await _deleteUserDevice({user_id, device_token})
  response.status(httpCode).json(res)
}

export async function graphqlDeleteUserDevice({user_id, device_token}: {user_id: string, device_token: string}) {
  const { res } = await _deleteUserDevice({user_id, device_token})
  return res
}


// ********* DELETE DEVICE ***********
function _deleteDevice(device_token: string): Promise<Number> {  
  return new Promise((resolve, reject) => {
    const match = `*${FastPlatformKeySeparator}${device_token}`
    getMatchingKeys(match).then(keys => {
      if (keys.length > 0) {
        client.del(keys, (err, reply) => {
          if (err) {
            reject(err)
          } else {
            resolve(reply)
          }
        })
      } else {
        resolve(0)
      }
    })
  })
}