from django.urls import path

from photos.events import on_delete_geo_tagged_photo, update_thumbnail
from photos.views import UploadGeoTaggedPhotoView

urlpatterns = [
    #path('on_delete_geo_tagged_photo', on_delete_geo_tagged_photo),
    path('geo_tagged_photo_create_thumbnail/', update_thumbnail),
    path('upload_geo_tagged_photo/', UploadGeoTaggedPhotoView.as_view()),
]
