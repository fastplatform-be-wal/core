```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "add_ons.Provider <Add-ons>" as add_ons.Provider #e8f4d6 {
    provider
    ..
    A provider of add-ons
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - The name of the provider, as displayed in the application
and in the admin portal
    + description (TextField) - A description of the provider, that will be
displayed in the app
    + logo (ImageField) - 
    + website (URLField) - URL of the marketing/vanity website of the provider
    + is_active (BooleanField) - Whether this provider and all its add-ons should
be active (visible) in the app. Disabling a provider also disables all the add-
ons of this provider.
    # members (ManyToManyField) - The users that are allowed to manage this
provider
    --
}


class "add_ons.ProviderMember <Add-ons>" as add_ons.ProviderMember #e8f4d6 {
    provider member
    ..
    ProviderMember(id, provider, user)
    --
    + id (BigAutoField) - 
    ~ provider (ForeignKey) - 
    ~ user (ForeignKey) - 
    --
}
add_ons.ProviderMember *-- add_ons.Provider


class "add_ons.AddOn <Add-ons>" as add_ons.AddOn #e8f4d6 {
    add-on
    ..
    AddOn(i18n, id, is_api_access, provider, name, description, is_active,
is_visible, logo, website, callback, callback_secret_key, callback_with_user_id,
callback_with_holding_id, callback_with_campaign_id, auto_subscribe)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + is_api_access (BooleanField) - 
    ~ provider (ForeignKey) - The provider that manages this add-on
    + name (CharField) - The name of the add-on, as displayed in the app. Try to
keep it short.
    + description (TextField) - A description of the add-on, that will be displayed
in the app
    + is_active (BooleanField) - Whether this add-on should be active. Inactive
add-ons cannot query the FaST API and are not visible in the app.
    + is_visible (BooleanField) - Whether this add-on should be visible
    + logo (ImageField) - 
    + website (URLField) - URL of the marketing/vanity website of the add-on
    + callback (URLField) - The callback URL that FaST will call using GET when the
user accesses the service from the app.
    + callback_secret_key (CharField) - The callback secret key is used to create
the jwt token. If a callback URL has been set and this field is left empty, a
secret key will be randomly generated on save.
    + callback_with_user_id (BooleanField) - Whether the username should be passed
when FaST calls the callback URL
    + callback_with_holding_id (BooleanField) - Whether the holding identifier
should be passed when FaST calls the callback URL
    + callback_with_campaign_id (BooleanField) - Whether the campaign identifier
should be passed when FaST calls the callback URL
    + auto_subscribe (BooleanField) - Whether this add-on can access the data of
all holdings (without users first needing to subscribe)
    # groups (ManyToManyField) - The groups this add-on belongs to. An add-on will
get all permissions granted to each of their groups.
    # permissions (ManyToManyField) - Specific permissions for this add-on
    # holdings (ManyToManyField) - 
    --
}
add_ons.AddOn *-- add_ons.Provider


class "add_ons.APIAccess <Add-ons>" as add_ons.APIAccess #e8f4d6 {
    API access
    ..
    APIAccess(i18n, id, is_api_access, provider, name, description, is_active,
is_visible, logo, website, callback, callback_secret_key, callback_with_user_id,
callback_with_holding_id, callback_with_campaign_id, auto_subscribe)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + is_api_access (BooleanField) - 
    ~ provider (ForeignKey) - The provider that manages this add-on
    + name (CharField) - The name of the add-on, as displayed in the app. Try to
keep it short.
    + description (TextField) - A description of the add-on, that will be displayed
in the app
    + is_active (BooleanField) - Whether this add-on should be active. Inactive
add-ons cannot query the FaST API and are not visible in the app.
    + is_visible (BooleanField) - Whether this add-on should be visible
    + logo (ImageField) - 
    + website (URLField) - URL of the marketing/vanity website of the add-on
    + callback (URLField) - The callback URL that FaST will call using GET when the
user accesses the service from the app.
    + callback_secret_key (CharField) - The callback secret key is used to create
the jwt token. If a callback URL has been set and this field is left empty, a
secret key will be randomly generated on save.
    + callback_with_user_id (BooleanField) - Whether the username should be passed
when FaST calls the callback URL
    + callback_with_holding_id (BooleanField) - Whether the holding identifier
should be passed when FaST calls the callback URL
    + callback_with_campaign_id (BooleanField) - Whether the campaign identifier
should be passed when FaST calls the callback URL
    + auto_subscribe (BooleanField) - Whether this add-on can access the data of
all holdings (without users first needing to subscribe)
    # groups (ManyToManyField) - The groups this add-on belongs to. An add-on will
get all permissions granted to each of their groups.
    # permissions (ManyToManyField) - Specific permissions for this add-on
    # holdings (ManyToManyField) - 
    --
}
add_ons.APIAccess *-- add_ons.Provider


class "add_ons.AddOnAPIKey <Add-ons>" as add_ons.AddOnAPIKey #e8f4d6 {
    add-on API key
    ..
    AddOnAPIKey(id, add_on, key, generated_by, generated_at)
    --
    - id (AutoField) - 
    ~ add_on (ForeignKey) - The add-on this key belongs to
    + key (CharField) - The randomly generated key
    ~ generated_by (ForeignKey) - The user that generated this key
    + generated_at (DateTimeField) - The date/time this key was generated
    --
}
add_ons.AddOnAPIKey *-- add_ons.AddOn


class "add_ons.AddOnSubscription <Add-ons>" as add_ons.AddOnSubscription #e8f4d6 {
    add-on subscription
    ..
    AddOnSubscription(id, add_on, holding, subscribed_at, subscribed_by)
    --
    + id (BigAutoField) - 
    ~ add_on (ForeignKey) - 
    ~ holding (ForeignKey) - 
    + subscribed_at (DateTimeField) - 
    ~ subscribed_by (ForeignKey) - 
    --
}
add_ons.AddOnSubscription *-- add_ons.AddOn


@enduml
```