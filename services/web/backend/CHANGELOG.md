# <img src="https://gitlab.com/fastplatform/website/-/raw/master/assets/images/logo-32x32.svg" /> FaST

# Release Notes


> Notes:
>
> - Some system-specific changes (not directly visible by end users) are not listed here.

## v1.0.0

### New

- `App` & `Admin Portal`
  - Fix/improve some translations in the 3 target languages, as per feedback

- `Admin Portal`
  - Finalize user documentation in the Portal, accessible from the `Documentation` link in the header bar. Note that this documentation is different from the technical / IT description of the platform, available at https://gitlab.com/fastplatform/docs.

- `App`
  - Add a specific page for adding a parcel to a campaign (was already possible from any map in the app, but a more obvious workflow has been added)
  - Implement downloading a fertilization plan as pdf (or all the plans of the current campaign in a single pdf)
  - Add the possibility to edit the plot "linked" to a geo-tagged photo (on the photo details page)

- `Other`
  - The mobile application is now available as "open beta" for installation on iOS and Android devices, using the following links:
    - Android: https://play.google.com/store/apps/details?id=eu.fastplatform.mobile.farmer
    - iOS: https://testflight.apple.com/join/7d0Nl9Oa
    
    This new configuration means that now anyone having the above links can install the application on his/her mobile, without needing to pre-whitelist his/her Apple ID or Google ID. However, the user will still need to be able to log in, either by having a login/password for the FaST identity provider or by using a federated provider such as DAT (Andalucia), OPCyL (CyL) or TARA (Estonia).

### Improvements & fixes

- `App`
  - Improve visibility of currently selected plot on the maps (in cases where a "thin" plot could be obscured by surrounding plot borders, as per Estonia feedback)
  - Fix plot thumbnail in plot list where the wrong thumbnail could be associated to a plot in some sorting situations
  - Add list selector for texture in custom soil samples form
  - Constrain soil sample dates to past dates only (Estonia feedback)
  - Harden the region selection screen of the app in case the network connectivity os flaky (Estonia feedback)
  - Remove the currently useless "filter" button on the plot list page (Estonia feedback)

- `Admin Portal`
  - Ensure the last_login date of a user is updated whatever the identity provider the user used to log in (Estonia feedback)
  
## v0.2.8
### New

- `Admin Portal` & `App`
  - Implement a dynamic map proxying and caching for MS-provided WMS and TMS sources. This proxying fixes the projection issues that have happened on sources with incompatible base projections.
  - Implement display of nitrogen limits and warnings for each plot in the app (+ configuration panels in the Portal)
  - Add proper attributions on all maps
  - Implement displaying and managing of T&Cs and Privacy Policy + deploy first version of both in Spanish, Estonian, Italian
  - Remove the cloud masking of Sentinel RGB images in the FaST processing

- `App`

- `Other`
  - The code base of FaST (including all backend services and the mobile application) is now fully open-sourced at https://gitlab.com/fastplatform/


### Improvements & fixes

- `App`
  - Adjust the PlantSpecies nomenclature (from Piemonte feedback)
  - Adjust the Visione pages flow and wording (from Piemonte feedback)
  - Improve translations for soil data (Fertilicalc, from Castilla y Leon feedback)
  - Improve target yield selection when irrigation method changes (Fertilicalc, from Castilla y Leon feedback)
  - Improve the add-on callback mechanism
  - Improve UI layout on the web version when the screen width decreases
  - Improve permission management when taking a photo
  - Improve token refresh management for the OPCyL connector
  - Fix an issue with the Android back button and the map fullscreen mode
  - Fix an issue where the connector to IACS/PRIA would not return any result

- `Admin Portal`
  - Improve and clarify the Visione Administration Portal
  - Improve the deletion of geo-tagged photos
  - Continue improving the documentation
  - Improvements to the GraphQL playground in the Portal
  - Fix an issue that would prevent users from sending broadcasts from the Portal
  - Fix an issue where a user logging in through a Federated Provider could be stripped of his/her staff/superuser status

- `Other`
  - General improvements to the stability and security of the platform

## v0.2.7

### New

- `Admin Portal`
  - Implement maps on all relevant pages of the farm objct hierarchy
  - Implement mechanism to display legal notices to users and get their consent
  - Add documentation structure and search
  - Add documentation for user management

- `App`:
  - Implement adding/removing polygons to a farm and edit crops (Castilla y Leon and Estonia only)
  - Implement adding/removing custom soil samples
  - Implement adding/removing custom farm fertilizers
  - Implement details and maps on plot constraints
  - All available identity providers are now activated: DAT (Andalucia), OPCyL (Castilla y Leon) and TARA (Estonia)
  - All available IACS connectors are now activated: REAFA (Andalucia), OPCyL (Castilla y Leon), PRIA (Estonia) and ARPEA (Piemonte)
  - Implement fertilizer product recommendation for the Fertilicalc algorithm
  - Highlight selected item on maps
  - Implement feature flags to disable unused features per region

- `API Gateway`
  - Implement ability for external services/actors to query the FaST API + object-level permissions + holding-level subscriptions. Documentation ongoing.

### Improvements

- `App`:
  - Improve map caching and performance
  - Improve fertilization plan result storage
  - Improve texts on the connectors available for logging in
  - Improve display of satellite imagery selector for readability
  - Normalize the display of fertilization recommendation results across algorithms, when possible
  - Improve readability of maps (make polygons more translucent)
  - Fix an issue that would prevent the creation of thumbnail of a geotagged photo in some cases

- `Admin Portal`
  - Clarify screens and messages in case the user is logged in but does not have the staff status (and therefore cannot access the portal)
  
### Fixes

- `App`:
  - Fix several issues linked to forward/back navigation during the course of editing a fertilization plan (Fertilicalc)
  - Fix Estonian tranlations, thanks to the great help from the Estonian ARC team
  - Fix logout issues that cause the process to stall on the desktop version of the app and generate display issues on the login screen
  - Fix missing wind bearing/speed forecast for Spain after D+3
  - Fix bug in case of icy weather
  - Fix issue that would prevent saving a fertilization plan in some cases (forward/back navigation)
  - Fix issue that would prevent the weather widget from loading after upgrade

- `Admin Portal`
  - Fix several display inconsistencies in the portal

## v0.2.6

This version includes a new batch of translations for Estonian, Italian and Spanish, as well as some corrections of existing translations, from user feedback.

### New

- `Admin Portal`: Added a page to view all the FaST objects, in a liner manner
- `Admin Portal`: Added a documentation section (in the header menu), content is being built

- Add-ons:
  - `Admin Portal` & `API Gateway`: Preparations to enable access to the API gateway through API keys
  - Documentation: Added documentation for API Key access in the git repository

- Fertilization:
  - `App` First version of the Vegsyst algorithm (for Andalucia only)

- Regulatory:
  - `Admin Portal` Added screens for management of nitrogen limitations and warnings 

- Weather:
  - `App` Added a detail screen for weather, that shows additional information returned by the regional weather provider.

### Improvements

- `Admin Portal` General cleanup, removed unused/unnecessary pages for each region
- `App` Improve app loading performance
- `App` Add notifications in case network connection is lost

- IACS
  - `App` Improved the consent gathering screen before querying the farmer's data on the IACS system

- Authentication
  - `Admin Portal` For the FaST identity provider, added lockout page in case a user repeatedly inputs wrong credentials
  - `Admin Portal` Improved the permission selection page for user groups

- Fertilization
  - `App` Added range controls on inputs for the Fertilicalc & Visione algorithms
  - `App` First version of the Vegsyst algorithm (for Andalucia only)

- Geotagged photos
  - `App` Added a warning on logout if not all photos have completed uploading

- Messaging
  - `App` Improved readability of ticket status + various UX improvements

- Maps
  - `App` Made polygons more translucent on the maps, for improved readibility

- Weather
  - `App` Added more details to the weather widget (wind, rain), if/when the data is available from the weather provider

### Fixes

- Fertilization
  - `App` Fixed an issue that prevented users from saving a fertilization under certain conditions

- Geotagged photos
  - `Admin Portal` Fixed thumbnail creation and display
  - `App` Fixed an issue that would cause a photo to be uploaded multiple times under certain conditions

- Messaging
  - `App` Fixed wrong message count
  - `App` Fixed HTML links being stripped by email clients for notifications

- Weather
  - `App` Fixed widget that would not load after app update



## v0.2.5

### New

- Authentication
  - `Admin Portal`: Added password reset workflow for the FaST identity provider (through email)

- IACS
  - `App` Implemented clearer Farm Management screen (simplified, less buttons)
  - `App` Implemened clearer consent request before requesting data from IACS
  - `App` Enabled create campaign from ARPEA IACS (Piemonte)
  - `App` Enabled create campaign from OPCyL IACS (Castilla y Leon)
  - `App` Enabled create campaign from REAFA (Andalucia)

- Maps
  - `App` Added link between a parcel polygon (on the map) and the parcel's detail page
  - `Admin Portal` Added cloud cover warning for NDVI/RGB images, in case the cloud cover is > 50%
  - `Admin Portal` Added NDVI color legend NDVI images
  - `Admin Portal` Added loading indicator to the RGB/DNVI control

- Messaging
  - `Admin Portal` Added email notifications to members of a queue when a ticket from this queue is changed
  - `App` Added email notifications to members of the farm (for users for which FaST has the email address)
  - `Admin Portal` Restricted visibility of tickets of a queue to members of this queue
  - `App` Added management/count of unread messages and broadcasts

- Other
    - `Admin Portal` Added the core version number in the Admin Portal header
    - `App` Deployed the FaST App as a webapp available at https://app.beta.fastplatform.eu
    - `App` Made the app downloadable from Apple and Google stores for any country (previously it was limited to the EU), but still in cosed beta (needs pre-enrollment)

### Improvements

- Maps
  - `Admin Portal & App` Improved the usability of the layer panel (especially closing) + cap layer name length
  - `Admin Portal` Improved the MapOverlay and MapBaseLayer configuration screens

- Messaging
  - `Admin Portal & App` Prepared for internationalisation of notifications (now waiting for final translations)

- External data
  - `Admin Portal` Improved filtering objects per version

- Fertilization
  - `App` Visione: Added default values for sand & clay when the user chooses a texture
  - `App` Fertilicalc: Added questions for residues for both the previous and the current campaign
  - `App` Fertilicalc: Replaced the binary question about residues collection with a percentage (with a default value overridable by the user)
  - `App` Fertilicalc: Moved the "strategy" screen before the "soil" screen
  - `App` Fertilicalc: In case the strategy is "maintenance", now request only the soil texture

- Geotagged photos
  - `App` Allowed user to take a picture even before the position is stabilized using the GSA algorithm. In this case, it is the smartphone's fused location that is uszed to geotag the photo.

- IACS
  - PRIA (Estonia), OPCyL (Castilla y Leon), and REAFA (Andalucia) Improved default naming for parcels imported from IACS
  - REAFA (Andalucia) Improved polygon deduplication

- Other
  - `Admin Portal` Improved performance of user group creation screen
  - `App` Improved popup notifications (close button, etc)
  - `App` Added ability to filter plots by crop on the plot selection page of the fertilization algorithms

### Fixes

- Maps
  - `Admin Portal & App` Fixed custom WMS/TMS layers configuration and display
  - `Admin Portal & App` Fixed behaviour of NDVI/RGB control is case of flaky network
  - `Admin Portal` Fixed maps do not display on Admin Portal ADD screens
  - `Admin Portal` Fixed maps do not display when in Estonian language
- Messaging
  - `Admin Portal` Fixed cascade delete of broadcasts and tickets
- External data
  - `Admin Portal` Fixed cascade delete for versions
