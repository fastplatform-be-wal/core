load("@web_backend_pip//:requirements.bzl", "all_requirements")
load("@web_backend_pip_image//:requirements.bzl", all_requirements_image = "all_requirements")
load("@web_backend_pip_dev//:requirements.bzl", all_dev_requirements = "all_requirements")
load("@rules_python//python:defs.bzl", "py_binary", "py_library")
load("@io_bazel_rules_docker//python:image.bzl", "py_layer")
load("@io_bazel_rules_docker//python3:image.bzl", "py3_image")
load("@io_bazel_rules_docker//container:container.bzl", "container_image")

package(default_visibility = ["//services/web:__pkg__"])

filegroup(
    name = "srcs",
    srcs = glob(
        ["**/*.py"],
        exclude = [
            "manage.py",
            "run.py",
            ".venv/**/*",
            "src/**/*.py",
        ],
    ),
    data = glob(
        [
            "**/*",
            ":version",
        ],
        exclude = [
            ".gitignore",
            ".dockerignore",
            "BUILD",
            "Dockerfile",
            "Makefile",
            "README.md",
            "kustomization.yaml",
            "deploy/**/*",
            "requirements*.txt",
            "**/*.py",
            ".venv/**/*",
            "src/**/*",
            "data/**/*",
            "*.env",
            "**/__pycache__/*",
        ],
    ),
)

py_library(
    name = "backend-lib",
    srcs = [
        ":srcs",
    ],
    deps = select({
        "//conditions:default": all_requirements + all_dev_requirements,
    }),
)

py_binary(
    name = "backend",
    srcs = [
        "manage.py",
    ],
    main = "manage.py",
    deps = [
        ":backend-lib",
    ],
)

py_binary(
    name = "backend-run",
    srcs = [
        "manage.py",
        "run.py",
    ],
    main = "run.py",
    deps = [
        ":backend-lib",
    ],
)

container_image(
    name = "image",
    base = ":image_layers",
    env = {
        "DEBUG": "FALSE",
        "PORT": "8000",
        "MEDIA_ROOT": "/media",
    },
)

py3_image(
    name = "image_layers",
    srcs = [
        "manage.py",
        "run.py",
        ":srcs",
    ],
    base = "@osgeo_gdal_image_base//image",
    layers = [":layer_requirements"],
    main = "run.py",
)

py_layer(
    name = "layer_requirements",
    deps = select({
        "//tools:debug": all_requirements_image + all_dev_requirements,
        "//conditions:default": all_requirements_image,
    }),
)

genrule(
    name = "version",
    outs = [
        "VERSION",
    ],
    cmd = "grep STABLE_CORE_VERSION < bazel-out/stable-status.txt | cut -d ' ' -f2 | tr -d '\n' > $@",
    stamp = True,
)

filegroup(
    name = "kustomize_resources",
    srcs = [
        "kustomization.yaml",
    ] + glob([
        "deploy/base/*",
    ]),
)

# RULE AUTOMATICALLY GENERATED DO NOT EDIT!
filegroup(
    name = "graphql_queries",
    srcs = glob(["**/*.gql","**/*.graphql"],exclude=["**/tests/**/*.gql","**/tests/**/*.graphql"]),
)
