```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "external.UnitOfMeasure <Imported GIS data sources>" as external.UnitOfMeasure #d6edf4 {
    unit of measure
    ..
    UnitOfMeasure(i18n, id, name, symbol)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - 
    + symbol (CharField) - 
    --
}


class "external.ManagementRestrictionOrRegulationZoneVersion <Imported GIS data sources>" as external.ManagementRestrictionOrRegulationZoneVersion #d6edf4 {
    management restriction or regulation zone version
    ..
    A version of the regulated zones data (NVZ)
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.ManagementRestrictionOrRegulationZone <Imported GIS data sources>" as external.ManagementRestrictionOrRegulationZone #d6edf4 {
    management restriction or regulation zone
    ..
    A zone where a specific regulation or restriction applies (eg NVZ)
    Inspire - Annex III - Area Management Restriction, Regulation Zones and
Reporting units
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    ~ version (ForeignKey) - 
    + geometry (MultiPolygonField) - 
    + thematic_id (CharField) - 
    + name (CharField) - 
    ~ specialized_zone_type (ForeignKey) - 
    + designation_period (DateRangeField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + source (JSONField) - The source data from which this object was derived.
    # zone_type (ManyToManyField) - 
    # environmental_domain (ManyToManyField) - 
    --
}
external.ManagementRestrictionOrRegulationZone *-- external.ManagementRestrictionOrRegulationZoneVersion
external.ManagementRestrictionOrRegulationZone *-- external.SpecialisedZoneTypeCode
external.ManagementRestrictionOrRegulationZone *--* external.ZoneTypeCode
external.ManagementRestrictionOrRegulationZone *--* external.EnvironmentalDomain


class "external.ZoneTypeCode <Imported GIS data sources>" as external.ZoneTypeCode #d6edf4 {
    zone type code
    ..
    A type of regulated zone
    http://inspire.ec.europa.eu/codelist/ZoneTypeCode
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.ZoneTypeCode *-- external.ZoneTypeCode


class "external.SpecialisedZoneTypeCode <Imported GIS data sources>" as external.SpecialisedZoneTypeCode #d6edf4 {
    specialized zone type code
    ..
    Additional classification value that defines the specialised type of zone
    https://inspire.ec.europa.eu/codelist/SpecialisedZoneTypeCode
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.SpecialisedZoneTypeCode *-- external.SpecialisedZoneTypeCode


class "external.EnvironmentalDomain <Imported GIS data sources>" as external.EnvironmentalDomain #d6edf4 {
    environmental domain
    ..
    Environmental domain, for which environmental objectives can be defined
    http://inspire.ec.europa.eu/codelist/EnvironmentalDomain
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.EnvironmentalDomain *-- external.EnvironmentalDomain


class "external.SurfaceWaterVersion <Imported GIS data sources>" as external.SurfaceWaterVersion #d6edf4 {
    surface water version
    ..
    A version of the surface water data
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.SurfaceWater <Imported GIS data sources>" as external.SurfaceWater #d6edf4 {
    surface water
    ..
    A surface water body (ie still water)
    Inspire - Annex I - Hydrography - Physical Waters
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    + hydro_id (CharField) - 
    + geometry (GeometryField) - 
    + geographical_name (CharField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + buffer (IntegerField) - 
    + local_type (CharField) - 
    + source (JSONField) - The source data from which this object was derived.
    ~ version (ForeignKey) - 
    --
}
external.SurfaceWater *-- external.SurfaceWaterVersion


class "external.WaterCourseVersion <Imported GIS data sources>" as external.WaterCourseVersion #d6edf4 {
    water course version
    ..
    A version of the water course data
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.WaterCourse <Imported GIS data sources>" as external.WaterCourse #d6edf4 {
    water course
    ..
    A water course (ie running water)
    Inspire - Annex I - Hydrography - Physical Waters
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    + hydro_id (CharField) - 
    + geometry (GeometryField) - 
    + geographical_name (CharField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + buffer (IntegerField) - 
    + local_type (CharField) - 
    + source (JSONField) - The source data from which this object was derived.
    ~ version (ForeignKey) - 
    --
}
external.WaterCourse *-- external.WaterCourseVersion


class "external.ProtectedSiteVersion <Imported GIS data sources>" as external.ProtectedSiteVersion #d6edf4 {
    protected site version
    ..
    A version of the protected sites data
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.ProtectedSite <Imported GIS data sources>" as external.ProtectedSite #d6edf4 {
    protected site
    ..
    A protected site (eg Natura 2000)
    Inspire - Annex I - Protected Site
    An area designated or managed within a framework of international,
    Community and Member States' legislation to achieve specific conservation
    objectives.
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    ~ version (ForeignKey) - 
    + geometry (GeometryField) - 
    + legal_foundation_date (DateTimeField) - The date that the protected site was
legally created. This is the date that the real world object was created, not
the date that its representation in an information system was created.
    + legal_foundation_document (CharField) - A URL or text citation referencing
the legal act that created the Protected Site.
    + site_name (CharField) - 
    + site_protection_classification (CharField) - 
    + source (JSONField) - The source data from which this object was derived.
    # site_designation (ManyToManyField) - 
    --
}
external.ProtectedSite *-- external.ProtectedSiteVersion
external.ProtectedSite *--* external.DesignationType


class "external.DesignationType <Imported GIS data sources>" as external.DesignationType #d6edf4 {
    designation type
    ..
    A data type designed to contain a designation for the Protected Site,
    including the designation scheme used and the value within that scheme.
    --
    - id (AutoField) - 
    ~ designation_scheme (ForeignKey) - The scheme from which the designation code
comes.
    ~ designation (ForeignKey) - 
    + percentage_under_designation (FloatField) - The percentage of the site that
falls under the designation. This is used in particular for the IUCN
categorisation. If a value is not provided for this attribute, it is assumed to
be 100%.
    + source (JSONField) - The source data from which this object was derived.
    --
}
external.DesignationType *-- external.DesignationScheme
external.DesignationType *-- external.Designation


class "external.DesignationScheme <Imported GIS data sources>" as external.DesignationScheme #d6edf4 {
    designation scheme
    ..
    A scheme used to assign a designation to the Protected Sites.
    Inspire - Annex I - Protected Sites
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.DesignationScheme *-- external.DesignationScheme


class "external.Designation <Imported GIS data sources>" as external.Designation #d6edf4 {
    designation
    ..
    An abstract base type for code lists containing the
    classification and desigation types under different schemes.
    Inspire - Annex I - Protected Sites
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.Designation *-- external.Designation


class "external.SoilSiteVersion <Imported GIS data sources>" as external.SoilSiteVersion #d6edf4 {
    soil site version
    ..
    SoilSiteVersion(id, is_active, status, import_started_at, import_completed_at,
import_log)
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.SoilSite <Imported GIS data sources>" as external.SoilSite #d6edf4 {
    soil site
    ..
    Soil sites
    Origin: INSPIRE Annex III - Soil
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    ~ version (ForeignKey) - 
    + geometry (GeometryField) - 
    ~ soil_investigation_purpose (ForeignKey) - 
    + valid_from (DateTimeField) - 
    + valid_to (DateTimeField) - 
    + begin_lifespan_version (DateTimeField) - 
    + end_lifespan_version (DateTimeField) - 
    + source (JSONField) - The source data from which this object was derived.
    --
}
external.SoilSite *-- external.SoilSiteVersion
external.SoilSite *-- external.SoilInvestigationPurpose


class "external.Observation <Imported GIS data sources>" as external.Observation #d6edf4 {
    observation
    ..
    Observation(id, soil_site, observed_property, result, phenomenon_time,
result_time, valid_time, source)
    --
    - id (AutoField) - 
    ~ soil_site (ForeignKey) - 
    ~ observed_property (ForeignKey) - 
    + result (JSONField) - Must be an JSON object with a "value" property, e.g.
{"value": 123}
    + phenomenon_time (DateTimeField) - 
    + result_time (DateTimeField) - 
    + valid_time (DateTimeRangeField) - 
    + source (JSONField) - The source data from which this object was derived.
    --
}
external.Observation *-- external.SoilSite
external.Observation *-- external.ObservableProperty


class "external.ObservableProperty <Imported GIS data sources>" as external.ObservableProperty #d6edf4 {
    observable property
    ..
    Observable property
    Origin: INSPIRE Annex III - Soil
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    ~ base_phenomenon (ForeignKey) - 
    ~ uom (ForeignKey) - 
    + validation_rules (JSONField) - 
    --
}
external.ObservableProperty *-- external.PhenomenonType
external.ObservableProperty *-- external.UnitOfMeasure


class "external.PhenomenonType <Imported GIS data sources>" as external.PhenomenonType #d6edf4 {
    phenomenon type
    ..
    Phenomenon type
    Origin: INSPIRE Annex III - Soil
    http://inspire.ec.europa.eu/codelist/SoilSiteParameterNameValue
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.PhenomenonType *-- external.PhenomenonType


class "external.SoilInvestigationPurpose <Imported GIS data sources>" as external.SoilInvestigationPurpose #d6edf4 {
    soil investigation purpose
    ..
    Soil investigation purpose
    Origin: INSPIRE Annex III - Soil
    https://inspire.ec.europa.eu/codelist/SoilInvestigationPurposeValue
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
external.SoilInvestigationPurpose *-- external.SoilInvestigationPurpose


class "external.AgriPlotVersion <Imported GIS data sources>" as external.AgriPlotVersion #d6edf4 {
    agricultural plot version
    ..
    AgriPlotVersion(id, is_active, status, import_started_at, import_completed_at,
import_log)
    --
    - id (AutoField) - 
    + is_active (BooleanField) - Whether this version is currently visible to the
users; there can be only one version active at a given time.
    + status (CharField) - Is COMPLETED when the version has been fully loaded
    + import_started_at (DateTimeField) - 
    + import_completed_at (DateTimeField) - 
    + import_log (TextField) - 
    --
}


class "external.AgriPlot <Imported GIS data sources>" as external.AgriPlot #d6edf4 {
    public agricultural plot
    ..
    Agricultural plots
    These are not the plots of the farms of each farmer, but the general flat
    layer of plots that the farmer can pick from to add to his farm/campaign
    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    --
    - id (AutoField) - 
    + authority_id (CharField) - Must be unique within each version
    ~ version (ForeignKey) - 
    + geometry (MultiPolygonField) - 
    + plant_species_id (CharField) - 
    + plant_variety_id (CharField) - 
    + area (FloatField) - In square meters (m2)
    + valid_from (DateField) - 
    + valid_to (DateField) - 
    + source (JSONField) - The source data from which this object was derived.
    --
}
external.AgriPlot *-- external.AgriPlotVersion


@enduml
```