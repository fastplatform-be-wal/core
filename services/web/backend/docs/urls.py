import os
from pathlib import Path
import json

from django.urls import path
from django.conf import settings

from .views import DocsView, DocsSearchView

DOCS_BUILD_DIR = os.path.join(settings.BASE_DIR, "docs", "build")

urlpatterns = [path("search/", DocsSearchView.as_view(), name="search_results")]

if Path(DOCS_BUILD_DIR, "urlpatterns.json").exists():
    docs_urlpatterns = json.loads(Path(DOCS_BUILD_DIR, "urlpatterns.json").read_text())
    urlpatterns += [
        path(
            urlpattern["url"],
            DocsView.as_view(file_name=urlpattern["file_name"]),
            name=urlpattern["name"],
        )
        for urlpattern in docs_urlpatterns
    ]
