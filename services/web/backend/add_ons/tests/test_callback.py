from unicodedata import name
from django.test import TestCase
from datetime import datetime

import itertools
import jwt

from django.conf import settings
from django.utils import timezone
from django.http import (
    HttpResponseNotFound,
    HttpResponseForbidden,
    HttpResponseRedirect,
)

from oidc_provider.tests.app.utils import create_fake_client
from oidc_provider.lib.utils.token import create_token

from authentication.models import User
from add_ons.models import AddOn, Provider, AddOnSubscription
from add_ons.views import AddOnCallbackView
from farm.models import Holding, UserRelatedParty, Campaign

TEST_PARAMS = {
    "user_id": "test-user",
    "holding_id": "test_holding",
    "campaign_id": 2022,
}

ADDON_CALLBACK_URL_SECRET_KEY = (
    "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

KEYS = ["user_id", "holding_id", "campaign_id"]


class AddOnCallbackTestCase(TestCase):
    add_on = None

    def setUp(self):
        super().setUp()

        self.user, _ = User.objects.get_or_create(username=TEST_PARAMS["user_id"])

        self.fake_callback_url = "https://test.com/callback/"
        self.fake_callback_jwt_secret_key = ADDON_CALLBACK_URL_SECRET_KEY

        self.active_campaign, _ = Campaign.objects.update_or_create(
            id=TEST_PARAMS["campaign_id"],
            defaults=dict(
                name="2021-2022",
                start_at=timezone.make_aware(
                    datetime(TEST_PARAMS["campaign_id"], 1, 1)
                ),
                end_at=timezone.make_aware(
                    datetime(TEST_PARAMS["campaign_id"] + 1, 1, 1)
                ),
                is_current=False,
            ),
        )

        provider, _ = Provider.objects.update_or_create(
            id="test_provider", defaults=dict(name="Test provider", is_active=True)
        )
        self.add_on, _ = AddOn.objects.update_or_create(
            id="test_add_on",
            defaults=dict(
                name="Test add-on",
                provider=provider,
                auto_subscribe=False,
                is_active=True,
                callback=self.fake_callback_url,
                callback_secret_key=self.fake_callback_jwt_secret_key,
                callback_with_user_id=True,
                callback_with_holding_id=True,
                callback_with_campaign_id=True,
            ),
        )
        self.holding, _ = Holding.objects.update_or_create(
            id=TEST_PARAMS["holding_id"], defaults=dict(name="Test holding")
        )
        self.subscription, _ = AddOnSubscription.objects.update_or_create(
            add_on=self.add_on,
            holding=self.holding,
            defaults=dict(subscribed_by=self.user),
        )
        self.related_party, _ = UserRelatedParty.objects.update_or_create(
            user=self.user, holding=self.holding, defaults=dict(role="farmer")
        )

        self.oidc_client = create_fake_client("code")
        self.access_token, refresh_token, at_hash, self.token = create_token(
            self.user, self.oidc_client, []
        )
        self.token.save()

    def test_refresh_callback_token(self):

        for p in itertools.product([True, False], repeat=3):
            includes = dict(zip(KEYS, p))

            with self.subTest(includes=includes):

                callback_token = AddOnCallbackView.refresh_callback_token(
                    ADDON_CALLBACK_URL_SECRET_KEY, includes, TEST_PARAMS
                )

                decoded_jwt = jwt.decode(
                    callback_token,
                    ADDON_CALLBACK_URL_SECRET_KEY,
                    settings.ADD_ONS_CALLBACK_JWT_SIGNING_ALGORITHM,
                )

                for key in includes.keys():
                    self.assertEqual(key in decoded_jwt, includes[key])

                    if includes[key]:
                        self.assertEqual(decoded_jwt[key], TEST_PARAMS[key])

        return callback_token

    def test_callback_all_ok(self):
        for p in itertools.product([True, False], repeat=3):
            includes = dict(zip(KEYS, p))
            for key in includes.keys():
                setattr(self.add_on, f"callback_with_{key}", includes[key])

            self.add_on.save()

            with self.subTest(includes=includes):
                response = self.client.get(
                    f"/add_ons/callback/{self.add_on.id}/?holding_id={self.holding.id}&active_campaign_id={self.active_campaign.id}&token={self.access_token}"
                )

                includes, params = AddOnCallbackView.build_query_params(self)
                callback_token = AddOnCallbackView.refresh_callback_token(
                    ADDON_CALLBACK_URL_SECRET_KEY, includes, params
                )

                self.assertRedirects(
                    response,
                    f"{self.fake_callback_url}?token={callback_token}",
                    status_code=HttpResponseRedirect.status_code,
                    fetch_redirect_response=False,
                )

    def test_callback_missing_holding(self):
        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?active_campaign_id={self.active_campaign.id}&token={self.access_token}"
        )
        self.assertIsInstance(response, HttpResponseNotFound)

    def test_callback_wrong_holding(self):
        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?holding_id=ABCD&active_campaign_id={self.active_campaign.id}&token={self.access_token}"
        )
        self.assertIsInstance(response, HttpResponseNotFound)

    def test_callback_missing_active_campaign(self):
        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?holding_id={self.holding.id}&token={self.access_token}"
        )
        self.assertIsInstance(response, HttpResponseNotFound)

    def test_callback_wrong_active_campaign(self):
        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?holding_id={self.holding.id}&active_campaign_id=1789&token={self.access_token}"
        )
        self.assertIsInstance(response, HttpResponseNotFound)

    def test_callback_missing_authorization(self):
        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?holding_id={self.holding.id}&active_campaign_id={self.active_campaign.id}"
        )
        self.assertIsInstance(response, HttpResponseForbidden)

    def test_callback_add_on_does_not_exist(self):
        response = self.client.get(
            f"/add_ons/callback/wrong_add_on/?token={self.access_token}"
        )
        self.assertIsInstance(response, HttpResponseNotFound)

    def test_callback_authorization_wrong(self):
        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?holding_id={self.holding.id}&token=ABCD"
        )
        self.assertIsInstance(response, HttpResponseForbidden)

    def test_callback_not_related_party(self):
        self.related_party.delete()

        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?holding_id={self.holding.id}&token={self.access_token}"
        )
        self.assertIsInstance(response, HttpResponseForbidden)

    def test_callback_no_subscription(self):
        self.subscription.delete()

        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?holding_id={self.holding.id}&token={self.access_token}"
        )
        self.assertIsInstance(response, HttpResponseForbidden)

    def test_callback_secret_key_null(self):

        self.add_on.callback = None
        self.add_on.callback_secret_key = None
        self.add_on.save()

        response = self.client.get(
            f"/add_ons/callback/{self.add_on.id}/?holding_id={self.holding.id}&active_campaign_id={self.active_campaign.id}&token={self.access_token}"
        )
        self.assertIsInstance(response, HttpResponseNotFound)
