# Maps

- [Maps](#maps)
  - [Introduction](#introduction)
  - [Configuring the maps](#configuring-the-maps)
    - [Base layers and overlays](#base-layers-and-overlays)
  - [How do I...?](#how-do-i)
      - [How do I add a base layer / overlay to the maps?](#how-do-i-add-a-base-layer--overlay-to-the-maps)
      - [How do I remove a base layer / overlay from the maps?](#how-do-i-remove-a-base-layer--overlay-from-the-maps)
      - [How do I edit a base layer /overlay?](#how-do-i-edit-a-base-layer-overlay)
    - [Other map settings](#other-map-settings)

## Introduction

Maps are a core feature of FaST, and appear in both the mobile application and the Administration Portal, in multiple locations. At their core, all maps are built the same way and embark the same layers:

- Base layers
- Raster overlays
- RGB/NDVI overlays
- Vector overlays

And the same controls (counter clockwise, starting from the top left):

- Zoom in/out
- Center map back to the initial location
- Fullscreen toggle
- Center map on user location
- RGB/NDVI date selector
- Layer(s) visibility menu


<img src="/services/web/backend/docs/static/docs/img/maps/map_elements.png" title="Map elements" class="img-docs" height="400" />

FaST maps can display in the same location data from various sources. These sources are proxied to the maps using several services that FaST runs to make them compatible (e.g. projections) or more efficient (like caching).

The main data sources displayed on maps are:

- Farmer data, like plots. These are served directly from the FaST main database as _vector tiles_. These layers can be removed from the maps and are configured by FaST depending on the map context.
- Public GIS data, like Natura2000 areas, NVZ, hydrological network. These are served from the secondary FaST database, as vector tiles as well. Those layers can be added or removed from the map to the user's liking (see layers visibility menu).
- Base maps and overlays. These are coming from external sources (like aerial photography provided by Paying Agency or road network from the OpenStreetMap project) and are served into the maps through a proxy running on the FaST infrastructure. This proxy performs on-the-fly map re-projection and caching.
- Sentinel imagery (RGB/NDVI). These images are served by a FaST add-on that performs the necessary calculations, cloud masking and tiling. This processing is fully automatic.

All FaST maps are currently displayed using the [WGS 84 / Pseudo-Mercator (EPSG:3857)](https://epsg.io/3857) projection (Spherical Mercator, Google Maps, OpenStreetMap, Bing, ArcGIS, ESRI).

```plantuml
skinparam defaultFontSize 12

title High-level map data architecture\n

rectangle "FaST maps" as map #white {
    rectangle "Base layers\n//(e.g. WMS services)//" as base #white
    rectangle "Raster overlays\n//(e.g. WMS services)//" as overlays #white
    rectangle "Vector overlays" as vector {
        rectangle "Public data\n//(e.g. NVZ, Natura2000)//" as public #white
        rectangle "Farm data\n//(e.g. plots)//" as farm #white
    }
    rectangle "Sentinel overlays\n//(RGB/NDVI)//" as ndvi #white
}

rectangle "FaST backend" as backend {
    component "Raster map\nproxying service" as mapproxy
    component "RGB/NDVI\ntiling service" as rgb_ndvi
    component "Farm vector\ntiles service" as farm_vector
    database "FaST database\n(farm data)" as farm_db
    component "Public vector\ntiles service" as public_vector
    database "FaST database\n(public GIS data)" as public_db
}

cloud "External data" as external {
    collections "Sentinel imagery\n(on Sobloo DIAS)" as sentinel #white
    collections "Source TMS/WMS layers" as source_wms #white
}

base --> mapproxy
overlays --> mapproxy
ndvi --> rgb_ndvi

farm --> farm_vector
public --> public_vector

farm_vector -> farm_db
public_vector -> public_db

mapproxy --> source_wms
rgb_ndvi --> sentinel
```

## Configuring the maps
### Base layers and overlays

The base layers and overlays of FaST maps are fully configurable in the Administration Portal. They are both similar in concept, with the following difference:

- base layers are **mutually exclusive**, i.e. there must and can be only one base layer displayed at a given time
- overlays are not exclusive and several of them (or none) can be displayed at once.

The base layers and overlays can be configured in the Administration Portal. The changes performed in these configurations are replicated across all FaST users (mobile app and Administration Portal), either immediately or with a slight delay due to reconfiguration of the various caches on the backend or in the interfaces.

<img src="/services/web/backend/docs/static/docs/img/maps/admin_base_overlays.png" title="Map elements" class="img-docs" height="200" />

The following configuration options are available for each base layer or overlay:

- _Identifier_: a unique identifier (_will_ not be displayed to the user)
- _Name_: a display name (_will_ be displayed to the used)
- Display parameters:
    - _Minimum zoom_: the minimum [zoom level](https://wiki.openstreetmap.org/wiki/Zoom_levels), under which the layer will not be displayed
    - _Maximum zoom_: the maximum [zoom level](https://wiki.openstreetmap.org/wiki/Zoom_levels) ,above which the layer will not be displayed
    - _Active_ in Farmer App_: whether this layer will be displayed in the layer selection menu in the Farmer (mobile) App. 
    - _Active_ in Admin Portal: whether this layer will be displayed in the layer selection menu in the Administration Portal.
    - _Display by default_:
        - Base layers: whether this layer will be the default base layer when a map is started
        - Overlays: whether this layer will be displayed by default when a map is started
- Source parameters:
    - _Source type_: the type of layer served by your provider, either [TMS](https://en.wikipedia.org/wiki/Tile_Map_Service) or [WMS](https://en.wikipedia.org/wiki/Web_Map_Service)
    - _URL_: the distant URL to the service:
        - The URLs for TMS services should follow a format similar to `https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png` (example OpenStreetMap), i.e. with templating placeholders for the `z`, `x` and `y` parameters (the `s` parameter is optional).
        - The URL for a WMS service should point to the root of the WMS server.
    - _Source maximum native zoom_: the maximum [zoom level](https://wiki.openstreetmap.org/wiki/Zoom_levels) served by the layer source. If this level is less than the _Maximum zoom_ level defined above, then tiles between _(Source maximum native zoom + 1)_ and _Maximum zoom_ will be extrapolated in the frontend.
    - _Source attribution_: an attribution string that will be displayed at the bottom of the map if this layer is active. Try to keep the attribution short for cleaner display.
- WMS-specific parameters:
    - _Source layer(s)_: the layer ID(s) that should be queried. If multiple layers IDs, separate them with a comma.
    - _Styles_: the style(s) to use for the layer. If multiple styles, separate them with a comma.
    - _Source format_: the image format returned by the WMS server
    - _Server version_: the version of the WMS server
    - _Is transparent_: whether the WMS server supports transparency (alpha channel)
    - _Projection authority name_: the projection of the source WMS layer, in EPSG:XXXX format.
    - _Projection OGC WKT_: the projection of the source WMS layer, in OGC WKT format.

## How do I...?
#### How do I add a base layer / overlay to the maps?

- Open the [Map Base Layers list](/admin/configuration/mapbaselayer) or the [Map Overlays list](/admin/configuration/mapoverlay)
- Click on <kbd>Add Map Base Layer</kbd> or <kbd>Add Map Overlay</kbd>
- Fill in the fields that describe the layer and the connection to your distant layer provider (see above)
- Click <kbd>Save</kbd>

> The process of making a new layer available (and setting up the proxying mechanism described above) usually takes at most a few seconds. Users will still need to refresh their web page (or restart the app) as the layers are not queried continuously by the interfaces.
#### How do I remove a base layer / overlay from the maps?

- Open the [Map Base Layers list](/admin/configuration/mapbaselayer) or the [Map Overlays list](/admin/configuration/mapoverlay)
- Click on the base layer / overlay that you wish to remove
- Click <kbd>Delete</kbd>

#### How do I edit a base layer /overlay?

- Open the [Map Base Layers list](/admin/configuration/mapbaselayer) or the [Map Overlays list](/admin/configuration/mapoverlay)
- Click on the base layer / overlay that you wish to edit
- Edit the fields you wish to modify
- Click <kbd>Save</kbd>

> If you edit the source URL of the distant service (or the WMS layer), this will not be immediately visible by the user: this is due to the fact that the layer tiles are cached according to the identifier of the layer; if the identifier does not change, then the cache is not invalidated and the served tiles remain the same until they expire (between 24h and 7 days). If you want a layer to be immediately available, then it is better to create a new layer with a distinct identifier.

### Other map settings

Some other settings are available to customize the maps and are available in the [Map configuration object](/admin/configuration/configuration/CONFIG/). You can edit those and changes will be reflected in the maps.

_Example: changing a layer attribution_
<img src="/services/web/backend/docs/static/docs/img/maps/layer_attribution.png" title="Layer attribution" class="img-docs" height="200" />

