from django.utils.translation import gettext_lazy as _
from user_sessions.models import Session
from user_sessions.admin import SessionAdmin

from fastplatform.site import admin_site


class UserSession(Session):
    class Meta:
        proxy = True
        verbose_name = _('User session')
        verbose_name_plural = _('User sessions')
        help_text = _('Authentication sessions of users logged in using web access')
        app_label = "authentication"

admin_site.register(UserSession, SessionAdmin)