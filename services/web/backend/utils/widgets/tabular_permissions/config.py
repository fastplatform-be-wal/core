from django.utils.translation import ugettext as _
from django.conf import settings


EXCLUDE_APPS = ["admin_interface", "oidc_provider", "user_sessions", "axes"]

EXCLUDE_MODELS = ["contenttype"]
TEMPLATE = "widgets/tabular_permissions/tabular_permissions.html"
USE_FOR_CONCRETE = False


def should_exclude_model(model):
    """
    Return whether a model's permissions should be excluded from the permission
    selection widget
    """

    # pylint: disable=import-outside-toplevel
    from authentication.models import Country, Region, Language
    from external.models import (
        AgriPlot,
        AgriPlotVersion,
        ProtectedSite,
        ProtectedSiteVersion,
        Designation,
        DesignationScheme,
        DesignationType,
        ManagementRestrictionOrRegulationZone,
        ManagementRestrictionOrRegulationZoneVersion,
        ZoneTypeCode,
        SpecialisedZoneTypeCode,
        EnvironmentalDomain,
        SoilSite as ExternalSoilSite,  # avoid collision with non-external model
        SoilInvestigationPurpose as ExternalSoilInvestigationPurpose,  # avoid collision with non-external model
        SoilSiteVersion as ExternalSoilSiteVersion,  # avoid collision with non-external model
        Observation as ExternalObservation,  # avoid collision with non-external model
        ObservableProperty as ExternalObservableProperty,  # avoid collision with non-external model
        PhenomenonType as ExternalPhenomenonType,  # avoid collision with non-external model
        WaterCourse,
        WaterCourseVersion,
        SurfaceWater,
        SurfaceWaterVersion,
    )
    from fieldbook.models import (
        RegisteredFertilizer,
        RegisteredFertilizerElement,
        ChemicalElement,
        CustomFertilizer,
        CustomFertilizerElement,
        FertilizerType,
    )
    from messaging.models import (
        Broadcast,
        BroadcastUserReadStatus,
        Ticket,
        TicketMessage,
        TicketUserReadStatus,
    )
    from photos.models import (
        GeoTaggedPhoto
    )
    from soil.models import (
        SoilSite,
        SoilDerivedObject,
        SoilInvestigationPurpose,
        SoilOrigin,
        Observation,
        DerivedObservation,
        ObservableProperty,
    )

    INCLUDE_WHEN = {
        Country: settings.ENABLE_MULTIPLE_REGIONS,
        Region: settings.ENABLE_MULTIPLE_REGIONS,
        Language: settings.ENABLE_USER_PREFERRED_LANGUAGES,
        AgriPlot: settings.ENABLE_EXTERNAL_AGRI_PLOT,
        AgriPlotVersion: settings.ENABLE_EXTERNAL_AGRI_PLOT,
        ProtectedSite: settings.ENABLE_EXTERNAL_PROTECTED_SITE,
        ProtectedSiteVersion: settings.ENABLE_EXTERNAL_PROTECTED_SITE,
        Designation: settings.ENABLE_EXTERNAL_PROTECTED_SITE,
        DesignationScheme: settings.ENABLE_EXTERNAL_PROTECTED_SITE,
        DesignationType: settings.ENABLE_EXTERNAL_PROTECTED_SITE,
        ManagementRestrictionOrRegulationZone: settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE,
        ManagementRestrictionOrRegulationZoneVersion: settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE,
        ZoneTypeCode: settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE,
        SpecialisedZoneTypeCode: settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE,
        EnvironmentalDomain: settings.ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE,
        ExternalSoilSite: settings.ENABLE_EXTERNAL_SOIL_SITE,
        ExternalSoilInvestigationPurpose: settings.ENABLE_EXTERNAL_SOIL_SITE,
        ExternalSoilSiteVersion: settings.ENABLE_EXTERNAL_SOIL_SITE,
        ExternalObservation: settings.ENABLE_EXTERNAL_SOIL_SITE,
        ExternalObservableProperty: settings.ENABLE_EXTERNAL_SOIL_SITE,
        ExternalPhenomenonType: settings.ENABLE_EXTERNAL_SOIL_SITE,
        WaterCourse: settings.ENABLE_EXTERNAL_WATER_COURSE,
        WaterCourseVersion: settings.ENABLE_EXTERNAL_WATER_COURSE,
        SurfaceWater: settings.ENABLE_EXTERNAL_SURFACE_WATER,
        SurfaceWaterVersion: settings.ENABLE_EXTERNAL_SURFACE_WATER,
        RegisteredFertilizer: settings.ENABLE_REGISTERED_FERTILIZER,
        RegisteredFertilizerElement: settings.ENABLE_REGISTERED_FERTILIZER,
        ChemicalElement: settings.ENABLE_CUSTOM_FERTILIZER
        or settings.ENABLE_REGISTERED_FERTILIZER,
        CustomFertilizer: settings.ENABLE_CUSTOM_FERTILIZER,
        CustomFertilizerElement: settings.ENABLE_CUSTOM_FERTILIZER,
        FertilizerType: settings.ENABLE_CUSTOM_FERTILIZER
        or settings.ENABLE_REGISTERED_FERTILIZER,
        Broadcast: settings.ENABLE_BROADCAST,
        BroadcastUserReadStatus: settings.ENABLE_BROADCAST,
        Ticket: settings.ENABLE_TICKET,
        TicketMessage: settings.ENABLE_TICKET,
        TicketUserReadStatus: settings.ENABLE_TICKET,
        GeoTaggedPhoto: settings.ENABLE_GEOTAGGED_PHOTO,
        SoilSite: settings.ENABLE_SOIL_SITE,
        SoilDerivedObject: settings.ENABLE_SOIL_SITE,
        SoilInvestigationPurpose: settings.ENABLE_SOIL_SITE,
        SoilOrigin: settings.ENABLE_SOIL_SITE,
        Observation: settings.ENABLE_SOIL_SITE,
        DerivedObservation: settings.ENABLE_SOIL_SITE,
        ObservableProperty: settings.ENABLE_SOIL_SITE,
    }

    should_include = INCLUDE_WHEN.get(model, True)

    return not should_include


EXCLUDE_FUNCTION = should_exclude_model


def custom_permissions_translator(codename, verbose_name, content_type_id):
    """
    A Hook for multi-lingual applications to translate custom permissions
    By default it will try to translated the permission verbose_name
    :param codename: permission codename
    :param verbose_name: permission verbose_name specified on model meta
    :param content_type_id: relevant permission content_type
    :return: a translated verbose_name
    """
    return _(verbose_name)


TRANSLATION_FUNC = custom_permissions_translator
