# `core/mobile/push_notification`: Mobile push notifications

This is the push notification server for the FaST Farmer mobile application. 
The [core/mobile/push_notification](./) service is a [NodeJS](https://nodejs.org/en/) exposes a GraphQL schema destined to be mounted as a remote schema in the [core/api_gateway](../../api_gateway/).

It serves the following goals:
* register a new device token for a user: *APNs tokens* for the Apple Push Notification service or *FCM tokens* for the Firebase Cloud Messaging
* send push notifications via APNs and FCM to the specified users
* unregister a device token for a user

## Architecture

This push notification server is a [NodeJS](https://nodejs.org/en/) GraphQL server written in [TypeScript](https://www.typescriptlang.org/).

To interface with the **Apple Push Notification service** and the **Firebase Cloud Messaging** the server relies on the [node-pushnotifications](https://github.com/appfeel/node-pushnotifications) library.

> This is the only service in the FaST platform written in TypeScript because it is one the oldest services written before finalizing the platform architecture.
For legacy reasons, it also has a REST API but we will ignore it in this documentation.

As this service is destined to be mounted as a remote schema in the Hasura API Gateway, it doesn't handle the authentication.

```plantuml
component "API Gateway" as api_gateway
component "Push notifications service (API gateway remote schema)" as push_notifications
component "Apple Push Notifications service" as apns
component "Firebase Cloud Messaging" as fcm
actor "User" as user
storage "Redis database" as redis

api_gateway --> push_notifications: Authorize and forward GraphQL request
push_notifications --> redis: Store device tokens
push_notifications --> apns: Send notification
push_notifications --> fcm: Send notification
apns --> user : iOS push notification
fcm --> user : Android push notification
```

### Registration workflow

1. A user logs in into the mobile application (iOS or Android): its device id/token is registered and stored on the database (if the same device id is already registered for another user, the existing entry is first deleted).
2. Notifications are triggered for the user by the [core/messaging](../../messaging/) event handler: push notifications are sent to all of the user devices.
3. The user logs out: its device token is unregistered and removed from the database.
*OR*
4. The user uninstall the application: the notification will bounce back and the Push notication service providers will unregister the device from the FaST notifications (the device will however still be in the database).

> Note: it is possible that the device id expires on the side of the Push notification provider. In that case when we try to notify the user, it might return an error. Ideally we should then remove the device id from our database but we haven't implemented this feature because the errors seem to be inconsistent between the different services.

### Database

The push notification server stores its fata on a *key-value* [Redis](https://redis.io/) database.

A user device token is stored as a composite key: `{user_id}|fastplatform|{device_id}`.
The value for these entries is the date of the registration but it has no importance as it is not used by the service.

This simple structure allow to scan for all matching keys using the Redis `scan` command with a `MATCH` operator.
In this way we can easily retrieve the devices tokens by user id.

> There is an expiration date for the entries set to 1 year by default (the entries will be removed automatically).

## API

### Endpoints

#### Base URL
The GraphQL API base URL is `/fastplatform/api/<api-version>`, where `<api-version>` is 
either a value defined in the env variable `API_VERSION` or the version defined is the `package.json` file. 

If there is no version set for `API_VERSION` or in the `package.json`, it will default to `latest`.


#### GraphQL API
You can find the full schema definition in `src/graphql/schema.graphql`

There are 3 endpoints that comes with the GraphQL server:
- `<BASE_URL>/graphql`: HTTP endpoint
- `<BASE_URL>/subscriptions`: Subscriptions (websocket) endpoint
- `<BASE_URL>/playground`: A GraphQL interface that lets you play with the GraphQL Api

Extract of the `schema.graphql`:
```graphql
# ...
type Query {
  user_registered_devices(user_id: String!): push_notification__user_registered_devices_response
}

type Mutation {
  register_user_device(user_id: String!, device_token: String!): push_notification__standard_response
  send_notification(user_ids: [String!]!, message_payload: push_notification__notification_message_payload!): push_notification__notification_sent_result
  unregister_user_all_devices(user_id: String!): push_notification__standard_response
  unregister_user_device(user_id: String!, device_token: String!): push_notification__standard_response
}
```

### Environment variables
All the environment variables used for this project can be found in the `src/config/env.ts` file.

| Variable | Default Value | Description |
|-|-|-|
| NODE_ENV | production | Node js environment |
| API_VERSION |  | Version of the API. If not set, the api version used is the one set in the `package.json` file or default to 'latest'. |
| SERVER_PORT | 5000 | Port of the server. |
| IS_GRAPHQL_SERVER_ELSE_REST | true | Define which API to start: true => GraphQL, false => REST |
| APNS_ENCRYPTION_KEY_PATH |  | Path to the file (`.p8`) containing the encryption key required to communicate with APNs (Apple Push Notification service) |
| APNS_ENCRYPTION_KEY_VALUE |  | Value of the APNs encryption key |
| FCM_API_KEY_PATH |  | Path to the file (`.txt`) containing the Api key required to communicate with FCM (Firebase-Cloud Messaging) |
| FCM_API_KEY_VALUE |  | Value of the FCM Api key |
| APNS_KEY_ID |  | APNs key id coming along with the encryption key |
| APNS_TEAM_ID |  | FaST Project team id. |
| APP_BUNDLE_ID_IOS | eu.fastplatform.mobile.farmer-app | FaST app bundle id used as topic for the notifications |
| REDIS_CONNECT_MAX_ATTEMPT | 10 | Max number of retry to connect to redis |
| REDIS_HOST | 0.0.0.0 | Redis host |
| REDIS_PORT | 6379 | Redis port |
| REDIS_AUTH_PASSWORD |  | Redis auth password
| REDIS_KEY_EXPIRE_USER_DEVICE | 31536000 | Expiration time in seconds for user device keys |
| HASURA_ADMIN_ROLES | 'admin,service' | Hasura roles that have "admin" role, meaning allowed to request every endpoint. Must be a comma separated string. |

## Development

## Getting started

First, you need to install the required packages:
```
npm install
```

Start the Redis server on port `6379` (it will mount the `_data/` directory):
```
make start-redis
```
Stop the Redis server:
```
make stop-redis
```

Once you have started the redis server, you can start the push notification server in *dev mode* by running:
```sh
make start-graphql
```
>**Note:** This requires you to have a `secrets.env` file a the root of the project that contains the definition of the 
required environment variables that are *secret* (password, api key, etc).



