# Generated by Django 3.0.7 on 2020-11-18 10:27

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("messaging", "0009_ticket_status"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="ticketmessage",
            options={
                "ordering": ("ticket", "created_at"),
                "verbose_name": "ticket message",
                "verbose_name_plural": "ticket messages",
            },
        ),
        migrations.AlterField(
            model_name="ticket",
            name="created_by",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="tickets_created",
                to=settings.AUTH_USER_MODEL,
                verbose_name="created by",
            ),
        ),
        migrations.AlterUniqueTogether(
            name="ticketmessage",
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name="ticketmessage",
            name="order",
        ),
    ]
