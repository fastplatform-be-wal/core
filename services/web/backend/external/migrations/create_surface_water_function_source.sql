CREATE OR REPLACE FUNCTION public.surface_water_function_source(
        z integer,
        x integer,
        y integer
    ) RETURNS bytea AS $$
DECLARE mvt bytea;
BEGIN
SET enable_seqscan TO off;
SELECT INTO mvt ST_AsMVT(tile, 'surface_water', 4096, 'geom')
FROM (
        SELECT ST_AsMVTGeom(
                ST_Transform(
                    ST_SimplifyPreserveTopology(s.geometry, 1 /(2 ^ z)),
                    3857
                ),
                TileBBox(z, x, y, 3857),
                4096,
                64,
                true
            ) AS geom,
            s.id,
            s.authority_id,
            s.geographical_name,
            s.local_type,
            s.buffer
        FROM surface_water s
            INNER JOIN surface_water_version sv ON s.version_id = sv.id
        WHERE sv.is_active
            AND ST_Intersects(
                s.geometry,
                ST_Transform(TileBBox(z, x, y, 3857), 4258)
            )
    ) as tile
WHERE z >= 5
    AND geom IS NOT NULL;
RETURN mvt;
SET enable_seqscan TO on;
END $$ LANGUAGE plpgsql VOLATILE STRICT PARALLEL SAFE;