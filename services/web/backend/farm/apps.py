from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class FarmConfig(AppConfig):
    name = "farm"
    verbose_name = _("Farm")

    help_text = _("Definitions of user farms and their geometries")
