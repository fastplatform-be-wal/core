from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django import forms
from django.core.exceptions import ValidationError
from django.conf import settings

from import_export.admin import ImportExportMixin

from fastplatform.site import admin_site

from soil.resources import (
    PhenomenonTypeResource,
    SoilInvestigationPurposeResource,
    ObservablePropertyResource,
)
from soil.models import (
    SoilSite,
    Observation,
    PhenomenonType,
    SoilInvestigationPurpose,
    ObservableProperty,
)

from farm.models import HoldingCampaign

from utils.admin import CommonModelAdmin

from common.resources import UnitOfMeasureResource


class SoilInvestigationPurposeAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = SoilInvestigationPurposeResource
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class PhenomenonTypeAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = PhenomenonTypeResource
    autocomplete_fields = ("parent",)
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class UnitOfMeasureAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = UnitOfMeasureResource
    list_display = (
        "id",
        "name",
        "symbol",
    )
    search_fields = (
        "id",
        "name",
    )

    def get_fieldsets(self, request, obj=None):
        fieldsets = [[None, {"fields": ("id", "name", "symbol")}]]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class ObservablePropertyAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = ObservablePropertyResource
    autocomplete_fields = ("base_phenomenon",)
    list_display = ("id", "label", "base_phenomenon", "uom")
    search_fields = ("id", "label")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [
                None,
                {
                    "fields": (
                        "id",
                        "label",
                        "base_phenomenon",
                        "uom",
                        "validation_rules",
                    )
                },
            ]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


class ObservationAdmin(CommonModelAdmin):
    hide_from_dashboard = True
    raw_id_fields = ("soil_site",)
    autocomplete_fields = (
        "soil_site",
        "observed_property",
    )
    model = Observation
    exclude = ["source"]


class ObservationInlineForm(forms.ModelForm):
    """
    Override the default form for the Observation inline
    to deal with the result JSONField
    """

    class Meta:
        model = Observation
        fields = ("id", "observed_property", "phenomenon_time", "result")
        widgets = {
            "result": forms.Textarea(attrs={"rows": 1, "cols": 60}),
        }


class ObservationInline(admin.TabularInline):
    hide_from_dashboard = True
    model = Observation
    form = ObservationInlineForm

    extra = 0

    verbose_name = _("property")
    verbose_name_plural = _("properties")

    def get_queryset(self, request):
        return super().get_queryset(request).select_related("observed_property")


class SoilSiteAdmin(CommonModelAdmin):
    hide_from_dashboard = True
    model = SoilSite
    # filter_horizontal = (
    #     'soil_site_observation',
    # )
    inlines = [ObservationInline]
    exclude = ["source"]
    list_display = (
        "_link",
        "label",
        "authority_id",
        "soil_investigation_purpose",
    )
    search_fields = ("_link", "label", "authority_id")
    raw_id_fields = ("holding",)
    list_display_links = ("_link",)
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "holding",
                    "label",
                    "authority_id",
                    "soil_investigation_purpose",
                )
            },
        ),
        (_("Validity"), {"fields": (("valid_from", "valid_to"),)}),
        (
            _("Lifespan"),
            {"fields": (("begin_lifespan_version", "end_lifespan_version"),)},
        ),
        (None, {"fields": ("geometry",)}),
    )

    def get_form(self, request, obj=None, **kwargs):
        form = super(SoilSiteAdmin, self).get_form(request, obj, **kwargs)
        field = form.base_fields["soil_investigation_purpose"]
        field.widget.can_add_related = False
        field.widget.can_change_related = False
        field.widget.can_delete_related = False
        return form

    # Map parameters
    # --------------
    geometry_fields = ["geometry"]

    def _link(self, obj):
        if obj:
            return obj.get_admin_link()
        return ""

    def get_holding_campaign_id(self, obj):
        if obj:
            holding_campaign_id = (
                HoldingCampaign.objects.filter(
                    holding=obj.holding, campaign__is_current=True
                )
                .values_list("id", flat=True)
                .first()
            )
            if holding_campaign_id:
                return holding_campaign_id
        return None

    _link.short_description = _("link")


admin_site.register(SoilInvestigationPurpose, SoilInvestigationPurposeAdmin)
admin_site.register(PhenomenonType, PhenomenonTypeAdmin)
admin_site.register(ObservableProperty, ObservablePropertyAdmin)
admin_site.register(Observation, ObservationAdmin)
admin_site.register(SoilSite, SoilSiteAdmin)
