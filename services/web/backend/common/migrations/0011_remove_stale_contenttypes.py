from django.db import migrations


def remove_stale_contenttypes(apps, schema_editor):
    ContentType = apps.get_model("contenttypes.ContentType")
    ContentType.objects.filter(
        model="logentryproxy",
        app_label="admin",
    ).delete()


class Migration(migrations.Migration):

    dependencies = [
        ("common", "0010_auto_20201121_2201"),
    ]

    operations = [
        migrations.RunPython(remove_stale_contenttypes),
    ]
