from django.utils.translation import gettext_lazy as _
from django.conf import settings

from import_export.admin import ImportMixin, ExportMixin

from fastplatform.site import admin_site
from authentication.models import FederatedProvider, FederatedProviderLogEntry
from authentication.resources import (
    FederatedProviderResource,
    FederatedProviderLogEntryResource,
)
from utils.admin import CommonModelAdmin


class FederatedProviderAdmin(ImportMixin, ExportMixin, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = FederatedProviderResource
    short_description = _(
        "The federated authentication providers are external services to which FaST might delegate user authentication, such as services provided by a Paying Agency or a national eID node."
    )
    list_display = (
        "id",
        "name",
        "description",
        "logo",
        "url",
        "background_color",
        "is_active",
    )
    hide_from_dashboard = False

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [
                None,
                {
                    "fields": (
                        "id",
                        "name",
                        "description",
                        "logo",
                        "url",
                        "background_color",
                        "text_color",
                        "is_active",
                    )
                },
            ]
        ]

        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


admin_site.register(FederatedProvider, FederatedProviderAdmin)


class FederatedProviderLogEntryAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = FederatedProviderLogEntryResource
    list_display = (
        "entry_time",
        "federated_provider",
        "entry_type",
        "username",
        "ip_address",
        "user_agent",
        "path",
    )
    list_display_id = ("entry_time",)
    hide_from_dashboard = False


admin_site.register(FederatedProviderLogEntry, FederatedProviderLogEntryAdmin)
