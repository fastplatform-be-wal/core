# Generated by Django 3.0.7 on 2020-07-27 16:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('farm', '0012_auto_20200727_1324'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plot',
            name='groups',
        ),
        migrations.AddField(
            model_name='plotgroup',
            name='plots',
            field=models.ManyToManyField(blank=True, db_table='plot_plot_group', related_name='plot_groups', to='farm.Plot', verbose_name='plots'),
        ),
    ]
