# Observations
- [Observations](#observations)
  - [Private vs Public Observations](#private-vs-public-observations)
  - [Structure](#structure)
  - [Validation rules for observable property](#validation-rules-for-observable-property)
    - [Numeric](#numeric)
    - [Text](#text)
    - [Boolean](#boolean)
    - [Category](#category)
  - [How do I...?](#how-do-i)
    - [Create a new Observable Property](#create-a-new-observable-property)
    - [Why is it sometimes not possible to delete Observable Property ?](#why-is-it-sometimes-not-possible-to-delete-observable-property-)

Observations are scientific measurements or determinations carried out at a specific location and time. They are not directly associated to any given plot, but are ultimately used through various mechanisms of intersection and distance computations to enrich the plots data. 

They can be used to compute Nitrogen Limitations, or serve as default inputs for the fertilization algorithms. They can be directly viewed by the user from within the app, associated to a given plot.

## Private vs Public Observations

Observations can be of 2 types:
- [<g>Private Observations</g>](/admin/soil/observation/): They represent data gathered by the farmer, and are associated by their geographical location to a given plot.
- [<g>Public Observations</g>](/admin/external/observation/): They represent data gathered during certain large soil surveys carried out by a governmental agency for example. They are notably used to interpolate soil sample estimates for a given plot, when no specific private soil sample has been provided by the farmer (private observations).

**<x>Private Observations</x>** always supersede **<x>Public Observations</x>**, that are only there to provide some approximations in the absence of private data (deemed always more accurate).

```plantuml
skinparam defaultFontSize 12

title Private vs Public Observations Flow

(*) --> "Does any private observation of the farmer\n- intersect the geometry of the plot\n- was gathered before the end of the current campaign" as private

private --> [yes] "Return data" as return_success #lightgreen
private --> [no] "Does any public observation\n- is close enough to the geometry of the plot\n- was gathered before the end of the current campaign" as public
public --> [yes] return_success
public --> [no] "Return no observation" as return_failure #red

return_success --> (*)
return_failure --> (*)
```

## Structure

Private and Public observations follow the same exact structure.

A **<x>Soil Site<x>** is a collection of one or several **<x>Observations<x>** of different kinds. Amongst other things, a **<x>Soil Site<x>** thus has a validity period, as well as a precise geographical location. 

Each **<x>Observations<x>** is of specific type, ie. what kind of thing is measured (for example, `pH` of the soil, `slope` of the plot, or the `soil depth`, etc…), and has a specific result (= value) attributed to it. 

Finally, each **<x>Observation<x>** has a **<x>Phenomenon Type<x>**, which represents an more abstract categorization of the type of thing measured (ie. is the property physical, chemical, biological in nature ? etc…).

This can be summarized as follows:
```plantuml
skinparam defaultFontSize 12

title Structure of Observations

entity SoilSite
entity Observation
entity ObservableProperty
entity PhenomenonType

SoilSite ||--o{ Observation
Observation ||--|| ObservableProperty
ObservableProperty ||--|| PhenomenonType
```

## Validation rules for observable property

Since the result measured for a given observation can either come from public data or from **direct user input**, it is important to maintain some kind of validation rules, so as to make sure that the data can be used by various algorithms without any problem.

> If no validation rule is defined, the result is always considered valid as long as it is defined.

When it comes to validating the result of an **<x>Observation<x>**, the same exact following logic is applied, irrespective of whether the observation is **<x>Private<x>** (user input) or a **<x>Public<x>** (public data) in nature.

Validation rules are expressed in the form of a **JSON object**.

Here is a straightforward example, typical for an observation whose unit is expressed as a percentage that cannot be negative or greater than 100%:

```json
{
    "min": 0,
    "max": 100,
    "type": "numeric"
}
```

Validation rules can be of 4 different types: **<g>numerical<g>**, **<g>textual<g>**, **<g>boolean<g>**, or a **<g>category<g>**.

### Numeric
The result must be a number.

- If a `min` key is found inside the JSON object, the result must be greater than or equal to the value of this key,
- If a `max` key is found inside the JSON object, the result must be lower than or equal to the value of this key,
- If no `min` or `max` key are found, the result is considered valid as long as it is a number.

In the example below, we only check whether the result is greater than zero:
```json
{
    "min": 0,
    "type": "numeric"
}
```

### Text
The result must be a text.

```json
{
    "type": "text"
}
```

### Boolean
The result must be either true or false.

```json
{
    "type": "boolean"
}
```

### Category
The result must be a specific text chosen amongst the available _choices_.

Here is an example of a validation rule for soil density:

```json
{
    "choices": ["light", "medium", "heavy"],
    "type": "category"
}
```

## How do I...?

### Create a new Observable Property

When creating a [<g>Private Observation</g>](/admin/soil/observation/) or a [<g>Public Observation</g>](/admin/external/observation/), the same exact logic applies:

- Click on <kbd>Add Observable Property</kbd> on the top right of the page
- Fill in the identifier (must be unique) 
- Fill in the label of the property (how it will be displayed to the user)
- Specify a Base Phenomenon that categorizes the kind of measurement
- Specify a unit of measure (like percentage, ppm, kg/ha, etc…)
- Specify a Validation Rule (optional)
- Click <kbd>Save</kbd>

### Why is it sometimes not possible to delete Observable Property ?

Observable Properties can only be deleted if there are no **<x>Observations<x>** associated to it.

If you absolutely need to delete a specific observable property, you can do so by first deleting all the **<x>Observations<x>** associated to it, and then deleting the Observable Property itself.