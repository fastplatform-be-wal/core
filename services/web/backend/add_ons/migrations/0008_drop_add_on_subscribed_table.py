from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("add_ons", "0007_set_db_comments_add_ons"),
    ]

    operations = [
        migrations.RemoveField(
            model_name='addon',
            name='subscriptions',
        ),
        migrations.DeleteModel(
            name='AddOnSubscription',
        ),
    ]
