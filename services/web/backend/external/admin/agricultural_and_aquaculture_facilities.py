from django.utils.translation import gettext_lazy as _
from django.conf import settings

from django_object_actions import DjangoObjectActions

from fastplatform.site import admin_site

from external.models import (
    AgriPlot,
    AgriPlotVersion,
)
from external.resources import (
    AgriPlotVersionResource,
    AgriPlotResource,
)

from external.admin.version import VersionAdmin, VersionActionsMixin
from utils.admin import CommonModelAdmin


class AgriPlotVersionAdmin(VersionAdmin):
    hide_from_dashboard = True
    model = AgriPlotVersion
    resource_class = AgriPlotVersionResource


class AgriPlotAdmin(VersionActionsMixin, DjangoObjectActions, CommonModelAdmin):
    hide_from_dashboard = False
    model = AgriPlot
    version_model = AgriPlotVersion
    resource_class = AgriPlotResource

    list_display = (
        "id",
        "authority_id",
        "version",
    )
    list_filter = ("version__is_active",)

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "authority_id",
                    "version",
                    "area",
                )
            },
        ),
        (_("Validity"), {"fields": (("valid_from", "valid_to"),)}),
        (
            _("Geographic information"),
            {"fields": ("geometry",)},
        ),
    )

    # Map parameters
    # --------------
    geometry_fields = ["geometry"]


if settings.ENABLE_EXTERNAL_AGRI_PLOT:
    admin_site.register(AgriPlot, AgriPlotAdmin)
    admin_site.register(AgriPlotVersion, AgriPlotVersionAdmin)
