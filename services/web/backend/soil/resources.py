from common.resources import ModelResourceWithColumnIds

from soil.models import ObservableProperty, SoilInvestigationPurpose, PhenomenonType, SoilSite


class SoilInvestigationPurposeResource(ModelResourceWithColumnIds):
    class Meta:
        model = SoilInvestigationPurpose


class PhenomenonTypeResource(ModelResourceWithColumnIds):
    class Meta:
        model = PhenomenonType


class ObservablePropertyResource(ModelResourceWithColumnIds):
    class Meta:
        model = ObservableProperty


class SoilSiteResource(ModelResourceWithColumnIds):
    class Meta:
        model = SoilSite
