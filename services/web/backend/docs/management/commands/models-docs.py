import warnings
from pathlib import Path

from django.apps import apps
from django.conf import settings
from django.db import models, connections, router, OperationalError, DEFAULT_DB_ALIAS
from django.core.management.base import BaseCommand, CommandError
from django.db.backends.postgresql.base import DatabaseWrapper
from django.contrib.postgres.fields import JSONField


class Command(BaseCommand):
    help = "Output the documentation for the models in Markdown in the /docs folder"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.verbosity = 1

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **kwargs):

        postgres_datatypes = DatabaseWrapper.data_types

        title = ["# FaST object model", ""]

        toc = ["## Table of contents", ""]

        details = ["## Model details", ""]

        for app in apps.get_app_configs():

            app_models = list(app.get_models())
            app_models = [m for m in app_models if not m._meta.proxy]
            app_models = sorted(app_models, key=lambda m: m.__name__)
            if not app_models:
                continue
            
            app_anchor = app.verbose_name.lower().replace(' ', '-').replace('&', '')
            toc += [f"- [{app.verbose_name}](#{app_anchor})"]
            for model in app_models:
                model_anchor = model.__name__.lower()
                toc += [f"  - [{model.__name__}](#{model_anchor})"]

            details += [f"### {app.verbose_name}", ""]
            for model in app_models:
                details += [
                    "",
                    f"#### `{model.__name__}`",
                    f"{model._meta.help_text}"
                    if hasattr(model._meta, "help_text")
                    else "",
                    "",
                    f"##### Metadata",
                    f"- Database table: `{model._meta.db_table}`",
                    f"- Verbose name: {model._meta.verbose_name}",
                    "",
                    f"##### Fields",
                    f"| PK | Unique | Column | Type | Default | Comment | Nullable |",
                    f"| --- | --- | --- | --- | --- | --- | --- |",
                ]

                fields = model._meta.get_fields(
                    include_parents=False, include_hidden=True
                )
                fields = [
                    field
                    for field in fields
                    if not isinstance(
                        field,
                        (
                            models.ManyToOneRel,
                            models.ManyToManyField,
                            models.ManyToManyRel,
                        ),
                    )
                ]

                for field in fields:

                    if hasattr(field, "primary_key") and field.primary_key is not None:
                        pk = "Yes" if field.primary_key else ""
                    else:
                        pk = ""
                    if hasattr(field, "unique") and field.unique is not None:
                        unique = "Yes" if field.unique else ""
                    else:
                        unique = ""
                    if hasattr(field, "column") and field.column is not None:
                        column = field.column
                    else:
                        column = "-"

                    if isinstance(field, JSONField):
                        postgres_type = "jsonb"
                    else:
                        if isinstance(field, models.ForeignKey):
                            real_field = field.target_field
                        else:
                            real_field = field

                        django_type = real_field.__class__.__name__
                        if django_type in postgres_datatypes:
                            postgres_type = postgres_datatypes.get(django_type)
                        else:
                            postgres_type = "-"
                            for base_type in real_field.__class__.__mro__:
                                if base_type.__name__ in postgres_datatypes:
                                    postgres_type = postgres_datatypes.get(
                                        base_type.__name__
                                    )
                                    break

                    if hasattr(real_field, "max_length"):
                        postgres_type = postgres_type % {
                            "max_length": real_field.max_length
                        }

                    if (
                        hasattr(field, "default")
                        and field.default is not None
                        and field.default != models.fields.NOT_PROVIDED
                    ):
                        if hasattr(field.default, "__call__"):
                            default = f"`{field.default.__name__}()`"
                        else:
                            if isinstance(field.default, str):
                                default = f'`"{field.default}"`'
                            else:
                                default = f"`{field.default}`"
                    else:
                        default = ""

                    if (
                        hasattr(field, "help_text")
                        and field.help_text is not None
                        and field.help_text != models.fields.NOT_PROVIDED
                    ):
                        help_text = field.help_text
                    else:
                        help_text = ""
                    if (
                        hasattr(field, "choices")
                        and field.choices is not None
                        and field.choices != models.fields.NOT_PROVIDED
                    ):
                        if help_text and help_text[-1] != '.':
                            help_text += '. '
                        elif help_text and help_text[-1] == '.':
                            help_text += ' '
                        help_text += "_Values: "
                        help_text += ", ".join([f"`{v[0]}`" for v in field.choices]) + '_'

                    if (
                        hasattr(field, "null")
                        and field.null is not None
                        and field.null != models.fields.NOT_PROVIDED
                    ):
                        null = "Yes" if field.null else "No"
                    else:
                        null = "-"

                    details += [
                        f"| {pk} | {unique} | `{column}` | `{postgres_type}` | {default} | {help_text} | {null} |"
                    ]

            details += [""]

        file_path = Path(settings.BASE_DIR) / "docs/en/technical/models.md"
        markdown = "\n".join(title + details)
        file_path.write_text(markdown)
