from django.utils.translation import gettext_lazy as _

from axes.models import AccessAttempt, AccessLog
from axes.admin import AccessAttemptAdmin, AccessLogAdmin

from fastplatform.site import admin_site


class LoginAccessAttempt(AccessAttempt):
    class Meta:
        proxy = True
        verbose_name = _("Login attempt")
        verbose_name_plural = _("Login attempts")
        help_text = _("Login attempts on the FaST identity provider for all users")
        app_label = "authentication"


class LoginAccessLog(AccessLog):
    class Meta:
        proxy = True
        verbose_name = _("Login access log")
        verbose_name_plural = _("Login access logs")
        help_text = _("Login access logs of the FaST identity provider")
        app_label = "authentication"


admin_site.register(LoginAccessAttempt, AccessAttemptAdmin)
admin_site.register(LoginAccessLog, AccessLogAdmin)
