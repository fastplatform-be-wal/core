from .debug import DEBUG, DEBUG_TOOLBAR

INSTALLED_APPS = [
    # Admin customizations
    "common.apps.CommonConfig",
    "admin_interface",
    "colorfield",
    "import_export",
    "django_json_widget",
    "django_object_actions",
    "django.contrib.admin",
    # Django customizations
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "user_sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.gis",
    "ordered_model",
    "corsheaders",
    "storages",
    "django_cleanup.apps.CleanupConfig",
    # Authentication
    "oidc_provider",
    # Brute force protection
    "axes",
    # FaST apps
    "add_ons.apps.AddOnsConfig",
    "authentication.apps.AuthenticationConfig",
    "configuration.apps.ConfigurationConfig",
    "docs.apps.DocsConfig",
    "external.apps.ExternalConfig",
    "farm.apps.FarmConfig",
    "fieldbook.apps.FieldbookConfig",
    "messaging.apps.MessagingConfig",
    "photos.apps.PhotosConfig",
    "soil.apps.SoilConfig",
    "utils",
    # Needs to come after utils as we are overriding its statics
    "leaflet.apps.LeafletConfig",
]

if DEBUG:
    INSTALLED_APPS += [
        "sslserver",
        "django_extensions",
        "puml_generator",
    ]

if DEBUG and DEBUG_TOOLBAR:
    INSTALLED_APPS += ["debug_toolbar"]
