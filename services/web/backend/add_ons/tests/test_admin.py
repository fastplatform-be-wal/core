from django.test import TestCase, override_settings

from utils.tests.test_admin import AdminTestCaseMixin

from add_ons.models import AddOn, Provider, APIAccess
from add_ons.tests.recipes import provider_recipe, add_on_recipe, api_access_recipe


@override_settings(AXES_ENABLED=False)
class ProviderAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Provider
    recipe = provider_recipe


@override_settings(AXES_ENABLED=False)
class AddOnAdminTestCase(AdminTestCaseMixin, TestCase):
    model = AddOn
    recipe = add_on_recipe


@override_settings(AXES_ENABLED=False)
class APIAccessAdminTestCase(AdminTestCaseMixin, TestCase):
    model = APIAccess
    recipe = api_access_recipe
