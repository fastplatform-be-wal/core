from django.conf import settings
from django.contrib import admin
from django.contrib.gis.db.models import Extent
from django.utils.translation import gettext_lazy as _
from django.utils.html import mark_safe

from import_export.admin import ExportMixin
from storages.backends.s3boto3 import S3Boto3Storage

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin
from utils.widgets.map_widget import MapWidget

from farm.models import HoldingCampaign, Site, Plot
from photos.models import GeoTaggedPhoto
from farm.resources import HoldingCampaignResource


s3boto3storage = S3Boto3Storage()


class GeoTaggedPhotoInline(admin.TabularInline):
    model = GeoTaggedPhoto
    extra = 0
    fields = ["_link", "id", "shot_at", "_thumbnail_for_list"]
    readonly_fields = ["_link", "id", "_thumbnail_for_list"]

    def has_add_permission(self, *args, **kwargs):
        return False

    def _thumbnail_for_list(self, obj):
        size = 100
        if obj and obj.photo and obj.thumbnail:
            return mark_safe(
                f'<a href="{s3boto3storage.url(name=obj.photo.name)}"><img src="{s3boto3storage.url(name=obj.thumbnail.name)}" style="max-width: {size}px; max-height: {size}px;"/></a>'
            )
        else:
            return "-"

    _thumbnail_for_list.short_description = _("thumbnail")

    def _link(self, obj):
        if obj:
            return obj.get_admin_link()
        return ""

    _link.short_description = ""


class SiteInline(admin.TabularInline):
    model = Site
    extra = 0
    fields = ["_link", "authority_id", "name", "number_of_plots"]
    readonly_fields = ["_link", "number_of_plots"]

    def _link(self, obj):
        if obj:
            return obj.get_admin_link()
        return ""

    _link.short_description = ""


class HoldingCampaignAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = HoldingCampaignResource
    search_fields = ("campaign",)
    raw_id_fields = ("holding",)
    ordering = ("-campaign",)
    list_display = [
        "campaign",
        "number_of_sites",
        "number_of_plots",
        "number_of_plots_with_fertilization_plan",
    ]

    fields = [
        "id",
        "holding",
        "campaign",
        "number_of_sites",
        "number_of_plots",
        "number_of_plots_with_fertilization_plan",
    ]

    readonly_fields = (
        "id",
        "number_of_sites",
        "number_of_plots",
        "number_of_plots_with_fertilization_plan",
    )

    inlines = [SiteInline, GeoTaggedPhotoInline]

    # Map parameters
    # --------------
    static_map = True

    def get_map_extent(self, obj):
        return Plot.objects.filter(site__holding_campaign=obj).aggregate(
            Extent("geometry")
        )["geometry__extent"]

    def get_holding_campaign_id(self, obj):
        return obj.id

    class Media(MapWidget.Media):
        pass


admin_site.register(HoldingCampaign, HoldingCampaignAdmin)
