from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("add_ons", "0024_generate_fk_cascade_add_ons"),
    ]

    operations = [
        migrations.RunSQL(
            sql="ALTER TABLE add_on ALTER COLUMN auto_subscribe SET DEFAULT FALSE;",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE add_on ALTER COLUMN callback_with_user_id SET DEFAULT FALSE;",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE add_on ALTER COLUMN callback_with_holding_id SET DEFAULT FALSE;",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE add_on ALTER COLUMN is_active SET DEFAULT FALSE;",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE add_on ALTER COLUMN is_visible SET DEFAULT FALSE;",
        ),
    ]
