# Generated by Django 3.0.7 on 2021-08-02 08:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0036_auto_20210225_1102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='broadcast',
            name='is_sending_triggered',
            field=models.BooleanField(blank=True, default=None, null=True),
        ),
    ]
