from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AddOnsConfig(AppConfig):
    name = "add_ons"
    verbose_name = _("Add-ons")

    help_text = None
