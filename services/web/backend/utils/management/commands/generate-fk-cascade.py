import warnings

from django.apps import apps
from django.conf import settings
from django.db import models, connections, router, OperationalError, DEFAULT_DB_ALIAS
from django.db.backends.ddl_references import (
    Columns,
    Table,
)
from django.db.backends.base.schema import BaseDatabaseSchemaEditor

from django.db.migrations import Migration
from django.db.migrations.operations.special import RunSQL
from django.db.migrations.loader import MigrationLoader
from django.db.migrations.autodetector import MigrationAutodetector
from django.db.migrations.questioner import NonInteractiveMigrationQuestioner
from django.db.migrations.state import ProjectState

from django.core.management.commands.makemigrations import (
    Command as MakeMigrationCommand,
)
from django.core.management.base import BaseCommand, CommandError


class Command(MakeMigrationCommand):
    help = "Generate database foreign key constraints with CASCADE statements"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.verbosity = 1
        self.include_header = True
        self.dry_run = False

    def add_arguments(self, parser):
        parser.add_argument("app", type=str, help="App to generate FK CASCADE migrations for")

    def handle(self, *args, **kwargs):
        app_label = kwargs["app"]
        statements = self.generate_sql_statements(app_label)
        self.write_migration(app_label, statements)

    def generate_sql_statements(self, app_label):

        connection = connections["default"]
        editor = BaseDatabaseSchemaEditor(connection)

        sql_delete_constraint = "ALTER TABLE %(table)s DROP CONSTRAINT IF EXISTS %(name)s;"
        sql_create_fk = (
            "ALTER TABLE %(table)s ADD CONSTRAINT %(name)s FOREIGN KEY (%(column)s) "
            "REFERENCES %(to_table)s (%(to_column)s) "
            "ON DELETE %(on_delete)s ON UPDATE %(on_delete)s%(deferrable)s;"
        )

        sql_statements = []

        for model_name, model in apps.all_models[app_label].items():
            fields = model._meta.get_fields(include_parents=False, include_hidden=True)
            fk_fields = list(filter(lambda f: isinstance(f, models.ForeignKey), fields))

            for field in fk_fields:
                suffix = "_fk_%(to_table)s_%(to_column)s"
                table = Table(model._meta.db_table, editor.quote_name)
                name = editor._fk_constraint_name(model, field, suffix)
                column = Columns(model._meta.db_table, [field.column], editor.quote_name)
                to_table = Table(field.target_field.model._meta.db_table, editor.quote_name)
                to_column = Columns(
                    field.target_field.model._meta.db_table,
                    [field.target_field.column],
                    editor.quote_name,
                )
                deferrable = connection.ops.deferrable_sql()
                on_delete = {
                    models.CASCADE: "CASCADE",
                    models.PROTECT: "RESTRICT",
                    models.DO_NOTHING: "NO ACTION",
                    models.SET_NULL: "SET NULL",
                    models.SET_DEFAULT: "SET DEFAULT",
                }[field.remote_field.on_delete]

                sql_drop_constraint = sql_delete_constraint % {"table": table, "name": name}
                sql_create_constraint = sql_create_fk % {
                    "table": table,
                    "name": name,
                    "column": column,
                    "to_table": to_table,
                    "to_column": to_column,
                    "deferrable": deferrable,
                    "on_delete": on_delete,
                }

                sql_statements += [sql_drop_constraint, sql_create_constraint]

        return sql_statements

    def write_migration(self, app_label, sql_statements):
        # Load the current graph state. Pass in None for the connection so
        # the loader doesn't try to resolve replaced migrations from DB.
        loader = MigrationLoader(None, ignore_no_migrations=True)

        # Raise an error if any migrations are applied before their dependencies.
        consistency_check_labels = {config.label for config in apps.get_app_configs()}
        # Non-default databases are only checked if database routers used.
        aliases_to_check = (
            connections if settings.DATABASE_ROUTERS else [DEFAULT_DB_ALIAS]
        )
        for alias in sorted(aliases_to_check):
            connection = connections[alias]
            if connection.settings_dict["ENGINE"] != "django.db.backends.dummy" and any(
                # At least one model must be migrated to the database.
                router.allow_migrate(
                    connection.alias, app_label, model_name=model._meta.object_name
                )
                for app_label in consistency_check_labels
                for model in apps.get_app_config(app_label).get_models()
            ):
                try:
                    loader.check_consistent_history(connection)
                except OperationalError as error:
                    warnings.warn(
                        "Got an error checking a consistent migration history "
                        "performed for database connection '%s': %s" % (alias, error),
                        RuntimeWarning,
                    )

        # Before anything else, see if there's conflicting apps and drop out
        # hard if there are any and they don't want to merge
        conflicts = loader.detect_conflicts()

        if app_label in conflicts:
            name_str = "; ".join(
                "%s in %s" % (", ".join(names), app) for app, names in conflicts[app_label].items()
            )
            raise CommandError(
                "Conflicting migrations detected; multiple leaf nodes in the "
                "migration graph: (%s).\nTo fix them run "
                "'python manage.py makemigrations --merge'" % name_str
            )

        # Set up autodetector
        questioner = NonInteractiveMigrationQuestioner(
            specified_apps=[app_label], dry_run=self.dry_run
        )
        autodetector = MigrationAutodetector(
            loader.project_state(),
            ProjectState.from_apps(apps),
            questioner,
        )

        migration = Migration("generate_fk_cascade", app_label)
        migration.operations = [RunSQL(sql=statement) for statement in sql_statements]

        changes = {app_label: [migration]}

        changes = autodetector.arrange_for_graph(
            changes=changes,
            graph=loader.graph,
            migration_name=f"generate_fk_cascade_{app_label}",
        )
        self.write_migration_files(changes)
