from django.urls import include, path
from django.conf import settings
from django.views.generic import RedirectView, TemplateView
from django.conf.urls.i18n import i18n_patterns

from fastplatform.site import admin_site

urlpatterns = [
    path(
        "<string>/admin/login/",
        RedirectView.as_view(url=settings.LOGIN_URL, query_string=True),
        name="admin_login"
    ),
    path(
        "authentication/",
        include(("authentication.urls", "authentication"), namespace="authentication"),
    ),
    path("openid/", include("oidc_provider.urls")),
]

urlpatterns += i18n_patterns(
    path(
        "admin/all_objects/",
        admin_site.admin_view(admin_site.index_all_objects),
        name="all_objects",
    ),
    path("admin/", admin_site.urls),
    path("docs/", include(("docs.urls", "docs"), namespace="docs")),
    # Prefix needed for English as otherwise
    # the language selector for django-amin-interface
    # does not work
    prefix_default_language=True,
)

urlpatterns += [
    path("", RedirectView.as_view(url="/admin", query_string=True)),
    path("add_ons/", include(("add_ons.urls", "add_ons"), namespace="add_ons")),
    path("i18n/", include("django.conf.urls.i18n")),
]

if settings.ENABLE_GEOTAGGED_PHOTO:
    urlpatterns += [
        path("photos/", include("photos.urls")),
    ]

if settings.DEBUG and settings.DEBUG_TOOLBAR:
    # Debug toolbar
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
