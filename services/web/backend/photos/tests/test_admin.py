from django.test import TestCase, override_settings
from django.conf import settings

from utils.tests.test_admin import AdminTestCaseMixin

from photos.models import (
    GeoTaggedPhoto
)

from configuration.tests.test_admin import configuration_recipe

# @override_settings(AXES_ENABLED=False)
# class GeoTaggedPhotoAdminTestCase(AdminTestCaseMixin, TestCase):
#     model = GeoTaggedPhoto
#     recipe = geo_tagged_photo_recipe
#     enable = settings.ENABLE_GEOTAGGED_PHOTO
#     create_files = True

#     def setUp(self):
#         super().setUp()
#         configuration_recipe.prepare().save()
