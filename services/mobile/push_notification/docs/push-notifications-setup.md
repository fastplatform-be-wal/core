# Push Notifications Server Setup

This documentation explains how to configure a push notification provider server.  
The requirements are that you already created your application on the Apple Developer portal and that you subscribed to 
the apple developer program.

## Using a Certificate based connection
Apple Doc: [Establishing a Certificate-Based Connection to APNs](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/establishing_a_certificate-based_connection_to_apns)

The configuration of the server is done in 3 steps:
1) Activate "Push Notification" Capabilities
2) Generate a certificate signing request
3) Generate and download the APNs (Apple Push Notification service) certificate

### 1) Activate "Push Notification" Capabilities

First, you need to activate the push notification capability on your app. To do so go to the Apple Developer Portal => 
Certificates, IDs & Profiles => Identifiers => eu.fastplatform.mobile.farmer-app and check "Push Notification"

![Activate Push Notification](./images/push-notifications-setup/activate-push-notification.png "Activate Push Notification")


### 2) Generate a certificate signing request

This certificate allows you to create an APNs certificate (step 3).  
To generate it follow the instructions listed in the [Apple Documentation](https://help.apple.com/developer-account/#/devbfa00fef7)

### 3) Generate and download the APNs certificate

This certificate is what allow your server to communicate with the APNs.

First go to the Apple Developer Portal => 
Certificates, IDs & Profiles => Identifiers => eu.fastplatform.mobile.farmer-app and click on either the "Configure" or "Edit" button. A modal should appear listing the existing certificates grouped by environment (development and production). 

![Apple Push Notification service SSL Certificates](./images/push-notifications-setup/push-notification-conf-choice.png "Apple Push Notification service SSL Certificates")

Then, click on the "Create Certificate" button for the desired environment. Chose the platform (iOS by default) and upload the certificate signing request generated at step 2. 

![Create a new certificate](./images/push-notifications-setup/push-notification-generate-certificate.png "Generate a new certificate")
Finally, click the "Continue" button and download and save the newly generated certificate under your server project.


## Using a Token based connection
Apple Doc: [Establishing a Token-Based Connection to APNs](https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/establishing_a_token-based_connection_to_apns)

The configuration of the server is done in 2 steps:
1) Activate "Push Notification" Capabilities
2) Generate an authentication token signing key

## 1) Activate "Push Notification" Capabilities
Same as "Using a Certificate based connection".

## 2) Generate an authentication token signing key

Go to the Apple Developer Portal => Certificates, IDs & Profiles => Keys page and click on the add key button.

On the "Register a New Key" page, enter the key name and check the "Apple Push Notification service (APNs)" option. Then click on the "Continue" button.

![Generate an authentication token signing key](./images/push-notifications-setup/keys-token.png "Generate an authentication token signing key")

On the next page, download the generated key and save it in the provider server project.

That's it.
