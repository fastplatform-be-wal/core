import os
import sys

from pathlib import Path, PurePath

from pydantic import BaseSettings

from typing import Dict, Union

class Settings(BaseSettings):

    APP_DIR: Path = PurePath(__file__).parent

    API_GATEWAY_FASTPLATFORM_URL: str

    API_GATEWAY_SERVICE_KEY: str

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1
    OPENTELEMETRY_SERVICE_NAME: str = "mapproxy-api"

    MAPPROXY_STANDARD_PROJ: str = "EPSG:3857"
    MAPPROXY_DEFAULT_PROJ: str
    MAPPROXY_DEFAULT_PROJ_BBOX: str
    MAPPROXY_CONF_FILE_PATH: str = "../data/dev/mapproxy/config/mapproxy.yaml"
    MAPPROXY_CONF_FROM_K8S: bool
    MAPPROXY_K8S_NAMESPACE: str
    MAPPROXY_K8S_CONFIGMAP_NAME: str
    MAPPROXY_K8S_CONFIGMAP_KEY: str
    MAPPROXY_K8S_USE_INCLUSTER_CONFIG: bool
    MAPPROXY_CONF_ATTR_REGIONAL_GRID_NAME: str = "regional-grid"
    MAPPROXY_CONF_CACHE_FILE_DIR: str = "/tmp/cache/mapproxy"
    MAPPROXY_CONF_CACHE_S3_BUCKET_NAME: str = "my-bucket"
    MAPPROXY_CONF_CACHE_S3_LOCATION: str = "cache/mapproxy"
    MAPPROXY_CONF_CACHE_S3_URL: str = "https://s3.url"
    MAPPROXY_CONF_CACHE_TYPE: str = "file"
    MAPPROXY_SERVER_LABELS_SELECTOR: str = "serving.knative.dev/service=mapproxy-server"

    INIT_CONFIGMAP_ON_STARTUP: bool = True

    REMOTE_DEBUG: bool = False

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }

config = Settings()