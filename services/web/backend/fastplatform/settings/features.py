import os

# Authentication

ENABLE_INSERT_DEMO_HOLDING_FROM_ADMIN = (
    os.environ.get("ENABLE_INSERT_DEMO_HOLDING_FROM_ADMIN", "TRUE").upper() == "TRUE"
)

ENABLE_SYNC_HOLDINGS_FROM_ADMIN = (
    os.environ.get("ENABLE_SYNC_HOLDINGS_FROM_ADMIN", "FALSE").upper() == "TRUE"
)

ENABLE_SYNC_HOLDING_FROM_ADMIN = (
    os.environ.get("ENABLE_SYNC_HOLDING_FROM_ADMIN", "FALSE").upper() == "TRUE"
)

ENABLE_MULTIPLE_REGIONS = (
    os.environ.get("ENABLE_MULTIPLE_REGIONS", "TRUE").upper() == "TRUE"
)

ENABLE_USER_PREFERRED_LANGUAGES = (
    os.environ.get("ENABLE_USER_PREFERRED_LANGUAGES", "TRUE").upper() == "TRUE"
)

# External

ENABLE_EXTERNAL_AGRI_PLOT = (
    os.environ.get("ENABLE_EXTERNAL_AGRI_PLOT", "TRUE").upper() == "TRUE"
)

ENABLE_EXTERNAL_PROTECTED_SITE = (
    os.environ.get("ENABLE_EXTERNAL_PROTECTED_SITE", "TRUE").upper() == "TRUE"
)

ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE = (
    os.environ.get(
        "ENABLE_EXTERNAL_MANAGEMENT_RESTRICTION_OR_REGULATION_ZONE", "TRUE"
    ).upper()
    == "TRUE"
)

ENABLE_EXTERNAL_SOIL_SITE = (
    os.environ.get("ENABLE_EXTERNAL_SOIL_SITE", "TRUE").upper() == "TRUE"
)

ENABLE_EXTERNAL_WATER_COURSE = (
    os.environ.get("ENABLE_EXTERNAL_WATER_COURSE", "TRUE").upper() == "TRUE"
)

ENABLE_EXTERNAL_SURFACE_WATER = (
    os.environ.get("ENABLE_EXTERNAL_SURFACE_WATER", "TRUE").upper() == "TRUE"
)

# Fieldbook

ENABLE_REGISTERED_FERTILIZER = (
    os.environ.get("ENABLE_REGISTERED_FERTILIZER", "TRUE").upper() == "TRUE"
)

ENABLE_CUSTOM_FERTILIZER = (
    os.environ.get("ENABLE_CUSTOM_FERTILIZER", "TRUE").upper() == "TRUE"
)

# Messaging

ENABLE_BROADCAST = os.environ.get("ENABLE_BROADCAST", "TRUE").upper() == "TRUE"

ENABLE_TICKET = os.environ.get("ENABLE_TICKET", "TRUE").upper() == "TRUE"

# Photos

ENABLE_GEOTAGGED_PHOTO = (
    os.environ.get("ENABLE_GEOTAGGED_PHOTO", "TRUE").upper() == "TRUE"
)

# Soil

ENABLE_SOIL_SITE = os.environ.get("ENABLE_SOIL_SITE", "TRUE").upper() == "TRUE"

# I18N

ENABLE_MODEL_TRANSLATIONS = (
    os.environ.get("ENABLE_MODEL_TRANSLATIONS", "TRUE").upper() == "TRUE"
)
