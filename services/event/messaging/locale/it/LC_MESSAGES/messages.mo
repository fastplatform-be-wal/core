��          �               ,  #   -  I   Q  3   �  @   �  ,     4   =  A   r  5   �  &   �  A     4   S     �  	   �     �     �     �  �  �  *   >  Y   i  3   �  @   �  1   8  8   j  E   �  ;   �  #   %  J   I  6   �     �  	   �     �     �     �   %(user_display_name)s sent a photo. %(user_link)s created ticket %(ticket_link)s for holding %(holding_link)s %(user_link)s replied to the ticket %(ticket_link)s %(user_link)s replied to the ticket %(ticket_link)s with a photo %(user_name)s created ticket %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Request %(ticket_link)s status changed to %(status)s. The request status has been changed to This email has been generated automatically, please do not reply. Ticket %(ticket_link)s status changed to %(status)s. closed duplicate open reopened resolved Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-25 19:03+0100
PO-Revision-Date: 2020-12-04 14:35+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: it
Language-Team: it <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 %(user_display_name)s ha inviato una foto. %(user_link)s ha creato il ticket %(ticket_link)s per l'azienda agricola %(holding_link)s %(user_link)s ha risposto al ticket %(ticket_link)s %(user_link)s ha risposto al ticket %(ticket_link)s con una foto %(user_name)s ha creato il ticket %(ticket_link)s %(user_name)s ha risposto alla richiesta %(ticket_link)s %(user_name)s ha risposto alla richiesta %(ticket_link)s con una foto Lo stato della richiesta %(ticket_link)s è ora %(status)s. La richiesta è stata modificata in Questa mail è stata generata automaticamente, si prega di non rispondere. Lo stato del ticket %(ticket_link)s è ora %(status)s. chiusa duplicata aperta riaperta risolta 