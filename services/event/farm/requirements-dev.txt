# Dependencies managment
pip-tools==6.5.1

# Linting
pylint==2.9.6
pylint-plugin-utils==0.6

# Formatting
black==22.3.0

# Remote debugging
debugpy==1.4.1

# Testing
coverage==6.1.1
requests>=2.23.0