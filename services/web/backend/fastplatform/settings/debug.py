import os

DEBUG = os.environ.get("DEBUG", "TRUE").upper() == "TRUE"

DEBUG_TOOLBAR = os.environ.get("DEBUG_TOOLBAR", "FALSE").upper() == "TRUE"

SHOW_TOOLBAR_CALLBACK = (
    os.environ.get("SHOW_TOOLBAR_CALLBACK", "TRUE").upper() == "TRUE"
)
