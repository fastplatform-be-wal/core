# `core/web/vector_tiles` Map vector tiles generator

The [core/web/vector_tiles](./) service is a [pg_tileserv](https://github.com/CrunchyData/pg_tileserv) server whose role is to compute vector tiles from a PostGIS database and serve them over HTTP. 

The tiles are computed and served under the [XYZ/Slippy Map tile scheme](https://wiki.openstreetmap.org/wiki/Slippy_Map) and are dynamic: they are computed *live* based the content of the databases. For example, the tiles of the plots of a farm are updated as soon as the farm is updated in the database. A [caching strategy](#caching) is used to avoid computing the same tiles too many times for data that does not change often.

<div align="center">
    <img src="docs/img/vector-tiles-map.png" width="420">
    <div>Natura2000 vector tiles (green) and current farm vector tiles (black)</div>
    <br>
    <img src="docs/img/vector-tiles-map-blank.png" width="420">
    <div>Without the base raster layers</div>
    <br>
</div>

This service serves only **vector tiles**, not raster tiles. It is the [core/mapproxy/server](../../mapproxy/) service that serves raster tiles in FaST. In addition, if it is installed, the [addons/sobloo](https://gitlab.com/fastplatform/addons/sobloo) add-on serves tiles for the RGB and NDVI satellite imagery.

## Architecture

The vector tiles are requested by the map components instantiated in both the farmer mobile application and the Administration Portal. All the map components use the same underlying library: [Leaflet](https://leafletjs.com/). The requested tiles come in the form of HTTP requests, where the URL defines the layer and the geographical extent of the requested tile and query parameters to filter the requested data.

```plantuml
actor "User" as user
actor "Admin" as admin

component "FaST app" as app {
    component "Leaflet maps" as leaflet_app
    component "Browser cache" as browser_cache_app #line.dashed
}
component "FaST Administration Portal" as portal {
    component "Leaflet maps" as leaflet_portal
    component "Browser cache" as browser_cache_portal #line.dashed
}

rectangle "FaST backend" as backend {

    component "Reverse proxy / Cache" as cache_fastplatform << NGINX >>  #yellow
    component "Reverse proxy / Cache" as cache_external << NGINX >>  #yellow

    storage "Cache storage" as cache_fastplatform_storage  #yellow
    storage "Cache storage" as cache_external_storage  #yellow

    component "Vector tiles server\nFarm data\n(plots...)" as vector_tiles_fastplatform <<pg_tileserv>>  #yellow
    component "Vector tiles server\nPublic data\n(NVZ, Natura2000, hydro...)" as vector_tiles_external <<pg_tileserv>>  #yellow

    database postgres_fastplatform <<PostGIS>> [
        <b>Farm data
        (plots...)
        ====
        <i>SQL functions
        ---
        plot_function_source
        ...
    ]

    database postgres_external <<PostGIS>> [
        <b>Public data
        (NVZ, Natura2000, hydro...)
        ====
        <i>SQL functions
        ---
        protected_site_function_source
        surface_water_function_source
        water_course_function_source
        ...
    ]

}

user --> app
admin --> portal

leaflet_app --> browser_cache_app
browser_cache_app --> cache_fastplatform
browser_cache_app --> cache_external

leaflet_portal --> browser_cache_portal
browser_cache_portal --> cache_fastplatform
browser_cache_portal --> cache_external

cache_fastplatform -> cache_fastplatform_storage
cache_external -> cache_external_storage

cache_fastplatform --> vector_tiles_fastplatform
cache_external --> vector_tiles_external

vector_tiles_fastplatform --> postgres_fastplatform
vector_tiles_external --> postgres_external

legend 
yellow: ""core/web/vector-tiles"" service
endlegend
```


For example, the below URL will request the tile `x = 1053` / `y = 692` of zoom level `z = 11` for the `protected_site_function_source` layer (Natura 2000):
```http
https://web-vector-tiles-external.beta.be-wal.fastplatform.eu/public.protected_site_function_source/11/1053/692.pbf
```

In turn, this request is converted by the [pg_tileserv](https://github.com/CrunchyData/pg_tileserv) server into the following SQL query:
```sql
SELECT * FROM public.protected_site_function_source(11, 1053, 692)
```

Which returns a vector tile compliant with the [Mapbox Vector Tile specification](https://postgis.net/docs/ST_AsMVT.html).

### SQL functions

The SQL functions, in the FaST databases, that are called to generate the tiles, are created as part of the database schema migrations managed by the [core/web/backend](../backend/) server. These functions are:

#### [plot_function_source](../backend/farm/migrations/create_plot_function_source.sql)

This function computes the tiles for the plots of a given farm campaign (from the `plot` table of the `fastplatform` database), if the provided authentication token is valid for the data being requested. If returns empty tiles if the token/session key is invalid for this farm campaign.

The function has a signature that includes extra parameters:
- `z`, `x` and `y`: zoom level, tile column and tile row
- `p_session_key`: the Django session key of the user requesting the tile (if the tile is requested from the Administration Portal) to authenticate the user and assess its `staff` status. Can be null.
- `p_access_token`: the OIDC access token of the user (if the tile is requested from the farmer application) to authenticate the user and assess whether he can access the tiles of a given farm. Can be null.
- `p_holding_campaign_id`: the identifier of the farm campaign to filter plots. Can be null.
- `p_site_id`: the identifier of the farm site to filter plots. Can be null.
- `p_plot_id`: the identifier of a specific plot. Can be null.

The `p_holding_campaign_id`, `p_site_id` and `p_plot_id` parameters are mutually inclusive, i.e. the resulting plots will be the intersection of the filtering of each of these parameters.

If none of `p_holding_campaign_id`, `p_site_id` and `p_plot_id` is provided, the function returns all the plots that the user (given his token/session_key) can access.

If none of `p_session_key` and `p_access_token` is provided, the function returns an empty tile.

Example:
```sql
-- Request tile 1053,692 of zoom level 11 for the plots of
-- holding_campaign_id = 128, authenticating with token 'ABCDEF12345'
SELECT plot_function_source(11, 1053, 692, null, 'ABCDEF12345', 128, null, null)
```

#### [protected_site_function_source](../backend/external/migrations/create_protected_site_function_source.sql)

This function computes the tiles for the protected sites (Natura2000, RAMSAR...) currently active in the database (from the `protected_site` table of the `external` database). These tiles do not require authentication.

The function has the basic signature `protected_site_function_source(z, x, y)`, where `z`, `x` and `y` are the zoom level, tile column and tile row.

The function filters the features from the database and *keeps only the ones that belong to the active `ProtectedSite` version*.

#### [management_restriction_or_regulation_zone_function_source](../backend/external/migrations/create_management_restriction_or_regulation_zone_function_source.sql)

This function computes the tiles for the regulated zones (Nitrate Vulnerables Zones..) currently active in the database (from the `management_restriction_or_regulation_zone` table of the `external` database). These tiles do not require authentication.

The function has the basic signature `protected_site_function_source(z, x, y)`, where `z`, `x` and `y` are the zoom level, tile column and tile row.

The function filters the features from the database and *keeps only the ones that belong to the active `ManagementRestrictionOrRegulationZone` version*.

#### [surface_water_function_source](../backend/external/migrations/create_surface_water_function_source.sql)

This function computes the tiles for the surface water (lakes...) currently active in the database. These tiles do not require authentication.

The function has the basic signature `protected_site_function_source(z, x, y)`, where `z`, `x` and `y` are the zoom level, tile column and tile row. The function filters the features from the database and *keeps only the ones that belong to the active `SurfaceWater` version*.

#### [water_course_function_source](../backend/external/migrations/create_water_course_function_source.sql)

This function computes the tiles for the water courses (rivers...) currently active in the database. These tiles do not require authentication.

The function has the basic signature `protected_site_function_source(z, x, y)`, where `z`, `x` and `y` are the zoom level, tile column and tile row.

The function filters the features from the database and *keeps only the ones that belong to an active `WaterCourse` version*.

#### [agri_plot_function_source](../backend/external/migrations/create_agri_plot_function_source.sql)

This function computes the tiles for the layer of all the public agricultural parcels (the layer that the farmer can choose from to add a parcel to his farm) currently active in the database. These tiles do not require authentication.

The function has the basic signature `protected_site_function_source(z, x, y)`, where `z`, `x` and `y` are the zoom level, tile column and tile row.

The function filters the features from the database and *keeps only the ones that belong to an active `AgriPlot` version*.

#### Caching

The tiles, computed by the PostGIS servers and served by the [pg_tileserv](https://github.com/CrunchyData/pg_tileserv) server are cached at two levels:
1. In the user's browser cache: for tiles of "public data", the `Cache-Control` header is set by the reverse proxy for all tiles, with the value `public, max-age=3600`, therefore instructing the user's browser to cache the tiles for 1 hour. See the configuration of the reverse proxy, [here](./nginx-external.conf). No `Cache-Control` header is set for tiles of "private data" (i.e. tiles of the parcels of the farm): the farm plot tiles are not cached in the browser, as they are too likely to change.
2. In a storage attached to a reverse proxy:
    - for "public data" (NVZ, etc), the tiles are cached in the storage for 60 hours (see [here](./nginx-external.conf)). This implies that, if a public layer is changed in the FaST Administration Portal (such as loading a new version), it can take up to 60 hours for the tiles to be updated and visible by the users.
    - for "private data" (farm plots), the tiles are cached in the storage for 1 minute (see [here](./nginx-fastplatform.conf))

## Environment variables

Not applicable

## Development

The service is based on existing Docker images of [pg_tileserv](https://github.com/CrunchyData/pg_tileserv) and [NGINX](https://www.nginx.com/). Configuration of those services is described in the deployment manifests and in the [NGINX](https://www.nginx.com/) configurations ([here](./nginx-fastplatform.conf) and [here](./nginx-external.conf)).

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- The `make` utility

Start the PostGIS servers of FaST:
```bash
cd to/where/web/backend/is  # Go to the core/web/backend service directory
make start-postgres  # Start the PostGIS servers
```

Start the server:
```bash
make start
```

### Management commands

The following management commands are available from the `Makefile`.
- `make start`: Start the 2 [pg_tileserv](https://github.com/CrunchyData/pg_tileserv) servers in daemon mode
- `make starts`: Start the 2 [NGINX](https://www.nginx.com/) servers on `https` (assumes that you have the relevant certificates in the `/certificates` directory).
- `make stop`: Stop all the servers (2 [pg_tileserv](https://github.com/CrunchyData/pg_tileserv) servers and 2 [NGINX](https://www.nginx.com/) servers)
    	
Some additional information is available in the `Makefile`, we encourage you to read it (or to type `make help` to get the full list of commands).
