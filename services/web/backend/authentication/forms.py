from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import User


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = ('username', 'password', 'email', 'first_name', 'last_name',
                  'name', 'region', 'picture', 'preferred_language',
                  'additional_data')


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'name', 'picture',
                  'preferred_language')
