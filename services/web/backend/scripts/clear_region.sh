files=`ls ../../../../custom/$1/init/fastplatform/*.csv`

tables_to_truncate=""
for filename in $files;
do
    base_name=`basename "$filename" .csv`;
    table_name=`echo "$base_name" | awk -F. '{print $2}'`
    tables_to_truncate=`echo "public.$table_name,$tables_to_truncate"`
done
tables_to_truncate=`echo "${tables_to_truncate%?}"`
echo "TRUNCATE fastplatform $tables_to_truncate"
PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -h localhost -p ${POSTGRES_PORT} -d ${POSTGRES_DATABASE} -c "TRUNCATE $tables_to_truncate CASCADE"
echo "-----------------------------------------"
