from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("add_ons", "0009_auto_20201216_1824"),
    ]

    operations = [
        migrations.RunSQL(
            sql="ALTER TABLE add_on_subscription ALTER COLUMN subscribed_at SET DEFAULT now();",
        ),
    ]
