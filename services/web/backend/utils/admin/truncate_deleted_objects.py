from django.utils.translation import gettext_lazy as _


class TruncateDeleteObjectsMixin:
    def get_deleted_objects(self, objs, request):
        (
            deleted_objects,
            model_count,
            perms_needed,
            protected,
        ) = super().get_deleted_objects(objs, request)

        def truncate_list(the_list, max_length):
            extra = (
                [_("And %(extra)s more...") % {"extra": len(the_list) - max_length}]
                if len(the_list) > max_length
                else []
            )
            return [
                truncate_list(x, max_length) if isinstance(x, list) else x
                for x in the_list
            ][:max_length] + extra

        deleted_objects = truncate_list(deleted_objects, 25)
        return deleted_objects, model_count, perms_needed, protected