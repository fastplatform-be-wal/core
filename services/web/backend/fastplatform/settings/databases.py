import os

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": os.environ.get("POSTGRES_DATABASE", "fastplatform"),
        "USER": os.environ.get("POSTGRES_USER", "fast"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "HOST": os.environ.get("POSTGRES_HOST", "localhost"),
        "PORT": os.environ.get("POSTGRES_PORT", "5437"),
        "CONN_MAX_AGE": int(os.environ.get("POSTGRES_CONN_MAX_AGE", 0)),
        "DISABLE_SERVER_SIDE_CURSORS": os.environ.get(
            "DISABLE_SERVER_SIDE_CURSORS", "TRUE"
        ).upper()
        == "TRUE",
    },
    "external": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": os.environ.get("POSTGRES_EXTERNAL_DATABASE", "external"),
        "USER": os.environ.get("POSTGRES_EXTERNAL_USER", "fast"),
        "PASSWORD": os.environ.get("POSTGRES_EXTERNAL_PASSWORD"),
        "HOST": os.environ.get("POSTGRES_EXTERNAL_HOST", "localhost"),
        "PORT": os.environ.get("POSTGRES_EXTERNAL_PORT", "5438"),
        "CONN_MAX_AGE": int(os.environ.get("POSTGRES_EXTERNAL_CONN_MAX_AGE", 0)),
        "DISABLE_SERVER_SIDE_CURSORS": os.environ.get(
            "DISABLE_SERVER_SIDE_CURSORS", "TRUE"
        ).upper()
        == "TRUE",
    },
}

DATABASE_ROUTERS = ["fastplatform.routers.FaSTRouter"]
