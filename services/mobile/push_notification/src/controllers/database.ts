import redis from 'redis'
import {
  REDIS_CONNECT_MAX_ATTEMPT,
  REDIS_HOST,
  REDIS_PORT,
  REDIS_AUTH_PASSWORD
} from '../config/env'

export const FastPlatformKeySeparator = '|fastplatform|'

console.log(`INFO: open Redis connection at ${REDIS_HOST}:${REDIS_PORT}`)
const client = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT,
  password: REDIS_AUTH_PASSWORD,
  max_attempts: REDIS_CONNECT_MAX_ATTEMPT,
  retry_strategy: function(options) {
    if (options.error && options.error.code === "ECONNREFUSED") {
      // End reconnecting on a specific error and flush all commands with
      // a individual error
      return new Error("The server refused the connection");
    }
    if (options.total_retry_time > 1000 * 60 * 60) {
      // End reconnecting after a specific timeout and flush all commands
      // with a individual error
      return new Error("Retry time exhausted");
    }
    if (options.attempt > REDIS_CONNECT_MAX_ATTEMPT) {
      // End reconnecting after reaching the max number of retry
      return new Error("Retry attempt limit reached");
    }
    // reconnect after a minimum of 3 seconds
    return Math.min(options.attempt * 100, 3000);
  }
})

export default client

// resursively scan all matching keys and return a promise
export function getMatchingKeys(match: string, cursor: string = '0'): Promise<string[]> {
  return new Promise((resolve, reject) => {    
    const args = [cursor, 'MATCH', match]
    client.scan(args, (err, reply) => { 
      if (err) {
        reject(err)
      } else {
        const newCursor = reply[0]
        const newKeys = reply[1]        
        if (newCursor === '0') {
          resolve(newKeys)
        } else {
          return getMatchingKeys(match, newCursor).then(keys => {
            resolve(newKeys.concat(keys))
          })
        }
      }
    })
  })
}