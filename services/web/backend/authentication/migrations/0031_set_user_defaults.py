from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ("authentication", "0030_auto_20210117_1756"),
    ]

    operations = [
        migrations.RunSQL(
            sql="ALTER TABLE public.user ALTER COLUMN is_active SET DEFAULT TRUE;",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE public.user ALTER COLUMN is_staff SET DEFAULT FALSE;",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE public.user ALTER COLUMN is_superuser SET DEFAULT FALSE;",
        )
    ]
