from django.utils.translation import gettext_lazy as _
from django.conf import settings

from import_export.admin import ImportExportMixin

from common.models import UnitOfMeasure
from common.resources import UnitOfMeasureResource
from utils.admin import CommonModelAdmin
from fastplatform.site import admin_site


class UnitOfMeasureAdmin(ImportExportMixin, CommonModelAdmin):
    resource_class = UnitOfMeasureResource
    hide_from_dashboard = True
    list_display = ("id", "name", "symbol")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [[None, {"fields": ("id", "name", "symbol")}]]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )
        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


admin_site.register(UnitOfMeasure, UnitOfMeasureAdmin)
