from typing import Type
from unittest import TestCase
from unittest.mock import MagicMock
import asyncio
import copy

from app.api.resolvers import ComputedConstraintsSurfaceWaterResolver
from app.db.graphql_clients import fastplatform
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError
from app.api.types import ComputedConstraint


from .mock_data import constraints_surface_water as md


async def async_magic():
    pass


MagicMock.__await__ = lambda x: async_magic().__await__()


class MockGraphQLSession:
    data_plot_by_pk: dict
    data_plot_constraints_of_type_surface_water: dict
    data_external__surface_water: dict

    async def execute(self, *args, **kwargs):
        query_name = [*args][0].definitions[0].name.value

        if query_name == "plot_by_pk":
            return {"plot_by_pk": self.data_plot_by_pk}

        if query_name == "plot_constraints_of_type":
            return {"constraint": self.data_plot_constraints_of_type_surface_water}

        if query_name == "external__surface_water":
            return {"external__surface_water": self.data_external__surface_water}


class ConstraintsSurfaceWaterTests(TestCase):
    mock = MockGraphQLSession()
    sut = None
    loop = None

    @classmethod
    def setUpClass(cls) -> None:
        print("ConstraintsSurfaceWaterTests")
        cls.loop = asyncio.get_event_loop()

        # Replace fastplatform.session with our mock
        setattr(fastplatform, "session", cls.mock)
        return super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        # TODO: if following line uncommented, multiple async class test not working
        # cls.loop.close()
        return super().tearDownClass()

    def run_async(self, method):
        return self.loop.run_until_complete(method)

    def setUp(self) -> None:
        self.sut = ComputedConstraintsSurfaceWaterResolver()
        return super().setUp()

    def test_returns_plot_surface_water_constraints_if_exists(self):
        self.mock.data_plot_constraints_of_type_surface_water = md.EXISTING_CONSTRAINTS

        result = self.run_async(self.sut.resolve(md.PLOT_ID))
        self.assertIsInstance(result, list)

        self.assertEqual(len(result), 1)
        constraint = result[0]
        self.assertIsInstance(constraint, ComputedConstraint)
        self.assertEqual(constraint, md.EXPECTED_EXISTING_CONSTRAINT)

    def test_computed_constraint(self):

        # Simulate no constraints_of_type_surface_water associated to requested plot
        self.mock.data_plot_constraints_of_type_surface_water = []
        self.mock.data_plot_by_pk = md.QUERY_PLOT_BY_PK
        self.mock.data_external__surface_water = md.CLOSE_BY_WATER_COURSE

        self.sut.insert_surface_water_constraints = MagicMock()

        self.run_async(self.sut.resolve(md.PLOT_ID))

        self.sut.insert_surface_water_constraints.assert_called()

        # Get value of `constraints` with which function is called
        constraints = self.sut.insert_surface_water_constraints.call_args_list[0][0][0]
        self.assertIsInstance(constraints, list)
        self.assertEqual(len(constraints), 1)
        constraint = constraints[0]

        self.assertEqual(constraint, md.COMPUTED_CONSTRAINT)

    def test_no_data_available_still_writes_empty_mutation(
        self,
    ):
        # Simulate no constraints_of_type_surface_water associated to requested plot
        # and computation finds nothing.
        self.mock.data_plot_constraints_of_type_surface_water = []
        self.mock.data_plot_by_pk = md.QUERY_PLOT_BY_PK
        self.mock.data_external__surface_water = []

        self.sut.insert_surface_water_constraints = MagicMock()

        self.run_async(self.sut.resolve(md.PLOT_ID))

        # Still need to call insert
        self.sut.insert_surface_water_constraints.assert_called()

        # Get value of `constraints` with which function is called
        constraints = self.sut.insert_surface_water_constraints.call_args_list[0][0][0]
        self.assertIsInstance(constraints, list)
        self.assertEqual(len(constraints), 1)
        constraint = constraints[0]

        self.assertIsInstance(constraint, dict)

        # Assert an empty constraint about to be created
        self.assertDictEqual(constraint, md.EMPTY_CONSTRAINT)

    def test_no_plot_details_raises_plot_not_found_error(self):
        self.mock.data_plot_constraints_of_type_surface_water = []
        self.mock.data_plot_by_pk = None

        with self.assertRaises(GraphQLHoldingPlotNotFoundError):
            self.run_async(self.sut.resolve(md.PLOT_ID))

    def test_erroneous_data_raises_unexpected_data_error(self):
        erroneous_data = copy.deepcopy(md.EXISTING_CONSTRAINT)
        del erroneous_data["id"]
        self.mock.data_plot_constraints_of_type_surface_water = [erroneous_data]

        with self.assertRaises(GraphQLUnexpectedDataError):
            self.run_async(self.sut.resolve(md.PLOT_ID))
