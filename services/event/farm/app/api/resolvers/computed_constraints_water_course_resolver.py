import logging
from functools import partial

from opentelemetry import trace
from gql import gql
from pydantic.types import constr
from shapely.geometry import shape


from app.api.types.computed_constraint import ComputedConstraint
from app.api.lib.gis import Projection, add_crs
from app.settings import config
from app.db.graphql_clients import fastplatform
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError

# Log
logger = logging.getLogger(__name__)

CONSTRAINT_TYPE = "water_course"

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class ComputedConstraintsWaterCourseResolver:
    async def resolve(self, plot_id: int):
        """
        For a given plot, compute its 'water_course' constraints.

        Args:
            plot_id (int): the id of the plot whose constraints of type
            'water_course' are calculated here.

        Raises:
            GraphQLHoldingPlotNotFoundError: No plot associated with the id provided.
            GraphQLUnexpectedDataError: The graphql operation could not be performed successfully.

        Returns:
            affected_rows
        """
        logger.info("water_course constraints: resolve()")
        try:
            constraints = await self.get_plot_water_course_constraints(plot_id)

            if not constraints == []:
                logger.info("constraints already computed for plot_id %s", plot_id)
                return constraints

            logger.info("Computing water_course constraints for plot_id %s", plot_id)

            plot_details = await self.get_plot_details(plot_id)

            constraints = await self.compute_water_course_constraints(plot_details)

            if constraints == []:
                return None

            await self.insert_water_course_constraints(constraints)

            return await self.get_plot_water_course_constraints(plot_id)

        except GraphQLHoldingPlotNotFoundError as ex:
            raise GraphQLHoldingPlotNotFoundError() from ex

        except Exception as ex:
            logger.exception("water_course_constraints error")
            raise GraphQLUnexpectedDataError() from ex

    async def get_plot_water_course_constraints(self, plot_id: int):
        logger.info("water_course constraints: get_plot_water_course_constraints()")

        with tracer().start_as_current_span("get_constraint") as span:
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_plot_constraints_of_type.graphql"
            ).read_text()

            variables = {"plot_id": plot_id, "constraint_type": CONSTRAINT_TYPE}

            response = await fastplatform.execute(gql(query), variables)

            constraints = response["constraint"]
            if constraints == []:
                logger.info("No water_course constraint found for plot: %s", plot_id)
                return []

            constraints = [
                ComputedConstraint(
                    id=int(constraint["id"]),
                    name=constraint["name"],
                    description=constraint["description"],
                    plot_id=constraint["plot_id"],
                )
                for constraint in constraints
            ]

            if len(constraints) == 1 and constraints[0].description == None:
                constraints = None

            return constraints

    async def get_plot_details(self, plot_id: int):
        logger.info("water_course constraints: get_plot_details()")

        with tracer().start_as_current_span("get_plot_details") as span:
            query = (
                config.API_DIR / "resolvers" / "graphql" / "query_plot_by_pk.graphql"
            ).read_text()
            variables = {"plot_id": plot_id}

            response = await fastplatform.execute(gql(query), variables)

            plot_details = response["plot_by_pk"]

            if plot_details == None or plot_details == {}:
                logger.error(
                    "No plot found for id: %s. They will now be computed.", plot_id
                )
                raise GraphQLHoldingPlotNotFoundError()

            return plot_details

    async def compute_water_course_constraints(self, plot_details):
        logger.info("water_course constraints: compute_water_course_constraints()")
        plot_id = int(plot_details["id"])
        geometry = shape(plot_details["geometry"])

        with tracer().start_as_current_span("get_nearby_water_courses") as span:

            # Query the surface waters that intersect the plot geometry (with the buffer)
            query = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "query_external__water_course_near_plot.graphql"
            ).read_text()

            variables = {
                "geometry": add_crs(
                    geometry.__geo_interface__, config.EPSG_SRID_ETRS89
                ),
                "distance": config.API_PLOT_COMPUTE_CONSTRAINTS_WATER_COURSE_SEARCH_DISTANCE,
            }

            response = await fastplatform.execute(gql(query), variables)

            water_courses = response["external__water_course"]

            span.add_event("{} water courses found".format(len(water_courses)))

        with tracer().start_as_current_span("compute_distances_from_water_courses"):

            def _constraint_from_water_course(target, water_course):
                """Convert a water course dict as returned by GraphQL
                to a FaST constraint object
                """

                g_water_course_projected = Projection.etrs89_to_etrs89_laea(
                    shape(water_course["geometry"])
                )

                distance = target.distance(g_water_course_projected)

                return {
                    "name": CONSTRAINT_TYPE,
                    "plot_id": plot_id,
                    "description": {
                        "water_course_id": water_course["id"],
                        "water_course_name": water_course["geographical_name"],
                        "distance_to_water_course": distance,
                    },
                }

            geometry_projected = Projection.etrs89_to_etrs89_laea(geometry)
            constraints = map(
                partial(_constraint_from_water_course, geometry_projected),
                water_courses,
            )
            constraints = list(constraints)

            # This `empty constraint` acknowledges that computation took place but turned out nothing.
            if constraints == []:
                constraints = [
                    {
                        "name": CONSTRAINT_TYPE,
                        "plot_id": plot_id,
                        # no description json node set
                    }
                ]

            return constraints

    async def insert_water_course_constraints(self, constraints):
        logger.info("water_course constraints: insert_water_course_constraints()")

        with tracer().start_as_current_span("insert_constraints"):
            # Build the query to insert constraints
            mutation = (
                config.API_DIR
                / "resolvers"
                / "graphql"
                / "mutation_insert_constraint.graphql"
            ).read_text()
            variables = {"objects": constraints}

            response = await fastplatform.execute(gql(mutation), variables)

        return response
