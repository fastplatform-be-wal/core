import json

from django.contrib.auth.views import LogoutView
from django.http import JsonResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from oidc_provider.models import Token

from authentication.models import User


class OIDCLogoutView(View):

    http_method_names = ["post"]

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(OIDCLogoutView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        body = json.loads(request.body)
        session_variables = body["session_variables"]

        authorization = request.META.get("HTTP_AUTHORIZATION", None)
        user_id = session_variables.get("x-hasura-user-id")

        if authorization and user_id:
            # The authentication webhook guarantees that the X-Hasura-User-Id
            # corresponds to the token in the Authorization header
            user = User.objects.get(id=user_id)

            access_token = authorization.split(" ")[1]
            oidc_token = Token.objects.get(access_token=Token.hash_token(access_token))
            oidc_token.delete()

            user.session_set.all().delete()

            return JsonResponse({"status": "ok", "username": user.username})
        else:
            return JsonResponse({"status": "error", "username": None})


class AuthenticationLogoutView(LogoutView):
    def dispatch(self, request, *args, **kwargs):

        if "federated_provider_logout_url" in request.session:
            federated_provider_logout_url = request.session[
                "federated_provider_logout_url"
            ]
            del request.session["federated_provider_logout_url"]
            return HttpResponseRedirect(federated_provider_logout_url)

        return super(AuthenticationLogoutView, self).dispatch(request, *args, **kwargs)
