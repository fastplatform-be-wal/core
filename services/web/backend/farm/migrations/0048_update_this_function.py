from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farm', '0047_update_this_function')
    ]

    operations = [
        migrations.RunSQL((Path(__file__).parent / Path('create_plot_function_source.sql')).read_text())
    ]
