# Messaging

FaST provides a two-way messaging system between members of the administration (i.e. users of the **Administration Portal**) and users of the **Farmer application**.

<img src="/services/web/backend/docs/static/docs/img/messaging/inbox.png" title="Messaging inbox" class="img-docs" height="200"/>

Two communication modes are available:
- Broadcasting: these are one-way communications that are broadcast to _all users_
- Ticketing: these are two-way communication between the administration and the farmer

All communications (either **broadcasts** or **tickets**) are persisted in the FaST database.

```plantuml
skinparam defaultFontSize 12

actor portal_user as "Administration Portal user"
actor app_user as "Farm member\n(farmer / advisor)"

component portal as "Administration Portal"
component app as "Mobile app"
usecase broadcast as "
**Broadcast**
---
All users
One-way
"
usecase ticket as "
**Ticket**
---
Specific farm
Two-way
"

portal_user --> portal
portal --> broadcast: send
portal <--> ticket: send & receive

broadcast --> app: send
ticket <--> app: send & receive

app <--> app_user
```

## Broadcasts

A **broadcast** is a one-way communication that is pushed to all users currently registered in FaST. Broadcasts can be used to send notifications like regulation change, weather warnings, or any other communication that all FaST users might be interested in, and where a response is not expected.

The users receive a push notification on the **mobile application** and an email (if their email adress is known from FaST). They can also find a copy of the broadcast in their inbox. As the communication is one-way, a farmer (application user) _cannot reply to a broadcast_.

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/messaging/broadcast_screen.png" height="300"/>
<figcaption>A broadcast on the farmer mobile app</figcaption>
</figure>

Broadcasts are sent using the Administration Portal, and hence sending broadcasts can be restricted to certain groups of users, using relevant [permission](/services/web/backend/docs/en/users/authorization.md).

## Tickets

A ticket is a two-way communication between the farmer and the Paying Agency. A ticket can be opened by the farmer (using the app) or by the Paying Agency (using the Administration Portal).

<img src="/services/web/backend/docs/static/docs/img/messaging/ticket_screen.png" title="Ticket" class="img-docs" height="300"/>

A ticket is attached to a farm, and hence is visible by all the users that have the right to view this farm's data (see [Authorization](/services/web/backend/docs/en/users/authorization.md) for more details about this).

A ticket will have several statuses during its lifetime: OPEN, RESOLVED, CLOSED, etc. Ticket status change is managed by the Paying Agency through the Administration Portal.

```plantuml
skinparam defaultFontSize 12

title "Example ticketing flowchart when the ticket originates from the mobile app\n"

partition "In the mobile application" {
    (*) --> "Ticket is created\nby a farmer in\nthe mobile application" as created
    created --> "Ticket appears\nin the inbox" as inbox_app
    note right: Ticket is now visible\nby all members of the farm
}

partition "In the Administration Portal" {
    created --> "Ticket is assigned\nto <b>default queue</b>\nwith status <b>OPEN</b>" as default_queue
    note right: Ticket is now only\nvisible by **members**\n**of the default queue**\n(and superusers)

    default_queue --> "Ticket is reassigned\nto another queue\nby a member of the\ndefault queue" as another_queue
    note right: Ticket is now only\nvisible by <b>members</b>\n<b>of this other queue</b>\n(and superusers)
    
    another_queue --> "Admin Portal user\nreplies to ticket" as admin_replies
}

partition "In the mobile application" {
    admin_replies -> "Reply is visible\nin the app" as visible_in_app
    inbox_app -[hidden]--> visible_in_app
    visible_in_app -left> "Push notification" as push
    visible_in_app --> "Farmer user replies\nin the app" as farmer_replies
}

partition "In the Administration Portal" {
    farmer_replies --> "Reply is visible\nin the portal" as visible_in_portal
    admin_replies -[hidden]-> visible_in_portal
    visible_in_portal -right> "Email notification\nto all members\nof the queue" as email_notif
    visible_in_portal --> "Admin Portal user\nchanges ticket priority\nto <b>HIGH</b>" as high
    note right: The priority of a ticket\nis not visible in the app\n(nor is the queue)
    high --> "Admin Portal user\nchanges ticket status\nto <b>RESOLVED</b>" as resolved
    note right: The status of a ticket\n<b>is</b> visible in the app
}

partition "In the mobile application" {
    resolved -> "Status change is\nvisible in the app" as status
    farmer_replies --[hidden]-> status
    status -left> "Push notification" as push_2
    status --> (*)
}
```

### Queues

On the Administration Portal side, a ticket is assigned to a queue, and a queue is assigned to a set of (groups of) Paying Agency users. These queues allow to triage and filter out tickets into meaningful groups that will be attended by different Paying Agency users.

By default, when a ticket is created by a farmer on the mobile application, it is assigned to the _default queue_. It can then be moved to another queue, in the Administration Portal: this means that only members of the _default queue_ can move tickets from this queue to another.

To summarize the effect of queues: as an Administration Portal user, you can see only the tickets that are assigned to queues that you are a member of.

### Conversation within a ticket

The Paying Agency and the farmer can "chat" on a ticket. In the mobile application, this really looks like a regular chat, while in the Administration Portal, the interface is based on forms; but in the end they are equivalent.

In the mobile app, all farm members can chat on an open ticket. In the Administration Portal, all users with the right permissions (being member of the queue of the ticket **and** having the "add Ticket Message" permission) can reply to any ticket.

### Status of a ticket

A ticket can have several statuses:

- OPEN (the default status when the ticket is created on the mobile application)
- RESOLVED
- CLOSED
- REOPENED
- DUPLICATE

The status of a ticket can only be changed in the Administration Portal, and is fully visible in the mobile application.

### Priority of a ticket

A ticket can have several priority levels:

- CRITICAL
- HIGH
- NORMAL (the default priority level when the ticket is created on the mobile application)
- LOW
- VERY_LOW

The priority level of a ticket can only be changed in the Administration Portal, and is **not** visible in the mobile application.

It serves as a way to prioritize tickets for more efficient internal processing.

### Tickets notifications

Ticket creation, replies and status change trigger notifications:

- Farmer app users will be receive a push notification on the mobile app and an email notification when a ticket is assigned to its farm(s).
- Admin portal users will receive an email notification when the ticket belongs to a queue of which they are members

>Email notifications are sent only if the user email adress is known from FaST (which may not be the case depending on the region authentication service)

## How do I...?

### Broadcasts

#### How do I send a new broadcast?

To send a new broadcast, follow these steps:

- Open the [broadcasts list](/admin/messaging/broadcast/)
- Click on <kbd>Add Broadcast</kbd>, on the top right of the page
- Fill in a title and a body text. Note that both of these fields will be visible in the mobile application and are limited in length (48 characters for the title, and 178 for the body text)
- Click <kbd>Save and continue editing</kbd>

Your broadcast is now saved, and is in "DRAFT" mode. It is not yet visible by the mobile application users. Now:

- Click on <kbd>Send this broadcast</kbd> on the top right of the screen

Your broadcast is now being sent. Once this process is done, the status will be updated to "SENT" (or "FAILED" if there was an error).

#### Can I delete a broadcast?

Yes, you can either:

- Set the broadcast's status to "CANCELED"

Or:

- Delete the broadcast by clicking on the <kbd>Delete</kbd> button

Both of these actions will remove the broadcast from every FaST user's inbox.

#### Can I edit a broadcast *after* it was sent?

No, you cannot. Once a broadcast is sent, it becomes readonly. You can only delete it (see above) and create a new one.

#### Will users be notified when a broadcast is sent?

Yes, mobile application users will receive a push notification when the broadcast is sent (as well as an email if their email adress is known from FaST).

#### Can I create a new ticket from the Administration Portal?

Yes, to create a ticket:

- Open the [list of tickets](/admin/messaging/ticket/)
- Click on <kbd>Add ticket</kbd> on the top right of the page
- Fill in the necessary parameters:

> Note: to be able to add a ticket:
> - You must have the `ADD` permission on the <x>Ticket</x> object type or be a superuser
> - You must have the `VIEW` permission on the <x>Holding</x> object type or be a superuser (to be able to attach a ticket to a holding)
> - You must be a member of at least one queue (to be able to create the ticket in this queue) or be a superuser

#### How do I reply to a ticket received from a farmer?

- Open the [list of tickets](/admin/messaging/ticket/)
- Click on the ticket you want to reply to
- In the <kbd>Messages</kbd> section, click on <kbd>Add another message</kbd>
- Fill in your message in the *body* input
- Click <kbd>Save</kbd> on the ticket (this will save the ticket and the messages associated with the ticket)

Once the ticket is saved, the conversation will be updated in the mobile application for all the users that are member of the holding associated to this ticket, and they will all receive a push notification.

Note that, if you have the corresponding object-level permissions, you will also be able to delete messages from a ticket conversation. This should however be done with parsimony as the mobile app users will *not* be notified that messages have been suppressed (and might therefore be surprised).

#### I cannot see some tickets I think I should see, what's wrong?

In the Administration Portal, you can only see tickets of queues that you are a member of (unless you are superuser, in which case you can access everything). Therefore if you do not see a given ticket, you need to ask your local support in which queue this ticket can be found, and ask them that they grant you access to this queue or change the queue of this ticket to a queue you have access to.

#### Will users be notified when I reply to a ticket?

Yes, users receive a push notification (if using the mobile application) every time someone creates a new ticket on their farm, or add a message to such a ticket (as well as an email if their email adress is known from FaST).

Users are also notified when the ticket status changes.