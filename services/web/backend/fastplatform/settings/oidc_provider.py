import os

OIDC_AFTER_USERLOGIN_HOOK = "authentication.oidc_provider.oidc_after_userlogin_hook"

OIDC_EXTRA_SCOPE_CLAIMS = "authentication.oidc_provider.CustomScopeClaims"

OIDC_SESSION_MANAGEMENT_ENABLE = True

OIDC_UNAUTHENTICATED_SESSION_MANAGEMENT_KEY = os.environ.get(
    "OIDC_UNAUTHENTICATED_SESSION_MANAGEMENT_KEY", "arandomkey"
)

OIDC_TOKEN_EXPIRE = int(os.environ.get("OIDC_TOKEN_EXPIRE", 7200))

OIDC_REFRESH_TOKEN_EXPIRE = int(os.environ.get("OIDC_REFRESH_TOKEN_EXPIRE", 604800))

OIDC_REFRESH_TOKEN_ALIVE_HOOK = (
    "authentication.oidc_provider.oidc_refresh_token_alive_hook"
)
