from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CommonConfig(AppConfig):
    name = "common"

    verbose_name = _("Reference datasets")
    help_text = _("Tables shared and used as references for all the database")
