from django.test import TestCase, override_settings
from django.conf import settings

from utils.tests.test_admin import AdminTestCaseMixin

from authentication.models import (
    User,
    Group,
    Country,
    Region,
    Language,
    FederatedProvider,
    FederatedProviderLogEntry,
    TermsAndConditionsConsentRequest,
    UserTermsAndConditionsConsent,
    UserIACSConsent,
)

from authentication.tests.recipes import (
    user_recipe,
    group_recipe,
    language_recipe,
    country_recipe,
    region_recipe,
    terms_and_conditions_consent_request_recipe,
    user_terms_and_conditions_consent_recipe,
    user_iacs_consent_recipe,
    federated_provider_recipe,
    federated_provider_log_entry_recipe
)


# @override_settings(AXES_ENABLED=False)
# class UserAdminTestCase(AdminTestCaseMixin, TestCase):
#     model = User
#     recipe = user_recipe


@override_settings(AXES_ENABLED=False)
class GroupAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Group
    recipe = group_recipe


@override_settings(AXES_ENABLED=False)
class CountryAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Country
    recipe = country_recipe
    enable = settings.ENABLE_MULTIPLE_REGIONS


@override_settings(AXES_ENABLED=False)
class RegionAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Region
    recipe = region_recipe
    enable = settings.ENABLE_MULTIPLE_REGIONS


@override_settings(AXES_ENABLED=False)
class LanguageAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Language
    recipe = language_recipe
    enable = settings.ENABLE_USER_PREFERRED_LANGUAGES


@override_settings(AXES_ENABLED=False)
class FederatedProviderAdminTestCase(AdminTestCaseMixin, TestCase):
    model = FederatedProvider
    recipe = federated_provider_recipe


@override_settings(AXES_ENABLED=False)
class FederatedProviderLogEntryAdminTestCase(AdminTestCaseMixin, TestCase):
    model = FederatedProviderLogEntry
    recipe = federated_provider_log_entry_recipe


@override_settings(AXES_ENABLED=False)
class TermsAndConditionsConsentRequestAdminTestCase(AdminTestCaseMixin, TestCase):
    model = TermsAndConditionsConsentRequest
    recipe = terms_and_conditions_consent_request_recipe


@override_settings(AXES_ENABLED=False)
class UserTermsAndConditionsConsentAdminTestCase(AdminTestCaseMixin, TestCase):
    model = UserTermsAndConditionsConsent
    recipe = user_terms_and_conditions_consent_recipe
    skip_add = True
    skip_change = True


@override_settings(AXES_ENABLED=False)
class UserIACSConsentAdminTestCase(AdminTestCaseMixin, TestCase):
    model = UserIACSConsent
    recipe = user_iacs_consent_recipe
    skip_add = True
    skip_change = True
