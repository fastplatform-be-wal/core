# Generated by Django 3.0.5 on 2020-06-26 11:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0003_auto_20200620_1823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(db_table='user_group', to='authentication.Group'),
        ),
    ]
