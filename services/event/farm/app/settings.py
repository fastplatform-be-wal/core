import os
import sys
from typing import Dict, Union
from pathlib import Path, PurePath

from pydantic import BaseSettings


class Settings(BaseSettings):

    API_DIR: Path = PurePath(__file__).parent / "api"

    EPSG_SRID_ETRS89: str = "4258"
    EPSG_SRID_ETRS89_LAEA: str = "3035"
    EPSG_SRID_WGS84: str = "4326"
    EPSG_SRID_WGS84_PSEUDO_MERCATOR: str = "3857"

    API_GATEWAY_FASTPLATFORM_URL: str

    API_GATEWAY_SERVICE_KEY: str
    # 2 minutes, there can be some heavy farms to write to Hasura
    API_GATEWAY_TIMEOUT: int = 120

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1
    OPENTELEMETRY_SERVICE_NAME: str = "event-farm"

    API_PLOT_COMPUTE_CONSTRAINTS_SURFACE_WATER_SEARCH_DISTANCE: float = 20.0 / (
        60 * 1852
    )  # degrees

    API_PLOT_COMPUTE_CONSTRAINTS_WATER_COURSE_SEARCH_DISTANCE: float = 20.0 / (
        60 * 1852
    )  # degrees

    API_PLOT_COMPUTE_SOIL_DERIVED_OBJECT_PUBLIC_SOIL_SITE_SEARCH_DISTANCE: float = (
        15000.0 / (60 * 1852)
    )  # degrees

    REMOTE_DEBUG: bool = False

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }


config = Settings()
