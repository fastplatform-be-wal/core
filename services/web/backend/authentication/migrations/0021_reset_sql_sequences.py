# Generated by Django 3.0.7 on 2020-11-21 22:13

from django.db import migrations
import user_sessions.models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', "0020_loginaccessattempt_loginaccesslog_oidcclient_oidccode_oidctoken_usersession"),
    ]

    operations = [
        migrations.RunSQL(
            """
            BEGIN;
            SELECT setval(pg_get_serial_sequence('"country_language"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "country_language";
            SELECT setval(pg_get_serial_sequence('"region_language"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "region_language";
            SELECT setval(pg_get_serial_sequence('"user_group"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "user_group";
            SELECT setval(pg_get_serial_sequence('"user_user_permissions"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "user_user_permissions";
            COMMIT;
        """
        )
    ]
