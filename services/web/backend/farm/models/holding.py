from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings

from utils.models import GetAdminURLMixin, validate_latin1


class Holding(GetAdminURLMixin, models.Model):
    """A holding

    The whole area and all infrastructures included on it, covering the same or
    different "sites", under the control of an operator to perform
    agricultural or aquaculture activities. The holding includes one
    specialisation of ActivityComplex, ie. Activity. the values of
    ActivityType are expressed in conformity with the classification of the
    economic activity of the holding, according to the NACE rev. 2.0 coding
    Holding is a thematic extension of the generic Class “Activity Complex”
    shared with other thematic areas describing entities related with
    Economical Activities (Legal Entity Class – Business).

    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    """

    # From ActivityComplex

    id = models.CharField(
        primary_key=True,
        editable=True,
        max_length=256,
        verbose_name=_("identifier"),
        help_text=_("This identifier must be unique in FaST"),
        validators=[validate_latin1],
    )

    name = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_("name"),
    )

    description = models.TextField(verbose_name=_("description"), null=True, blank=True)

    def number_of_plots(self):
        from farm.models import Plot

        return Plot.objects.filter(
            site__holding_campaign__holding=self,
            site__holding_campaign__campaign__is_current=True,
        ).count()

    def number_of_sites(self):
        from farm.models import Site

        return Site.objects.filter(
            holding_campaign__holding=self,
            holding_campaign__campaign__is_current=True,
        ).count()

    def __str__(self):
        return f"{self.name} ({self.id})" if self.name else self.id

    class Meta:
        db_table = "holding"
        verbose_name = _("farm")
        verbose_name_plural = _("farms")
        help_text = _(
            "Agricultural holdings, covering one or several sites and one or several campaigns, under the control of an operator to perform agricultural activities."
        )

        permissions = [
            (
                "insert_demo_holding",
                "Can insert/update the demo holding for a given user",
            ),
        ]


class HoldingCampaign(GetAdminURLMixin, models.Model):

    id = models.AutoField(primary_key=True, editable=True, verbose_name=_("identifier"))

    holding = models.ForeignKey(
        "farm.Holding",
        null=False,
        blank=False,
        related_name="holding_campaigns",
        on_delete=models.CASCADE,
        verbose_name=_("farm"),
    )

    campaign = models.ForeignKey(
        "farm.Campaign",
        null=False,
        blank=False,
        related_name="holding_campaigns",
        on_delete=models.PROTECT,
        verbose_name=_("campaign"),
    )

    def __str__(self):
        return str(self.campaign) + " - " + str(self.holding)

    def number_of_plots(self):
        from farm.models import Plot

        return Plot.objects.filter(site__holding_campaign=self).count()

    number_of_plots.short_description = _("number of plots")

    def number_of_sites(self):
        from farm.models import Site

        return Site.objects.filter(holding_campaign=self).count()

    number_of_sites.short_description = _("number of sites")

    def number_of_plots_with_fertilization_plan(self):
        """Number of plots covered by a fertilization plan in this campaign"""
        from farm.models import Plot

        return Plot.objects.filter(
            site__holding_campaign=self, fertilization_plans__isnull=False
        ).count()

    number_of_plots_with_fertilization_plan.short_description = _(
        "number of plots with a fertilization plan"
    )

    class Meta:
        db_table = "holding_campaign"
        verbose_name = _("farm campaign")
        verbose_name_plural = _("farm campaigns")

        constraints = (
            models.UniqueConstraint(
                name="holding_campaign_holding_id_campaign_id_unique",
                fields=("holding", "campaign"),
            ),
        )


class RelatedPartyRole(models.TextChoices):
    """A role for a related party"""

    farmer = "farmer", _("farmer")
    advisor = "advisor", _("advisor")
    farmer_association = "farmer_association", _("farmer association")
    public_administration = "public_administration", _("public administration")


class UserRelatedParty(GetAdminURLMixin, models.Model):
    """A party (person) linked to an activity complex within a certain role"""

    id = models.AutoField(
        primary_key=True, editable=False, verbose_name=_("identifier")
    )

    holding = models.ForeignKey(
        to="farm.Holding",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="user_related_parties",
        verbose_name=_("farm"),
    )

    role = models.CharField(
        max_length=30,
        null=False,
        blank=False,
        default=RelatedPartyRole.farmer,
        choices=RelatedPartyRole.choices,
        verbose_name=_("role"),
    )

    user = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="user_related_parties",
        verbose_name=_("user"),
    )

    def __str__(self):
        return f"{self.user} - {self.holding}: {self.role}"

    class Meta:
        db_table = "user_related_party"
        verbose_name = _("farm member")
        verbose_name_plural = _("farm members")

        constraints = (
            models.UniqueConstraint(
                name="user_related_party_holding_id_user_id_unique",
                fields=["holding", "user"],
            ),
        )
