# `core/web/backend`: Administration Portal and authentication

- [`core/web/backend`: Administration Portal and authentication](#corewebbackend-administration-portal-and-authentication)
  - [Architecture](#architecture)
    - [Data model (ORM)](#data-model-orm)
    - [Caching with memcached](#caching-with-memcached)
  - [Authentication](#authentication)
  - [Administration Portal](#administration-portal)
  - [Data model](#data-model)
  - [Development setup](#development-setup)
    - [Prerequisites](#prerequisites)
    - [Environment variables](#environment-variables)
    - [Setup](#setup)
    - [Troubleshooting](#troubleshooting)
  - [Tests](#tests)

The web backend of FaST is built using the [Django](https://www.djangoproject.com/) framework, and manages mainly:
- the core data model of FaST: the models are defined in `**/models.py` files (see [Models](https://docs.djangoproject.com/en/3.2/topics/db/models/))
- the Administration Portal, built around the [Django Admin](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/) framework.
- authentication of GraphQL queries via an authentication web hook called by the [core/api_gateway](../../api_gateway/)
- authentication of media queries via an authentication web hook called by the [core/web/media](../media/)

## Architecture

The `backend` architecture as per **ORM** and **caching** can be summarized as follows:

```plantuml
package "FaST" as fast {
    storage "Cache storage\n\n- authentication\n- import/export" as memcached << memcached >> #yellow
    component "Django web backend\n""core/web/backend""" as webbackend << Backend >>  #yellow 
    storage "Media storage" as s3_media_storage  << S3 >> #yellow
    database "Postgres" as pg_fastplatform {
      folder "fastplatform" <<private data>> {
        [farm,\nplots,\nplant_species,…]
      }
    }

    database "Postgres" as pg_external {
      folder "external" <<public GIS data>>{
        [surface water,\nsoil estimates,\nNVZs,…]
      }
    }
}

webbackend <-left-> s3_media_storage
webbackend <-> memcached
webbackend <---> pg_fastplatform
webbackend <---> pg_external
```

### Data model (ORM)

`core/web/backend` serves as a repository, storing data in the 2 main data stores: `fastplatform` (=default) and `external`. 

`core/web/backend` leverages Django ORM to manage the data model. Objects are created as Python classes that inherit from the Django's [Model](https://docs.djangoproject.com/en/3.2/ref/models/base/#model-inheritance) class, and define any number of fields required for the business logic. They are then mapped to the database using the [Django ORM](https://docs.djangoproject.com/en/3.2/topics/db/models/#model-api), through migrations. The migrations are defined in `**/migrations/` folders.

### Caching with memcached

`memcached` caches the results of the authentication web hook for GraphQL queries. It is also used while uploading files using the [import/export](https://pypi.org/project/django-import-export/) functionality from the Administration Portal.

<img src="/services/web/backend/docs/static/docs/img/admin/import-export.png" title="Import / Export" class="img-docs" height="40" />


## Authentication

`backend` exposes an authentication web hook that is called by the [core/api_gateway](../../api_gateway/) and [core/web/media](../media/) web services. It authenticates GraphQL queries and media queries. It uses `memcached` to cache the authentication results.


```plantuml
package "Authentication" as fast {
    component """core/web/backend""\n\n Authentication" as webbackend << Backend REPLICA >>  #yellow 

    storage " " as memcached << memcached >> #yellow

    frame """core / web / media""" as media <<media>>
    cloud "Images" as images <<""Amazon S3"">>

    component """api_gateway/fastplatform""" as api_gateway << hasura >>
}


webbackend <-right-> memcached
webbackend <-left-> media
media <-left-> images
webbackend <-down-> api_gateway
```

## Administration Portal
`backend` also exposes the Administration Portal. It is built around the [Django Admin](https://docs.djangoproject.com/en/3.2/ref/contrib/admin/) framework. 

<img src="/services/web/backend/docs/static/docs/img/admin/backend-admin.png" title="Backend admin" class="img-docs" height="300" />

The Administration Portal allows admin users to manage the data model on all objects of the data stores. If a model is not directly accessible from the Administration Portal's main page (/admin/), it can always be accessed by clicking on the `View all FaST objects` link in the top menu.

<img src="/services/web/backend/docs/static/docs/img/admin/view-all-fast-objects.png" title="View all FaST objects" class="img-docs" height="40" />

## Data model

- [add_ons](add_ons/data_model.md)
- [authentication](authentication/data_model.md)
- [common](common/data_model.md)
- [configuration](configuration/data_model.md)
- [external](external/data_model.md)
- [farm](farm/data_model.md)
- [fieldbook](fieldbook/data_model.md)
- [messaging](messaging/data_model.md)
- [photos](photos/data_model.md)
- [soil](soil/data_model.md)

## Development setup

### Prerequisites

- Geospatial libraries required by GeoDjango: https://docs.djangoproject.com/en/3.0/ref/contrib/gis/install/geolibs/
- Python 3.8+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)
- Docker

### Environment variables

The web backend expects the following environment variables to be set:
- `DJANGO_SECRET_KEY`: a secret key for Django cryptographic features
- `AUTHENTICATION_API_GATEWAY_SERVICE_KEY`: a secret key for being authenticated under the "service" role. This secret key should be shared with services, within the backend, that wish to be granted "service" access rights.
- `POSTGRES_PASSWORD`: password to the main PostGIS database
- `POSTGRES_EXTERNAL_PASSWORD`: password to the external PostGIS database (storing public datasets)
- `S3_ACCESS_KEY`: access key to the S3 buckets to to store media files
- `S3_SECRET_KEY`: secret key to the S3 buckets to to store media files
- `DJANGO_MEMCACHED_PASSWORD`: password for the memcached cache

### Setup

Install [libev](http://software.schmorp.de/pkg/libev.html) first.

Create a Python virtualenv and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies (see [Troubleshooting](#troubleshooting) for certain issues regarding the installation of the `bjoern` package):
```bash
# install prod packages
pip install -r requirements.txt

# Optionally install dev packages (such as linting)
pip install -r requirements-dev.txt
```

Start the PostGIS databases:
```bash
# This will start 2 PostGIS servers (private data and public data) using Docker
make start-postgres
```

Start the cache (optional):
```bash
make start-memcached
```

Setup the PostGIS databases schemas:
```bash
make migrate
```

Pre-fill with regional data:
```bash
# Take <REGION> in [be-wal, bg, it-21, gr, ro, sk]
# for fastplatform data:
make clear-region init-region REGION=<REGION>

# for external data:
make clear-region-external init-region-external REGION=<REGION>
```

### Troubleshooting

Installing the `bjoern` package may require the following steps:

```bash
# Troubleshooting: if install problems for bjoern, install it separately
# https://github.com/jonashaag/bjoern/issues/101 
pip install --global-option=build_ext --global-option="-I/usr/local/include/" --global-option="-L/usr/local/lib" bjoern
```

## Tests
To launch tests, run the following command:

```bash
# Launch Full test suite run on all modules (can take more than 20 minutes)
make test

# Launch Single Django app test suite run:
make test APP=<name_of_app> # ex: make test APP=farm

# Launch Single method test suite run:
make test APP=<name_of_app> TEST_CASE=<test_case_class>

# Example, to launch only one set of tests and keep the DB afterwards
make test APP=add_ons TEST_CASE=AddOnCallbackTestCase KEEPDB=TRUE
```

- `APP=`: name of the Django app (optional)
- `TEST_CASE=`: name of the test case to run (optional)
- `KEEPDB=`: if true, the database will not be dropped after running the tests. Next execution will not start by running all migrations, and thus be significantly faster (optional)