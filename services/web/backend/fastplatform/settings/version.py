from pathlib import Path
from fastplatform.settings import BASE_DIR

VERSION = (Path(BASE_DIR) / "VERSION").read_text()
