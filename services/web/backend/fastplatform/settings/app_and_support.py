import os

WEB_APP_URL = os.environ.get("WEB_APP_URL", "https://app.beta.fastplatform.eu")
LOCAL_SUPPORT_EMAIL_ADDRESS = os.environ.get(
    "LOCAL_SUPPORT_EMAIL_ADDRESS", "support@fastplatform.eu"
)
DEV_SUPPORT_EMAIL_ADDRESS = os.environ.get(
    "DEV_SUPPORT_EMAIL_ADDRESS", "support@fastplatform.eu"
)
