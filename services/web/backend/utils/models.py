from django.utils.safestring import mark_safe
from django.urls import reverse
from django.templatetags.static import static
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from django.forms.models import model_to_dict
from decimal import Decimal


def validate_latin1(value):
    """Because of OpenTelemetry, we need to make sure that our URLs (and therefore our identifiers)
    contain only latin-1 characters
    """
    try:
        value.encode("latin1")
    except UnicodeEncodeError:
        raise ValidationError(
            mark_safe(
                _(
                    'This field must use only characters from the <a href="https://en.wikipedia.org/wiki/ISO/IEC_8859-1" target="_blank">latin1</a> charset.'
                )
            )
        )


class GetAdminURLMixin:
    def get_admin_url(self, action="change"):
        if self.pk:
            return reverse(
                "admin:%s_%s_%s"
                % (self._meta.app_label, self._meta.model_name, action),
                args=(self.pk,),
            )
        return None

    def get_admin_link(self, action="change"):
        if self.pk:
            url = self.get_admin_url(action)
            icon_url = static(f"admin/img/icon-{action}link.svg")
            edit = _("Edit this %(object_name)s") % {
                "object_name": self._meta.verbose_name
            }
            return mark_safe(f'<a href="{url}"><img src="{icon_url}">&nbsp;{edit}</a>')
        return ""


class TranslatableModel(models.Model):
    i18n = models.JSONField(null=True, blank=True, verbose_name=_("Translations"))

    def clean(self, *args, **kwargs) -> None:
        super().clean(*args, **kwargs)

        if self.i18n is not None:
            i18n = {}
            fields = self._meta.get_fields()
            fields = {
                f.attname: f
                for f in fields
                if isinstance(f, (models.CharField, models.TextField))
                and not f.primary_key
            }

            for key, value in self.i18n.items():
                if value is None:
                    continue
                tokens = key.split("_")
                field, language = "_".join(tokens[:-1]), tokens[-1]
                if language not in dict(settings.LANGUAGES):
                    raise ValidationError(
                        "Translations: language '{}' is not valid. Available languages are {}.".format(
                            language,
                            ", ".join([f"'{l[0]}'" for l in settings.LANGUAGES]),
                        )
                    )

                if field not in fields:
                    raise ValidationError(
                        "Translations: field '{}' is not valid. Available fields are {}.".format(
                            field,
                            ", ".join([f"'{f}'" for f in fields]),
                        )
                    )
                i18n[key] = value.strip()

            self.i18n = i18n

    class Meta:
        abstract = True


class ModelDiffMixin(models.Model):
    """
    http://stackoverflow.com/a/13842223
    https://github.com/sknutsonsf/python-contrib/blob/master/src/django/utils/ModelDiffMixin.py

    A model mixin that tracks model fields' values and provide useful API
    to know what fields have been changed.

    The main value is to allow simply changing the values and then saving the
    object only if it is "really changed"
    """

    def __init__(self, *args, **kwargs):
        super(ModelDiffMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        # original version did not deal with floating point rounding (decimal(9,6) in database)
        #  diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        diffs = {}
        for k, v1 in d1.items():
            v2 = d2[k]
            if isinstance(v1, Decimal):
                v1 = float(v1)
            if isinstance(v2, Decimal):
                v2 = float(v2)
            elif isinstance(v2, float) or isinstance(v1, float):
                # CAUTION: we assume the field is stored in db with 5 or more digits of precision
                # should really get the field definition and find the number of digits
                change = self.is_float_changed(v1, v2)
            else:
                change = v1 != v2
            if change:
                diffs[k] = (v1, v2)

        return dict(diffs)

    def is_float_changed(self, v1, v2):
        """Compare two floating point or decimal values to the proper precision
        Default precision is 5 digits
        Override this method if all float/decimal fields have fewer digits in database
        """
        return abs(round(v1 - v2, 5)) > 0.00001

    @property
    def has_changed(self):
        return bool(self.diff)

    @property
    def changed_fields(self):
        return self.diff.keys()

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(self, fields=[field.name for field in self._meta.fields])

    class Meta:
        abstract = True
