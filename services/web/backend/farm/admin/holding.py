from django.conf import settings
from django.contrib import admin
from django.contrib.gis.db.models import Extent
from django.utils.translation import gettext_lazy as _

from import_export.admin import ExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin
from utils.widgets.map_widget import MapWidget

from add_ons.models import AddOn
from farm.models import Holding, HoldingCampaign, Plot, UserRelatedParty
from farm.resources import HoldingResource
from farm.actions import sync_holding
from fieldbook.admin.fertilization import CustomFertilizerElementInline


class HoldingAddOnInline(admin.TabularInline):
    model = AddOn.holdings.through
    extra = 0
    verbose_name = _("subscribed add-on")
    verbose_name_plural = _("subscribed add-ons")
    fields = ["add_on", "subscribed_by", "subscribed_at"]
    readonly_fields = ["subscribed_at"]
    autocomplete_fields = ["subscribed_by"]


class HoldingUserRelatedPartyInline(admin.TabularInline):
    model = UserRelatedParty
    extra = 0
    fields = ["user", "role"]
    autocomplete_fields = ["user"]


class HoldingCampaignInline(admin.TabularInline):
    model = HoldingCampaign
    extra = 0
    fields = ["_link", "campaign"]
    readonly_fields = ["_link"]

    def _link(self, obj):
        if obj:
            return obj.get_admin_link()
        return ""

    _link.short_description = ""


class CustomFertilizerAdmin(CommonModelAdmin):
    list_display = ("name", "fertilizer_type", "organic")
    list_select_related = ("fertilizer_type",)
    fields = ("name", "organic", "created_at", "created_by")
    readonly_fields = ("created_at", "created_by")
    inlines = [CustomFertilizerElementInline]

    def save_model(self, request, obj, form, change):
        if not hasattr(obj, "created_by") or not obj.created_by:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)


class HoldingAdmin(ExportMixin, CommonModelAdmin):
    resource_class = HoldingResource
    search_fields = ("id", "name")
    list_display = ("id", "name")

    fieldsets = [
        [
            None,
            {
                "fields": [
                    "id",
                    "name",
                    "description",
                ]
            },
        ],
    ]

    def get_readonly_fields(self, request, obj):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            readonly_fields += ("id",)
        return readonly_fields

    def get_queryset(self, request):
        queryset = (
            super()
            .get_queryset(request)
            .prefetch_related("holding_campaigns")
            .prefetch_related("user_related_parties")
        )

        if settings.ENABLE_CUSTOM_FERTILIZER:
            queryset = queryset.prefetch_related("custom_fertilizers")

        if settings.ENABLE_SOIL_SITE:
            queryset = queryset.prefetch_related("soil_sites")

        return queryset

    # Map parameters
    # --------------
    static_map = True

    def get_map_extent(self, obj):
        return Plot.objects.filter(
            site__holding_campaign__holding=obj,
            site__holding_campaign__campaign__is_current=True,
        ).aggregate(Extent("geometry"))["geometry__extent"]

    def get_holding_campaign_id(self, obj):
        # For the map we use the current campaign
        holding_campaign_id = (
            HoldingCampaign.objects.filter(holding=obj, campaign__is_current=True)
            .values_list("id", flat=True)
            .first()
        )
        if holding_campaign_id:
            return holding_campaign_id

    class Media(MapWidget.Media):
        pass

    # Actions
    # ---------------------------------------

    # Action to manually synchronize the plots of this farm
    def sync_holding_action(self, request, obj):
        if settings.ENABLE_SYNC_HOLDING_FROM_ADMIN and (
            request.user.has_perm("farm.sync_holding") or request.user.is_superuser
        ):
            sync_holding(request, obj)

    sync_holding_action.label = _("Synchronize plots with IACS")
    sync_holding_action.short_description = _(
        "This will download the plots of this farm from the IACS system"
    )

    # Fix for this bug: https://github.com/crccheck/django-object-actions/issues/25
    change_actions = ["sync_holding_action"]

    def get_change_actions(self, request, object_id, form_url):
        # Only show the 'sync holding' action if the user has the right permission
        actions = []
        if settings.ENABLE_SYNC_HOLDING_FROM_ADMIN and (
            request.user.has_perm("farm.sync_holding") or request.user.is_superuser
        ):
            actions += ["sync_holding_action"]
        return actions

    inlines = [HoldingCampaignInline, HoldingUserRelatedPartyInline, HoldingAddOnInline]


# TODO Create those inlines
# if settings.ENABLE_CUSTOM_FERTILIZER:
#     HoldingAdmin.inlines += [CustomFertilizerInline]
# if settings.ENABLE_SOIL_SITE:
#     HoldingAdmin.inlines += [SoilSiteInline]


admin_site.register(Holding, HoldingAdmin)
