import re

from django.contrib.gis.db import models
from django.contrib.auth.models import AbstractUser, Group as DjangoGroup
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator

from user_sessions.models import Session
from oidc_provider.models import Token

from utils.models import GetAdminURLMixin


class Country(GetAdminURLMixin, models.Model):

    iso_code = models.CharField(
        max_length=2,
        primary_key=True,
        validators=[RegexValidator(regex=r"[a-z0-9]{2}")],
        verbose_name=_("ISO code"),
        help_text=_("The ISO code of the country as per ISO 3166-1 alpha-2"),
    )

    name = models.TextField(
        verbose_name=_("name"),
        help_text=_("The name of the country in its local language"),
    )

    default_language = models.ForeignKey(
        to="authentication.Language",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="countries_as_default",
        verbose_name=_("default language"),
        help_text=_("The default/official language of the country"),
    )

    languages = models.ManyToManyField(
        to="authentication.Language",
        blank=True,
        db_table="country_language",
        verbose_name=_("languages"),
        help_text=_("The acceptable languages for this country"),
    )

    def __str__(self):
        return f"{self.name} ({self.iso_code})"

    class Meta:
        ordering = ("iso_code",)
        db_table = "country"
        verbose_name = _("country")
        verbose_name_plural = _("countries")
        constraints = (
            models.CheckConstraint(
                name="country_iso_code_must_follow_iso_3166_1_alpha_2",
                check=models.Q(iso_code__regex=r"[a-z0-9]{2}"),
            ),
        )


class Region(GetAdminURLMixin, models.Model):

    id = models.CharField(
        max_length=6,
        primary_key=True,
        validators=[
            RegexValidator(
                regex=r"[a-z0-9]{2}-[a-z0-9]{2,3}",
                message="ISO code must be country ISO code dash region ISO code",
            )
        ],
        verbose_name=_("identifier"),
        help_text=_(
            "The region identifier, as XX-YYY, where XX is the ISO 3166-1 alpha 2 code of the country and YY is the ISO 3166-2 alpha 2 code of the region."
        ),
    )

    iso_code = models.CharField(
        max_length=3,
        null=False,
        blank=False,
        db_index=True,
        validators=[
            RegexValidator(
                regex=r"[a-z0-9]{2,3}",
                message="ISO code must be a valid ISO 3166-2 alpha 2 code",
            )
        ],
        verbose_name=_("ISO code"),
        help_text=_("The ISO 3166-2 alpha 2 code of the region"),
    )

    name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_("name"),
        help_text=_("The region name in its local language"),
    )

    country = models.ForeignKey(
        to="authentication.Country",
        on_delete=models.PROTECT,
        related_name="regions",
        null=False,
        blank=False,
        verbose_name=_("country"),
        help_text=_("The country this region belongs in"),
    )

    default_language = models.ForeignKey(
        to="authentication.Language",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="regions_as_default",
        verbose_name=_("default language"),
        help_text=_("The default/official language of the region"),
    )

    languages = models.ManyToManyField(
        to="authentication.Language",
        blank=True,
        db_table="region_language",
        verbose_name=_("languages"),
        help_text=_("The acceptable languages of the region"),
    )

    def __str__(self):
        return f"{self.name} ({self.id})"

    class Meta:
        ordering = ("iso_code",)
        db_table = "region"
        verbose_name = _("region")
        verbose_name_plural = _("regions")
        constraints = (
            models.CheckConstraint(
                name="region_id_must_be_country_iso2_dash_region_iso2",
                check=models.Q(id__regex=r"[a-z0-9]{2}-[a-z0-9]{2,3}"),
            ),
            models.CheckConstraint(
                name="region_iso_code_must_follow_iso_3166_2",
                check=models.Q(iso_code__regex=r"[a-z0-9]{2,3}"),
            ),
        )


class Language(GetAdminURLMixin, models.Model):
    """A language available in the system"""

    iso_code = models.CharField(
        primary_key=True,
        max_length=2,
        validators=[
            RegexValidator(
                regex=r"[a-z0-9]{2}",
                message=_("ISO code must be a valid ISO 639-1 code"),
            )
        ],
        verbose_name=_("ISO code"),
        help_text=_("The ISO 639-1 code of the language"),
    )

    name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_("name"),
        help_text=_("The name of the language"),
    )

    def __str__(self):
        return f"{self.name} ({self.iso_code})"

    class Meta:
        ordering = ("iso_code",)
        db_table = "language"
        verbose_name = _("language")
        verbose_name_plural = _("languages")


def user_default_additional_data():
    return {"placeholder": True}


class User(GetAdminURLMixin, AbstractUser):
    """A user of the FaST platform

    Can be a farmer, advisor or PA staff, or all at once
    """

    id = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("identifier")
    )

    username = models.CharField(
        primary_key=True,
        error_messages={"unique": _("A user with that username already exists.")},
        help_text=_(
            "Required. 255 characters or fewer. "
            "Letters, digits and @/./+/-/_ only. "
            "For users authenticated through a third party service, this username "
            "is determined by the the third party service. The username MUST be "
            "unique accross the FaST environment."
        ),
        max_length=255,
        verbose_name=_("username"),
        validators=[
            RegexValidator(
                regex=re.compile(r"^[a-zA-Z0-9\.\-_@]{1,255}$"),
                message=_(
                    'Username can contain only letters ("a" to "z" and "A" to "Z"), '
                    'digits ("0" to "9"), dashes ("-"), underscore ("_"), at sign ("@") and dots ("."),'
                    "and contain no more than 255 characters."
                ),
            )
        ],
    )

    first_name = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_("first name"),
        help_text=_("The first name(s) of the user"),
    )

    last_name = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_("last name"),
        help_text=_("The last name(s) of the user"),
    )

    name = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name=_("full name"),
        help_text=_(
            "The full name of the user. This field is not computed based on first "
            "name and last name, and must be filled manually."
        ),
    )

    region = models.ForeignKey(
        to="authentication.Region",
        blank=True,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("region"),
        help_text=_("The region this user belongs to"),
    )

    picture = models.ImageField(
        blank=True,
        null=True,
        upload_to="user/picture/%Y/%m/%d/",
        verbose_name=_("picture"),
    )

    preferred_language = models.ForeignKey(
        to="authentication.Language",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        related_name="user_as_preferred_language",
        verbose_name=_("preferred language"),
    )

    additional_data = models.JSONField(
        default=user_default_additional_data,
        null=True,
        blank=True,
        verbose_name=_("additional data"),
        help_text=_(
            "Additional data stored for the user, as a key-value JSON, usually to "
            "store region-specific authentication data."
        ),
    )

    email = models.EmailField(
        null=True,
        blank=True,
        verbose_name=_("email"),
        help_text=_(
            "Email address of the user, if provided by the third-party "
            "authentication system or by the user"
        ),
    )

    groups = models.ManyToManyField(
        to="authentication.Group",
        blank=True,
        db_table="user_group",
        related_name="users",
        verbose_name=_("groups"),
    )

    def __str__(self):
        if self.name is not None:
            return f"{self.name} ({self.username})"
        elif self.first_name is not None and self.last_name is not None:
            return f"{self.first_name} {self.last_name} ({self.username})"
        else:
            return f"{self.username}"

    class Meta:
        ordering = ("username",)
        db_table = "user"
        verbose_name = _("User")
        verbose_name_plural = _("Users")
        help_text = _(
            "All the FaST users (farmers, advisors, Paying Agency staff, etc) are in a single list, with varying access rights. Some users can have both farmer/advisor and staff roles."
        )

        constraints = (
            models.CheckConstraint(
                name="user_username_must_be_letters_digits_etc",
                check=models.Q(username__regex=r"^[a-zA-Z0-9\.\-_@]{1,255}$"),
            ),
        )

    def save(self, *args, **kwargs):
        self.id = self.username
        super().save(*args, **kwargs)

    def remove_all_sessions(self):
        user_sessions = []
        for session in Session.objects.all():
            if str(self.pk) == session.get_decoded().get("_auth_user_id"):
                user_sessions.append(session.pk)
        return Session.objects.filter(pk__in=user_sessions).delete()

    def remove_all_tokens(self):
        user_tokens = Token.objects.filter(user=self)
        return user_tokens.delete()

    def logout(self):
        # delete all user's sessions
        self.remove_all_sessions()
        # delete all user's tokens
        self.remove_all_tokens()


class Group(GetAdminURLMixin, DjangoGroup):
    class Meta:
        proxy = True
        verbose_name = _("group")
        verbose_name_plural = _("groups")
        help_text = _(
            "Groups are used to consistently apply access rights to staff users "
            "who have the same usage of the management portal."
        )

