import hashlib
import hmac
import re
from datetime import datetime
from urllib.parse import urlparse

from django.conf import settings
from django.http import HttpResponseForbidden, HttpResponse
from django.http.response import HttpResponseBadRequest
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.db.models import Q

from authentication.models import User
from add_ons.models import AddOn

from authentication.views.utils import identify

MEDIA_AUTH_WEBHOOK_WHITELIST = []

# Geo-tagged photos


def check_photo_access(identity, holding_id, campaign_id, filename):
    if identity["X-Hasura-Role"] == "farmer":
        # The user must be related party to the holding and be active
        return User.objects.filter(
            id=identity["X-Hasura-User-Id"],
            is_active=True,
            user_related_parties__holding__id=holding_id,
        ).exists()

    elif identity["X-Hasura-Role"] == "add_on":
        return (
            # The add-on must be active
            AddOn.objects.filter(
                id=identity["X-Hasura-AddOn-Id"],
                is_active=True,
                provider__is_active=True,
            )
            # The holding must be subscribed to the add-on
            .filter(
                Q(add_on_subscriptions__holding__id=holding_id) | Q(auto_subscribe=True)
            )
            # The add-on must have the view permission on the geo-tagged photo objects
            .filter(
                Q(permissions__codename="view_geotaggedphoto")
                | Q(groups__permissions__codename="view_geotaggedphoto")
            ).exists()
        )

    elif identity["X-Hasura-Role"] == "service":
        return True


def check_photo_thumbnail_access(identity, holding_id, campaign_id, filename):
    return check_photo_access(identity, holding_id, campaign_id, filename)


MEDIA_AUTH_WEBHOOK_WHITELIST += [
    (
        "photos/holding/(?P<holding_id>[^/]+)/campaign/(?P<campaign_id>[^/]+)/photo/(?P<filename>[^/]+)",
        check_photo_access,
    ),
    (
        "photos/holding/(?P<holding_id>[^/]+)/campaign/(?P<campaign_id>[^/]+)/thumbnail/(?P<filename>[^/]+)",
        check_photo_thumbnail_access,
    ),
]

# Logos


def _simple_check_is_active(identity):
    if identity["X-Hasura-Role"] == "farmer":
        # The user must be active
        return User.objects.filter(
            id=identity["X-Hasura-User-Id"], is_active=True
        ).exists()

    elif identity["X-Hasura-Role"] == "add_on":
        # The add-on must be active
        return AddOn.objects.filter(
            id=identity["X-Hasura-AddOn-Id"],
            is_active=True,
            provider__is_active=True,
        )

    elif identity["X-Hasura-Role"] == "service":
        return True


def check_provider_logo_access(identity, filename):
    return _simple_check_is_active(identity)


def check_add_on_logo_access(identity, filename):
    return _simple_check_is_active(identity)


def check_federated_provider_logo_access(identity, filename):
    return _simple_check_is_active(identity)


MEDIA_AUTH_WEBHOOK_WHITELIST += [
    ("provider/logo/(?P<filename>[^/]+)", check_provider_logo_access),
    ("add_on/logo/(?P<filename>[^/]+)", check_add_on_logo_access),
    (
        "federated_provider/logo/(?P<filename>[^/]+)",
        check_federated_provider_logo_access,
    ),
]

# User profile pictures


def check_user_picture_access(identity, user_id, filename):
    if identity["X-Hasura-Role"] == "farmer":
        # The user must be related party to the holding
        return User.objects.filter(
            id=identity["X-Hasura-User-Id"], is_active=True
        ).exists()

    elif identity["X-Hasura-Role"] == "add_on":
        # TODO maybe allow access later but not needed for now
        return False

    elif identity["X-Hasura-Role"] == "service":
        return True


MEDIA_AUTH_WEBHOOK_WHITELIST += [
    (
        "authentication/user/(?P<user_id>[^/]+)/picture/(?P<filename>[^/]+)",
        check_user_picture_access,
    ),
]

# Pre-compile the regexes
MEDIA_AUTH_WEBHOOK_WHITELIST = [
    (re.compile(rule), _) for rule, _ in MEDIA_AUTH_WEBHOOK_WHITELIST
]


class MediaAuthWebhookView(View):
    """Authenticates requests to the media files hosted on S3

    This webhook is called by the web/media core service (an NGINX reverse proxy)
    that will forward signed requests to S3 if the request is authenticated and 
    authorized.

    Uses part of the logic defined for the Hasura (API gateway) webhook, like the roles
    "farmer", "add_on" and "service"
    """

    http_method_names = ["get"]
    
    allowed_roles = ["farmer", "add_on", "service"]

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(MediaAuthWebhookView, self).dispatch(request, *args, **kwargs)

    def get(self, request):

        # Return a 200 to the OPTIONS pre-flight
        if request.META["HTTP_X_ORIGINAL_METHOD"] == "OPTIONS":
            return HttpResponse(status=200)

        if request.META["HTTP_X_ORIGINAL_METHOD"] != "GET":
            return HttpResponseForbidden()

        # Identify the user/add-on/service
        identity = identify(request)

        if identity is None:
            return HttpResponseForbidden()

        if identity["X-Hasura-Role"] not in self.allowed_roles:
            return HttpResponseForbidden()

        original_uri = request.META["HTTP_X_ORIGINAL_URI"]
        path = urlparse(original_uri).path

        # The path must start with /AWS_STORAGE_BUCKET_NAME/AWS_LOCATION/
        prefix = f"/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/"
        if not path.startswith(prefix):
            return HttpResponseForbidden()

        # Remove the path prefix
        path = path[len(prefix):]

        can_access = None
        for regex, check_access in MEDIA_AUTH_WEBHOOK_WHITELIST:
            r = re.match(regex, path)
            if r is not None:
                can_access = bool(check_access(identity, **r.groupdict()))
                break

        # If there was a match but access was denied
        if not can_access:
            return HttpResponseForbidden()

        # Now that access is granted, build the S3 signature

        # Create a date for headers and the credential string
        t = datetime.utcnow()
        amzdate = t.strftime("%Y%m%dT%H%M%SZ")
        datestamp = t.strftime("%Y%m%d")  # Date w/o time, used in credential scope
        method = request.META["HTTP_X_ORIGINAL_METHOD"]
        request_parameters = ""
        host = settings.S3_URL.split("https://")[1]
        if host.find(":") != -1:
            host = host.split(":")[0]

        # ************* TASK 1: CREATE A CANONICAL REQUEST *************
        # http://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html

        # Step 1 is to define the verb (GET, POST, etc.)--already done.

        # Step 2: Create canonical URI--the part of the URI from domain to query
        # string (use '/' if no path)
        canonical_uri = original_uri
        # Step 3: Create the canonical query string. In this example (a GET request),
        # request parameters are in the query string. Query string values must
        # be URL-encoded (space=%20). The parameters must be sorted by name.
        # For this example, the query string is pre-formatted in the request_parameters variable.
        canonical_querystring = request_parameters

        # Step 6: Create payload hash (hash of the request body content). For GET
        # requests, the payload is an empty string ("").
        # In obs, we need to add 'x-amz-content-sha256' to canonical_headers
        payload_hash = hashlib.sha256(("").encode("utf-8")).hexdigest()

        # Step 4: Create the canonical headers and signed headers. Header names
        # must be trimmed and lowercase, and sorted in code point order from
        # low to high. Note that there is a trailing \n.
        canonical_headers = (
            ("host:" + host + "\n")
            + ("x-amz-content-sha256:" + payload_hash + "\n")
            + ("x-amz-date:" + amzdate + "\n")
        )
        # Step 5: Create the list of signed headers. This lists the headers
        # in the canonical_headers list, delimited with ";" and in alpha order.
        # Note: The request can include any headers; canonical_headers and
        # signed_headers lists those that you want to be included in the
        # hash of the request. "Host" and "x-amz-date" are always required.
        signed_headers = "host;x-amz-content-sha256;x-amz-date"

        # Step 7: Combine elements to create canonical request
        canonical_request = (
            (method + "\n")
            + (canonical_uri + "\n")
            + (canonical_querystring + "\n")
            + (canonical_headers + "\n")
            + (signed_headers + "\n")
            + payload_hash
        )

        # ************* TASK 2: CREATE THE STRING TO SIGN*************
        # Match the algorithm to the hashing algorithm you use, either SHA-1 or
        # SHA-256 (recommended)
        algorithm = "AWS4-HMAC-SHA256"
        credential_scope = (
            datestamp
            + "/"
            + settings.S3_REGION
            + "/"
            + settings.S3_SERVICE
            + "/"
            + "aws4_request"
        )
        string_to_sign = (
            (algorithm + "\n")
            + (amzdate + "\n")
            + (credential_scope + "\n")
            + hashlib.sha256(canonical_request.encode("utf-8")).hexdigest()
        )

        # ************* TASK 3: CALCULATE THE SIGNATURE *************
        # Create the signing key using the function defined above.
        signing_key = getSignatureKey(
            settings.S3_SECRET_KEY, datestamp, settings.S3_REGION, settings.S3_SERVICE
        )

        # Sign the string_to_sign using the signing_key
        signature = hmac.new(
            signing_key, (string_to_sign).encode("utf-8"), hashlib.sha256
        ).hexdigest()

        # ************* TASK 4: ADD SIGNING INFORMATION TO THE REQUEST *************
        # The signing information can be either in a query string value or in
        # a header named Authorization. This code shows how to use a header.
        # Create authorization header and add to request headers
        authorization_header = (
            (algorithm + " ")
            + ("Credential=" + settings.S3_ACCESS_KEY + "/" + credential_scope + ", ")
            + ("SignedHeaders=" + signed_headers + ", ")
            + ("Signature=" + signature)
        )

        response = HttpResponse(status=200)
        response["Authorization"] = authorization_header
        response["host"] = host
        response["x-amz-content-sha256"] = payload_hash
        response["x-amz-date"] = amzdate

        return response


# Key derivation functions. See:
# http://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html#signature-v4-examples-python
def sign(key, msg):
    return hmac.new(key, msg.encode("utf-8"), hashlib.sha256).digest()


def getSignatureKey(key, dateStamp, regionName, serviceName):
    kDate = sign(("AWS4" + key).encode("utf-8"), dateStamp)
    kRegion = sign(kDate, regionName)
    kService = sign(kRegion, serviceName)
    kSigning = sign(kService, "aws4_request")
    return kSigning
