import uvicorn
import logging
from fastapi import FastAPI
from app.settings import config
from app.tracing import Tracing
from app.api.router import pdf_generator

# FastAPI
app = FastAPI()

app.include_router(pdf_generator.router, prefix="")

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
Tracing.init(app)

@app.on_event("startup")
async def startup():
    logger.debug(config)

    # Debuging instrumentation
    if config.REMOTE_DEBUG:
        import debugpy
        debugpy.listen(5678)


#@app.on_event("shutdown")
#async def shutdown():
#	logger.debug(config)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8888)
