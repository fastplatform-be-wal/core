import logging

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import Permission
from django.utils import timezone
from django.db import models
from django.db.models import Q

from add_ons.models import AddOn
from authentication.models import User
from oidc_provider.models import Token


logger = logging.getLogger(__name__)


def identify(request):
    # Check if an authorization header is present
    # If not return a 401 Unauthorized
    if "HTTP_AUTHORIZATION" in request.META.keys():
        authorization = request.META.get("HTTP_AUTHORIZATION")
    else:
        logger.info("Missing Authorization header")
        return None

    try:
        authorization_type, authorization_value = authorization.split(" ")
    except ValueError:
        logger.info("%s: wrong format of Authorization header", authorization)
        return None

    identity = {"X-Hasura-Date": timezone.now().isoformat()}

    if "HTTP_USER_LANGUAGE" in request.META.keys():
        identity["X-Hasura-Language"] = request.META.get("HTTP_USER_LANGUAGE")

    authorization_type = authorization_type.lower()

    if authorization_type == "bearer":
        try:
            user = User.objects.get(
                is_active=True,
                token__access_token=Token.hash_token(authorization_value),
                token__access_expires_at__gt=timezone.now(),
            )
        except ObjectDoesNotExist:
            logger.info("Active user was not found with non-expired token")
            return None

        identity.update(
            {
                "X-Hasura-User-Id": user.id,
                "X-Hasura-Role": "farmer",
            }
        )
        return identity

    elif authorization_type == "apikey":
        try:
            add_on = AddOn.objects.filter(
                models.Q(provider__is_active=True) | models.Q(provider__isnull=True)
            ).get(
                is_active=True,
                api_keys__key=authorization_value,
            )
        except ObjectDoesNotExist:
            logger.info(
                "%s: active add-on was not found with this API key", authorization
            )
            return None

        # Combine addon and addon_group permission codenames
        add_on_permissions = set(
            Permission.objects.filter(
                Q(add_ons=add_on) | Q(group__add_ons=add_on)
            ).values_list("codename", flat=True)
        )

        # Arrays in Postgres are written like "{1,2,3}"
        x_hasura_addon_permissions = f"{{{','.join(add_on_permissions)}}}"

        identity.update(
            {
                "X-Hasura-AddOn-Id": add_on.id,
                "X-Hasura-Role": "add_on",
                "X-Hasura-AddOn-Permissions": x_hasura_addon_permissions,
            }
        )

        print("identity", identity)





        return identity

    # If it is a service
    elif authorization_type == "service":

        if authorization_value == settings.AUTHENTICATION_API_GATEWAY_SERVICE_KEY:
            identity.update({"X-Hasura-Role": "service"})

            # Optional forcing
            forced_role = request.META.get("HTTP_X_HASURA_ROLE", None)

            if forced_role is not None:
                if forced_role == "farmer":
                    forced_user_id = request.META.get("HTTP_X_HASURA_USER_ID", None)
                    if forced_user_id is not None:
                        # If the user_id is forced, check that the user exists
                        try:
                            User.objects.get(pk=forced_user_id)
                        except ObjectDoesNotExist:
                            logger.info(
                                "%s: forced user_id %s does not exist",
                                authorization,
                                forced_user_id,
                            )
                            return None

                        identity.update(
                            {
                                "X-Hasura-Role": "farmer",
                                "X-Hasura-User-Id": forced_user_id,
                            }
                        )
                        return identity
                    else:
                        # If the role is forced to "farmer" then the user_id MUST be
                        # forced as well
                        logger.info(
                            "%s: missing user_id for forced role farmer", authorization
                        )
                        return None
                elif forced_role == "add_on":
                    forced_add_on_id = request.META.get("HTTP_X_HASURA_ADDON_ID", None)
                    if forced_add_on_id is not None:
                        # If the add_on_id is forced, check that the add-on exists
                        try:
                            add_on: AddOn = AddOn.objects.get(pk=forced_add_on_id)
                        except ObjectDoesNotExist:
                            logger.info(
                                "%s: forced add_on_id %s does not exist",
                                authorization,
                                forced_add_on_id,
                            )
                            return None

                        # Combine addon and addon_group permission codenames
                        add_on_permissions = set(
                            Permission.objects.filter(
                                Q(add_ons=add_on) | Q(group__add_ons=add_on)
                            ).values_list("codename", flat=True)
                        )

                        # Arrays in Postgres are written like "{1,2,3}"
                        x_hasura_addon_permissions = (
                            f"{{{','.join(add_on_permissions)}}}"
                        )

                        identity.update(
                            {
                                "X-Hasura-Role": "add_on",
                                "X-Hasura-AddOn-Id": forced_add_on_id,
                                "X-Hasura-AddOn-Permissions": x_hasura_addon_permissions,
                            }
                        )
                        return identity
                    else:
                        # If the role is forced to "addon" then the add_on_id MUST be
                        # forced as well
                        logger.info(
                            "%s: missing add_on_id for forced role add_on",
                            authorization,
                        )
                        return None
                else:
                    logger.info(
                        "%s: role %s cannot be forced, must be farmer or add_on",
                        authorization,
                        forced_role,
                    )
                    return None
            else:
                return identity
        else:
            logger.info("%s: wrong service key", authorization)
            return None

    elif authorization_type == "staff":
        # Checks if session_key corresponds to an existing session and the user is staff/superuser + active
        try:
            user = (
                User.objects.filter(
                    session__session_key=authorization_value,
                    is_active=True,
                    session__expire_date__gt=timezone.now(),
                )
                .filter(models.Q(is_staff=True) | models.Q(is_superuser=True))
                .get()
            )
        except ObjectDoesNotExist:
            logger.info(
                "%s: active user was not found with this session key", authorization
            )
            return None

        identity.update({"X-Hasura-User-Id": user.id, "X-Hasura-Role": "staff"})
        return identity

    logger.info("%s: unknown authorization type %s", authorization, authorization_type)
    return None
