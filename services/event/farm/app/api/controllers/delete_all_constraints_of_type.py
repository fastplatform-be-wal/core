import logging

from opentelemetry import trace
from gql import gql
from app.settings import config
from app.db.graphql_clients import fastplatform

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def delete_all_constraints_of_type(constraint_type: str):
    """
    Delete all constraints from db fastplatform
    whose `name` value equals `constraint_type` argument.

    Args:
        constraint_type (str): can be:
            - water_course
            - surface_water
            - protected_site
            - management_restriction_or_regulation_zone

    Returns:
        affected_rows
    """

    with tracer().start_as_current_span("delete_all_constraints_of_type"):
        logger.info("Deleting all constraints of type %s", constraint_type)
        mutation = (
            config.API_DIR
            / "controllers"
            / "graphql"
            / "mutation_delete_all_constraints_of_type.graphql"
        ).read_text()
        variables = {
            "constraint_type": constraint_type,
        }

        return await fastplatform.execute(gql(mutation), variables)
