from django.test import TestCase, override_settings
from django.conf import settings

from utils.tests.test_admin import AdminTestCaseMixin

from fieldbook.models import (
    CustomFertilizer,
    RegisteredFertilizer,
    CustomFertilizerElement,
    RegisteredFertilizerElement,
    ChemicalElement,
    FertilizerType,
    FertilizationPlan,
    PlantSpeciesGroup,
    PlantSpecies,
    PlantVariety,
)

from fieldbook.tests.recipes import (
    fertilization_plan_recipe,
    chemical_element_recipe,
    custom_fertilizer_recipe,
    registered_fertilizer_recipe,
    custom_fertilizer_element_recipe,
    registered_fertilizer_element_recipe,
    fertilizer_type_recipe,
    plant_species_group_recipe,
    plant_species_recipe,
    plant_variety_recipe,
)


@override_settings(AXES_ENABLED=False)
class CustomFertilizerAdminTestCase(AdminTestCaseMixin, TestCase):
    model = CustomFertilizer
    recipe = custom_fertilizer_recipe
    enable = settings.ENABLE_CUSTOM_FERTILIZER


@override_settings(AXES_ENABLED=False)
class RegisteredFertilizerAdminTestCase(AdminTestCaseMixin, TestCase):
    model = RegisteredFertilizer
    recipe = registered_fertilizer_recipe
    enable = settings.ENABLE_REGISTERED_FERTILIZER


@override_settings(AXES_ENABLED=False)
class CustomFertilizerElementAdminTestCase(AdminTestCaseMixin, TestCase):
    model = CustomFertilizerElement
    recipe = custom_fertilizer_element_recipe
    enable = settings.ENABLE_CUSTOM_FERTILIZER


@override_settings(AXES_ENABLED=False)
class RegisteredFertilizerElementAdminTestCase(AdminTestCaseMixin, TestCase):
    model = RegisteredFertilizerElement
    recipe = registered_fertilizer_element_recipe
    enable = settings.ENABLE_REGISTERED_FERTILIZER


@override_settings(AXES_ENABLED=False)
class ChemicalElementAdminTestCase(AdminTestCaseMixin, TestCase):
    model = ChemicalElement
    recipe = chemical_element_recipe
    enable = settings.ENABLE_CUSTOM_FERTILIZER or settings.ENABLE_REGISTERED_FERTILIZER


@override_settings(AXES_ENABLED=False)
class FertilizerTypeAdminTestCase(AdminTestCaseMixin, TestCase):
    model = FertilizerType
    recipe = fertilizer_type_recipe
    enable = settings.ENABLE_CUSTOM_FERTILIZER or settings.ENABLE_REGISTERED_FERTILIZER


@override_settings(AXES_ENABLED=False)
class FertilizationPlanAdminTestCase(AdminTestCaseMixin, TestCase):
    model = FertilizationPlan
    recipe = fertilization_plan_recipe


@override_settings(AXES_ENABLED=False)
class PlantSpeciesGroupAdminTestCase(AdminTestCaseMixin, TestCase):
    model = PlantSpeciesGroup
    recipe = plant_species_group_recipe


@override_settings(AXES_ENABLED=False)
class PlantSpeciesAdminTestCase(AdminTestCaseMixin, TestCase):
    model = PlantSpecies
    recipe = plant_species_recipe


@override_settings(AXES_ENABLED=False)
class PlantVarietyAdminTestCase(AdminTestCaseMixin, TestCase):
    model = PlantVariety
    recipe = plant_variety_recipe
