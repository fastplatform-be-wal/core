#!/busybox/sh

set -o errexit
set -o nounset
set -o pipefail


NAMESPACE=${NAMESPACE:-default}

PGBACKREST_BACKUP_TYPE=${PGBACKREST_BACKUP_TYPE:-full}
PGBACKREST_BACKUP_TYPE=$(echo ${PGBACKREST_BACKUP_TYPE} | tr '[:upper:]' '[:lower:]')

# Set the pgbackrest backup options
PGBACKREST_BACKUP_OPTS="--type=${PGBACKREST_BACKUP_TYPE}"
PGBACKREST_BACKUP_RETENTION=${PGBACKREST_BACKUP_RETENTION:-}
if [ ! -z ${PGBACKREST_BACKUP_RETENTION} ] && [ ${PGBACKREST_BACKUP_RETENTION} -eq ${PGBACKREST_BACKUP_RETENTION} ]; then # check if it's an integer (in ash)
  PGBACKREST_BACKUP_OPTS="${PGBACKREST_BACKUP_OPTS} --repo1-retention-${PGBACKREST_BACKUP_TYPE}=${PGBACKREST_BACKUP_RETENTION}"
fi
echo "[+] PGBACKREST_BACKUP_OPTS=${PGBACKREST_BACKUP_OPTS}"

PGCLUSTERS=$(echo ${PGCLUSTERS:-} | tr -d '[[:space:]]' | tr -s ',' ' ' )


# Start a backup task for each Postgres cluster
for pgcluster in $PGCLUSTERS; do
  pgtask_name=backrest-backup-$pgcluster-${PGBACKREST_BACKUP_TYPE}
  kubectl -n $NAMESPACE delete pgtask $pgtask_name &> /dev/null || true
  kubectl -n $NAMESPACE delete job $pgtask_name &> /dev/null || true
  pgbackrest_repo_podname=$(kubectl -n $NAMESPACE get pod -l pg-cluster=$pgcluster,pgo-backrest-repo=true -o NAME | sed 's,.*/,,g')
  echo -n "[+] starting a backup task for '$pgcluster Postgres cluster' ... "
  output=$(cat << EOF | kubectl -n $NAMESPACE apply -f -
apiVersion: crunchydata.com/v1
kind: Pgtask
metadata:
  labels:
    pg-cluster: $pgcluster
    pgouser: admin
  name: $pgtask_name
  namespace: $NAMESPACE
spec:
  name: $pgtask_name
  parameters:
    backrest-command: backup
    backrest-opts: $PGBACKREST_BACKUP_OPTS
    backrest-s3-verify-tls: "true"
    backrest-storage-type: ""
    containername: database
    job-name: $pgtask_name
    pg-cluster: $pgcluster
    podname: $pgbackrest_repo_podname
  tasktype: backrest
EOF
)
  echo OK 
  echo "[+]   --> $output"
done

# Wait for each backup task to succeed
for pgcluster in $PGCLUSTERS; do 
  timeout=18000
  pgtask_name=backrest-backup-$pgcluster-${PGBACKREST_BACKUP_TYPE}
  echo -n "[+] waiting for backup of '$pgcluster' Postgres cluster to complete (timeout=$timeout) ... "
  while ! kubectl wait -n $NAMESPACE jobs $pgtask_name --for condition=Complete >/dev/null 2>&1; do
    sleep 1;
    timeout=$((timeout-1))
    if [[ $timeout -le 0 ]]; then
      echo "timeout !"
      return 1
    fi
  done
  echo OK
done
