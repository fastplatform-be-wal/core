from pathlib import Path

from django.db import models, migrations

MIGRATIONS_DIR = Path(__file__).parent


class Migration(migrations.Migration):

    dependencies = [("external", "0008_set_db_comments_external")]

    # create function to get nearest soil sites for a geometry.

    operations = [
        migrations.RunSQL(
            (MIGRATIONS_DIR / "create_soil_site_function.sql").read_text()
        ),
    ]