import os

DEFAULT_FILE_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"

S3_URL = os.environ.get("S3_URL", "http://localhost.fastplatform.eu:9000")
AWS_LOCATION = os.environ.get("AWS_LOCATION", "")
AWS_S3_ENDPOINT_URL = S3_URL + "/"
AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_STORAGE_BUCKET_NAME", "media")
MEDIA_URL = AWS_S3_ENDPOINT_URL + AWS_STORAGE_BUCKET_NAME + "/"

S3_ACCESS_KEY = os.environ.get("S3_ACCESS_KEY")
S3_SECRET_KEY = os.environ.get("S3_SECRET_KEY")
AWS_ACCESS_KEY_ID = S3_ACCESS_KEY
AWS_SECRET_ACCESS_KEY = S3_SECRET_KEY

AWS_DEFAULT_ACL = None

S3_REGION = os.environ.get("S3_REGION", "eu-west-0")
S3_SERVICE = os.environ.get("S3_SERVICE", "s3")

PHOTO_ALLOWED_EXTENSIONS = os.environ.get(
    "PHOTO_ALLOWED_EXTENSIONS", "png,jpg,jpeg"
).split(",")

MAX_UPLOAD_SIZE = int(os.environ.get("MAX_UPLOAD_SIZE", 5242880))
DATA_UPLOAD_MAX_MEMORY_SIZE = int(
    os.environ.get("DATA_UPLOAD_MAX_MEMORY_SIZE", 5242880)
)
