from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [("common", "0003_auto_20200709_1659")]

    # operations = [
    #     migrations.RunSQL((Path(__file__).parent / Path('create_geometry_to_geojson_cast.sql')).read_text())
    # ]
