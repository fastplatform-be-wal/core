import os

TIME_ZONE = os.environ.get("TIME_ZONE", "UTC")
USE_TZ = os.environ.get("USE_TZ", "TRUE").upper() == "TRUE"
