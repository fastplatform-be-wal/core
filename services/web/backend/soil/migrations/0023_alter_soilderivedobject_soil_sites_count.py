# Generated by Django 3.2.7 on 2022-03-29 13:37

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('soil', '0022_auto_20220216_1844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='soilderivedobject',
            name='soil_sites_count',
            field=models.IntegerField(help_text='Number of soil samples used to estimate the soil properties', validators=[django.core.validators.MinValueValidator(0)], verbose_name='count of soil samples'),
        ),
    ]
