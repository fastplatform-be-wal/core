# Generated by Django 3.0.7 on 2020-10-15 15:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('external', '0004_auto_20201014_0814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agribuilding',
            name='type_of_building',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='external.TypeOfAgriBuilding', verbose_name='type of building'),
        ),
        migrations.AlterField(
            model_name='designation',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.Designation', verbose_name='parent'),
        ),
        migrations.AlterField(
            model_name='designationscheme',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.DesignationScheme', verbose_name='parent'),
        ),
        migrations.AlterField(
            model_name='designationtype',
            name='designation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='external.Designation', verbose_name='designation'),
        ),
        migrations.AlterField(
            model_name='designationtype',
            name='designation_scheme',
            field=models.ForeignKey(help_text='The scheme from which the designation code comes.', on_delete=django.db.models.deletion.PROTECT, to='external.DesignationScheme', verbose_name='designation scheme'),
        ),
        migrations.AlterField(
            model_name='environmentaldomain',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.EnvironmentalDomain', verbose_name='parent'),
        ),
        migrations.AlterField(
            model_name='observableproperty',
            name='base_phenomenon',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='observable_properties', to='external.PhenomenonType', verbose_name='base phenomenon'),
        ),
        migrations.AlterField(
            model_name='observableproperty',
            name='uom',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.UnitOfMeasure', verbose_name='unit of measuree'),
        ),
        migrations.AlterField(
            model_name='observation',
            name='observed_property',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='observations', to='external.ObservableProperty', verbose_name='observed property'),
        ),
        migrations.AlterField(
            model_name='phenomenontype',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.PhenomenonType', verbose_name='parent'),
        ),
        migrations.AlterField(
            model_name='soilinvestigationpurpose',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.SoilInvestigationPurpose', verbose_name='parent'),
        ),
        migrations.AlterField(
            model_name='soilsite',
            name='soil_investigation_purpose',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='soil_sites', to='external.SoilInvestigationPurpose', verbose_name='soil investigation purpose'),
        ),
        migrations.AlterField(
            model_name='specialisedzonetypecode',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.SpecialisedZoneTypeCode', verbose_name='parent'),
        ),
        migrations.AlterField(
            model_name='typeofagribuilding',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.TypeOfAgriBuilding', verbose_name='parent'),
        ),
        migrations.AlterField(
            model_name='unitofmeasurelinearconversion',
            name='unit_from',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='units_as_from', to='external.UnitOfMeasure'),
        ),
        migrations.AlterField(
            model_name='unitofmeasurelinearconversion',
            name='unit_to',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='units_as_to', to='external.UnitOfMeasure'),
        ),
        migrations.AlterField(
            model_name='watermeasurementobservation',
            name='observed_property',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='water_measurement_observations', to='external.ObservableProperty', verbose_name='observed property'),
        ),
        migrations.AlterField(
            model_name='zonetypecode',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='external.ZoneTypeCode', verbose_name='parent'),
        ),
    ]
