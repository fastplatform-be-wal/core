from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [("messaging", "0023_auto_20201207_1514")]

    operations = [
        migrations.RunSQL(
            sql="INSERT INTO queue(id, title) VALUES (setval(pg_get_serial_sequence('queue', 'id'), 1), 'Default') ON CONFLICT DO NOTHING;",
        ),
        migrations.RunSQL(
            sql="UPDATE ticket SET queue_id = 1 WHERE queue_id IS NULL;",
        ),
    ]