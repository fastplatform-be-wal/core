# Generated by Django 3.2.7 on 2022-01-25 14:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fieldbook', '0031_auto_20211207_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plantvariety',
            name='name',
            field=models.CharField(blank=True, help_text='The name of the variety, can be left empty to designate an undetermined variety within a species', max_length=256, null=True, verbose_name='name'),
        ),
    ]
