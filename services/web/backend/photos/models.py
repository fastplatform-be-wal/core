import os
from io import BytesIO

from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.core.validators import (
    FileExtensionValidator,
    MinValueValidator,
    MaxValueValidator,
)
from django.core.files.base import ContentFile


from PIL import Image

from utils.models import GetAdminURLMixin


def get_geo_tagged_photo_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/holding/<holding_id>/photo/<filename>
    return "{app}/holding/{holding_id}/campaign/{campaign_id}/photo/{filename}".format(
        app=instance._meta.app_label,
        holding_id=instance.holding_campaign.holding.id,
        campaign_id=instance.holding_campaign.campaign.id,
        filename=filename,
    )


def get_geo_tagged_thumbnail_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/holding/<holding_id>/thumbnail/<filename>
    return (
        "{app}/holding/{holding_id}/campaign/{campaign_id}/thumbnail/{filename}".format(
            app=instance._meta.app_label,
            holding_id=instance.holding_campaign.holding.id,
            campaign_id=instance.holding_campaign.campaign.id,
            filename=filename,
        )
    )


class GeoTaggedPhoto(GetAdminURLMixin, models.Model):
    """Photo with geolocation information

    As per 6.2.5 of
    https://publications.jrc.ec.europa.eu/repository/bitstream/JRC108816/cts-2017pubsy-online.pdf

    Geo-tagged photos
    =================
    Geotagging is the process of adding and embedding geographical information,
    and possibly additional temporal and/or textual information into the metadata
    file of a photograph. Usually the location and time of acquisition will be obtained
    through the GNSS antenna of the photo camera or smartphone.
    Member State Administrations should strongly consider the utility of this lowcost,
    readily available, and easy to use technology. They could integrate, in their
    IACS, geo-tagged photos provided by farmers as evidence of compliance to
    specific conditions or rules. For instance, a farmer could send a geo-tagged
    photo of his Durum wheat field(s) being part of a Voluntary Couple Support.
    Also, a Geo-tagged photo could be sent of a grassland just mowed to show
    compliance with a mowing date imposed by the legislation. The provision of such
    geo-tagged information will avoid the field visit that otherwise would be
    necessary.
    The minimum information to be embedded or stored, for each image taken,
    should consist of:
    - Its geographical location (longitude, latitude);
    - Its date and time of shooting;
    - The direction the camera was pointing. This could be obtain, among
    other possibilities, directly from a compass system embedded in the device, or
    by pointing the camera toward a feature/ a landscape element that could be
    easily depicted afterwards on imagery maps (see example below);
    Example of photo containing elements allowing determining the direction the
    camera was pointing when shooting the photo.
    - It is also recommended to register the elevation and the Dilution of
    precision (DOP). This last parameter gives a qualitative indication on the
    positional precision.
    - At last, a procedure should exist to identify the farmer providing the
    photo (e.g. image uploaded in the IACS after an authentication through personal
    login and password).
    Administrations should develop a standard operating procedure to ensure the
    information integrity and security of received geo-tagged photos; in other
    words, ensure that the whole information content has, in no manner, been
    falsified.
    Some administration have already developed such dedicated smartphone/tablet
    Applets to be used by farmers (or by inspectors).
    Geo-tagged images should be stored in their original file formats. Critical
    image information may be lost and artefacts introduced as a result of a lossy
    compression process. Files should also be as ‘read-only’ to avoid modifications
    or deletions.
    """

    id = models.AutoField(
        primary_key=True, editable=False, verbose_name=_("identifier")
    )

    shot_at = models.DateTimeField(
        null=False, blank=False, verbose_name=_("date of photo")
    )

    description = models.TextField(null=True, blank=True, verbose_name=_("description"))

    uploaded_at = models.DateTimeField(
        null=False, blank=False, auto_now=True, verbose_name=_("date uploaded")
    )

    uploaded_by = models.ForeignKey(
        to="authentication.User",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="geo_tagged_photos_uploaded",
        verbose_name=_("uploaded by"),
    )

    # Farm/site/plot information

    holding_campaign = models.ForeignKey(
        "farm.HoldingCampaign",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="geo_tagged_photos",
        verbose_name=_("farm campaign"),
    )

    plot = models.ForeignKey(
        "farm.Plot",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="geo_tagged_photos",
        verbose_name=_("plot"),
    )

    # Location information

    location = models.PointField(null=False, blank=False, verbose_name=_("location"))

    heading = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("heading"),
        validators=[
            MinValueValidator(0),
            MaxValueValidator(360),
        ],
    )

    elevation = models.FloatField(null=True, blank=True, verbose_name=_("elevation"))

    horizontal_dilution_of_precision = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("horizontal dilution of precision"),
        validators=[
            MinValueValidator(0),
        ],
    )

    vertical_dilution_of_precision = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("vertical dilution of precision"),
        validators=[
            MinValueValidator(0),
        ],
    )

    # Files

    photo = models.ImageField(
        null=False,
        blank=False,
        verbose_name=_("photo"),
        validators=[
            FileExtensionValidator(allowed_extensions=settings.PHOTO_ALLOWED_EXTENSIONS)
        ],
        upload_to=get_geo_tagged_photo_path,
    )

    photo_width = models.IntegerField(
        null=True, blank=True, verbose_name=_("photo width"), help_text=_("pixels")
    )

    photo_height = models.IntegerField(
        null=True, blank=True, verbose_name=_("photo height"), help_text=_("pixels")
    )

    thumbnail = models.ImageField(
        null=True,
        blank=True,
        verbose_name=_("thumbnail"),
        validators=[
            FileExtensionValidator(allowed_extensions=settings.PHOTO_ALLOWED_EXTENSIONS)
        ],
        width_field="thumbnail_width",
        height_field="thumbnail_height",
        upload_to=get_geo_tagged_thumbnail_path,
    )

    thumbnail_width = models.IntegerField(
        null=True, blank=True, verbose_name=_("thumbnail width")
    )

    thumbnail_height = models.IntegerField(
        null=True, blank=True, verbose_name=_("thumbnail height")
    )

    def __str__(self):
        return f"{_('Geo-tagged photo')} #{self.id}"

    def create_thumbnail(self):

        THUMB_SIZE = 400, 400

        byte_string = self.photo.read()
        image = Image.open(BytesIO(byte_string))
        image.thumbnail(THUMB_SIZE, Image.ANTIALIAS)

        thumb_name, thumb_extension = os.path.splitext(
            self.photo.name.rsplit("/", 1)[-1]
        )
        thumb_extension = thumb_extension.lower()

        thumb_filename = thumb_name + thumb_extension

        if thumb_extension in [".jpg", ".jpeg"]:
            FTYPE = "JPEG"
        elif thumb_extension == ".gif":
            FTYPE = "GIF"
        elif thumb_extension == ".png":
            FTYPE = "PNG"
        else:
            return False  # Unrecognized file type

        # Save thumbnail to in-memory file as bytes
        temp_thumb = BytesIO()
        image.save(temp_thumb, FTYPE)
        temp_thumb.seek(0)

        self.thumbnail.save(thumb_filename, ContentFile(temp_thumb.read()))
        temp_thumb.close()

    def save(self, *args, **kwargs):
        # Verify the photo width and height
        if self.photo_height is None or self.photo_width is None:
            if self.photo.width:
                self.photo_width = self.photo.width
            if self.photo.height:
                self.photo_height = self.photo.height

        super().save(*args, **kwargs)

    class Meta:
        db_table = "geo_tagged_photo"
        verbose_name = _("geo-tagged photo")
        verbose_name_plural = _("geo-tagged photos")
        help_text = _(
            "Photos that include geolocation, precision and orientation of the user"
        )
