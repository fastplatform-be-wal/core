import os

from .debug import DEBUG, DEBUG_TOOLBAR


SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")

DOMAIN_NAME = os.environ.get("DOMAIN_NAME", "fastplatform.eu")
SESSION_COOKIE_DOMAIN = os.environ.get("SESSION_COOKIE_DOMAIN", None)
DJANGO_ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS", "localhost")
ALLOWED_HOSTS = DJANGO_ALLOWED_HOSTS.split(",")
SITE_URL = os.environ.get("SITE_URL", "https://localhost.fastplatform.eu:48000")

# HTTPS security
CSRF_COOKIE_SECURE = os.environ.get("CSRF_COOKIE_SECURE", "TRUE").upper() == "TRUE"
SESSION_COOKIE_SECURE = (
    os.environ.get("SESSION_COOKIE_SECURE", "TRUE").upper() == "TRUE"
)

SECURE_HSTS_SECONDS = int(os.environ.get("SECURE_HSTS_SECONDS", 0))
if SECURE_HSTS_SECONDS > 0:
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True

SECURE_REFERRER_POLICY = "same-origin"

if DEBUG:
    ALLOWED_HOSTS = ["172.17.0.1", "host.docker.internal", "localhost.fastplatform.eu"]
    ALLOWED_HOSTS += DJANGO_ALLOWED_HOSTS.split(",")

if DEBUG and DEBUG_TOOLBAR:
    INTERNAL_IPS = type(str("c"), (), {"__contains__": lambda *a: True})()

# The age of session cookies, in seconds.
SESSION_COOKIE_AGE = int(os.environ.get("SESSION_COOKIE_AGE", 604800))  # 7 days
