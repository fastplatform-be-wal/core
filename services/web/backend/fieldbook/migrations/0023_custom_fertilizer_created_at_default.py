from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [("fieldbook", "0022_generate_fk_cascade_fieldbook")]

    operations = [
        migrations.RunSQL(
            sql="ALTER TABLE custom_fertilizer ALTER COLUMN created_at SET DEFAULT now();",
        )
    ]
