import { Router } from 'express';
import {
  sendNotification,
} from '../controllers/notification-controller'

const router = Router();


router.route('/send-notification')
  .post(sendNotification)

export default router
