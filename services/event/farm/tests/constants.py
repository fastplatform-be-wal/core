from shapely.geometry.polygon import Polygon
from math import cos, pi
from app.settings import config


class Utils:
    # TODO: improve on formulae bu using reprojections
    @classmethod
    def convert_meter_distance_in_latitude(cls, distance_in_meters):
        return (1 / 110.574) * (distance_in_meters / 1000)

    @classmethod
    def convert_meter_distance_in_longitude_at_latitude(
        cls, distance_in_meters, latitude
    ):
        return (1 / 111.320 / cos(latitude / 180 * pi)) * (distance_in_meters / 1000)

    @classmethod
    def create_square(cls, center_long, center_lat, size_in_meters):

        half_distance = size_in_meters / 2
        lat_distance = Utils.convert_meter_distance_in_latitude(
            distance_in_meters=half_distance
        )
        long_distance = Utils.convert_meter_distance_in_longitude_at_latitude(
            distance_in_meters=half_distance, latitude=center_lat
        )

        coords = [
            (
                center_long - long_distance,
                center_lat - lat_distance,
            ),
            (
                center_long - long_distance,
                center_lat + lat_distance,
            ),
            (
                center_long + long_distance,
                center_lat + lat_distance,
            ),
            (
                center_long + long_distance,
                center_lat - lat_distance,
            ),
        ]

        square = Polygon(coords)

        return square

    @classmethod
    def move_polygon(cls, polygon: Polygon, north_in_meters, east_in_meters):
        coords = Utils.get_coords(polygon)
        first_point_latitude = coords[0][1]

        lat_distance = Utils.convert_meter_distance_in_latitude(
            distance_in_meters=north_in_meters
        )
        long_distance = Utils.convert_meter_distance_in_longitude_at_latitude(
            distance_in_meters=east_in_meters, latitude=first_point_latitude
        )

        moved_coords = []
        for x, y in coords:
            moved_coords.append((x + long_distance, y + lat_distance))

        moved_square = Polygon(moved_coords)

        return moved_square

    @classmethod
    def get_coords(cls, polygon: Polygon):
        coords = list(zip(*polygon.exterior.coords.xy))
        return coords

    @classmethod
    def get_coords_string(cls, polygon: Polygon):
        coords = Utils.get_coords(polygon)
        for p in coords:
            print(f"    {{ lat: {p[1]}, lng: {p[0]} }},")

    @classmethod
    def get_coords_list(cls, polygon: Polygon):
        coords = [[c[1], c[0]] for c in list(zip(*polygon.exterior.coords.xy))]
        return coords


PLOT_SIZE = 100

# TODO: use etrs89laea_to_etrs89 to convert first to
# A random square polygon of size 100 meters
PLOT: Polygon = Utils.create_square(
    center_long=4.349888, center_lat=50.845005, size_in_meters=PLOT_SIZE
)

PLOT_CHANGED: Polygon = Utils.create_square(
    center_long=4.449888, center_lat=51.845005, size_in_meters=PLOT_SIZE
)

# An intersecting polygon North-East of plot, with 1/4 surface intersection
GEOM_INTERSECTING_PLOT = Utils.move_polygon(
    PLOT, north_in_meters=PLOT_SIZE / 2, east_in_meters=PLOT_SIZE / 2
)

GEOM_NOT_INTERSECTING_PLOT = Utils.move_polygon(
    PLOT,
    north_in_meters=0,
    east_in_meters=1000,
)

WATER_COURSE_SEARCH_DISTANCE = (
    config.API_PLOT_COMPUTE_CONSTRAINTS_WATER_COURSE_SEARCH_DISTANCE
    * 60
    * 1852
    * (14.445728521911198 / 20)  # TODO: this proportion corrects distortion at latitude
)

SURFACE_WATER_SEARCH_DISTANCE = (
    config.API_PLOT_COMPUTE_CONSTRAINTS_SURFACE_WATER_SEARCH_DISTANCE
    * 60
    * 1852
    * (14.445728521911198 / 20)  # TODO: this proportion corrects distortion at latitude
)

WATER_COURSE_WITHIN = Utils.move_polygon(
    PLOT,
    north_in_meters=0,
    east_in_meters=PLOT_SIZE + WATER_COURSE_SEARCH_DISTANCE - 2,
)
WATER_COURSE_OUTSIDE = Utils.move_polygon(
    PLOT,
    north_in_meters=0,
    east_in_meters=PLOT_SIZE + WATER_COURSE_SEARCH_DISTANCE + 2,
)

SURFACE_WATER_WITHIN = Utils.move_polygon(
    PLOT,
    north_in_meters=0,
    east_in_meters=PLOT_SIZE + SURFACE_WATER_SEARCH_DISTANCE - 2,
)
SURFACE_WATER_OUTSIDE = Utils.move_polygon(
    PLOT,
    north_in_meters=0,
    east_in_meters=PLOT_SIZE + SURFACE_WATER_SEARCH_DISTANCE + 2,
)


def format_geom(coords_list):
    return f'{{\\"type\\": \\"MultiPolygon\\", \\"coordinates\\": [[{coords_list},]], \\"crs\\": {{\\"type\\": \\"name\\", \\"properties\\": {{\\"name\\": \\"EPSG:4258\\"}}}}}}'


SHARED_VALUES = {
    "PWD": "azerty1234",
    "PLOT_GEOM": format_geom(Utils.get_coords_list(PLOT)),
    "PLOT_GEOM_CHANGED": format_geom(Utils.get_coords_list(PLOT_CHANGED)),
    "GEOM_INTERSECTING_PLOT": format_geom(
        Utils.get_coords_list(GEOM_INTERSECTING_PLOT)
    ),
    "GEOM_NOT_INTERSECTING_PLOT": format_geom(
        Utils.get_coords_list(GEOM_NOT_INTERSECTING_PLOT)
    ),
    "WATER_COURSE_WITHIN_GEOM": format_geom(Utils.get_coords_list(WATER_COURSE_WITHIN)),
    "WATER_COURSE_OUTSIDE_GEOM": format_geom(
        Utils.get_coords_list(WATER_COURSE_OUTSIDE)
    ),
    "SURFACE_WATER_WITHIN_GEOM": format_geom(
        Utils.get_coords_list(SURFACE_WATER_WITHIN)
    ),
    "SURFACE_WATER_OUTSIDE_GEOM": format_geom(
        Utils.get_coords_list(SURFACE_WATER_OUTSIDE)
    ),
}
