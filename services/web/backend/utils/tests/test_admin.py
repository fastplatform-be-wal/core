from datetime import datetime
from unittest import SkipTest
import json
import io
import csv
import os
import urllib.parse

from django.urls import reverse
from django.utils import translation
from django.conf import settings
from django.contrib.gis.db import models
from django.core.files.base import ContentFile

from import_export.admin import ExportMixin, ImportExportMixin

from model_bakery import baker
from bs4 import BeautifulSoup

from fastplatform.site import admin_site
from authentication.models import User


# The below code is inspired by the django-admin-auto-tests package from alsur,
# released under MIT license.
# You can find the original code here: https://github.com/alsur/django-admin-auto-tests
# And the original license: https://github.com/alsur/django-admin-auto-tests/blob/master/LICENSE


class AdminTestCaseMixin:
    model = None
    recipe = None
    enable = True
    all_locales = os.environ.get("TEST_ALL_LOCALES", False)
    using = "default"

    skip_changelist = False
    skip_add = False
    skip_change = False
    skip_delete = False

    create_files = False

    expected_status_code_changelist = 200
    expected_status_code_add_view = 200
    expected_status_code_add = 200
    expected_status_code_change_view = 200
    expected_status_code_change = 200
    expected_status_code_delete = 200
    expected_import_status_code = 200
    expected_export_status_code = 200

    def create_instance(self):
        if self.recipe:
            return self.recipe.make(_create_files=self.create_files, _using=self.using)
        return baker.make(
            self.model, _create_files=self.create_files, _using=self.using
        )

    def prepare_instance(self):
        if self.recipe:
            return self.recipe.prepare(_save_related=True, _using=self.using)
        return baker.prepare(self.model, _save_related=True, _using=self.using)

    def create_user(self):
        self.username = "test-user"
        self.password = User.objects.make_random_password()
        user, _ = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user

    def setUp(self):
        super().setUp()

        # Create a superuser and log in
        self.create_user()
        self.client.login(username=self.username, password=self.password)

    def get_add_url(self):
        return reverse(
            "admin:{model._meta.app_label}_{model._meta.model_name}_add".format(
                model=self.model
            )
        )

    def get_changelist_url(self):
        return reverse(
            "admin:{model._meta.app_label}_{model._meta.model_name}_changelist".format(
                model=self.model
            )
        )

    def get_change_url(self, instance):
        return reverse(
            "admin:{model._meta.app_label}_{model._meta.model_name}_change".format(
                model=self.model
            ),
            args=(instance.pk,),
        )

    def get_delete_url(self, instance):
        return reverse(
            "admin:{model._meta.app_label}_{model._meta.model_name}_delete".format(
                model=self.model
            ),
            args=(instance.pk,),
        )

    def get_import_url(self):
        return self.get_changelist_url() + "import/"

    def get_process_import_url(self):
        return self.get_changelist_url() + "process_import/"

    def get_export_url(self):
        return self.get_changelist_url() + "export/"

    def get_view_permission(self):
        return f"{self.model._meta.app_label}.can_view_{self.model._meta.model_name}"

    def get_change_permission(self):
        return f"{self.model._meta.app_label}.can_change_{self.model._meta.model_name}"

    def get_add_permission(self):
        return f"{self.model._meta.app_label}.can_add_{self.model._meta.model_name}"

    def get_delete_permission(self):
        return f"{self.model._meta.app_label}.can_delete_{self.model._meta.model_name}"

    def create_instance_data(self, instance):
        data = {}
        for field in instance._meta.concrete_fields:
            if not getattr(field, "editable", False):
                continue

            value = field.value_from_object(instance)

            if value is None:
                continue

            if isinstance(field, (models.FileField, models.ImageField)) and not value:
                continue

            if isinstance(value, dict):
                # For JSONFields
                data[field.name] = json.dumps(value)
            elif isinstance(field, models.GeometryField):
                # For GeometryFields
                data[field.name] = value.wkt
            elif isinstance(value, datetime):
                data[field.name] = value.isoformat()
                # In case the widget being used in the admin is a SplitDateTimeWidget:
                data[field.name + "_0"] = value.isoformat().split("T")[0]
                data[field.name + "_1"] = value.isoformat().split("T")[1].split("+")[0]
            else:
                data[field.name] = value

        return data

    def instance_data_to_csv_file(self, instance_data, filename="test.csv"):
        output = io.StringIO()
        writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(instance_data.keys())
        writer.writerow(instance_data.values())
        return ContentFile(output.getvalue().encode(), name=filename)

    def create_form_instance_data(self, response, instance_data=None):

        fields = {
            key: value.initial
            for key, value in response.context_data["adminform"].form.fields.items()
        }
        for inline_admin_formset in response.context_data["inline_admin_formsets"]:
            # Add the hidden values for management forms
            for (
                key,
                value,
            ) in inline_admin_formset.formset.management_form.initial.items():
                fields["{}-{}".format(inline_admin_formset.formset.prefix, key)] = value

        if instance_data:
            for x, y in instance_data.items():
                fields[x] = y

        fields = {x: y for x, y in fields.items() if y is not None}

        return fields

    def _test_all_links(self, html):
        soup = BeautifulSoup(html, features="html.parser")
        for a in soup.find_all("a"):
            href = a.get("href")
            if href and not bool(urllib.parse.urlparse(href).netloc):
                with self.subTest(href=href):
                    response = self.client.get(href, follow=True)
                    self.assertEqual(response.status_code, 200)

    def test_model_is_registered(self):
        if not self.enable:
            raise SkipTest("Feature is disabled")

        self.assertTrue(admin_site.is_registered(self.model))

    def test_changelist_view(self):
        if not self.enable:
            raise SkipTest("Feature is disabled")

        if self.skip_changelist:
            raise SkipTest("Skip changelist is enabled")

        self.assertTrue(
            self.user.has_perm(
                self.get_view_permission() or self.get_change_permission()
            )
        )

        for i in range(3):
            self.create_instance()

        def inner():
            response = self.client.get(self.get_changelist_url(), follow=True)
            self.assertEqual(response.status_code, self.expected_status_code_changelist)

        if self.all_locales:
            for language_code, language in settings.LANGUAGES:
                translation.activate(language_code)
                inner()
        else:
            inner()

    def test_add_view(self):
        if not self.enable:
            raise SkipTest("Feature is disabled")

        if self.skip_add:
            raise SkipTest("Skip add is enabled")

        self.assertTrue(self.user.has_perm(self.get_add_permission()))

        def inner():
            response = self.client.get(self.get_add_url(), follow=True)
            self.assertEqual(response.status_code, self.expected_status_code_add_view)
            self._test_all_links(response.content)

        if self.all_locales:
            for language_code, language in settings.LANGUAGES:
                translation.activate(language_code)
                inner()
        else:
            inner()

    def test_add(self):
        if not self.enable:
            raise SkipTest("Feature is disabled")

        if self.skip_add:
            raise SkipTest("Skip add is enabled")

        self.assertTrue(self.user.has_perm(self.get_add_permission()))

        def inner():
            # Get the Add page
            response = self.client.get(self.get_add_url(), follow=True)

            # Build a new object to add
            instance = self.prepare_instance()
            instance_data = self.create_instance_data(instance)
            data = self.create_form_instance_data(response, instance_data)

            data["_continue"] = ""

            response = self.client.post(self.get_add_url(), data, follow=True)
            self.assertEqual(response.status_code, self.expected_status_code_add)
            if response.context_data.get("errors"):
                self.assertEqual(
                    len(response.context_data["errors"]),
                    0,
                    " * ".join(
                        [
                            "{}: {}".format(x, ", ".join(y))
                            for x, y in response.context_data[
                                "adminform"
                            ].form.errors.items()
                        ]
                    ),
                )
            if "original" not in response.context_data:
                self.fail("Instance is not created.")

        if self.all_locales:
            for language_code, language in settings.LANGUAGES:
                translation.activate(language_code)
                inner()
        else:
            inner()

    def test_change_view(self):
        if not self.enable:
            raise SkipTest("Feature is disabled")

        if self.skip_change:
            raise SkipTest("Skip change is enabled")

        self.assertTrue(self.user.has_perm(self.get_change_permission()))

        def inner():
            # Create and save an instance
            instance = self.create_instance()

            # GET the change page for this instance
            response = self.client.get(
                self.get_change_url(instance),
                follow=True,
            )
            self.assertEqual(
                response.status_code, self.expected_status_code_change_view
            )
            self._test_all_links(response.content)

        if self.all_locales:
            for language_code, language in settings.LANGUAGES:
                translation.activate(language_code)
                inner()
        else:
            inner()

    def test_change(self):
        if not self.enable:
            raise SkipTest("Feature is disabled")

        if self.skip_change:
            raise SkipTest("Skip change is enabled")

        self.assertTrue(self.user.has_perm(self.get_change_permission()))

        def inner():
            # Create and save a first instance
            instance = self.create_instance()

            # Get the change page/form for that instance
            response = self.client.get(self.get_change_url(instance), follow=True)

            # Create a new instance and format it for the change form
            new_instance = self.prepare_instance()
            new_instance_data = self.create_instance_data(new_instance)
            new_data = self.create_form_instance_data(response, new_instance_data)

            new_data["_continue"] = ""

            # POST to the change form
            response = self.client.post(
                self.get_change_url(instance), new_data, follow=True
            )
            self.assertEqual(response.status_code, self.expected_status_code_change)

        if self.all_locales:
            for language_code, language in settings.LANGUAGES:
                translation.activate(language_code)
                inner()
        else:
            inner()

    def test_delete_view(self):
        if not self.enable:
            raise SkipTest("Feature is disabled")

        if self.skip_delete:
            raise SkipTest("Skip delete is enabled")

        self.assertTrue(self.user.has_perm(self.get_delete_permission()))

        def inner():
            instance = self.create_instance()
            response = self.client.get(
                self.get_delete_url(instance),
                follow=True,
            )
            self.assertEqual(response.status_code, self.expected_status_code_delete)
            self._test_all_links(response.content)

        if self.all_locales:
            for language_code, language in settings.LANGUAGES:
                translation.activate(language_code)
                inner()
        else:
            inner()

    # def test_import(self):
    #     if not self.enable:
    #         raise SkipTest("Feature is disabled")

    #     admin = admin_site._registry[self.model]

    #     if not isinstance(admin, (ImportMixin, ImportExportMixin)):
    #         raise SkipTest("Not registered for importing")

    #     resource_class = getattr(admin, "resource_class", None)
    #     self.assertFalse(resource_class is None)

    #     model_from_resource_class = resource_class._meta.model
    #     self.assertEqual(model_from_resource_class, self.model)

    #     instance = self.prepare_instance()
    #     instance_data = self.create_instance_data(instance)
    #     csv_file = self.instance_data_to_csv_file(instance_data, filename="test-import.csv")

    #     input_format = "0"
    #     data = {
    #         "input_format": input_format,
    #         "import_file": csv_file,
    #     }
    #     response = self.client.post(self.get_import_url(), data, follow=True)
    #     self.assertEqual(response.status_code, self.expected_import_status_code)
    #     self.assertIn("result", response.context)
    #     self.assertFalse(
    #         response.context["result"].has_errors(),
    #         msg=" * ".join(chain(
    #             [base_error.message for base_error in response.context["result"].base_errors[:1]],
    #             [row_error.error.message for row_index, row_errors in response.context["result"].row_errors()[:1] for row_error in row_errors[:1]],
    #         ))[:100],
    #     )
    #     self.assertIn("confirm_form", response.context)
    #     confirm_form = response.context["confirm_form"]
    #     data = confirm_form.initial
    #     self.assertEqual(data["original_file_name"], "test-import.csv")

    #     response = self.client.post(self.get_process_import_url(), data, follow=True)
    #     self.assertEqual(response.status_code, self.expected_import_status_code)

    def test_export(self):
        if not self.enable:
            raise SkipTest("Feature is disabled")

        admin = admin_site._registry[self.model]

        if not isinstance(admin, (ExportMixin, ImportExportMixin)):
            raise SkipTest("Not registered for exporting")

        resource_class = getattr(admin, "resource_class", None)
        self.assertFalse(resource_class is None)

        model_from_resource_class = resource_class._meta.model
        self.assertEqual(model_from_resource_class, self.model)

        instance = self.create_instance()

        response = self.client.get(self.get_export_url())
        self.assertEqual(response.status_code, self.expected_export_status_code)

        data = {
            "file_format": "0",
        }
        date_str = datetime.now().strftime("%Y-%m-%d")
        response = self.client.post(self.get_export_url(), data, follow=True)

        self.assertEqual(response.status_code, self.expected_export_status_code)
        self.assertContains(response, str(instance.pk), status_code=200)
        self.assertTrue(response.has_header("Content-Disposition"))
        self.assertEqual(response["Content-Type"], "text/csv")
        self.assertEqual(
            response["Content-Disposition"],
            f'attachment; filename="{str(instance._meta.model.__name__)}-{date_str}.csv"',
        )
