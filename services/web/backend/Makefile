# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
S3_PORT=9000

include service.env
export $(shell sed 's/=.*//' service.env)
include secrets.env
export $(shell sed 's/=.*//' secrets.env)

# PATH to the locale folder
LOCALIZATION_PATH = ./locale
# Locales currently supported
LOCALES = bg de el es et fr it ro sk

# Django management targets
# =========================

.PHONY: shell
shell: ## Start the Django shell
	python3 manage.py shell

.PHONY: start
start: ## Start the local Django dev server on HTTP
	python3 manage.py runserver 0.0.0.0:8000

.PHONY: starts
starts: ## Start the local Django dev server on HTTPS (using certificate from the /certificates/ directory)
	python3 manage.py runsslserver 0.0.0.0:48000 \
		--certificate $(shell pwd)/certificates/localhost.fastplatform.eu/fullchain1.pem \
		--key $(shell pwd)/certificates/localhost.fastplatform.eu/privkey1.pem

.PHONY: migrations
migrations: ## Generate Django migrations
	python3 manage.py makemigrations

.PHONY: migrate
migrate: ## Apply Django migrations
	python3 manage.py migrate --database default
	python3 manage.py migrate --database external

.PHONY: list-static
list-static:
	python3 manage.py collectstatic --dry-run --no-input


# Local Postgres services
# -----------------------

.PHONY: start-postgres
start-postgres: ## Start the local Postgres servers (fastplatform and external)
	docker run --name postgres-fastplatform \
		-e POSTGRES_USER=${POSTGRES_USER} \
		-e POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) \
		-e POSTGRES_DB=${POSTGRES_DATABASE} \
		-p ${POSTGRES_PORT}:5432 \
		-h 0.0.0.0 \
		-v $(shell pwd)/data/dev/postgres-default:/var/lib/postgresql/data \
		-d docker.io/postgis/postgis:16-3.4
	docker run --name postgres-external \
		-e POSTGRES_USER=${POSTGRES_EXTERNAL_USER} \
		-e POSTGRES_PASSWORD=$(POSTGRES_EXTERNAL_PASSWORD) \
		-e POSTGRES_DB=${POSTGRES_EXTERNAL_DATABASE} \
		-p ${POSTGRES_EXTERNAL_PORT}:5432 \
		-h 0.0.0.0 \
		-v $(shell pwd)/data/dev/postgres-external:/var/lib/postgresql/data \
		-d docker.io/postgis/postgis:16-3.4

.PHONY: stop-postgres
stop-postgres: ## Stop the local Postgres servers
	-docker stop postgres-fastplatform
	-docker rm postgres-fastplatform
	-docker stop postgres-external
	-docker rm postgres-external

.PHONY: reset-postgres
reset-postgres: ## Delete the data of the local Postgres servers
	rm -rf ./data/dev/postgres-default/*
	rm -rf ./data/dev/postgres-external/*

.PHONY: reset-all-postgres
reset-all-postgres: ## Shorthand to stop/reset/start the local Postgres servers
	make stop-postgres
	make reset-postgres
	make start-postgres

.PHONY: restart-postgres
restart-postgres: ## Shorthand to stop/start the local Postgres servers (no reset)
	make stop-postgres
	make start-postgres

# Local Jaeger service (telemetry)
# --------------------------------

.PHONY: start-tracing
start-tracing: ## Start the Jaeger tracing service
	docker run --rm --name tracing-fastplatform \
		-e COLLECTOR_ZIPKIN_HOST_PORT=:9411 \
		-p 9411:9411 \
		-p 16686:16686 \
		-d docker.io/jaegertracing/all-in-one

.PHONY: stop-tracing
stop-tracing: ## 
	-docker stop tracing-fastplatform

# Local memcached service (Django cache)
# -------------------------------------

.PHONY: start-memcached
start-memcached: ##
	docker run --name memcached-fastplatform \
		-p 11211:11211 \
		-e MEMCACHED_USERNAME=memcached \
		-e MEMCACHED_PASSWORD=$(DJANGO_MEMCACHED_PASSWORD) \
		-d bitnami/memcached:1.6.6

.PHONY: stop-memcached
stop-memcached: ## 
	-docker stop memcached-fastplatform
	-docker rm memcached-fastplatform


# Data loading targets
# ====================

.PHONY: create-rsa-key
create-rsa-key: ## 
	python3 manage.py creatersakey

REGION=be-wal # Select amongst be-wal, bg, ee, es-an, es-cl, gr, it-21, ro, sk
TABLE=  # If empty, load all tables
.PHONY: init-region
init-region:  ## Load seed data for a given region on the fastplatform database
	./scripts/init_region.sh $(REGION) $(TABLE)

.PHONY: init-region-external
init-region-external:  ## Load seed data for a given region on the external database
	./scripts/init_region_external.sh $(REGION) $(TABLE)
	
.PHONY: clear-region
clear-region: ## Clear data for a given region
	./scripts/clear_region.sh $(REGION)

/.PHONY: clear-region-external
clear-region-external: ## Clear data for a given region
	./scripts/clear_region_external.sh $(REGION)

# I18n targets
# ============

# To work on translations:
# 1: Run `make messages` to update the locale/xx/LC_MESSAGES/django.po files with the current strings of the code
# 2: Run `make messages-csv` to create/overwrite a locale/xx/LC_MESSAGES/django.csv for each language
# 3: Edit the translations in the csv file
# 4: Run `make csv-messages` to convert your django.csv edits in the django.po files
# 5: Run `make compile-messages` to regenerate the django.mo binary files

.PHONY: messages
messages: ## Generate localized files (.po) for defined locales and extract untranslated strings
	@for lang in $(LOCALES); do \
		django-admin makemessages -l $$lang; \
	done;

.PHONY: messages-csv
messages-csv: ## Generate localized files (.csv) for defined locales
	@for lang in $(LOCALES); do \
		echo "processing locale $$lang"; \
		po2csv --input=locale/$$lang/LC_MESSAGES/django.po --output=locale/$$lang/LC_MESSAGES/django.csv; \
	done;

.PHONY: csv-messages
csv-messages: ## Generate .po files from .csv for defined locales
	@for lang in $(LOCALES); do \
		echo "processing locale $$lang"; \
		csv2po --input=locale/$$lang/LC_MESSAGES/django.csv --output=locale/$$lang/LC_MESSAGES/django.po --charset=UTF-8 --template=locale/$$lang/LC_MESSAGES/django.po; \
	done;

.PHONY: compile-messages
compile-messages: ## Compile django.po in django.mo
	django-admin compilemessages

.PHONY: check-verbose-names
check-verbose-names: ## Look for untranslated strings
	python manage.py check-verbose-names

# Postgres / Hasura targets
# =========================

APP=
.PHONY: sqlsequencereset
sqlsequencereset: ## Generates a migration that resets all Postgres sequences of the target app
	python manage.py sqlsequencereset $(if $(APP),"$(APP)",)

.PHONY: fk-cascade
fk-cascade: ## Generates a migration that pushes the foreign key "CASCADE" configuration to the Postgres databases
	python manage.py generate-fk-cascade $(if $(APP),"$(APP)",)

.PHONY: db-comments
db-comments: ## Generates a migration that pushes help_text of tables and fields to the Postgres databases
	python manage.py set-db-comments $(if $(APP),"$(APP)",)

# Docs targets
# ============

.PHONY: build-docs
build-docs: ## Rebuild the docs md files to HTML + index
	python manage.py build-docs

.PHONY: models-docs
models-docs: ## Rebuild the docs of the Django models to /docs/en/technical/models.md
	python manage.py models-docs

.PHONY: show-urls
show-urls: ## List the URL patterns being exposed by Django
	python manage.py show_urls

.PHONY: build-permissions-map
build-permissions-map:
	python manage.py build-permissions-map

## Don't forget to set the LOCALE to the desired language when building data_model
.PHONY: build-datamodel
build-datamodel: ## Generate the datamodel schema for each Django app in the language specified by LOCALE
	python manage.py build-datamodel

# Testing targets
# ===============

TEST_CASE=
KEEPDB=
# Full test suite run:           make test
# Single app test suite run:     make test APP=<name_of_app>
# Single method test suite run:  make test APP=<name_of_app> TEST_CASE=<test_case_class>
# You can keep (ie, not destroy) the current test database by adding KEEPDB=TRUE to the make command
.PHONY: test
test:  ## Run the test suite
	coverage run --rcfile=.coveragerc \
				 manage.py test $(if $(APP),$(if $(TEST_CASE),"$(APP).tests.$(TEST_CASE)","$(APP)"),) \
				 --verbosity=2 \
				 --no-input \
				 $(if $(KEEPDB), --keepdb,)

.PHONY: report
report:
	coverage html
	open ./.coverage_html/index.html
	
.PHONY: check-deploy
check-deploy:
	python manage.py check --deploy

# Kubernetes/live cluster targets
# ===============================

# The targets below expect the KUBECONFIG env variable to be set to a valid config file for the namespace being targeted

K8S_NAMESPACE=es-cl-staging
.PHONY: postgres-fastplatform-port-forward
postgres-fastplatform-port-forward: ## Start a port forward of the fastplatform Postgres server to local port 5437
	kubectl -n $(K8S_NAMESPACE) port-forward $$(kubectl -n $(K8S_NAMESPACE) get pod --selector pg-cluster=fastplatform,name=fastplatform -o NAME) ${POSTGRES_PORT}:5432

.PHONY: postgres-external-port-forward
postgres-external-port-forward: ## Start a port forward of the external Postgres server to local port 5437
	kubectl -n $(K8S_NAMESPACE) port-forward $$(kubectl -n $(K8S_NAMESPACE) get pod --selector pg-cluster=external,name=external -o NAME) ${POSTGRES_EXTERNAL_PORT}:5432

# Bazel-related targets
# =====================

.PHONY: bazel-run
bazel-run: ## Run the service locally with bazel
	bazel run :backend-run

.PHONY: bazel-run-init
bazel-run-init: ## 
	bazel run :backend-run -- init $(shell pwd)/../../../../custom/$(REGION)/init/fastplatform --clear
	bazel run :backend-run -- init $(shell pwd)/../../../../custom/$(REGION)/init/external --db-alias external --clear

.PHONY: bazel-run-collectstatic
bazel-run-collectstatic: ## 
	bazel run :backend-run -- collectstatic

.PHONY: bazel-run-creatersakey
bazel-run-creatersakey: ## 
	bazel run :backend-run -- creatersakey

.PHONY: bazel-django-manage-check-deploy
bazel-django-manage-check-deploy: ## 
	bazel run . -- check --deploy

.PHONY: bazel-django-manage-makemigrations
bazel-django-manage-makemigrations: ## 
	bazel run . -- makemigrations

# Pip compile (Python dependencies)
# =====================

.PHONY: pip-compile
pip-compile: ## Lock Python dependencies
	pip-compile --allow-unsafe requirements.in