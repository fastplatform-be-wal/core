from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.template.defaultfilters import truncatechars
from django.core.exceptions import ValidationError
from utils.models import GetAdminURLMixin


class Queue(GetAdminURLMixin, models.Model):
    """
    A collection of tickets
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    title = models.CharField(
        max_length=50, null=False, blank=False, verbose_name=_("title")
    )

    groups = models.ManyToManyField(
        to="authentication.Group", blank=True, verbose_name=_("groups")
    )

    def __str__(self):
        return truncatechars(self.title, 30)

    class Meta:
        ordering = ("title",)
        db_table = "queue"
        verbose_name = _("Queue")
        verbose_name_plural = _("Queues")
        help_text = _("Queues are used to categorise tickets depending on the topic.")


class Ticket(GetAdminURLMixin, models.Model):
    """A ticket is a two-way communication between the Paying Agency (users with "staff" status) and
    a **holding** (not a specific user, but all the users that are related parties to this holding)
    """

    class Status(models.TextChoices):
        OPEN = "OPEN", _("OPEN")
        REOPENED = "REOPENED", _("REOPENED")
        RESOLVED = "RESOLVED", _("RESOLVED")
        CLOSED = "CLOSED", _("CLOSED")
        DUPLICATE = "DUPLICATE", _("DUPLICATE")

    class Priority(models.IntegerChoices):
        CRITICAL = 1
        HIGH = 2
        NORMAL = 3
        LOW = 4
        VERY_LOW = 5

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    queue = models.ForeignKey(
        Queue,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="tickets",
        verbose_name=_("queue"),
        help_text=_("Queues you are a member of"),
    )

    # there is also a default now value on the postgres column (for hasura)
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("date created"),
    )

    created_by = models.ForeignKey(
        to="authentication.User",
        on_delete=models.PROTECT,
        related_name="tickets_created",
        null=True,
        blank=True,
        verbose_name=_("created by"),
    )

    holding = models.ForeignKey(
        "farm.Holding",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="tickets",
        verbose_name=_("farm"),
        help_text=_("Only members of this holding will be able to view and reply."),
    )

    assigned_to = models.ForeignKey(
        to="authentication.User",
        on_delete=models.PROTECT,
        related_name="tickets_assigned",
        blank=True,
        null=True,
        verbose_name=_("assigned to"),
    )

    title = models.CharField(
        verbose_name=_("title"),
        max_length=200,
    )

    body = models.TextField(
        blank=True,
        null=True,
        help_text=_(
            "The text content of the ticket. Will appear at the top of the conversation in the app."
        ),
        verbose_name=_("body"),
    )

    status = models.CharField(
        max_length=12,
        choices=Status.choices,
        default=Status.OPEN,
        null=False,
        blank=False,
        verbose_name=_("status"),
    )

    resolution = models.TextField(
        _("resolution"),
        blank=True,
        null=True,
        help_text=_(
            "(Optional) The resolution provided to the ticket. Will appear at the top of the conversation in the app."
        ),
    )

    priority = models.IntegerField(
        _("priority"),
        choices=Priority.choices,
        default=Priority.NORMAL,
        blank=False,
        help_text=_("1 = Highest Priority, 5 = Low Priority"),
    )

    def __str__(self):
        return truncatechars(self.title, 30)

    def clean(self):
        super().clean()

        # Ensure assigned_to user can see the ticket
        if self.assigned_to and not self.assigned_to.is_superuser:
            common_groups = (
                self.assigned_to.groups.values() & self.queue.groups.values()
            )
            if not common_groups:
                raise ValidationError(
                    _(
                        "The user assigned to this ticket does not belong to a group that can access the specified ticket queue."
                    )
                )

    class Meta:
        db_table = "ticket"
        verbose_name = _("ticket")
        verbose_name_plural = _("tickets")
        ordering = ("-created_at",)
        help_text = _(
            "Tickets correspond to a conversation between a holding (not a specific farmer) and the Paying Agency about a certain topic"
        )


def get_ticket_message_attachment_path(instance, filename):
    return "{app}/holding/{holding_id}/ticket/{ticket_id}/ticket_message/{ticket_message_id}/attachment/{filename}".format(
        app=instance._meta.app_label,
        holding_id=instance.ticket.holding.id,
        ticket_id=instance.ticket.id,
        ticket_message_id=instance.id,
        filename=filename,
    )


class TicketMessage(GetAdminURLMixin, models.Model):
    """A message of a conversation/ticket"""

    id = models.AutoField(
        primary_key=True, editable=False, verbose_name=_("identifier")
    )

    ticket = models.ForeignKey(
        "messaging.Ticket",
        verbose_name=_("ticket"),
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="ticket_messages",
    )

    # there is also a default now value on the postgres column (for hasura)
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("date created"),
    )

    created_by = models.ForeignKey(
        to="authentication.User",
        on_delete=models.PROTECT,
        related_name="ticket_messages_created",
        blank=True,
        null=True,
        verbose_name=_("created by"),
    )

    body = models.TextField(
        blank=True,
        default="",
        verbose_name=_("body"),
        help_text=_(
            "The body is overridden with '<<< FaST GeoTaggedPhoto id=XXX >>>' when a geo-tagged photo is attached to the message"
        ),
    )

    file = models.FileField(
        null=True,
        blank=True,
        upload_to=get_ticket_message_attachment_path,
        verbose_name=_("file"),
    )
    file_name = models.CharField(
        max_length=256, null=True, blank=True, verbose_name=_("name")
    )
    file_type = models.CharField(
        max_length=256, null=True, blank=True, verbose_name=_("MIME type")
    )
    file_size = models.IntegerField(null=True, blank=True, verbose_name=_("size"))

    geo_tagged_photo = models.ForeignKey(
        to="photos.GeoTaggedPhoto",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("geo-tagged photo"),
        help_text=_(
            "(Optional) The photo attached to the message (mutually exclusive with the body)"
        ),
    )

    def __str__(self):
        return truncatechars(self.body, 30)

    class Meta:
        ordering = ("ticket", "created_at")
        db_table = "ticket_message"
        verbose_name = _("ticket message")
        verbose_name_plural = _("ticket messages")


def get_ticket_attachment_path(instance, filename):
    return "{app}/holding/{holding_id}/ticket/{ticket_id}/attachment/{filename}".format(
        app=instance._meta.app_label,
        holding_id=instance.ticket.holding.id,
        ticket_id=instance.ticket.id,
        filename=filename,
    )


class Broadcast(GetAdminURLMixin, models.Model):
    """A one-way notification sent by a a Paying Agency staff user to all holdings"""

    id = models.AutoField(
        primary_key=True, editable=False, verbose_name=_("identifier")
    )

    title = models.CharField(
        max_length=48,
        null=False,
        blank=False,
        verbose_name=_("title"),
        help_text=_("Limited to 48 characters."),
    )

    body = models.TextField(
        max_length=178,
        null=True,
        blank=True,
        verbose_name=_("body"),
        help_text=_("Limited to 178 characters to fit in a push notification."),
    )

    icon = models.ImageField(
        null=True,
        blank=True,
        verbose_name=_("icon"),
        help_text=_(
            "Try to have an image as square as possible. If not provided, FaST default icon will be used."
        ),
    )

    class Status(models.TextChoices):
        DRAFT = "DRAFT", _("DRAFT")
        CANCELED = "CANCELED", _("CANCELED")
        SENDING = "SENDING", _("SENDING")
        SENT = "SENT", _("SENT")
        FAILED = "FAILED", _("FAILED")

    class UserModifiableStatus(models.TextChoices):
        DRAFT = "DRAFT", _("DRAFT")
        CANCELED = "CANCELED", _("CANCELED")

    status = models.CharField(
        max_length=10,
        choices=Status.choices,
        default=Status.DRAFT,
        null=False,
        blank=False,
        verbose_name=_("status"),
    )

    # A technical field used to trigger sending using a Postgres event defined in Hasura: it is null initially, set to true to trigger the sending
    is_sending_triggered = models.BooleanField(
        default=None, null=True, blank=True, verbose_name=_("sending in progress")
    )

    sent_at = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("date sent"),
    )

    sent_by = models.ForeignKey(
        to="authentication.User",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name=_("sent by"),
    )

    def __str__(self):
        return truncatechars(self.title, 30)

    class Meta:
        db_table = "broadcast"
        verbose_name = _("broadcast")
        verbose_name_plural = _("broadcasts")
        help_text = _("Broadcasts are used to transmit messages to all farmers at once")
        permissions = (("send_broadcast", _("Can send broadcast")),)


class BroadcastUserReadStatus(GetAdminURLMixin, models.Model):
    """A joined model to set read status between a User and a Broadcast"""

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    broadcast = models.ForeignKey(
        to="messaging.Broadcast",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("broadcast"),
    )

    user = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("user"),
    )

    read_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("date read"),
        help_text=_("The date/time (UTC) at which the broadcast was read."),
    )

    def __str__(self):
        return f"{self.user}: {self.broadcast}"

    class Meta:
        db_table = "broadcast_user_read_status"
        verbose_name = _("Broadcast user read status")
        verbose_name_plural = _("Broadcast user read statuses")
        help_text = _("Stores whether a user has already read a given broadcast")
        constraints = (
            models.UniqueConstraint(
                name="broadcast_user_read_status_unique",
                fields=["broadcast", "user"],
            ),
        )


class TicketUserReadStatus(GetAdminURLMixin, models.Model):
    """A joined model to set read status between a User and a Ticket"""

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    ticket = models.ForeignKey(
        to="messaging.Ticket",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("ticket"),
    )

    user = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("user"),
    )

    read_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("date read"),
        help_text=_("The date/time (UTC) at which the ticket was read."),
    )

    last_read_ticket_message = models.ForeignKey(
        to="messaging.TicketMessage",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name=_("last read ticket message"),
    )

    def __str__(self):
        return f"{self.user}: {self.ticket}"

    class Meta:
        db_table = "ticket_user_read_status"
        verbose_name = _("Ticket user read status")
        verbose_name_plural = _("Ticket user read statuses")
        help_text = _("Stores whether a user has already read a given ticket")
        constraints = (
            models.UniqueConstraint(
                name="ticket_user_read_status_unique",
                fields=["ticket", "user"],
            ),
        )
