import os
import logging
import uvicorn

from fastapi import FastAPI
from fastapi.middleware import Middleware

from app.db.graphql import (
    fastplatform,
    GraphQLClient,
)
from app.middleware import GraphQLClientMiddleware
from app.api.router import broadcast, ticket
from app.settings import config
from app.tracing import Tracing

# FastAPI
app = FastAPI()

app.include_router(broadcast.router, prefix="/broadcast")
app.include_router(ticket.router, prefix="/ticket")

app.add_middleware(GraphQLClientMiddleware, graphql_client=fastplatform)

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():
    logger.debug(config)
    await fastplatform.connect()


@app.on_event("shutdown")
async def shutdown():
    await fastplatform.close()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)