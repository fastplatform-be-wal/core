# 03-returns_full_constraint_if_exists
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 management_restriction_or_regulation_zone that has some intersection with plot
- 1 management_restriction_or_regulation_zone constraint, with description containing **inaccurate** `plot_pct_in_management_restriction_or_regulation_zone` and `plot_area_in_management_restriction_or_regulation_zone`.

## Computed
Nothing.

## Returns
The constraint from setup, with inaccurate `plot_pct_in_management_restriction_or_regulation_zone` and `plot_area_in_management_restriction_or_regulation_zone`, proving *it has not been recalculated.*