from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.models import GetAdminURLMixin


class LoadingStatus(models.TextChoices):
    completed = "COMPLETED", _("COMPLETED")


def version_base_file_upload_to(instance, filename):
    return f"external/{instance._meta.db_table}/{filename}"


class AbstractModelVersion(GetAdminURLMixin, models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    is_active = models.BooleanField(
        default=False,
        null=False,
        blank=False,
        verbose_name=_('is active'),
        help_text=_(
            "Whether this version is currently visible to the users; there can be only one version active at a given time."
        ),
    )

    status = models.CharField(
        max_length=25,
        null=True,
        blank=True,
        verbose_name=_("status"),
        choices=LoadingStatus.choices,
        help_text=_(
            'Is COMPLETED when the version has been fully loaded'
        ),
    )

    import_started_at = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("import started at"),
    )

    import_completed_at = models.DateTimeField(
        null=True, blank=True, verbose_name=_("import completed at")
    )

    import_log = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("import log"),
    )

    def __str__(self):
        return f"Version {self.id}"

    class Meta:
        abstract = True
