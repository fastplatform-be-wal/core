import gettext
import os

locale = os.environ.get("EVENT_MESSAGING_LOCALE", "en")
languages = [locale]
localedir = os.path.join(
    (os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    "locale",
)
translation = gettext.translation(
    "messages", localedir, languages=languages, fallback=True
)
_ = translation.gettext
