import logging
from urllib.parse import urlencode
from datetime import datetime, timedelta, timezone

from django.views.generic import View
from django.http import (
    HttpResponseNotFound,
    HttpResponseForbidden,
    HttpResponseRedirect,
)
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin

import jwt

from oidc_provider.models import Token

from add_ons.models import AddOn, AddOnAPIKey, AddOnSubscription
from farm.models import Campaign
from authentication.models import User
from farm.models import Holding, UserRelatedParty

logger = logging.getLogger(__name__)


class AddOnCallbackView(View):

    add_on = None
    user = None
    holding = None

    def get(self, request, *args, **kwargs):

        # The add-on must exist and be active and must have a callback URL
        add_on_id = kwargs.pop("add_on_id", None)
        try:
            self.add_on = (
                AddOn.objects.exclude(callback__isnull=True)
                .filter(is_active=True, provider__is_active=True)
                .get(pk=add_on_id)
            )
        except ObjectDoesNotExist:
            return HttpResponseNotFound(
                "Add-on does not exist, is inactive or does not support callbacks"
            )

        # The user authentication token is expected in the query parameters
        token = request.GET.get("token", None)
        if token is None:
            return HttpResponseForbidden("Authorization token is missing")

        try:
            self.user = User.objects.get(
                is_active=True,
                token__access_token=Token.hash_token(token),
                token__access_expires_at__gt=timezone.now(),
            )
        except ObjectDoesNotExist:
            return HttpResponseForbidden("Authorization is not valid")

        # The holding_id is expected in the query parameters
        holding_id = request.GET.get("holding_id", None)
        try:
            self.holding = Holding.objects.get(pk=holding_id)
        except ObjectDoesNotExist:
            return HttpResponseNotFound("Holding does not exist")

        # The requesting user must be related party to the holding
        if not UserRelatedParty.objects.filter(
            holding=self.holding, user=self.user
        ).exists():
            logger.error(
                "Holding '%s' does not exist for user '%s'",
                holding_id,
                self.user.username,
            )
            return HttpResponseForbidden("User is not related party to this holding")

        # The holding must be subscribed to the add-on
        if (
            not self.add_on.auto_subscribe
            and not AddOnSubscription.objects.filter(
                add_on=self.add_on, holding=self.holding
            ).exists()
        ):
            return HttpResponseForbidden("Holding is not subscribed to this add-on")

        # The add_on must have its callback_secret_key set to some value
        if not self.add_on.callback_secret_key:
            return HttpResponseForbidden("Add-on has no callback secret key set")

        # The active campaign_id is expected in the query parameters
        # (active is the campaign that the user was browsing at the time of click,
        # not the current campaign of the app)
        active_campaign_id = request.GET.get("active_campaign_id", None)
        try:
            self.active_campaign = Campaign.objects.get(pk=active_campaign_id)
        except ObjectDoesNotExist:
            return HttpResponseNotFound(
                f"Campaign '{active_campaign_id}' does not exist"
            )

        includes, params = AddOnCallbackView.build_query_params(self)

        # Create jwt token, inject user_id and holding_id in payload
        callback_token = self.refresh_callback_token(
            self.add_on.callback_secret_key, includes, params
        )
        query = {"token": callback_token}

        # Build the callback URL and redirect
        url = f"{self.add_on.callback}?{urlencode(query)}"

        return HttpResponseRedirect(url)

    @classmethod
    def build_query_params(cls, obj):
        includes = {
            "user_id": obj.add_on.callback_with_user_id,
            "holding_id": obj.add_on.callback_with_holding_id,
            "campaign_id": obj.add_on.callback_with_campaign_id,
        }

        params = {
            "user_id": obj.user.username,
            "holding_id": obj.holding.id,
            "campaign_id": obj.active_campaign.id,
        }
        return includes, params

    @classmethod
    def refresh_callback_token(cls, callback_secret_key, includes, params):
        now = datetime.now(tz=timezone.utc)
        issued_at = datetime.timestamp(now)
        expiration_time = datetime.timestamp(
            now
            + timedelta(seconds=int(settings.ADD_ONS_CALLBACK_TOKEN_VALIDITY_PERIOD))
        )

        payload = {
            **{
                "exp": int(expiration_time),
                "iat": int(issued_at),
                "iss": settings.DOMAIN_NAME,
            },
            **{
                key: params[key]
                for key in ["user_id", "holding_id", "campaign_id"]
                if includes[key]
            },
        }

        callback_token = jwt.encode(
            key=callback_secret_key,
            algorithm=settings.ADD_ONS_CALLBACK_JWT_SIGNING_ALGORITHM,
            headers={
                "kid": settings.DOMAIN_NAME,
            },
            payload=payload,
        ).decode("utf-8")

        return callback_token


class GraphiQLView(UserPassesTestMixin, LoginRequiredMixin, TemplateView):

    template_name = "add_ons/graphiql.html"

    def test_func(self):
        return (
            self.request.user.is_staff
            and (
                self.request.user.has_perm("add_ons.view_addonapikey")
                or self.request.user.has_perm("add_ons.change_addonapikey")
            )
        ) or self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        api_key_id = self.request.GET["api_key_id"]
        api_key = AddOnAPIKey.objects.get(pk=api_key_id)

        context.update(
            {
                "user": self.request.user,
                "has_permission": True,
                "page_title": "GraphQL Playground",
                "api_key": api_key.key,
            }
        )

        return context
