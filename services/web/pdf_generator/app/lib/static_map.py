from staticmap import StaticMap
import itertools
import time
import requests
import logging
from concurrent.futures import ThreadPoolExecutor
from math import ceil, floor
from PIL import Image
from io import BytesIO

# Log
logger = logging.getLogger(__name__)

class StaticMapNoError(StaticMap):
  def _draw_base_layer(self, image):
    """
    :type image: Image.Image
    """
    x_min = int(floor(self.x_center - (0.5 * self.width / self.tile_size)))
    y_min = int(floor(self.y_center - (0.5 * self.height / self.tile_size)))
    x_max = int(ceil(self.x_center + (0.5 * self.width / self.tile_size)))
    y_max = int(ceil(self.y_center + (0.5 * self.height / self.tile_size)))

    # assemble all map tiles needed for the map
    tiles = []
    for x in range(x_min, x_max):
        for y in range(y_min, y_max):
            # x and y may have crossed the date line
            max_tile = 2 ** self.zoom
            tile_x = (x + max_tile) % max_tile
            tile_y = (y + max_tile) % max_tile

            if self.reverse_y:
                tile_y = ((1 << self.zoom) - tile_y) - 1

            url = self.url_template.format(z=self.zoom, x=tile_x, y=tile_y)
            tiles.append((x, y, url))

    thread_pool = ThreadPoolExecutor(4)

    for nb_retry in itertools.count():
        if not tiles:
            # no tiles left
            break

        if nb_retry > 0 and self.delay_between_retries:
            # to avoid stressing the map tile server to much, wait some seconds
            time.sleep(self.delay_between_retries)

        # DO NOT RAISE AN ERROR
        if nb_retry >= 3:
            # maximum number of retries exceeded
            logger.warning("could not download {} tiles: {}".format(len(tiles), tiles))
            break
            # raise RuntimeError("could not download {} tiles: {}".format(len(tiles), tiles))

        failed_tiles = []
        futures = [
            thread_pool.submit(self.get, tile[2], timeout=self.request_timeout, headers=self.headers)
            for tile in tiles
        ]

        for tile, future in zip(tiles, futures):
            x, y, url = tile

            try:
                response_status_code, response_content = future.result()
            except:
                response_status_code, response_content = None, None

            if response_status_code != 200:
                logger.info("request failed [{}]: {}".format(response_status_code, url))
                failed_tiles.append(tile)
                continue
            
            tile_image = None
            try:
                tile_image = Image.open(BytesIO(response_content)).convert("RGBA")
            except Exception as e:
                logger.warning("Cannot create PIL image from tile {}".format(url))
            if tile_image:
                box = [
                    self._x_to_px(x),
                    self._y_to_px(y),
                    self._x_to_px(x + 1),
                    self._y_to_px(y + 1),
                ]
                image.paste(tile_image, box, tile_image)
            else:
                failed_tiles.append(tile)
        # put failed back into list of tiles to fetch in next try
        tiles = failed_tiles

  def get(self, url, **kwargs):
    """
    returns the status code and content (in bytes) of the requested tile url
    """
    res = requests.get(url, **kwargs)
    return res.status_code, res.content