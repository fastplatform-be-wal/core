import os
from urllib.parse import quote, urlencode

from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.urls import reverse
from django.utils.html import mark_safe
from django.conf import settings

from import_export.admin import ImportExportMixin, ExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin

from add_ons.models import (
    AddOn,
    AddOnAPIKey,
    Provider,
    ProviderMember,
    AddOnSubscription,
    APIAccess,
)
from add_ons.resources import (
    APIAccessResource,
    AddOnResource,
    AddOnAPIKeyResource,
    AddOnSubscriptionResource,
    ProviderResource,
    ProviderMemberResource,
)


class ProviderMemberInline(admin.TabularInline):
    model = ProviderMember
    extra = 0
    fields = ("user",)
    ordering = ("user__name",)
    autocomplete_fields = ("user",)
    verbose_name = _("member")
    verbose_name_plural = _("members")


class ProviderAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = ProviderResource
    list_display = ("id", "name", "is_active")

    inlines = [ProviderMemberInline]

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [
                None,
                {
                    "fields": (
                        "id",
                        "name",
                        "description",
                        "logo",
                        "website",
                        "is_active",
                    )
                },
            ]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj):
        if obj:
            return ["id"]
        return []


admin_site.register(Provider, ProviderAdmin)


class ProviderMemberAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    raw_id_fields = ("user",)
    resource_class = ProviderMemberResource


admin_site.register(ProviderMember, ProviderMemberAdmin)


class AddOnAPIKeyInline(admin.TabularInline):
    model = AddOnAPIKey
    extra = 0
    ordering = ("key",)
    fields = ("key", "generated_by", "generated_at", "_graphiql")
    readonly_fields = ("generated_at", "_graphiql")
    autocomplete_fields = ("generated_by",)
    verbose_name = _("API key")
    verbose_name_plural = _("API keys")

    def _graphiql(self, obj):
        if obj:
            if obj.key:
                url = (
                    reverse("add_ons:graphiql")
                    + "?"
                    + urlencode({"api_key_id": obj.id}, quote_via=quote)
                )
                return mark_safe(
                    f"Open with this API key "
                    f'(<a href="{url}" target="_blank">{_("FaST data")}</a>)'
                )
        return "-"

    _graphiql.short_description = _("GraphQL editor")


class AddOnAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = AddOnResource
    list_display = (
        "id",
        "name",
        "provider",
        "is_active",
        "is_visible",
        "_number_of_subscriptions",
        "_admin_url",
        "_number_of_api_keys",
    )
    autocomplete_fields = ["groups", "permissions"]
    search_fields = ["id", "name", "provider__id", "provider__name", "callback_url"]

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .filter(is_api_access=False)
            .annotate(number_of_subscriptions=models.Count("holdings"))
            .annotate(number_of_api_keys=models.Count("api_keys"))
        )

    def _number_of_subscriptions(self, obj):
        if obj.auto_subscribe:
            return _("auto-subscribed")
        return obj.number_of_subscriptions

    _number_of_subscriptions.short_description = _("Number of subscriptions")

    def _number_of_api_keys(self, obj):
        return obj.number_of_api_keys

    _number_of_api_keys.short_description = _("Number of API Keys")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [
                None,
                {
                    "fields": [
                        "id",
                        "name",
                        "provider",
                        "is_active",
                        "is_visible",
                        "description",
                        "logo",
                        "website",
                    ],
                },
            ],
            [
                _("Callback"),
                {
                    "fields": (
                        "callback",
                        "callback_secret_key",
                        "callback_with_user_id",
                        "callback_with_holding_id",
                        "callback_with_campaign_id",
                    ),
                },
            ],
            [
                _("Permissions zone"),
                {
                    "fields": ("auto_subscribe", "groups", "permissions"),
                    "description": _(
                        "The fields in this section determine the permissions that "
                        "this add-on will be granted when accessing the FaST API. "
                        "Set them carefully."
                    ),
                    "classes": (
                        "collapse",
                        "danger-zone",
                    ),
                },
            ],
        ]

        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        if self._admin_url(obj) != "-":
            fieldsets[0][1]["fields"] += ["_admin_url"]

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ["_admin_url"]
        if obj:
            readonly_fields += ["id"]
        return readonly_fields

    inlines = [AddOnAPIKeyInline]

    def _admin_url(self, obj):
        if obj:
            admin_url = os.environ.get(obj.get_admin_url_environment_variable())
            if admin_url and admin_url != "":
                return mark_safe(f'<a href="{admin_url}">{admin_url}</a>')
        return "-"

    _admin_url.short_description = _("Add-on console URL")


admin_site.register(AddOn, AddOnAdmin)


class AddOnAPIKeyAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = AddOnAPIKeyResource
    autocomplete_fields = ("generated_by",)


admin_site.register(AddOnAPIKey, AddOnAPIKeyAdmin)


class AddOnSubscriptionAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = AddOnSubscriptionResource
    autocomplete_fields = ("holding", "subscribed_by")


admin_site.register(AddOnSubscription, AddOnSubscriptionAdmin)


class APIAccessAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = APIAccessResource
    list_display = ("id", "name", "is_active", "_number_of_api_keys")
    autocomplete_fields = ["groups", "permissions"]

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .filter(is_api_access=True)
            .annotate(number_of_api_keys=models.Count("api_keys"))
        )

    def _number_of_api_keys(self, obj):
        return obj.number_of_api_keys

    _number_of_api_keys.short_description = _("Number of API Keys")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [
                None,
                {
                    "fields": [
                        "id",
                        "name",
                        "is_active",
                        "description",
                    ],
                },
            ],
            [
                _("Permissions zone"),
                {
                    "fields": ("groups", "permissions"),
                    "description": _(
                        "The fields in this section determine the permissions that "
                        "this API access will be granted when accessing the FaST API. "
                        "Set them carefully."
                    ),
                    "classes": (
                        "collapse",
                        "danger-zone",
                    ),
                },
            ],
        ]

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []

    inlines = [AddOnAPIKeyInline]


admin_site.register(APIAccess, APIAccessAdmin)
