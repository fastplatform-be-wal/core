import logging

from fastapi import FastAPI

from app.api.fast_core_interface import init_config_map
from app.db.graphql_clients import fastplatform
from app.lib.event import HasuraEvent
from app.settings import config
from app.tracing import Tracing

"""
At start:
- ready environment variables
- get layers configuration from Django core backend
- create mapproxy.yaml configuration file
- apply it to configmap

At post:
If layer already exists:
- replace it in config 
- generate mapproxy.yaml
- apply it to configmap

If layer does not exist:
- add it to config
- generate mapproxy.yaml
- apply it to configmap


At delete:
If layer exists:
- delete it from config 
- generate mapproxy.yaml
- apply it to configmap

If layer does not exist:
- return error


At get:
- return configuration

"""

app = FastAPI()

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():
    logger.debug(config)

    # Debuging instrumentation
    if config.REMOTE_DEBUG:
        import debugpy
        debugpy.listen(5678)

    await fastplatform.connect()
    if config.INIT_CONFIGMAP_ON_STARTUP:
        await init_config_map()


@app.on_event("shutdown")
async def shutdown():
    fastplatform.close()


@app.post("/conf/update")
async def read_item(event: HasuraEvent):
    configmap = await init_config_map()
    return {"configmap": configmap}
