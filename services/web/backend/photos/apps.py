from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _
from django.conf import settings


class PhotosConfig(AppConfig):
    name = "photos"
    verbose_name = _("Photos")
    help_text = _("Geo-tagged photos")
