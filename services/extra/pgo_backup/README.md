# extra/pgo-backup

```extra/pgo-backup service``` is an [ash](https://github.com/brgl/busybox/tree/master/shell) script that executes a [pgBackRest](https://pgbackrest.org/) backup on CrunchyData Pgcluster (v4).

> **This service must be removed when migrating to CrunchyData PGO (Postgres Operator) [V5](https://access.crunchydata.com/documentation/postgres-operator/v5/tutorial/backup-management/)**


## Environment variables

- `NAMESPACE`: Namespace where the Pgcluster(s) to backup are deployed
- `PGBACKREST_BACKUP_RETENTION`: Number of backups to retain (for full and differential backups only)
- `PGBACKREST_BACKUP_TYPE`: Type of pgBackRest backup to perform. Possible values are: diff,incr,full
- `PGCLUSTERS`: Pgcluster(s) to backup

## Docker image

The script is designed to be run on the [Google Distroless static base image](gcr.io/distroless/base-debian10) with busybox included.
