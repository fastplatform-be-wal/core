from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.html import mark_safe

from utils.models import GetAdminURLMixin


class Constraint(GetAdminURLMixin, models.Model):
    """
    The Model for any constraint: NVZ, Natura 2000, hydrology, etc.
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    name = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        db_index=True,
        verbose_name=_("name"),
    )

    description = models.JSONField(
        verbose_name=_("description"),
        null=True,
        blank=True,
    )

    plot = models.ForeignKey(
        to="farm.Plot",
        null=False,
        blank=False,
        related_name="constraints",
        on_delete=models.CASCADE,
        verbose_name=_("plot"),
    )

    class Meta:
        db_table = "constraint"
        verbose_name = _("constraint")
        verbose_name_plural = _("constraints")
        help_text = _(
            "Constraints on plots, such as NVZ, Natura 2000, proximity to surface water or water course, etc"
        )

    def pretty(self):
        res = ""
        if self.name == "protected_site":
            res += f"{_('Type')}: <b>{_('protected site')}</b><br>"
            protected_site_name = self.description.get("protected_site_name", "")
            res += f"{_('Protected site name')}: <b>{protected_site_name}</b><br>"
            percentage = self.description.get("plot_pct_in_protected_site", None)
            if percentage is not None:
                percentage = f"{round(percentage * 100, 2)}%"
                res += f"{_('Percentage of plot in protected site')}: <b>{percentage}</b><br>"
            area = self.description.get("plot_area_in_protected_site", None)
            if area is not None:
                area = f"{round(area / 10000, 2)}ha"
                res += f"{_('Area of plot in protected site')}: <b>{area}</b>"

        elif self.name == "management_restriction_or_regulation_zone":
            res += f"{_('Type')}: <b>{_('management restriction or regulation zone')}</b><br>"
            management_restriction_or_regulation_zone_name = self.description.get(
                "management_restriction_or_regulation_zone_name", ""
            )
            res += f"{_('Regulation zone name')}: <b>{management_restriction_or_regulation_zone_name}</b><br>"
            percentage = self.description.get(
                "plot_pct_in_management_restriction_or_regulation_zone", None
            )
            if percentage is not None:
                percentage = f"{round(percentage * 100, 2)}%"
                res += f"{_('Percentage of plot in regulation zone')}: <b>{percentage}</b><br>"
            area = self.description.get(
                "plot_area_in_management_restriction_or_regulation_zone", None
            )
            if area is not None:
                area = f"{round(area / 10000, 2)}ha"
                res += f"{_('Area of plot in regulation zone')}: <b>{area}</b>"

        elif self.name == "surface_water":
            res += f"{_('Type')}: <b>{_('surface water proximity')}</b><br>"
            surface_water_name = self.description.get("surface_water_name", "")
            res += f"{_('Surface water name')}: <b>{surface_water_name}</b><br>"
            distance = self.description.get("distance_to_surface_water", None)
            if distance is not None:
                distance = f"{round(distance, 2)}m"
                res += f"{_('Distance from surface water')}: <b>{distance}</b>"

        elif self.name == "water_course":
            res += f"{_('Type')}: <b>{_('water course proximity')}</b><br>"
            water_course_name = self.description.get("water_course_name", "")
            res += f"{_('Water course name')}: <b>{water_course_name}</b><br>"
            distance = self.description.get("distance_to_water_course", None)
            if distance is not None:
                distance = f"{round(distance, 2)}m"
                res += f"{_('Distance from water course')}: <b>{distance}</b>"

        return mark_safe(res)
