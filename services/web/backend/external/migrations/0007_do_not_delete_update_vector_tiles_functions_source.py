from pathlib import Path

from django.db import models, migrations

MIGRATIONS_DIR = Path(__file__).parent


class Migration(migrations.Migration):

    dependencies = [("external", "0006_generate_fk_cascade_external")]

    # create function to get entities from previously views adding the possibility to add a minZoom and maxZoom
    # If the zoom is outside the minZoom and maxZoom, then doesn't display rows

    operations = [
        migrations.RunSQL(
            (MIGRATIONS_DIR / "create_surface_water_function_source.sql").read_text()
        ),
        migrations.RunSQL(
            (MIGRATIONS_DIR / "create_water_course_function_source.sql").read_text()
        ),
        migrations.RunSQL(
            (MIGRATIONS_DIR / "create_soil_site_function_source.sql").read_text()
        ),
        migrations.RunSQL(
            (MIGRATIONS_DIR / "create_protected_site_function_source.sql").read_text()
        ),
        migrations.RunSQL(
            (
                MIGRATIONS_DIR
                / "create_management_restriction_or_regulation_zone_function_source.sql"
            ).read_text()
        ),
    ]